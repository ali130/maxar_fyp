﻿Shader "Element/Contour" {
    Properties {
        _MainTex("Main Texture", 2D) = "white"{}
    }
    SubShader {
    Blend SrcAlpha OneMinusSrcAlpha
        Pass {
            CGPROGRAM
     
            sampler2D _MainTex;
 
            // <SamplerName>_TexelSize is a float2 that says how much screen space a texel occupies.
            float2 _MainTex_TexelSize;
 
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
             
            struct v2f {
                float4 pos : POSITION;
                float2 uvs : TEXCOORD0;
            };
             
            v2f vert(appdata_base v) {
                v2f o;
                 
                // Multiply vertices by our MVP matrix
                o.pos = UnityObjectToClipPos(v.vertex);
                 
                // Fix the UVs to match our screen space coordinates
                o.uvs = o.pos.xy / 2 + 0.5;
                 
                return o;
            }
            
            half4 frag(v2f i) : COLOR {
                // Arbitrary number of iterations for now
                int NumberOfIterations = 9;
 
                // Split texel size into smaller words
                float TX_x = _MainTex_TexelSize.x;
                float TX_y = _MainTex_TexelSize.y;
 
                // And a final intensity that increments based on surrounding intensities.
                float ColorIntensityInRadius;
 
                // If something already exists underneath the fragment, discard the fragment.
                if (tex2D(_MainTex, i.uvs.xy).r > 0) {
                    discard;
                }
 
                // For every iteration we need to do horizontally
                for (int k = 0; k < NumberOfIterations; ++k) {
                    // For every iteration we need to do vertically
                    for (int j = 0; j < NumberOfIterations; ++j) {
                        // Increase our output color by the pixels in the area
                        ColorIntensityInRadius += tex2D(
                                                        _MainTex, 
                                                        i.uvs.xy + float2(
                                                                          (k - NumberOfIterations/2) * TX_x,
                                                                          (j - NumberOfIterations/2) * TX_y
                                                                         )
                                                       ).r;
                    }
                }
 
                // Output some intensity of red
                return ColorIntensityInRadius * half4(1, 0, 0, 1);
            }
             
            ENDCG
        }        
    }
}
