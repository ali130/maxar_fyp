﻿// This shader goes on the objects themselves. It just draws the object as white, and has the "Contour" tag.
 
Shader "Element/White" {
    SubShader {
        ZWrite Off
        ZTest Always
        Lighting Off
        Pass {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
 
            struct v2f {
                float4 pos : POSITION;
            };
 
            //just get the position correct
            v2f vert(v2f i) {
                v2f o;
                o.pos = UnityObjectToClipPos(i.pos);
                return o;
            }
 
            //return white
            half4 frag() : COLOR {
                return half4(1, 1, 1, 1);
            }
 
            ENDCG
        }
    }
}