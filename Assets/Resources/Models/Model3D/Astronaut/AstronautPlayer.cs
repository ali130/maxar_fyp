﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AstronautPlayer : MonoBehaviour {

	// Use this for initialization
	void Start () {
        ShortDelay();

        Vector3 aniPos = gameObject.transform.localPosition;
        GameObject ani = Instantiate(Resources.Load<GameObject>("Effect_02"), aniPos, Quaternion.identity) as GameObject;
        ani.transform.parent = gameObject.transform;
        ani.transform.localPosition = new Vector3(ani.transform.position.x, ani.transform.position.y + 0.5f, ani.transform.position.z);
    }

    // Update is called once per frame
    void Update () {

 
    }
    //Play Particle system once while Enabled
    private void OnEnable()
    {
        ParticleSystem partSys = FindObjectOfType<ParticleSystem>() as ParticleSystem;
        if (partSys != null) partSys.Play();
    }

    private void ShortDelay()
    {
        StartCoroutine(TemporarilyDeactivate(0.25f));
    }

    private IEnumerator TemporarilyDeactivate(float duration)
    {
        transform.GetChild(0).gameObject.SetActive(false);
        transform.GetChild(1).gameObject.SetActive(false);
        transform.GetChild(2).gameObject.SetActive(false);
        yield return new WaitForSeconds(duration);
        transform.GetChild(0).gameObject.SetActive(true);
        transform.GetChild(1).gameObject.SetActive(true);
        transform.GetChild(2).gameObject.SetActive(true);
    }
}

