﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[ExecuteInEditMode]
public class iOS_Button : MonoBehaviour {

	public string buttonText="Button";
	public Color32 buttonColor = new Color32(0,122,255,255);
	public bool showBorder=false;
	public bool autoBorderWidth=true;
	public int borderWidth=150;
	
	private int retinaMultipler;
	private int lastretinaMultipler;
	private float PosX;
	private float PosY;
	
	void Start() {
		retinaMultipler = GetComponentInParent<CanvasHandler>().retinaMultipler;
	}
	
	void Update() {
		#if UNITY_EDITOR		
		retinaMultipler = GetComponentInParent<CanvasHandler>().retinaMultipler;
		lastretinaMultipler = GetComponentInParent<CanvasHandler>().lastretinaMultipler;
		#endif
		
		GetComponent<RectTransform>().sizeDelta = new Vector2(GetComponent<RectTransform>().sizeDelta.x, 29 * retinaMultipler);
		GetComponent<Text>().fontSize = 17 * retinaMultipler;
		
		if(lastretinaMultipler != retinaMultipler){
			if(lastretinaMultipler==1){
				PosX = GetComponent<RectTransform>().anchoredPosition.x * retinaMultipler;
				PosY = GetComponent<RectTransform>().anchoredPosition.y * retinaMultipler;	
				borderWidth = borderWidth * retinaMultipler;
			}
			if(lastretinaMultipler==2){
				if(retinaMultipler==1){
					PosX = GetComponent<RectTransform>().anchoredPosition.x / 2;
					PosY = GetComponent<RectTransform>().anchoredPosition.y / 2;
					borderWidth = borderWidth / 2;
				}
				if(retinaMultipler==3){
					PosX = (GetComponent<RectTransform>().anchoredPosition.x / 2) * retinaMultipler;
					PosY = (GetComponent<RectTransform>().anchoredPosition.y / 2) * retinaMultipler;
					borderWidth = (borderWidth / 2) * retinaMultipler;
				}
			}
			if(lastretinaMultipler==3){
				if(retinaMultipler==1){
					PosX = GetComponent<RectTransform>().anchoredPosition.x / 3;
					PosY = GetComponent<RectTransform>().anchoredPosition.y / 3;
					borderWidth = (borderWidth / 3);
				}
				if(retinaMultipler==2){
					PosX = (GetComponent<RectTransform>().anchoredPosition.x / 3) * retinaMultipler;
					PosY = (GetComponent<RectTransform>().anchoredPosition.y / 3) * retinaMultipler;
					borderWidth = (borderWidth / 3) * retinaMultipler;
				}			
			}
				
			GetComponent<RectTransform>().anchoredPosition = new Vector2(PosX, PosY);	
			
		}
		
		if(autoBorderWidth){
			borderWidth = buttonText.Length * (15 * retinaMultipler);
		}
		
		transform.GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(borderWidth, 29 * retinaMultipler);
		GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);					
		transform.GetChild(0).GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);					
		
	}		
}
