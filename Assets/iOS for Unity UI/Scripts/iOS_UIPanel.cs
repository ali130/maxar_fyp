﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

#if UNITY_EDITOR		
using UnityEditor;
#endif

[ExecuteInEditMode]
public class iOS_UIPanel : MonoBehaviour {
	
	//public bool StatusBarHidden=false;
	public float width;
	public float height;
	private bool NavigationBarExists=false;
	private float PosY;
	//private bool TabBarExists=false;
	private bool ToolBarExists=false;
	
	public Color32 PanelColor = new Color32(239, 239, 244, 255);
	
	private int retinaMultipler;
	
	void Start() {
		retinaMultipler = transform.GetComponentInParent<CanvasHandler>().retinaMultipler;
	}
	
	void Update() {
		
		#if UNITY_EDITOR		
			retinaMultipler = transform.GetComponentInParent<CanvasHandler>().retinaMultipler;
		#endif
		
		if(transform.tag == "rootUIPanel" || transform.tag == "UIPanel"){
			// Lock UI panel to fill screen space
			transform.GetComponent<RectTransform>().sizeDelta = new Vector2(0, 0);
			
		}
		
		foreach (Transform child in transform)
		{
			
			if(child.tag == "StatusBar"){
                #if UNITY_EDITOR
                    if (PlayerSettings.statusBarHidden){
					    child.gameObject.SetActive(false);
				    } else {
					    child.GetComponent<RectTransform>().sizeDelta = new Vector2(0, 20 * retinaMultipler);		
					    child.GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(105 * retinaMultipler, 20 * retinaMultipler);		
					    child.GetChild(1).GetComponent<RectTransform>().sizeDelta = new Vector2(46 * retinaMultipler, 20 * retinaMultipler);		
					    child.GetChild(2).GetComponent<RectTransform>().sizeDelta = new Vector2(57 * retinaMultipler, 20 * retinaMultipler);		
					    child.gameObject.SetActive(true);
					    
					    // Keep Status Bar in place!
					    child.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
					    child.GetComponent<RectTransform>().sizeDelta = new Vector2(0f,child.GetComponent<RectTransform>().sizeDelta.y);
					    child.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);					
					
				    }
                #else
                    child.gameObject.SetActive(false);
                #endif
            }

            if (child.tag == "NavigationBar"){
				
				if(child.gameObject.activeSelf){
					NavigationBarExists=true;					
				} else {
					NavigationBarExists=false;					
				}
			
				if(transform.tag == "rootUIPanel" || transform.tag == "UIPanel"){
                    #if UNITY_EDITOR
					    if(PlayerSettings.statusBarHidden){
						    child.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
					    } else {
						    child.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, (-20 * retinaMultipler));
					    }
                    #else
                        child.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
                    #endif
                }
            }
			
			/*if(child.tag == "TabBar"){
				
				if(child.gameObject.activeSelf){
					TabBarExists=true;					
				} else {
					TabBarExists=false;					
				}
								
			}*/
			
			if(child.tag == "ToolBar"){
				
				if(child.gameObject.activeSelf){
					ToolBarExists=true;
					
				} else {
					ToolBarExists=false;					
				}
				
			}
			
			if(child.tag == "SegmentedControl"){
				PosY=0;
				
				if(transform.tag == "rootUIPanel" || transform.tag == "UIPanel"){
                    #if UNITY_EDITOR
                        if (!PlayerSettings.statusBarHidden){
						    PosY = PosY + (20 * retinaMultipler);
					    }
                    #endif
                }
				
				if(NavigationBarExists){
					PosY = PosY + (44 * retinaMultipler);
				}
				
				child.GetComponent<RectTransform>().sizeDelta = new Vector2(0, 44 * retinaMultipler);
						
				child.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, PosY * -1);			
				
				width = transform.GetComponentInParent<CanvasScaler>().referenceResolution.x;			
				height = transform.GetComponentInParent<CanvasScaler>().referenceResolution.y;
				
				if(transform.tag == "rootUIPanel" || transform.tag == "UIPanel"){
                    #if UNITY_EDITOR
                        if (!PlayerSettings.statusBarHidden){
						    height = height - (20 * retinaMultipler);
					    }
                    #endif
                }
				
				if(NavigationBarExists){
					height = height - (44 * retinaMultipler);
				}
				
				if(transform.tag == "UITabPanel"){
					Debug.Log(height);
					height = height - (49 * retinaMultipler);
				}
				
				if(ToolBarExists){
					height = height - (44 * retinaMultipler);
				}
				
				if(transform.tag == "UITabPanel" || transform.tag == "UISegmentedPanel"){
					height = height - (44 * retinaMultipler);
				}
				
				//Size and position Segmented Panels
				if(transform.tag == "rootUIPanel" || transform.tag == "UIPanel"){
					child.GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(width, height-(44 * retinaMultipler));
				} else {
					child.GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(width, height);					
				}
				child.GetChild(0).GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -44 * retinaMultipler);
				
				child.GetChild(1).GetComponent<RectTransform>().sizeDelta = new Vector2(width, height-(44 * retinaMultipler));
				child.GetChild(1).GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -44 * retinaMultipler);
				
				child.GetChild(2).GetComponent<RectTransform>().sizeDelta = new Vector2(width, height-(44 * retinaMultipler));
				child.GetChild(2).GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -44 * retinaMultipler);
				
				child.GetChild(3).GetComponent<RectTransform>().sizeDelta = new Vector2(width, height-(44 * retinaMultipler));
				child.GetChild(3).GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -44 * retinaMultipler);
				
				//Size and position background
				child.GetChild(4).GetComponent<RectTransform>().sizeDelta = new Vector2(width, 44 * retinaMultipler);
				child.GetChild(4).GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
				
				//Size and position segment tabs and children
				child.GetChild(5).GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(child.GetChild(5).GetChild(0).GetComponent<RectTransform>().sizeDelta.x, 29 * retinaMultipler);
				child.GetChild(5).GetChild(1).GetComponent<RectTransform>().sizeDelta = new Vector2(child.GetChild(5).GetChild(1).GetComponent<RectTransform>().sizeDelta.x, 29 * retinaMultipler);
				child.GetChild(5).GetChild(2).GetComponent<Text>().fontSize = 14 * retinaMultipler;
				
				child.GetChild(6).GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(child.GetChild(6).GetChild(0).GetComponent<RectTransform>().sizeDelta.x, 29 * retinaMultipler);
				child.GetChild(6).GetChild(1).GetComponent<RectTransform>().sizeDelta = new Vector2(child.GetChild(6).GetChild(1).GetComponent<RectTransform>().sizeDelta.x, 29 * retinaMultipler);
				child.GetChild(6).GetChild(2).GetComponent<Text>().fontSize = 14 * retinaMultipler;
				
				child.GetChild(7).GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(child.GetChild(7).GetChild(0).GetComponent<RectTransform>().sizeDelta.x, 29 * retinaMultipler);
				child.GetChild(7).GetChild(1).GetComponent<RectTransform>().sizeDelta = new Vector2(child.GetChild(7).GetChild(1).GetComponent<RectTransform>().sizeDelta.x, 29 * retinaMultipler);
				child.GetChild(7).GetChild(2).GetComponent<Text>().fontSize = 14 * retinaMultipler;
				
				child.GetChild(8).GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(child.GetChild(8).GetChild(0).GetComponent<RectTransform>().sizeDelta.x, 29 * retinaMultipler);
				child.GetChild(8).GetChild(1).GetComponent<RectTransform>().sizeDelta = new Vector2(child.GetChild(8).GetChild(1).GetComponent<RectTransform>().sizeDelta.x, 29 * retinaMultipler);
				child.GetChild(8).GetChild(2).GetComponent<Text>().fontSize = 14 * retinaMultipler;
				
			}
				
			if(child.tag == "TableView"){
				PosY=0;
				if(transform.tag == "rootUIPanel" || transform.tag == "UIPanel"){
                    #if UNITY_EDITOR
                        if (!PlayerSettings.statusBarHidden){
						    PosY = PosY + (20 * retinaMultipler);
					    }
                    #endif
                }
			
				if(NavigationBarExists){
					PosY = PosY + (44 * retinaMultipler);
				}
				
				child.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, PosY * -1);			
				child.GetComponent<RectTransform>().sizeDelta = new Vector2(0, PosY * -1);
				
			}
			
		}
		
	}
	
}
