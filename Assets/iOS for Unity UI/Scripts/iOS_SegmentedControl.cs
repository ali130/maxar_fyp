﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[ExecuteInEditMode]
public class iOS_SegmentedControl : MonoBehaviour {
	
	public int segmentsIndex = 0;
	public int selectedSegmentIndex = 0;
	public Color32 SegmentsColor= new Color32(0,121,255,255);
	public Color32 BackgroundColor= new Color32(255,255,255,255);
	public string segment1Text;
	public string segment2Text;
	public string segment3Text;
	public string segment4Text;
	
	public Sprite middleBackground;
	public Sprite middleBorder;
	public Sprite rightBackground;
	public Sprite rightBorder;

		
	public void Segment1Touch(){
		selectedSegmentIndex = 0;
		Debug.Log ("Segment1 Touch " + selectedSegmentIndex);
	}
	
	public void Segment2Touch(){
		selectedSegmentIndex = 1;
		Debug.Log ("Segment2 Touch " + selectedSegmentIndex);
	}
	
	public void Segment3Touch(){
		selectedSegmentIndex = 2;
		Debug.Log ("Segment3 Touch " + selectedSegmentIndex);
	}
	
	public void Segment4Touch(){
		selectedSegmentIndex = 3;
		Debug.Log ("Segment4 Touch " + selectedSegmentIndex);
	}

	void Update() {
		//Sets selected segment
		//		Debug.Log ("selectedSegmentIndex=" + selectedSegmentIndex);
		
		if(selectedSegmentIndex == 0){
			transform.Find("SegmentPanel1").gameObject.SetActive(true);
			transform.Find("SegmentPanel2").gameObject.SetActive(false);
			transform.Find("SegmentPanel3").gameObject.SetActive(false);
			transform.Find("SegmentPanel4").gameObject.SetActive(false);
			transform.Find("Segment1/Background").transform.GetComponent<Image>().color = SegmentsColor;
			transform.Find("Segment1/Text").transform.GetComponent<Text>().color = new Color32(255, 255, 255, 255);
			transform.Find("Segment2/Background").transform.GetComponent<Image>().color = new Color32(0, 121, 255, 0);
			transform.Find("Segment2/Text").transform.GetComponent<Text>().color = SegmentsColor;		
			transform.Find("Segment3/Background").transform.GetComponent<Image>().color = new Color32(0, 121, 255, 0);
			transform.Find("Segment3/Text").transform.GetComponent<Text>().color = SegmentsColor;		
			transform.Find("Segment4/Background").transform.GetComponent<Image>().color = new Color32(0, 121, 255, 0);
			transform.Find("Segment4/Text").transform.GetComponent<Text>().color = SegmentsColor;		
		}
		if(selectedSegmentIndex == 1){
			transform.Find("SegmentPanel1").gameObject.SetActive(false);
			transform.Find("SegmentPanel2").gameObject.SetActive(true);
			transform.Find("SegmentPanel3").gameObject.SetActive(false);
			transform.Find("SegmentPanel4").gameObject.SetActive(false);
			transform.Find("Segment1/Background").transform.GetComponent<Image>().color = new Color32(0, 121, 255, 0);
			transform.Find("Segment1/Text").transform.GetComponent<Text>().color = SegmentsColor;
			transform.Find("Segment2/Background").transform.GetComponent<Image>().color = SegmentsColor;
			transform.Find("Segment2/Text").transform.GetComponent<Text>().color = new Color32(255, 255, 255, 255);			
			transform.Find("Segment3/Background").transform.GetComponent<Image>().color = new Color32(0, 121, 255, 0);
			transform.Find("Segment3/Text").transform.GetComponent<Text>().color = SegmentsColor;
			transform.Find("Segment4/Background").transform.GetComponent<Image>().color = new Color32(0, 121, 255, 0);
			transform.Find("Segment4/Text").transform.GetComponent<Text>().color = SegmentsColor;
		}
		if(selectedSegmentIndex == 2){
			transform.Find("SegmentPanel1").gameObject.SetActive(false);
			transform.Find("SegmentPanel2").gameObject.SetActive(false);
			transform.Find("SegmentPanel3").gameObject.SetActive(true);
			transform.Find("SegmentPanel4").gameObject.SetActive(false);
			transform.Find("Segment1/Background").transform.GetComponent<Image>().color = new Color32(0, 121, 255, 0);
			transform.Find("Segment1/Text").transform.GetComponent<Text>().color = SegmentsColor;
			transform.Find("Segment2/Background").transform.GetComponent<Image>().color = new Color32(0, 121, 255, 0);
			transform.Find("Segment2/Text").transform.GetComponent<Text>().color = SegmentsColor;
			transform.Find("Segment3/Background").transform.GetComponent<Image>().color = SegmentsColor;
			transform.Find("Segment3/Text").transform.GetComponent<Text>().color = new Color32(255, 255, 255, 255);			
			transform.Find("Segment4/Background").transform.GetComponent<Image>().color = new Color32(0, 121, 255, 0);
			transform.Find("Segment4/Text").transform.GetComponent<Text>().color = SegmentsColor;
		}
		if(selectedSegmentIndex == 3){
			transform.Find("SegmentPanel1").gameObject.SetActive(false);
			transform.Find("SegmentPanel2").gameObject.SetActive(false);
			transform.Find("SegmentPanel3").gameObject.SetActive(false);
			transform.Find("SegmentPanel4").gameObject.SetActive(true);
			transform.Find("Segment1/Background").transform.GetComponent<Image>().color = new Color32(0, 121, 255, 0);
			transform.Find("Segment1/Text").transform.GetComponent<Text>().color = SegmentsColor;
			transform.Find("Segment2/Background").transform.GetComponent<Image>().color = new Color32(0, 121, 255, 0);
			transform.Find("Segment2/Text").transform.GetComponent<Text>().color = SegmentsColor;
			transform.Find("Segment3/Background").transform.GetComponent<Image>().color = new Color32(0, 121, 255, 0);
			transform.Find("Segment3/Text").transform.GetComponent<Text>().color = SegmentsColor;
			transform.Find("Segment4/Background").transform.GetComponent<Image>().color = SegmentsColor;
			transform.Find("Segment4/Text").transform.GetComponent<Text>().color = new Color32(255, 255, 255, 255);			
		}
	}
}
