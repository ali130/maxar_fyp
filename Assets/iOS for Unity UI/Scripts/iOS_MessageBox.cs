﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[ExecuteInEditMode]
public class iOS_MessageBox : MonoBehaviour {

	public int buttonsIndex = 0;
	
	public bool isShown=false;
	
	public string titleText = "Title";
	public Color32 titleColor = new Color32(0,0,0,0);

	public string leftButtonText = "Left Button";
	public Color32 leftButtonColor = new Color32(0,122,255,255);
	
	public string middleButtonText = "Button";
	public Color32 middleButtonColor = new Color32(0,122,255,255);

	public string rightButtonText = "Right Button";
	public Color32 rightButtonColor = new Color32(0,122,255,255);

	public GameObject button1GO;
	public string button1FunctionName;
	public GameObject button2GO;
	public string button2FunctionName;
	public bool buttonCancel=false;
	
	private int retinaMultipler;
	
	void Start() {
		retinaMultipler = GetComponentInParent<CanvasHandler>().retinaMultipler;
	}
	
	void Update() {
		
		#if UNITY_EDITOR		
		retinaMultipler = GetComponentInParent<CanvasHandler>().retinaMultipler;
		#endif
		
		//Window
		transform.GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(270f * retinaMultipler, 114 * retinaMultipler);
		
		//Title
		//transform.GetChild(0).GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(0f, 22f * retinaMultipler);	
		transform.GetChild(0).GetChild(0).GetComponent<RectTransform>().anchoredPosition = new Vector3(0f, -57f * retinaMultipler, 0f);
		//TitleLabel
		transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(0f, 57f * retinaMultipler);	
		transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<Text>().fontSize = 17 * retinaMultipler;
		
		//Buttons
		transform.GetChild(0).GetChild(1).GetComponent<RectTransform>().sizeDelta = new Vector2(0f, 57f * retinaMultipler);	
		transform.GetChild(0).GetChild(1).GetComponent<RectTransform>().anchoredPosition = new Vector3(0f, 57f * retinaMultipler, 0f);
		
		//Footerline
		transform.GetChild(0).GetChild(1).GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(270f * retinaMultipler, 1f);	
		transform.GetChild(0).GetChild(1).GetChild(0).GetComponent<RectTransform>().anchoredPosition = new Vector3(0f, 0f, 0f);
		
		//SingleButton
		transform.GetChild(0).GetChild(1).GetChild(1).GetComponent<Text>().fontSize = 17 * retinaMultipler;
		
		//LeftButton
		transform.GetChild(0).GetChild(1).GetChild(2).GetComponent<RectTransform>().anchoredPosition = new Vector3(-67.5f * retinaMultipler, 0f, 0f);
		transform.GetChild(0).GetChild(1).GetChild(2).GetComponent<Text>().fontSize = 17 * retinaMultipler;
		
		//RightButton
		transform.GetChild(0).GetChild(1).GetChild(3).GetComponent<RectTransform>().anchoredPosition = new Vector3(67.5f * retinaMultipler, 0f, 0f);
		transform.GetChild(0).GetChild(1).GetChild(3).GetComponent<Text>().fontSize = 17 * retinaMultipler;
		
		//MidLine
		transform.GetChild(0).GetChild(1).GetChild(4).GetComponent<RectTransform>().sizeDelta = new Vector2(1f, 57f * retinaMultipler);	
		transform.GetChild(0).GetChild(1).GetChild(4).GetComponent<RectTransform>().anchoredPosition = new Vector3(0f, 57f * retinaMultipler, 0f);
		
	}
	
	public void ShowMessageBox(bool bShown){
		isShown=bShown;
		transform.gameObject.SetActive(isShown);
	}
	
	void Button1Click(){
		if(buttonCancel==true && buttonsIndex==0){
			ShowMessageBox(false);
		} else {
			if(button1GO){
				button1GO.SendMessage(button1FunctionName);
			} else {
				Debug.Log("No GameObect or Function Name assigned to Button1");
			}
		}
	}
	
	void Button2Click(){
		if(buttonCancel==true){
			ShowMessageBox(false);
		} else {
			if(button2GO){
				button2GO.SendMessage(button1FunctionName);
			} else {
				Debug.Log("No GameObect or Function Name assigned to Button2");
			}
		}
	}
	
	
}
