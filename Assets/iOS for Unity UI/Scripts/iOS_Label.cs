﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[ExecuteInEditMode]
public class iOS_Label : MonoBehaviour {

	public string labelText="Label";
    public int size = 17;
	public Color32 labelColor = new Color32(0,0,0,255);
	public iOS_Switch ConnectedSwitch;	
	
	private int retinaMultipler;
	private int lastretinaMultipler;
	private float PosX;
	private float PosY;
	
	void Start() {
		retinaMultipler = GetComponentInParent<CanvasHandler>().retinaMultipler;
	}
	
	void Update() {
		
		#if UNITY_EDITOR		
		retinaMultipler = GetComponentInParent<CanvasHandler>().retinaMultipler;
		lastretinaMultipler = GetComponentInParent<CanvasHandler>().lastretinaMultipler;
		#endif
		
		GetComponent<RectTransform>().sizeDelta = new Vector2(GetComponent<RectTransform>().sizeDelta.x, 29 * retinaMultipler);
		GetComponent<Text>().fontSize = size * retinaMultipler;
		GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);					
		
		if(lastretinaMultipler != retinaMultipler){
			if(lastretinaMultipler==1){
				PosX = GetComponent<RectTransform>().anchoredPosition.x * retinaMultipler;
				PosY = GetComponent<RectTransform>().anchoredPosition.y * retinaMultipler;	
			}
			if(lastretinaMultipler==2){
				if(retinaMultipler==1){
					PosX = GetComponent<RectTransform>().anchoredPosition.x / 2;
					PosY = GetComponent<RectTransform>().anchoredPosition.y / 2;
				}
				if(retinaMultipler==3){
					PosX = (GetComponent<RectTransform>().anchoredPosition.x / 2) * retinaMultipler;
					PosY = (GetComponent<RectTransform>().anchoredPosition.y / 2) * retinaMultipler;
				}
			}
			if(lastretinaMultipler==3){
				if(retinaMultipler==1){
					PosX = GetComponent<RectTransform>().anchoredPosition.x / 3;
					PosY = GetComponent<RectTransform>().anchoredPosition.y / 3;
				}
				if(retinaMultipler==2){
					PosX = (GetComponent<RectTransform>().anchoredPosition.x / 3) * retinaMultipler;
					PosY = (GetComponent<RectTransform>().anchoredPosition.y / 3) * retinaMultipler;
				}			
			}
			
			GetComponent<RectTransform>().anchoredPosition = new Vector2(PosX, PosY);	
			
		}
			
		
		if(ConnectedSwitch != null){
			if(ConnectedSwitch.GetComponent<Toggle>().isOn==true){
				labelText = ConnectedSwitch.OnStateText;
			} else {
				labelText = ConnectedSwitch.OffStateText;
			}
			GetComponent<Text>().text = labelText;
		}		
		
	}
	
}
