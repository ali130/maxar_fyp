﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[ExecuteInEditMode]
public class iOS_PopupMessage : MonoBehaviour {

	public int buttonsIndex = 0;

	public string titleText = "Title";
	public Color32 titleColor = new Color32(0,0,0,0);

	public string button1Text = "Button1";
	public Color32 button1Color = new Color32(0,122,255,255);
	public GameObject button1GO;
	public string button1FunctionName;
	
	public string button2Text = "Button2";
	public Color32 button2Color = new Color32(0,122,255,255);
	public GameObject button2GO;
	public string button2FunctionName;
	
	public string button3Text = "Button3";
	public Color32 button3Color = new Color32(0,122,255,255);
	public GameObject button3GO;
	public string button3FunctionName;
	
	public string button4Text = "Button4";
	public Color32 button4Color = new Color32(0,122,255,255);
	public GameObject button4GO;
	public string button4FunctionName;
	
	public string button5Text = "Button5";
	public Color32 button5Color = new Color32(0,122,255,255);
	public GameObject button5GO;
	public string button5FunctionName;
	
	public string button6Text = "Button6";
	public Color32 button6Color = new Color32(0,122,255,255);
	public GameObject button6GO;
	public string button6FunctionName;
	
	public string button7Text = "Button7";
	public Color32 button7Color = new Color32(0,122,255,255);
	public GameObject button7GO;
	public string button7FunctionName;
	
	public string button8Text = "Button8";
	public Color32 button8Color = new Color32(0,122,255,255);
	public GameObject button8GO;
	public string button8FunctionName;
	
	public string buttonCancelText = "Cancel";
	public Color32 buttonCancelColor = new Color32(0,122,255,255);

	private float ScreenWidth;
	
	private int retinaMultipler;
	
	void Start() {
		retinaMultipler = GetComponentInParent<CanvasHandler>().retinaMultipler;
	}
	
	void Update() {
		
		#if UNITY_EDITOR		
		retinaMultipler = GetComponentInParent<CanvasHandler>().retinaMultipler;
		#endif
		
		//Set size of popup message
		
		//Popup Message\Window
		transform.GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(304f * retinaMultipler, ((57f * retinaMultipler) * (buttonsIndex+3)) + (16f * retinaMultipler));
		//Popup Message\Window\Buttons
		transform.GetChild(0).GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(0f, (57f * retinaMultipler) * (buttonsIndex+2));	
		transform.GetChild(0).GetChild(0).GetComponent<RectTransform>().anchoredPosition = new Vector3(0f, (57f * retinaMultipler) + (8f * retinaMultipler), 0f);
		
		//Title
		transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(0f, 57f * retinaMultipler);	
		transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<RectTransform>().anchoredPosition = new Vector3(0f, 0f, 0f);
		//TitleLabel
		transform.GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetComponent<Text>().fontSize = 17 * retinaMultipler;
		
		
		//Popup Message\Window\Buttons\Button1-8
		transform.GetChild(0).GetChild(0).GetChild(1).GetComponent<RectTransform>().sizeDelta = new Vector2(0f, 57f * retinaMultipler);	
		transform.GetChild(0).GetChild(0).GetChild(1).GetComponent<RectTransform>().anchoredPosition = new Vector3(0f, (57f * retinaMultipler) * -1, 0f);
		transform.GetChild(0).GetChild(0).GetChild(1).GetChild(0).GetComponent<Text>().fontSize = 17 * retinaMultipler;
		transform.GetChild(0).GetChild(0).GetChild(2).GetComponent<RectTransform>().sizeDelta = new Vector2(0f, 57f * retinaMultipler);	
		transform.GetChild(0).GetChild(0).GetChild(2).GetComponent<RectTransform>().anchoredPosition = new Vector3(0f, ((57f*2) * retinaMultipler) * -1, 0f);
		transform.GetChild(0).GetChild(0).GetChild(2).GetChild(0).GetComponent<Text>().fontSize = 17 * retinaMultipler;
		transform.GetChild(0).GetChild(0).GetChild(3).GetComponent<RectTransform>().sizeDelta = new Vector2(0f, 57f * retinaMultipler);	
		transform.GetChild(0).GetChild(0).GetChild(3).GetComponent<RectTransform>().anchoredPosition = new Vector3(0f, ((57f*3) * retinaMultipler) * -1, 0f);
		transform.GetChild(0).GetChild(0).GetChild(3).GetChild(0).GetComponent<Text>().fontSize = 17 * retinaMultipler;
		transform.GetChild(0).GetChild(0).GetChild(4).GetComponent<RectTransform>().sizeDelta = new Vector2(0f, 57f * retinaMultipler);	
		transform.GetChild(0).GetChild(0).GetChild(4).GetComponent<RectTransform>().anchoredPosition = new Vector3(0f, ((57f*4) * retinaMultipler) * -1, 0f);
		transform.GetChild(0).GetChild(0).GetChild(4).GetChild(0).GetComponent<Text>().fontSize = 17 * retinaMultipler;
		transform.GetChild(0).GetChild(0).GetChild(5).GetComponent<RectTransform>().sizeDelta = new Vector2(0f, 57f * retinaMultipler);	
		transform.GetChild(0).GetChild(0).GetChild(5).GetComponent<RectTransform>().anchoredPosition = new Vector3(0f, ((57f*5) * retinaMultipler) * -1, 0f);
		transform.GetChild(0).GetChild(0).GetChild(5).GetChild(0).GetComponent<Text>().fontSize = 17 * retinaMultipler;
		transform.GetChild(0).GetChild(0).GetChild(6).GetComponent<RectTransform>().sizeDelta = new Vector2(0f, 57f * retinaMultipler);	
		transform.GetChild(0).GetChild(0).GetChild(6).GetComponent<RectTransform>().anchoredPosition = new Vector3(0f, ((57f*6) * retinaMultipler) * -1, 0f);
		transform.GetChild(0).GetChild(0).GetChild(6).GetChild(0).GetComponent<Text>().fontSize = 17 * retinaMultipler;
		transform.GetChild(0).GetChild(0).GetChild(7).GetComponent<RectTransform>().sizeDelta = new Vector2(0f, 57f * retinaMultipler);	
		transform.GetChild(0).GetChild(0).GetChild(7).GetComponent<RectTransform>().anchoredPosition = new Vector3(0f, ((57f*7) * retinaMultipler) * -1, 0f);
		transform.GetChild(0).GetChild(0).GetChild(7).GetChild(0).GetComponent<Text>().fontSize = 17 * retinaMultipler;
		transform.GetChild(0).GetChild(0).GetChild(8).GetComponent<RectTransform>().sizeDelta = new Vector2(0f, 57f * retinaMultipler);	
		transform.GetChild(0).GetChild(0).GetChild(8).GetComponent<RectTransform>().anchoredPosition = new Vector3(0f, ((57f*8) * retinaMultipler) * -1, 0f);
		transform.GetChild(0).GetChild(0).GetChild(8).GetChild(0).GetComponent<Text>().fontSize = 17 * retinaMultipler;
		
		
		//Popup Message\Window\CancelButton
		transform.GetChild(0).GetChild(1).GetComponent<RectTransform>().sizeDelta = new Vector2(0f, 57f * retinaMultipler);	
		//Popup Message\Window\CancelButton\Background
		transform.GetChild(0).GetChild(1).GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(0f, 57f * retinaMultipler);	
		transform.GetChild(0).GetChild(1).GetChild(1).GetComponent<Text>().fontSize = 17 * retinaMultipler;
		
		//Set position of window
		//transform.GetChild(0).GetComponent<RectTransform>().anchoredPosition = new Vector3(0f, 0f, 0f);
	}
	
	public void PopupTransition(bool showPopup){
		//Debug.Log ("Popup=" + showPopup);
		ScreenWidth = transform.parent.GetComponent<RectTransform>().rect.width;
		
		//Enable image to disable touches to background UI
		GetComponent<Image>().enabled = showPopup;
		
		transform.gameObject.SetActive(true);
		
		Debug.Log(ScreenWidth + " " + "PopupSlideUpClip" + (buttonsIndex + 1));
		
		if(showPopup==true){
			// Enable Animator of popup to animate up
			if(ScreenWidth==640 || ScreenWidth==960 || ScreenWidth==1136 || ScreenWidth==1536 || ScreenWidth==2048 || ScreenWidth==750 || ScreenWidth==1334 || ScreenWidth==2732){//iPhone4, 5, 6 & iPad Retina including Pro
				GetComponent<Animator>().Play("PopupSlideUpClip" + (buttonsIndex + 1));
		}
			if(ScreenWidth==768 || ScreenWidth==1024){//iPad1, 2 & Mini
				GetComponent<Animator>().Play("PopupSlideUpiPad2Clip" + (buttonsIndex + 1));				
			}
			if(ScreenWidth==1242 || ScreenWidth == 2208){//iPhone6+
				GetComponent<Animator>().Play("PopupSlideUpiPhone6PlusClip" + (buttonsIndex + 1));
			}
		} else {
			if(ScreenWidth==640 || ScreenWidth==960 || ScreenWidth==1136 || ScreenWidth==1536 || ScreenWidth==2048 || ScreenWidth==750 || ScreenWidth==1334 || ScreenWidth==2732){//iPhone4, 5, 6 & iPad Retina including Pro
				GetComponent<Animator>().Play("PopupSlideDownClip" + (buttonsIndex + 1));
		}
			if(ScreenWidth==768 || ScreenWidth==1024){//iPad1, 2 & Mini
				GetComponent<Animator>().Play("PopupSlideDowniPad2Clip" + (buttonsIndex + 1));				
			}
			if(ScreenWidth==1242 || ScreenWidth == 2208){//iPhone6+
				GetComponent<Animator>().Play("PopupSlideDowniPhone6PlusClip" + (buttonsIndex + 1));
			}
		}
	}
	
	void Button1Click(){
		if(button1GO){
			button1GO.SendMessage(button1FunctionName);
		} else {
			Debug.Log("No GameObect or Function Name assigned to Button1");
		}
	}

	void Button2Click(){
		if(button2GO){
			button2GO.SendMessage(button2FunctionName);
		} else {
			Debug.Log("No GameObect or Function Name assigned to Button2");
		}
	}
	
	void Button3Click(){
		if(button3GO){
			button3GO.SendMessage(button3FunctionName);
		} else {
			Debug.Log("No GameObect or Function Name assigned to Button3");
		}
	}
	
	void Button4Click(){
		if(button4GO){
			button4GO.SendMessage(button4FunctionName);
		} else {
			Debug.Log("No GameObect or Function Name assigned to Button4");
		}
	}
	
	void Button5Click(){
		if(button5GO){
			button5GO.SendMessage(button5FunctionName);
		} else {
			Debug.Log("No GameObect or Function Name assigned to Button5");
		}
	}
	
	void Button6Click(){
		if(button6GO){
			button6GO.SendMessage(button6FunctionName);
		} else {
			Debug.Log("No GameObect or Function Name assigned to Button6");
		}
	}
	
	void Button7Click(){
		if(button7GO){
			button7GO.SendMessage(button7FunctionName);
		} else {
			Debug.Log("No GameObect or Function Name assigned to Button7");
		}
	}
	
	void Button8Click(){
		if(button8GO){
			button8GO.SendMessage(button8FunctionName);
		} else {
			Debug.Log("No GameObect or Function Name assigned to Button8");
		}
	}
	
	
}
