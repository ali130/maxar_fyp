﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

#if UNITY_EDITOR		
using UnityEditor;
#endif

[ExecuteInEditMode]
public class iOS_UITabPanel : MonoBehaviour {
	
	private float width;
	private float height;
	private bool NavigationBarExists=false;
	private float PosY;
	private bool ToolBarExists=false;
	
	public Color32 PanelColor = new Color32(239, 239, 244, 255);
	
	private int retinaMultipler;
	
	void Update() {
		
		// Lock UI panel to fill screen space
		//transform.GetComponent<RectTransform>().sizeDelta = new Vector2(0, transform.GetComponentInParent<CanvasScaler>().referenceResolution.y);
		//transform.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
		
		
		retinaMultipler = transform.GetComponentInParent<CanvasHandler>().retinaMultipler;
		
		foreach (Transform child in transform)
		{
				
			if(child.tag == "NavigationBar"){
				
				if(child.gameObject.activeSelf){
					NavigationBarExists=true;
					child.GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(16 * retinaMultipler, 22 * retinaMultipler);		
					child.GetChild(0).GetComponent<RectTransform>().anchoredPosition = new Vector2(8 * retinaMultipler, 0);
					
					child.GetChild(1).GetComponent<RectTransform>().sizeDelta = new Vector2(78 * retinaMultipler, 20 * retinaMultipler);		
					child.GetChild(1).GetComponent<RectTransform>().anchoredPosition = new Vector2(30 * retinaMultipler, 0);
					child.GetChild(1).GetComponent<Text>().fontSize = 17 * retinaMultipler;
					
					child.GetChild(2).GetComponent<RectTransform>().sizeDelta = new Vector2(95 * retinaMultipler, 25 * retinaMultipler);		
					child.GetChild(2).GetComponent<Text>().fontSize = 17 * retinaMultipler;
					
					child.GetChild(3).GetComponent<RectTransform>().sizeDelta = new Vector2(78 * retinaMultipler, 20 * retinaMultipler);		
					child.GetChild(3).GetComponent<RectTransform>().anchoredPosition = new Vector2(-8 * retinaMultipler, 0);
					child.GetChild(3).GetComponent<Text>().fontSize = 17 * retinaMultipler;
					
					child.GetChild(4).GetComponent<RectTransform>().sizeDelta = new Vector2(16 * retinaMultipler, 22 * retinaMultipler);		
					child.GetChild(4).GetComponent<RectTransform>().anchoredPosition = new Vector2(-20 * retinaMultipler, 0);
					
					// Keep Navigation Bar in place!
					child.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
					child.GetComponent<RectTransform>().sizeDelta = new Vector2(0, 44 * retinaMultipler);
					child.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);					
					
				} else {
					NavigationBarExists=false;					
				}
							
			}
			
			if(child.tag == "ToolBar"){
				
				if(child.gameObject.activeSelf){
					ToolBarExists=true;
					child.GetComponent<RectTransform>().sizeDelta = new Vector2(0, 44 * retinaMultipler);
					
					child.GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(33 * retinaMultipler, 33 * retinaMultipler);		
					child.GetChild(0).GetComponent<RectTransform>().anchoredPosition = new Vector2((-128 * retinaMultipler), 0);
					
					child.GetChild(1).GetComponent<RectTransform>().sizeDelta = new Vector2(33 * retinaMultipler, 33 * retinaMultipler);		
					child.GetChild(1).GetComponent<RectTransform>().anchoredPosition = new Vector2((-64 * retinaMultipler), 0);
					
					child.GetChild(2).GetComponent<RectTransform>().sizeDelta = new Vector2(33 * retinaMultipler, 33 * retinaMultipler);		
					child.GetChild(2).GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
					
					child.GetChild(3).GetComponent<RectTransform>().sizeDelta = new Vector2(33 * retinaMultipler, 33 * retinaMultipler);		
					child.GetChild(3).GetComponent<RectTransform>().anchoredPosition = new Vector2((64 * retinaMultipler), 0);
					
					child.GetChild(4).GetComponent<RectTransform>().sizeDelta = new Vector2(33 * retinaMultipler, 33 * retinaMultipler);		
					child.GetChild(4).GetComponent<RectTransform>().anchoredPosition = new Vector2((128 * retinaMultipler), 0);
					
					// Keep Tool Bar in place!
					child.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
					child.GetComponent<RectTransform>().sizeDelta = new Vector2(0f,child.GetComponent<RectTransform>().sizeDelta.y);
					child.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);					
					
				} else {
					ToolBarExists=false;					
				}
				
			}
			
			if(child.tag == "SegmentedControl"){
									
				PosY=0;
				
				if(NavigationBarExists){
					PosY = PosY + (44 * retinaMultipler);
				}
				
				child.GetComponent<RectTransform>().sizeDelta = new Vector2(0, 44 * retinaMultipler);
				
				child.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, PosY * -1);			
				
				width = transform.GetComponentInParent<CanvasScaler>().referenceResolution.x;			
				height = transform.GetComponentInParent<CanvasScaler>().referenceResolution.y;
				
				if(NavigationBarExists){
					height = height - (44 * retinaMultipler);
				}
				Debug.Log(transform.name + " " + transform.parent.tag);
				if(transform.tag == "UITabPanel"){
					height = height - (25 * retinaMultipler);
				}
				
				if(ToolBarExists){
					height = height - (44 * retinaMultipler);
				}
				
				//Size and position Segmented Panels
				child.GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(width, height-(44 * retinaMultipler));
				child.GetChild(0).GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
				
				child.GetChild(1).GetComponent<RectTransform>().sizeDelta = new Vector2(width, height-(44 * retinaMultipler));
				child.GetChild(1).GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -44 * retinaMultipler);
				
				child.GetChild(2).GetComponent<RectTransform>().sizeDelta = new Vector2(width, height-(44 * retinaMultipler));
				child.GetChild(2).GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -44 * retinaMultipler);
				
				child.GetChild(3).GetComponent<RectTransform>().sizeDelta = new Vector2(width, height-(44 * retinaMultipler));
				child.GetChild(3).GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -44 * retinaMultipler);
				
				//Size and position background
				child.GetChild(4).GetComponent<RectTransform>().sizeDelta = new Vector2(width, 44 * retinaMultipler);
				child.GetChild(4).GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
				
				//Size and position segment tabs and children
				child.GetChild(5).GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(child.GetChild(5).GetChild(0).GetComponent<RectTransform>().sizeDelta.x, 29 * retinaMultipler);
				child.GetChild(5).GetChild(1).GetComponent<RectTransform>().sizeDelta = new Vector2(child.GetChild(5).GetChild(1).GetComponent<RectTransform>().sizeDelta.x, 29 * retinaMultipler);
				child.GetChild(5).GetChild(2).GetComponent<Text>().fontSize = 14 * retinaMultipler;
				
				child.GetChild(6).GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(child.GetChild(6).GetChild(0).GetComponent<RectTransform>().sizeDelta.x, 29 * retinaMultipler);
				child.GetChild(6).GetChild(1).GetComponent<RectTransform>().sizeDelta = new Vector2(child.GetChild(6).GetChild(1).GetComponent<RectTransform>().sizeDelta.x, 29 * retinaMultipler);
				child.GetChild(6).GetChild(2).GetComponent<Text>().fontSize = 14 * retinaMultipler;
				
				child.GetChild(7).GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(child.GetChild(7).GetChild(0).GetComponent<RectTransform>().sizeDelta.x, 29 * retinaMultipler);
				child.GetChild(7).GetChild(1).GetComponent<RectTransform>().sizeDelta = new Vector2(child.GetChild(7).GetChild(1).GetComponent<RectTransform>().sizeDelta.x, 29 * retinaMultipler);
				child.GetChild(7).GetChild(2).GetComponent<Text>().fontSize = 14 * retinaMultipler;
				
				child.GetChild(8).GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(child.GetChild(8).GetChild(0).GetComponent<RectTransform>().sizeDelta.x, 29 * retinaMultipler);
				child.GetChild(8).GetChild(1).GetComponent<RectTransform>().sizeDelta = new Vector2(child.GetChild(8).GetChild(1).GetComponent<RectTransform>().sizeDelta.x, 29 * retinaMultipler);
				child.GetChild(8).GetChild(2).GetComponent<Text>().fontSize = 14 * retinaMultipler;
				
				
				
				// Look at Navigation bars and Segmented Controls on sub panels, such as Tab Bar and Segmented Control panels.
			}
				
			//Add if for TableView
		}
		
	}
}
