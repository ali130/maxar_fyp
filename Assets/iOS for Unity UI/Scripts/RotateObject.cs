﻿using UnityEngine;
using System.Collections;

public class RotateObject : MonoBehaviour {

	void Update() {
		transform.Rotate(Vector3.right * Time.deltaTime * 100);
		transform.Rotate(Vector3.up * Time.deltaTime * 100, Space.World);
	}
	
	void Start ()
	{
		Random.seed = (int)Time.time;
	}
		
	void OnMouseDown ()
	{
		float r = Random.Range(0f,1f);
		float g = Random.Range(0f,1f);
		float b = Random.Range(0f,1f);
		Color randomColour = new Color(r,g,b,1f);
			
		GetComponent<Renderer>().material.color = randomColour;
	}
}
