﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[ExecuteInEditMode]
public class iOS_Text : MonoBehaviour {

	public bool showPlaceHolder=true;
	public string PlaceHolderText="Enter text...";
	public Color32 PlaceHolderColor = new Color32(146,146,146,255);
	public Color32 ClearButtonColor = new Color32(146,146,146,255);
	public Color32 TextColor = new Color32(0,0,0,255);
	public bool showBorder=true;
	public Color32 BorderColor = new Color32(146,146,146,255);
	public bool showBackground=true;
	public Color32 BackgroundColor = new Color32(200,200,200,255);
	public int typesIndex = 0;
//	public int keyboardsIndex = 0;
//	public int validationIndex = 0;
	public int formatIndex = 0;
	public int placeHolderIndex = 0;
	public bool showInputField=false;
	
	private int retinaMultipler;
	private int lastretinaMultipler;
	private float PosX;
	private float PosY;
	
	void Start() {
		retinaMultipler = GetComponentInParent<CanvasHandler>().retinaMultipler;
	}
	
	void Update() {
		
		#if UNITY_EDITOR		
		retinaMultipler = GetComponentInParent<CanvasHandler>().retinaMultipler;
		lastretinaMultipler = GetComponentInParent<CanvasHandler>().lastretinaMultipler;
		#endif
		
		GetComponent<InputField>().textComponent.fontSize = 17 * retinaMultipler;
		GetComponent<InputField>().placeholder.GetComponent<Text>().fontSize = 17 * retinaMultipler;		
		GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);					
		
		if(lastretinaMultipler != retinaMultipler){
			if(lastretinaMultipler==1){
				PosX = GetComponent<RectTransform>().anchoredPosition.x * retinaMultipler;
				PosY = GetComponent<RectTransform>().anchoredPosition.y * retinaMultipler;	
				GetComponent<RectTransform>().sizeDelta = new Vector2(GetComponent<RectTransform>().sizeDelta.x * retinaMultipler, 29 * retinaMultipler);
			}
			if(lastretinaMultipler==2){
				if(retinaMultipler==1){
					PosX = GetComponent<RectTransform>().anchoredPosition.x / 2;
					PosY = GetComponent<RectTransform>().anchoredPosition.y / 2;
					GetComponent<RectTransform>().sizeDelta = new Vector2(GetComponent<RectTransform>().sizeDelta.x / 2, 29 * retinaMultipler);
				}
				if(retinaMultipler==3){
					PosX = (GetComponent<RectTransform>().anchoredPosition.x / 2) * retinaMultipler;
					PosY = (GetComponent<RectTransform>().anchoredPosition.y / 2) * retinaMultipler;
					GetComponent<RectTransform>().sizeDelta = new Vector2((GetComponent<RectTransform>().sizeDelta.x / 2) * retinaMultipler, 29 * retinaMultipler);
				}
			}
			if(lastretinaMultipler==3){
				if(retinaMultipler==1){
					PosX = GetComponent<RectTransform>().anchoredPosition.x / 3;
					PosY = GetComponent<RectTransform>().anchoredPosition.y / 3;
					GetComponent<RectTransform>().sizeDelta = new Vector2(GetComponent<RectTransform>().sizeDelta.x / 3, 29 * retinaMultipler);
				}
				if(retinaMultipler==2){
					PosX = (GetComponent<RectTransform>().anchoredPosition.x / 3) * retinaMultipler;
					PosY = (GetComponent<RectTransform>().anchoredPosition.y / 3) * retinaMultipler;
					GetComponent<RectTransform>().sizeDelta = new Vector2((GetComponent<RectTransform>().sizeDelta.x / 3) * retinaMultipler, 29 * retinaMultipler);
				}			
			}
			
			GetComponent<RectTransform>().anchoredPosition = new Vector2(PosX, PosY);	
			
		}
		
	}		
	
	public void OnEdit(){
		transform.Find("ClearButton").gameObject.SetActive(true);		
	}
	
	public void EndEdit(){
		transform.Find("ClearButton").gameObject.SetActive(false);		
	}

	public void ClearText(){
		Debug.Log ("Clear Text");
		transform.GetComponent<InputField>().text = "";
		transform.Find("ClearButton").gameObject.SetActive(false);		
	}
	
}
