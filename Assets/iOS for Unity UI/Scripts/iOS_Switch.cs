﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class iOS_Switch : MonoBehaviour {

	public bool interactable=true;
	public bool isOn=false;
	public string OnStateText;
	public string OffStateText;
	
	private int retinaMultipler;
	private int lastretinaMultipler;
	private float PosX;
	private float PosY;
	
	void Start() {
		retinaMultipler = GetComponentInParent<CanvasHandler>().retinaMultipler;
		//PosX = GetComponent<RectTransform>().anchoredPosition.x;
		//PosY = GetComponent<RectTransform>().anchoredPosition.y;
	}
	
	void Update() {
		
		#if UNITY_EDITOR		
		retinaMultipler = GetComponentInParent<CanvasHandler>().retinaMultipler;
		lastretinaMultipler = GetComponentInParent<CanvasHandler>().lastretinaMultipler;
		#endif
		
		if(lastretinaMultipler != retinaMultipler){
			if(lastretinaMultipler==1){
				PosX = GetComponent<RectTransform>().anchoredPosition.x * retinaMultipler;
				PosY = GetComponent<RectTransform>().anchoredPosition.y * retinaMultipler;	
			}
			if(lastretinaMultipler==2){
				if(retinaMultipler==1){
					PosX = GetComponent<RectTransform>().anchoredPosition.x / 2;
					PosY = GetComponent<RectTransform>().anchoredPosition.y / 2;
				}
				if(retinaMultipler==3){
					PosX = (GetComponent<RectTransform>().anchoredPosition.x / 2) * retinaMultipler;
					PosY = (GetComponent<RectTransform>().anchoredPosition.y / 2) * retinaMultipler;
				}
			}
			if(lastretinaMultipler==3){
				if(retinaMultipler==1){
					PosX = GetComponent<RectTransform>().anchoredPosition.x / 3;
					PosY = GetComponent<RectTransform>().anchoredPosition.y / 3;
				}
				if(retinaMultipler==2){
					PosX = (GetComponent<RectTransform>().anchoredPosition.x / 3) * retinaMultipler;
					PosY = (GetComponent<RectTransform>().anchoredPosition.y / 3) * retinaMultipler;
				}			
			}
			
			GetComponent<RectTransform>().anchoredPosition = new Vector2(PosX, PosY);	
			
		}
		
		
		transform.GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(55 * retinaMultipler, 35 * retinaMultipler);
		transform.GetChild(0).GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);					
		
		transform.GetChild(0).GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(55 * retinaMultipler, 35 * retinaMultipler);
		transform.GetChild(0).GetChild(0).GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);					
		
	}
		
}
