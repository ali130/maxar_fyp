﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class rootUIPanel_BehaviourScript : MonoBehaviour {
	
	public GameObject go;
		
	/* Copy and rename this function to create custom behaviours for your GUI components.
	public void MyCustomBehaviour(){
	
	}
	*/
		
	public void ClickMeButtonPress(){
		Debug.Log ("Button clicked!");
	}
	
	public void ColorObjectRed(){
		go.GetComponent<Renderer>().material.color = Color.red;
	}
	
	public void ColorObjectGreen(){
		go.GetComponent<Renderer>().material.color = Color.green;
	}
}
