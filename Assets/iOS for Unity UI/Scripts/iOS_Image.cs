﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class iOS_Image : MonoBehaviour {

	public Sprite image;
	public bool actualImageSize=true;
	public int width=100;
	public int height=100;
	
	private int retinaMultipler;
	private int lastretinaMultipler;
	private float PosX;
	private float PosY;
	
	void Start() {
		retinaMultipler = GetComponentInParent<CanvasHandler>().retinaMultipler;
	}
	
	void Update() {
		
		#if UNITY_EDITOR		
		retinaMultipler = GetComponentInParent<CanvasHandler>().retinaMultipler;
		lastretinaMultipler = GetComponentInParent<CanvasHandler>().lastretinaMultipler;
		#endif
		
		GetComponent<iOS_Image>().height = 120 * retinaMultipler;
		GetComponent<iOS_Image>().width = 160 * retinaMultipler;
		
		GetComponent<RectTransform>().sizeDelta = new Vector2(GetComponent<iOS_Image>().width, GetComponent<iOS_Image>().height);
		GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);					
		
		if(lastretinaMultipler != retinaMultipler){
			if(lastretinaMultipler==1){
				PosX = GetComponent<RectTransform>().anchoredPosition.x * retinaMultipler;
				PosY = GetComponent<RectTransform>().anchoredPosition.y * retinaMultipler;	
				GetComponent<RectTransform>().sizeDelta = new Vector2(GetComponent<RectTransform>().sizeDelta.x * retinaMultipler, 29 * retinaMultipler);
			}
			if(lastretinaMultipler==2){
				if(retinaMultipler==1){
					PosX = GetComponent<RectTransform>().anchoredPosition.x / 2;
					PosY = GetComponent<RectTransform>().anchoredPosition.y / 2;
					GetComponent<RectTransform>().sizeDelta = new Vector2(GetComponent<RectTransform>().sizeDelta.x / 2, 29 * retinaMultipler);
				}
				if(retinaMultipler==3){
					PosX = (GetComponent<RectTransform>().anchoredPosition.x / 2) * retinaMultipler;
					PosY = (GetComponent<RectTransform>().anchoredPosition.y / 2) * retinaMultipler;
					GetComponent<RectTransform>().sizeDelta = new Vector2((GetComponent<RectTransform>().sizeDelta.x / 2) * retinaMultipler, 29 * retinaMultipler);
				}
			}
			if(lastretinaMultipler==3){
				if(retinaMultipler==1){
					PosX = GetComponent<RectTransform>().anchoredPosition.x / 3;
					PosY = GetComponent<RectTransform>().anchoredPosition.y / 3;
					GetComponent<RectTransform>().sizeDelta = new Vector2(GetComponent<RectTransform>().sizeDelta.x / 3, 29 * retinaMultipler);
				}
				if(retinaMultipler==2){
					PosX = (GetComponent<RectTransform>().anchoredPosition.x / 3) * retinaMultipler;
					PosY = (GetComponent<RectTransform>().anchoredPosition.y / 3) * retinaMultipler;
					GetComponent<RectTransform>().sizeDelta = new Vector2((GetComponent<RectTransform>().sizeDelta.x / 3) * retinaMultipler, 29 * retinaMultipler);
				}			
			}
			
			GetComponent<RectTransform>().anchoredPosition = new Vector2(PosX, PosY);	
			
		}
		
	}
	
}
