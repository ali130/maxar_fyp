﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[ExecuteInEditMode]
public class iOS_ToolBar : MonoBehaviour {
	
	public int iconsIndex = 0;
    public int spacing = 64;
	public Color32 IconsColor;
	public Sprite icon1Image;
	public GameObject button1GO;
	public string button1FunctionName;
	public Sprite icon2Image;
	public GameObject button2GO;
	public string button2FunctionName;
	public Sprite icon3Image;
	public GameObject button3GO;
	public string button3FunctionName;
	public Sprite icon4Image;
	public GameObject button4GO;
	public string button4FunctionName;
	public Sprite icon5Image;
	public GameObject button5GO;
	public string button5FunctionName;
	
	private int retinaMultipler;
	
	void Start() {
		retinaMultipler = GetComponentInParent<CanvasHandler>().retinaMultipler;
	}
	
	void Update() {
		
		#if UNITY_EDITOR		
			retinaMultipler = GetComponentInParent<CanvasHandler>().retinaMultipler;
		#endif
		
		GetComponent<RectTransform>().sizeDelta = new Vector2(0, 44 * retinaMultipler);
		
		transform.GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(33 * retinaMultipler, 33 * retinaMultipler);		
		transform.GetChild(0).GetComponent<RectTransform>().anchoredPosition = new Vector2((-spacing * 2 * retinaMultipler), 0);
		
		transform.GetChild(1).GetComponent<RectTransform>().sizeDelta = new Vector2(33 * retinaMultipler, 33 * retinaMultipler);		
		transform.GetChild(1).GetComponent<RectTransform>().anchoredPosition = new Vector2((-spacing * retinaMultipler), 0);
		
		transform.GetChild(2).GetComponent<RectTransform>().sizeDelta = new Vector2(33 * retinaMultipler, 33 * retinaMultipler);		
		transform.GetChild(2).GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
		
		transform.GetChild(3).GetComponent<RectTransform>().sizeDelta = new Vector2(33 * retinaMultipler, 33 * retinaMultipler);		
		transform.GetChild(3).GetComponent<RectTransform>().anchoredPosition = new Vector2((spacing * retinaMultipler), 0);
		
		transform.GetChild(4).GetComponent<RectTransform>().sizeDelta = new Vector2(33 * retinaMultipler, 33 * retinaMultipler);		
		transform.GetChild(4).GetComponent<RectTransform>().anchoredPosition = new Vector2((spacing * 2 * retinaMultipler), 0);
		
		// Keep Tool Bar in place!
		GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
		GetComponent<RectTransform>().sizeDelta = new Vector2(0f,GetComponent<RectTransform>().sizeDelta.y);
		GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);					
		
	}
	
	void Button1Click(){
		if(button1GO){
			button1GO.SendMessage(button1FunctionName);
		} else {
			Debug.Log("No GameObect or Function Name assigned to Button1");
		}
	}
	
	void Button2Click(){
		if(button2GO){
			button2GO.SendMessage(button2FunctionName);
		} else {
			Debug.Log("No GameObect or Function Name assigned to Button2");
		}
	}
	
	void Button3Click(){
		if(button3GO){
			button3GO.SendMessage(button3FunctionName);
		} else {
			Debug.Log("No GameObect or Function Name assigned to Button3");
		}
	}
	
	void Button4Click(){
		if(button4GO){
			button4GO.SendMessage(button4FunctionName);
		} else {
			Debug.Log("No GameObect or Function Name assigned to Button4");
		}
	}
	
	void Button5Click(){
		if(button5GO){
			button5GO.SendMessage(button5FunctionName);
		} else {
			Debug.Log("No GameObect or Function Name assigned to Button5");
		}
	}
	
}
