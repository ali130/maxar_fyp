﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[ExecuteInEditMode]
public class iOS_TableViewItem : MonoBehaviour {

	public bool showHeaderLine=true;
	public bool showLeftImage=true;
	public Sprite LeftImage;
	public Color32 LeftImageColor = new Color32(0,122,255,255);
	public bool showTitleText=true;
	public string TitleText="Title";
	public Color32 TitleTextColor = new Color32(0,0,0,255);
	public Color BackgroundColor = new Color32(239,239,239,255);
	public float BackgroundOpacity = 255;
	public GameObject ConnectedPanel;
	public bool showRightText=true;
	public iOS_Switch ConnectedSwitch;
	public string RightText="Label";
	public Color32 RightTextColor = new Color32(146,146,146,255);
	public bool showRightImage=true;
	public Sprite RightImage;
	public Color32 RightImageColor = new Color32(200,200,200,255);
	public bool showFooterLine=true;
	public bool FooterLineLength=true;

	private GameObject RightTextGO;
	private Text RightTextComponent;
	
	private int retinaMultipler;
	private int lastretinaMultipler;
	private float PosX;
	private float PosY;
	
	public void PanelTransition(){
		Debug.Log (transform.parent.name + ConnectedPanel.name);
		if(ConnectedPanel == null){
			// Animate panel out
			if(transform.parent.tag == "UITabPanel"){ //If UITabPanel, need to slide out TabPanel parent panel, and not individual TabPanel.
				if(transform.parent.parent.parent.GetComponent<iOS_UIPanel>().width == 640){ //iPhone4 and 5 Portrait
					transform.parent.parent.parent.GetComponent<Animator>().Play("iPhone4and5_640_PanelSlideOut");
				}
				if(transform.parent.parent.parent.GetComponent<iOS_UIPanel>().width == 960){ //iPhone4 Landscape
					transform.parent.parent.parent.GetComponent<Animator>().Play("iPhone4_960_PanelSlideOut");
				}
				if(transform.parent.parent.parent.GetComponent<iOS_UIPanel>().width == 1136){ //iPhone5 Landscape
					transform.parent.parent.parent.GetComponent<Animator>().Play("iPhone5_1136_PanelSlideOut");
				}
				if(transform.parent.parent.parent.GetComponent<iOS_UIPanel>().width == 750){ //iPhone6 Portrait
					transform.parent.parent.parent.GetComponent<Animator>().Play("iPhone6_750_PanelSlideOut");
				}
				if(transform.parent.parent.parent.GetComponent<iOS_UIPanel>().width == 1134){ //iPhone6 Landscape
					transform.parent.parent.parent.GetComponent<Animator>().Play("iPhone6_1134_PanelSlideOut");
				}
				if(transform.parent.parent.parent.GetComponent<iOS_UIPanel>().width == 1242){ //iPhone6+ Portrait
					transform.parent.parent.parent.GetComponent<Animator>().Play("iPhone6_1242_PanelSlideOut");
				}
				if(transform.parent.parent.parent.GetComponent<iOS_UIPanel>().width == 2208){ //iPhone6+ Landscape
					transform.parent.parent.parent.GetComponent<Animator>().Play("iPhone6_2208_PanelSlideOut");
				}
				if(transform.parent.parent.parent.GetComponent<iOS_UIPanel>().width == 768){ //iPad Portrait
					transform.parent.parent.parent.GetComponent<Animator>().Play("iPad_768_PanelSlideOut");
				}
				if(transform.parent.parent.parent.GetComponent<iOS_UIPanel>().width == 1024){ //iPad Landscape
					transform.parent.parent.parent.GetComponent<Animator>().Play("iPad_1024_PanelSlideOut");
				}
				if(transform.parent.parent.parent.GetComponent<iOS_UIPanel>().width == 1536){ //iPad Retina Portrait
					transform.parent.parent.parent.GetComponent<Animator>().Play("iPadRetina_1536_PanelSlideOut");
				}
				if(transform.parent.parent.parent.GetComponent<iOS_UIPanel>().width == 2048){ //iPad Retina Landscape
					transform.parent.parent.parent.GetComponent<Animator>().Play("iPadRetina_2048_PanelSlideOut");
				}
			} else {
				if(transform.parent.GetComponent<iOS_UIPanel>().width == 640){ //iPhone4 and 5 Portrait
					transform.parent.GetComponent<Animator>().Play("iPhone4and5_640_PanelSlideOut");
				}
				if(transform.parent.GetComponent<iOS_UIPanel>().width == 960){ //iPhone4 Landscape
					transform.parent.GetComponent<Animator>().Play("iPhone4_960_PanelSlideOut");
				}
				if(transform.parent.GetComponent<iOS_UIPanel>().width == 1136){ //iPhone5 Landscape
					transform.parent.GetComponent<Animator>().Play("iPhone5_1136_PanelSlideOut");
				}
				if(transform.parent.GetComponent<iOS_UIPanel>().width == 750){ //iPhone6 Portrait
					transform.parent.GetComponent<Animator>().Play("iPhone6_750_PanelSlideOut");
				}
				if(transform.parent.GetComponent<iOS_UIPanel>().width == 1134){ //iPhone6 Landscape
					transform.parent.GetComponent<Animator>().Play("iPhone6_1134_PanelSlideOut");
				}
				if(transform.parent.GetComponent<iOS_UIPanel>().width == 1242){ //iPhone6+ Portrait
					transform.parent.GetComponent<Animator>().Play("iPhone6_1242_PanelSlideOut");
				}
				if(transform.parent.GetComponent<iOS_UIPanel>().width == 2208){ //iPhone6+ Landscape
					transform.parent.GetComponent<Animator>().Play("iPhone6_2208_PanelSlideOut");
				}
				if(transform.parent.GetComponent<iOS_UIPanel>().width == 768){ //iPad Portrait
					transform.parent.GetComponent<Animator>().Play("iPad_768_PanelSlideOut");
				}
				if(transform.parent.GetComponent<iOS_UIPanel>().width == 1024){ //iPad Landscape
					transform.parent.GetComponent<Animator>().Play("iPad_1024_PanelSlideOut");
				}
				if(transform.parent.GetComponent<iOS_UIPanel>().width == 1536){ //iPad Retina Portrait
					transform.parent.GetComponent<Animator>().Play("iPadRetina_1536_PanelSlideOut");
				}
				if(transform.parent.GetComponent<iOS_UIPanel>().width == 2048){ //iPad Retina Landscape
					transform.parent.GetComponent<Animator>().Play("iPadRetina_2048_PanelSlideOut");
				}				
			}
			
		} else {
			// Enable Animator of connected panel to animate panel in
			ConnectedPanel.GetComponent<Animator>().enabled = true;
			Debug.Log(ConnectedPanel.GetComponent<iOS_UIPanel>().width);
			if(ConnectedPanel.GetComponent<iOS_UIPanel>().width == 0){ //iPhone4 and 5 Portrait
				ConnectedPanel.GetComponent<Animator>().Play("iPhone4and5_640_PanelSlideIn");
			}
			if(ConnectedPanel.GetComponent<iOS_UIPanel>().width == 960){ //iPhone4 Landscape
				ConnectedPanel.GetComponent<Animator>().Play("iPhone4_960_PanelSlideIn");
			}
			if(ConnectedPanel.GetComponent<iOS_UIPanel>().width == 1136){ //iPhone5 Landscape
				ConnectedPanel.GetComponent<Animator>().Play("iPhone5_1136_PanelSlideIn");
			}
			if(ConnectedPanel.GetComponent<iOS_UIPanel>().width == 750){ //iPhone6 Portrait
				ConnectedPanel.GetComponent<Animator>().Play("iPhone6_750_PanelSlideIn");
			}
			if(ConnectedPanel.GetComponent<iOS_UIPanel>().width == 1134){ //iPhone6 Landscape
				ConnectedPanel.GetComponent<Animator>().Play("iPhone6_1134_PanelSlideIn");
			}
			if(ConnectedPanel.GetComponent<iOS_UIPanel>().width == 1242){ //iPhone6+ Portrait
				ConnectedPanel.GetComponent<Animator>().Play("iPhone6_1242_PanelSlideIn");
			}
			if(ConnectedPanel.GetComponent<iOS_UIPanel>().width == 2208){ //iPhone6+ Landscape
				ConnectedPanel.GetComponent<Animator>().Play("iPhone6_2208_PanelSlideIn");
			}
			if(ConnectedPanel.GetComponent<iOS_UIPanel>().width == 768){ //iPad Portrait
				ConnectedPanel.GetComponent<Animator>().Play("iPad_768_PanelSlideIn");
			}
			if(ConnectedPanel.GetComponent<iOS_UIPanel>().width == 1024){ //iPad Landscape
				ConnectedPanel.GetComponent<Animator>().Play("iPad_1024_PanelSlideIn");
			}
			if(ConnectedPanel.GetComponent<iOS_UIPanel>().width == 1536){ //iPad Retina Portrait
				ConnectedPanel.GetComponent<Animator>().Play("iPadRetina_1536_PanelSlideIn");
			}
			if(ConnectedPanel.GetComponent<iOS_UIPanel>().width == 2048){ //iPad Retina Landscape
				ConnectedPanel.GetComponent<Animator>().Play("iPadRetina_2048_PanelSlideIn");
			}				
		}
	}

	void Start(){
		retinaMultipler = GetComponentInParent<CanvasHandler>().retinaMultipler;
		lastretinaMultipler = GetComponentInParent<CanvasHandler>().lastretinaMultipler;
		
		RightTextGO = transform.Find("RightText").gameObject;
		RightTextComponent = RightTextGO.GetComponent<Text>();
	}
		
	void Update(){		
		retinaMultipler = GetComponentInParent<CanvasHandler>().retinaMultipler;
		//lastretinaMultipler = GetComponentInParent<CanvasHandler>().lastretinaMultipler;
		
		GetComponent<RectTransform>().sizeDelta = new Vector2(GetComponent<RectTransform>().sizeDelta.x, 43 * retinaMultipler);
		
		if(lastretinaMultipler != retinaMultipler){
			if(lastretinaMultipler==1){
				PosX = GetComponent<RectTransform>().anchoredPosition.x * retinaMultipler;
				PosY = GetComponent<RectTransform>().anchoredPosition.y * retinaMultipler;	
			}
			if(lastretinaMultipler==2){
				if(retinaMultipler==1){
					PosX = GetComponent<RectTransform>().anchoredPosition.x / 2;
					PosY = GetComponent<RectTransform>().anchoredPosition.y / 2;
				}
				if(retinaMultipler==3){
					PosX = (GetComponent<RectTransform>().anchoredPosition.x / 2) * retinaMultipler;
					PosY = (GetComponent<RectTransform>().anchoredPosition.y / 2) * retinaMultipler;
				}
			}
			if(lastretinaMultipler==3){
				if(retinaMultipler==1){
					PosX = GetComponent<RectTransform>().anchoredPosition.x / 3;
					PosY = GetComponent<RectTransform>().anchoredPosition.y / 3;
				}
				if(retinaMultipler==2){
					PosX = (GetComponent<RectTransform>().anchoredPosition.x / 3) * retinaMultipler;
					PosY = (GetComponent<RectTransform>().anchoredPosition.y / 3) * retinaMultipler;
				}			
			}
			
			GetComponent<RectTransform>().anchoredPosition = new Vector2(PosX, PosY);
            lastretinaMultipler = retinaMultipler;
        }
		
		transform.GetChild(1).GetComponent<RectTransform>().sizeDelta = new Vector2(29 * retinaMultipler, 29 * retinaMultipler);
		transform.GetChild(1).GetComponent<RectTransform>().anchoredPosition = new Vector2(12 * retinaMultipler, 0);
		transform.GetChild(2).GetComponent<Text>().fontSize = 17 * retinaMultipler;
		if(showLeftImage){
			transform.GetChild(2).GetComponent<RectTransform>().anchoredPosition = new Vector2(54 * retinaMultipler, 0);
		} else {
			transform.GetChild(2).GetComponent<RectTransform>().anchoredPosition = new Vector2(15 * retinaMultipler, 0);
		}
		transform.GetChild(3).GetComponent<Text>().fontSize = 17 * retinaMultipler;
		transform.GetChild(3).GetComponent<RectTransform>().anchoredPosition = new Vector2(-32 * retinaMultipler, 0);
		transform.GetChild(4).GetComponent<RectTransform>().sizeDelta = new Vector2(14 * retinaMultipler, 14 * retinaMultipler);
		transform.GetChild(4).GetComponent<RectTransform>().anchoredPosition = new Vector2(-15 * retinaMultipler, 0);
		transform.GetChild(5).GetComponent<RectTransform>().sizeDelta = new Vector2(0, 1);
		if(FooterLineLength){
			transform.GetChild(5).GetComponent<RectTransform>().anchoredPosition = new Vector2(15 * retinaMultipler, 0);
		} else {
			transform.GetChild(5).GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);			
		}
		
//		Debug.Log ("Update " + ConnectedSwitch.isOn);
		if(ConnectedSwitch != null){
//			GameObject RightTextGO = transform.FindChild("RightText").gameObject;
//			Text RightTextComponent= RightTextGO.GetComponent<Text>();
			if(ConnectedSwitch.GetComponent<Toggle>().isOn==true){
				RightText = ConnectedSwitch.OnStateText;
//				Debug.Log ("on " + RightTextComponent.text);
			} else {
				RightText = ConnectedSwitch.OffStateText;
//				Debug.Log ("off " + RightTextComponent.text);
			}
			RightTextComponent.text = RightText;
		} else {
            RightTextComponent.text = RightText;
        }

    }
}
