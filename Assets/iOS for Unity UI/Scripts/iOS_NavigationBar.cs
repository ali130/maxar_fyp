﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[ExecuteInEditMode]
public class iOS_NavigationBar : MonoBehaviour {
	
	public bool showStatusbar;
	public bool showLeftImage=true;
	public Color32 LeftImageColor = new Color32(0,122,255,255);
	public Sprite LeftImage;	
	public GameObject ConnectedPanel;
	public GameObject LeftGO;
	public string LeftFunctionName;
	
	public bool showLeftText=true;
	public string LeftText="Back";
	public Color32 LeftTextColor = new Color32(0,122,255,255);
	public bool showTitleText=true;
	public string TitleText="Title";
	public Color32 TitleTextColor = new Color32(0,0,0,255);
	public bool showRightText=true;
	public string RightText="Edit";
	public Color32 RightTextColor = new Color32(0,122,255,255);
	public bool showRightImage=true;
	public Color32 RightImageColor = new Color32(0,122,255,255);
	public Sprite RightImage;
	public GameObject RightGO;
	public string RightFunctionName;
	
	//private Transform Statusbar;
	
	private int retinaMultipler;
	
	public void PanelTransition(){
		Debug.Log (transform.parent.name);
		if(LeftGO == null){
			if(ConnectedPanel == null){
				// Animate panel out
				if(transform.parent.tag == "UITabPanel"){ //If UITabPanel, need to slide out TabPanel parent panel, and not individual TabPanel.
					if(transform.parent.parent.parent.GetComponent<iOS_UIPanel>().width == 640){ //iPhone4 and 5 Portrait
						transform.parent.parent.parent.GetComponent<Animator>().Play("iPhone4and5_640_PanelSlideOut");
					}
					if(transform.parent.parent.parent.GetComponent<iOS_UIPanel>().width == 960){ //iPhone4 Landscape
						transform.parent.parent.parent.GetComponent<Animator>().Play("iPhone4_960_PanelSlideOut");
					}
					if(transform.parent.parent.parent.GetComponent<iOS_UIPanel>().width == 1136){ //iPhone5 Landscape
						transform.parent.parent.parent.GetComponent<Animator>().Play("iPhone5_1136_PanelSlideOut");
					}
					if(transform.parent.parent.parent.GetComponent<iOS_UIPanel>().width == 750){ //iPhone6 Portrait
						transform.parent.parent.parent.GetComponent<Animator>().Play("iPhone6_750_PanelSlideOut");
					}
					if(transform.parent.parent.parent.GetComponent<iOS_UIPanel>().width == 1134){ //iPhone6 Landscape
						transform.parent.parent.parent.GetComponent<Animator>().Play("iPhone6_1134_PanelSlideOut");
					}
					if(transform.parent.parent.parent.GetComponent<iOS_UIPanel>().width == 1242){ //iPhone6+ Portrait
						transform.parent.parent.parent.GetComponent<Animator>().Play("iPhone6_1242_PanelSlideOut");
					}
					if(transform.parent.parent.parent.GetComponent<iOS_UIPanel>().width == 2208){ //iPhone6+ Landscape
						transform.parent.parent.parent.GetComponent<Animator>().Play("iPhone6_2208_PanelSlideOut");
					}
					if(transform.parent.parent.parent.GetComponent<iOS_UIPanel>().width == 768){ //iPad Portrait
						transform.parent.parent.parent.GetComponent<Animator>().Play("iPad_768_PanelSlideOut");
					}
					if(transform.parent.parent.parent.GetComponent<iOS_UIPanel>().width == 1024){ //iPad Landscape
						transform.parent.parent.parent.GetComponent<Animator>().Play("iPad_1024_PanelSlideOut");
					}
					if(transform.parent.parent.parent.GetComponent<iOS_UIPanel>().width == 1536){ //iPad Retina Portrait
						transform.parent.parent.parent.GetComponent<Animator>().Play("iPadRetina_1536_PanelSlideOut");
					}
					if(transform.parent.parent.parent.GetComponent<iOS_UIPanel>().width == 2048){ //iPad Retina Landscape
						transform.parent.parent.parent.GetComponent<Animator>().Play("iPadRetina_2048_PanelSlideOut");
					}
				} else {
					if(transform.parent.GetComponent<iOS_UIPanel>().width == 0){ //iPhone4 and 5 Portrait
						transform.parent.GetComponent<Animator>().Play("iPhone4and5_640_PanelSlideOut");
					}
					if(transform.parent.GetComponent<iOS_UIPanel>().width == 960){ //iPhone4 Landscape
						transform.parent.GetComponent<Animator>().Play("iPhone4_960_PanelSlideOut");
					}
					if(transform.parent.GetComponent<iOS_UIPanel>().width == 1136){ //iPhone5 Landscape
						transform.parent.GetComponent<Animator>().Play("iPhone5_1136_PanelSlideOut");
					}
					if(transform.parent.GetComponent<iOS_UIPanel>().width == 750){ //iPhone6 Portrait
						transform.parent.GetComponent<Animator>().Play("iPhone6_750_PanelSlideOut");
					}
					if(transform.parent.GetComponent<iOS_UIPanel>().width == 1134){ //iPhone6 Landscape
						transform.parent.GetComponent<Animator>().Play("iPhone6_1134_PanelSlideOut");
					}
					if(transform.parent.GetComponent<iOS_UIPanel>().width == 1242){ //iPhone6+ Portrait
						transform.parent.GetComponent<Animator>().Play("iPhone6_1242_PanelSlideOut");
					}
					if(transform.parent.GetComponent<iOS_UIPanel>().width == 2208){ //iPhone6+ Landscape
						transform.parent.GetComponent<Animator>().Play("iPhone6_2208_PanelSlideOut");
					}
					if(transform.parent.GetComponent<iOS_UIPanel>().width == 768){ //iPad Portrait
						transform.parent.GetComponent<Animator>().Play("iPad_768_PanelSlideOut");
					}
					if(transform.parent.GetComponent<iOS_UIPanel>().width == 1024){ //iPad Landscape
						transform.parent.GetComponent<Animator>().Play("iPad_1024_PanelSlideOut");
					}
					if(transform.parent.GetComponent<iOS_UIPanel>().width == 1536){ //iPad Retina Portrait
						transform.parent.GetComponent<Animator>().Play("iPadRetina_1536_PanelSlideOut");
					}
					if(transform.parent.GetComponent<iOS_UIPanel>().width == 2048){ //iPad Retina Landscape
						transform.parent.GetComponent<Animator>().Play("iPadRetina_2048_PanelSlideOut");
					}				
				}		
			} else {
				if(ConnectedPanel != null){
					ConnectedPanel.GetComponent<Animator>().enabled = true;
					if(ConnectedPanel.GetComponent<iOS_UIPanel>().width == 640){ //iPhone4 and 5 Portrait
						ConnectedPanel.GetComponent<Animator>().Play("iPhone4and5_640_PanelSlideIn");
					}
					if(ConnectedPanel.GetComponent<iOS_UIPanel>().width == 960){ //iPhone4 Landscape
						ConnectedPanel.GetComponent<Animator>().Play("iPhone4_960_PanelSlideIn");
					}
					if(ConnectedPanel.GetComponent<iOS_UIPanel>().width == 1136){ //iPhone5 Landscape
						ConnectedPanel.GetComponent<Animator>().Play("iPhone5_1136_PanelSlideIn");
					}
					if(ConnectedPanel.GetComponent<iOS_UIPanel>().width == 750){ //iPhone6 Portrait
						ConnectedPanel.GetComponent<Animator>().Play("iPhone6_750_PanelSlideIn");
					}
					if(ConnectedPanel.GetComponent<iOS_UIPanel>().width == 1134){ //iPhone6 Landscape
						ConnectedPanel.GetComponent<Animator>().Play("iPhone6_1134_PanelSlideIn");
					}
					if(ConnectedPanel.GetComponent<iOS_UIPanel>().width == 1242){ //iPhone6+ Portrait
						ConnectedPanel.GetComponent<Animator>().Play("iPhone6_1242_PanelSlideIn");
					}
					if(ConnectedPanel.GetComponent<iOS_UIPanel>().width == 2208){ //iPhone6+ Landscape
						ConnectedPanel.GetComponent<Animator>().Play("iPhone6_2208_PanelSlideIn");
					}
					if(ConnectedPanel.GetComponent<iOS_UIPanel>().width == 768){ //iPad Portrait
						ConnectedPanel.GetComponent<Animator>().Play("iPad_768_PanelSlideIn");
					}
					if(ConnectedPanel.GetComponent<iOS_UIPanel>().width == 1024){ //iPad Landscape
						ConnectedPanel.GetComponent<Animator>().Play("iPad_1024_PanelSlideIn");
					}
					if(ConnectedPanel.GetComponent<iOS_UIPanel>().width == 1536){ //iPad Retina Portrait
						ConnectedPanel.GetComponent<Animator>().Play("iPadRetina_1536_PanelSlideIn");
					}
					if(ConnectedPanel.GetComponent<iOS_UIPanel>().width == 2048){ //iPad Retina Landscape
						ConnectedPanel.GetComponent<Animator>().Play("iPadRetina_2048_PanelSlideIn");
					}				
				}
			}
		} else {	
			LeftGO.SendMessage(LeftFunctionName);			
		}
	}
	
	void RightClick(){
		if(RightGO){
			RightGO.SendMessage(RightFunctionName);
		} else {
			Debug.Log("No GameObect or Function Name assigned to Right Text & Image");
		}
	}
	
	void Start(){
		retinaMultipler = GetComponentInParent<CanvasHandler>().retinaMultipler;
	}
	
	void Update(){
		#if UNITY_EDITOR		
		retinaMultipler = GetComponentInParent<CanvasHandler>().retinaMultipler;
		#endif
		
		transform.GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(16 * retinaMultipler, 22 * retinaMultipler);		
		transform.GetChild(0).GetComponent<RectTransform>().anchoredPosition = new Vector2(8 * retinaMultipler, 0);
		
		transform.GetChild(1).GetComponent<RectTransform>().sizeDelta = new Vector2(78 * retinaMultipler, 20 * retinaMultipler);		
		if(showLeftImage){
			transform.GetChild(1).GetComponent<RectTransform>().anchoredPosition = new Vector2(30 * retinaMultipler, 0);
		} else {
			transform.GetChild(1).GetComponent<RectTransform>().anchoredPosition = new Vector2(8 * retinaMultipler, 0);		
		}	
		transform.GetChild(1).GetComponent<Text>().fontSize = 17 * retinaMultipler;
		
		transform.GetChild(2).GetComponent<RectTransform>().sizeDelta = new Vector2(95 * retinaMultipler, 25 * retinaMultipler);		
		transform.GetChild(2).GetComponent<Text>().fontSize = 17 * retinaMultipler;
		
		transform.GetChild(3).GetComponent<RectTransform>().sizeDelta = new Vector2(78 * retinaMultipler, 20 * retinaMultipler);		
		transform.GetChild(3).GetComponent<RectTransform>().anchoredPosition = new Vector2(-8 * retinaMultipler, 0);
		transform.GetChild(3).GetComponent<Text>().fontSize = 17 * retinaMultipler;
		
		transform.GetChild(4).GetComponent<RectTransform>().sizeDelta = new Vector2(16 * retinaMultipler, 22 * retinaMultipler);		
		transform.GetChild(4).GetComponent<RectTransform>().anchoredPosition = new Vector2(-20 * retinaMultipler, 0);

        // Save button scaling
        if (transform.childCount == 6) {
            transform.GetChild(5).GetComponent<RectTransform>().sizeDelta = new Vector2(78 * retinaMultipler, 20 * retinaMultipler);
            transform.GetChild(5).GetComponent<RectTransform>().anchoredPosition = new Vector2(-64 * retinaMultipler, 0);
            transform.GetChild(5).GetComponent<Text>().fontSize = 17 * retinaMultipler;
        }

        // Keep Navigation Bar in place!
        GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
		GetComponent<RectTransform>().sizeDelta = new Vector2(0, 44 * retinaMultipler);
		GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);					
		
	}
	
}
