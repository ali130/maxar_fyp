﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
#if UNITY_EDITOR		
using UnityEditor;
#endif

[ExecuteInEditMode]
public class CanvasHandler : MonoBehaviour {
	
	float ScreenHeight;
	float ScreenWidth;
	
	public int defaultScreenWidth=640;
	public int defaultScreenHeight=960;
	public bool useDeviceScreenResolution=true;
	
	public int retinaMultipler=2;
	public int lastretinaMultipler=2;

    private int GetRetinaMultiplier() {
        #if UNITY_EDITOR    
            if(ScreenHeight==960 || ScreenWidth==960){
                return 2; 
            }
            if(ScreenHeight==1136 || ScreenWidth==1136){
                return 2; 
            }
            if(ScreenHeight==1334 || ScreenWidth==1334){
                return 2; 
            }
            if(ScreenHeight==1920 || ScreenWidth==1920){
                return 3; 
            }
            if(ScreenHeight==2208 || ScreenWidth==2208){
                return 3;
            }
            if(ScreenHeight==1024 || ScreenWidth==1024){
                return 1; 
            }
            if(ScreenHeight==2048 || ScreenWidth==2048){
                return 2; 
            }
            if(ScreenHeight==2732 || ScreenWidth==2732){
                return 2; 
            }
            if(ScreenHeight==2436 || ScreenWidth==2436){  // iPhone X
                return 3; 
            }
            if(ScreenHeight==2688 || ScreenWidth==2688){  // iPhone XS Max
                return 3; 
            }
        #else
            if(Screen.height==960 || Screen.width==960){
                return 2; 
            }
            if(Screen.height==1136 || Screen.width==1136){
                return 2; 
            }
            if(Screen.height==1334 || Screen.width==1334){
                return 2; 
            }
            if(Screen.height==1920 || Screen.width==1920){
                return 3;
            }
            if(Screen.height==1024 || Screen.width==1024){
                return 1; 
            }
            if(Screen.height==2048 || Screen.width==2048){
                return 2; 
            }
            if(Screen.height==2732 || Screen.width==2732){
                return 2; 
            }
            if(Screen.height==2436 || Screen.width==2436){  // iPhone X
                return 3; 
            }
            if(Screen.height==2688 || Screen.width==2688){  // iPhone XS Max
                return 3; 
            }
        #endif
        return 2;
    }

    // Set retinaMultipler on load
    private void Awake() {
        #if !UNITY_EDITOR
            lastretinaMultipler = retinaMultipler;
            retinaMultipler = GetRetinaMultiplier();
        #endif
    }

    // Update is called once per frame
    void Update () {
		CanvasScaler canvasScaler = GetComponent<CanvasScaler>();
        #if UNITY_EDITOR
			SetScreenWidthAndHeightFromEditorGameViewViaReflection();
			//Debug.Log(ScreenHeight + "x" + ScreenWidth);
			canvasScaler.referenceResolution = new Vector2(ScreenWidth, ScreenHeight);
        #else
			if(useDeviceScreenResolution){
				canvasScaler.referenceResolution = new Vector2(Screen.width, Screen.height);						
			} else {
				canvasScaler.referenceResolution = new Vector2(defaultScreenWidth, defaultScreenHeight);				
			}
        #endif
		
		lastretinaMultipler = retinaMultipler;
        retinaMultipler = GetRetinaMultiplier();

        //Debug.Log("retineMultipler=" + retinaMultipler + " lastretinaMultipler=" + lastretinaMultipler);
    }
	
#if UNITY_EDITOR
	void SetScreenWidthAndHeightFromEditorGameViewViaReflection()
	{
		//Taking game view using the method shown below    
		var gameView = GetMainGameView();
		var prop = gameView.GetType().GetProperty("currentGameViewSize", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
		var gvsize = prop.GetValue(gameView, new object[0]{});
		var gvSizeType = gvsize.GetType();
		
		//I have 2 instance variable which this function sets:
		ScreenHeight = (int)gvSizeType.GetProperty("height", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance).GetValue(gvsize, new object[0]{});
		ScreenWidth = (int)gvSizeType.GetProperty("width", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance).GetValue(gvsize, new object[0]{});
	}
	
	UnityEditor.EditorWindow GetMainGameView()
	{
		System.Type T = System.Type.GetType("UnityEditor.GameView,UnityEditor");
		System.Reflection.MethodInfo GetMainGameView = T.GetMethod("GetMainGameView",System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static);
		System.Object Res = GetMainGameView.Invoke(null,null);
		return (UnityEditor.EditorWindow)Res;
	}
#endif
}
