﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

#if UNITY_EDITOR		
using UnityEditor;
#endif

[ExecuteInEditMode]
public class iOS_TabBar : MonoBehaviour {

	public int tabsIndex = 0;
	public int selectedTabIndex = 0;
	public Color32 selectedTabColor;
	public Color32 deselectedTabColor;
	
	public string icon1Text = "Icon1";	
	public Sprite icon1SelectedImage;
	public Sprite icon1DeselectedImage;
	
	public string icon2Text = "Icon2";	
	public Sprite icon2SelectedImage;
	public Sprite icon2DeselectedImage;
	
	public string icon3Text = "Icon3";	
	public Sprite icon3SelectedImage;
	public Sprite icon3DeselectedImage;
	
	public string icon4Text = "Icon4";	
	public Sprite icon4SelectedImage;
	public Sprite icon4DeselectedImage;
	
	public string icon5Text = "Icon5";	
	public Sprite icon5SelectedImage;
	public Sprite icon5DeselectedImage;
	
	private GameObject TabIcon1GO;
	private GameObject TabIcon2GO;
	private GameObject TabIcon3GO;
	private GameObject TabIcon4GO;
	private GameObject TabIcon5GO;
	private GameObject TabPanel1GO;
	private GameObject TabPanel2GO;
	private GameObject TabPanel3GO;
	private GameObject TabPanel4GO;
	private GameObject TabPanel5GO;
	
	private float height;
	private int retinaMultipler;
	
	public void Icon1Touch(){
		selectedTabIndex = 0;
		Debug.Log ("Icon1 Touch " + selectedTabIndex);
	}
	
	public void Icon2Touch(){
		selectedTabIndex = 1;
		Debug.Log ("Icon2 Touch " + selectedTabIndex);
		//tabsIndex = 1;
	}
	
	public void Icon3Touch(){
		selectedTabIndex = 2;
		Debug.Log ("Icon3 Touch " + selectedTabIndex);
	}
	
	public void Icon4Touch(){
		selectedTabIndex = 3;
		Debug.Log ("Icon4 Touch " + selectedTabIndex);
	}
	
	public void Icon5Touch(){
		selectedTabIndex = 4;
		Debug.Log ("Icon5 Touch " + selectedTabIndex);
	}
	
	void Start () {
		retinaMultipler = transform.GetComponentInParent<CanvasHandler>().retinaMultipler;
		
		TabIcon1GO = transform.Find("Icon1").gameObject;
//		TabIcon1GO.SetActive(false);
		
		TabIcon2GO = transform.Find("Icon2").gameObject;
//		TabIcon2GO.SetActive(false);
		
		TabIcon3GO = transform.Find("Icon3").gameObject;
//		TabIcon3GO.SetActive(false);
		
		TabIcon4GO = transform.Find("Icon4").gameObject;
//		TabIcon4GO.SetActive(false);
		
		TabIcon5GO = transform.Find("Icon5").gameObject;
//		TabIcon5GO.SetActive(false);
		
		TabPanel1GO = transform.Find("TabPanel1").gameObject;
//		TabPanel1GO.SetActive(false);
		
		TabPanel2GO = transform.Find("TabPanel2").gameObject;
//		TabPanel2GO.SetActive(false);
		
		TabPanel3GO = transform.Find("TabPanel3").gameObject;
//		TabPanel3GO.SetActive(false);
		
		TabPanel4GO = transform.Find("TabPanel4").gameObject;
//		TabPanel4GO.SetActive(false);
		
		TabPanel5GO = transform.Find("TabPanel5").gameObject;
//		TabPanel5GO.SetActive(false);		
	}
	
	void Update() {
		
		#if UNITY_EDITOR		
		retinaMultipler = transform.GetComponentInParent<CanvasHandler>().retinaMultipler;
		#endif
		
		height = transform.GetComponentInParent<CanvasScaler>().referenceResolution.y;

        #if UNITY_EDITOR
            if (!PlayerSettings.statusBarHidden){
			    height = height - (20 * retinaMultipler);
		    }
        #endif

        GetComponent<RectTransform>().sizeDelta = new Vector2(0, 49 * retinaMultipler);
		
		transform.GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(0, height-(49 * retinaMultipler));
		transform.GetChild(0).GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 49 * retinaMultipler);
		
		transform.GetChild(1).GetComponent<RectTransform>().sizeDelta = new Vector2(0, height-(49 * retinaMultipler));
		transform.GetChild(1).GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 49 * retinaMultipler);
		
		transform.GetChild(2).GetComponent<RectTransform>().sizeDelta = new Vector2(0, height-(49 * retinaMultipler));
		transform.GetChild(2).GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 49 * retinaMultipler);
		
		transform.GetChild(3).GetComponent<RectTransform>().sizeDelta = new Vector2(0, height-(49 * retinaMultipler));
		transform.GetChild(3).GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 49 * retinaMultipler);
		
		transform.GetChild(4).GetComponent<RectTransform>().sizeDelta = new Vector2(0, height-(49 * retinaMultipler));
		transform.GetChild(4).GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 49 * retinaMultipler);
		
		transform.GetChild(5).GetComponent<RectTransform>().sizeDelta = new Vector2((29 * retinaMultipler),(29 * retinaMultipler));
		transform.GetChild(5).GetComponent<RectTransform>().anchoredPosition = new Vector2((-128 * retinaMultipler), 16 * retinaMultipler);
		transform.GetChild(5).GetChild(0).GetComponent<RectTransform>().anchoredPosition = new Vector2(0, (-22 * retinaMultipler));
		transform.GetChild(5).GetChild(0).GetComponent<Text>().fontSize = 10 * retinaMultipler;
		
		transform.GetChild(6).GetComponent<RectTransform>().sizeDelta = new Vector2((29 * retinaMultipler),(29 * retinaMultipler));
		transform.GetChild(6).GetComponent<RectTransform>().anchoredPosition = new Vector2((-64 * retinaMultipler), 16 * retinaMultipler);
		transform.GetChild(6).GetChild(0).GetComponent<RectTransform>().anchoredPosition = new Vector2(0, (-22 * retinaMultipler));
		transform.GetChild(6).GetChild(0).GetComponent<Text>().fontSize = 10 * retinaMultipler;
		
		transform.GetChild(7).GetComponent<RectTransform>().sizeDelta = new Vector2((29 * retinaMultipler),(29 * retinaMultipler));
		transform.GetChild(7).GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 16 * retinaMultipler);
		transform.GetChild(7).GetChild(0).GetComponent<RectTransform>().anchoredPosition = new Vector2(0, (-22 * retinaMultipler));
		transform.GetChild(7).GetChild(0).GetComponent<Text>().fontSize = 10 * retinaMultipler;
		
		transform.GetChild(8).GetComponent<RectTransform>().sizeDelta = new Vector2((29 * retinaMultipler),(29 * retinaMultipler));
		transform.GetChild(8).GetComponent<RectTransform>().anchoredPosition = new Vector2((64 * retinaMultipler), 16 * retinaMultipler);
		transform.GetChild(8).GetChild(0).GetComponent<RectTransform>().anchoredPosition = new Vector2(0, (-22 * retinaMultipler));
		transform.GetChild(8).GetChild(0).GetComponent<Text>().fontSize = 10 * retinaMultipler;
		
		transform.GetChild(9).GetComponent<RectTransform>().sizeDelta = new Vector2((29 * retinaMultipler),(29 * retinaMultipler));
		transform.GetChild(9).GetComponent<RectTransform>().anchoredPosition = new Vector2((128 * retinaMultipler), 16 * retinaMultipler);
		transform.GetChild(9).GetChild(0).GetComponent<RectTransform>().anchoredPosition = new Vector2(0, (-22 * retinaMultipler));
		transform.GetChild(9).GetChild(0).GetComponent<Text>().fontSize = 10 * retinaMultipler;
		
		// Keep Tab Bar in place!
		GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
		GetComponent<RectTransform>().sizeDelta = new Vector2(0f, GetComponent<RectTransform>().sizeDelta.y);
		GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);					
		
		
		//Sets selected tab
//		Debug.Log ("selectedTabIndex=" + selectedTabIndex);
		
		if(selectedTabIndex == 0){
			TabPanel1GO.SetActive(true);
			if(TabIcon1GO.activeSelf==true){	
				TabIcon1GO.GetComponentInChildren<Text>().color = selectedTabColor;			
				TabIcon1GO.GetComponent<Image>().color = selectedTabColor;
			}
			TabPanel2GO.SetActive(false);
			if(TabIcon2GO.activeSelf==true){	
				TabIcon2GO.GetComponentInChildren<Text>().color = deselectedTabColor;			
				TabIcon2GO.GetComponent<Image>().color = deselectedTabColor;
			}
			TabPanel3GO.SetActive(false);
			if(TabIcon3GO.activeSelf==true){	
				TabIcon3GO.GetComponentInChildren<Text>().color = deselectedTabColor;			
				TabIcon3GO.GetComponent<Image>().color = deselectedTabColor;
			}
			TabPanel4GO.SetActive(false);
			if(TabIcon4GO.activeSelf==true){	
				TabIcon4GO.GetComponentInChildren<Text>().color = deselectedTabColor;			
				TabIcon4GO.GetComponent<Image>().color = deselectedTabColor;
			}
			TabPanel5GO.SetActive(false);
			if(TabIcon5GO.activeSelf==true){	
				TabIcon5GO.GetComponentInChildren<Text>().color = deselectedTabColor;			
				TabIcon5GO.GetComponent<Image>().color = deselectedTabColor;
			}
		}
		if(selectedTabIndex == 1){
			TabPanel1GO.SetActive(false);
			if(TabIcon1GO.activeSelf==true){	
				TabIcon1GO.GetComponentInChildren<Text>().color = deselectedTabColor;			
				TabIcon1GO.GetComponent<Image>().color = deselectedTabColor;
			}
			TabPanel2GO.SetActive(true);
			if(TabIcon2GO.activeSelf==true){	
				TabIcon2GO.GetComponentInChildren<Text>().color = selectedTabColor;			
				TabIcon2GO.GetComponent<Image>().color = selectedTabColor;
			}
			TabPanel3GO.SetActive(false);
			if(TabIcon3GO.activeSelf==true){	
				TabIcon3GO.GetComponentInChildren<Text>().color = deselectedTabColor;			
				TabIcon3GO.GetComponent<Image>().color = deselectedTabColor;
			}
			TabPanel4GO.SetActive(false);
			if(TabIcon4GO.activeSelf==true){	
				TabIcon4GO.GetComponentInChildren<Text>().color = deselectedTabColor;			
				TabIcon4GO.GetComponent<Image>().color = deselectedTabColor;
			}
			TabPanel5GO.SetActive(false);	
			if(TabIcon5GO.activeSelf==true){	
				TabIcon5GO.GetComponentInChildren<Text>().color = deselectedTabColor;			
				TabIcon5GO.GetComponent<Image>().color = deselectedTabColor;
			}
		}
		if(selectedTabIndex == 2){
			TabPanel1GO.SetActive(false);
			if(TabIcon1GO.activeSelf==true){	
				TabIcon1GO.GetComponentInChildren<Text>().color = deselectedTabColor;			
				TabIcon1GO.GetComponent<Image>().color = deselectedTabColor;
			}
			TabPanel2GO.SetActive(false);
			if(TabIcon2GO.activeSelf==true){	
				TabIcon2GO.GetComponentInChildren<Text>().color = deselectedTabColor;			
				TabIcon2GO.GetComponent<Image>().color = deselectedTabColor;
			}
			TabPanel3GO.SetActive(true);
			if(TabIcon3GO.activeSelf==true){	
				TabIcon3GO.GetComponentInChildren<Text>().color = selectedTabColor;			
				TabIcon3GO.GetComponent<Image>().color = selectedTabColor;
			}
			TabPanel4GO.SetActive(false);
			if(TabIcon4GO.activeSelf==true){	
				TabIcon4GO.GetComponentInChildren<Text>().color = deselectedTabColor;			
				TabIcon4GO.GetComponent<Image>().color = deselectedTabColor;
			}
			TabPanel5GO.SetActive(false);	
			if(TabIcon5GO.activeSelf==true){	
				TabIcon5GO.GetComponentInChildren<Text>().color = deselectedTabColor;			
				TabIcon5GO.GetComponent<Image>().color = deselectedTabColor;
			}
		}
		if(selectedTabIndex == 3){
			TabPanel1GO.SetActive(false);
			if(TabIcon1GO.activeSelf==true){	
				TabIcon1GO.GetComponentInChildren<Text>().color = deselectedTabColor;			
				TabIcon1GO.GetComponent<Image>().color = deselectedTabColor;
			}
			TabPanel2GO.SetActive(false);
			if(TabIcon2GO.activeSelf==true){	
				TabIcon2GO.GetComponentInChildren<Text>().color = deselectedTabColor;			
				TabIcon2GO.GetComponent<Image>().color = deselectedTabColor;
			}
			TabPanel3GO.SetActive(false);
			if(TabIcon3GO.activeSelf==true){	
				TabIcon3GO.GetComponentInChildren<Text>().color = deselectedTabColor;			
				TabIcon3GO.GetComponent<Image>().color = deselectedTabColor;
			}
			TabPanel4GO.SetActive(true);
			if(TabIcon4GO.activeSelf==true){	
				TabIcon4GO.GetComponentInChildren<Text>().color = selectedTabColor;			
				TabIcon4GO.GetComponent<Image>().color = selectedTabColor;
			}
			TabPanel5GO.SetActive(false);	
			if(TabIcon5GO.activeSelf==true){	
				TabIcon5GO.GetComponentInChildren<Text>().color = deselectedTabColor;			
				TabIcon5GO.GetComponent<Image>().color = deselectedTabColor;
			}
		}
		if(selectedTabIndex == 4){
			TabPanel1GO.SetActive(false);
			if(TabIcon1GO.activeSelf==true){	
				TabIcon1GO.GetComponentInChildren<Text>().color = deselectedTabColor;			
				TabIcon1GO.GetComponent<Image>().color = deselectedTabColor;
			}
			TabPanel2GO.SetActive(false);
			if(TabIcon1GO.activeSelf==true){	
				TabIcon2GO.GetComponentInChildren<Text>().color = deselectedTabColor;			
				TabIcon2GO.GetComponent<Image>().color = deselectedTabColor;
			}
			TabPanel3GO.SetActive(false);
			if(TabIcon3GO.activeSelf==true){	
				TabIcon3GO.GetComponentInChildren<Text>().color = deselectedTabColor;			
				TabIcon3GO.GetComponent<Image>().color = deselectedTabColor;
			}
			TabPanel4GO.SetActive(false);
			if(TabIcon4GO.activeSelf==true){	
				TabIcon4GO.GetComponentInChildren<Text>().color = deselectedTabColor;			
				TabIcon4GO.GetComponent<Image>().color = deselectedTabColor;
			}
			TabPanel5GO.SetActive(true);	
			if(TabIcon5GO.activeSelf==true){	
				TabIcon5GO.GetComponentInChildren<Text>().color = selectedTabColor;			
				TabIcon5GO.GetComponent<Image>().color = selectedTabColor;
			}
			
		}
		
	}
	
}
