﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using UnityEngine.UI;

[CustomEditor(typeof(iOS_TableViewItem))]
public class iOS_TableViewItemEditor : Editor 
{
	
//	string[] tabs =  new[] { "1", "2", "3", "4", "5" };
	private CanvasHandler canvasHandler;
	
	[MenuItem("GameObject/UI/iOS GUI/Table View Item")]
	private static void iOSGUITableViewItem(){
		//Debug.Log ("Table View Item menu item!");
		
		//Check if selected object is a TableView		
		if(Selection.activeGameObject.tag == "TableView" || Selection.activeGameObject.tag == "TableViewContainer" || Selection.activeGameObject.tag == "TableViewItem"){
			//OK its a Tableview		
			GameObject go;
			go = Instantiate(Resources.Load("Prefabs/TableViewItem")) as GameObject;
			Undo.RegisterCreatedObjectUndo (go, "Create Table View Item");
			
			if(Selection.activeGameObject.tag == "TableView"){
				go.transform.SetParent(Selection.activeGameObject.transform.Find("TableViewContainer").transform, true);
			}
			if(Selection.activeGameObject.tag == "TableViewContainer"){
				go.transform.SetParent(Selection.activeGameObject.transform, true);
			}
			if(Selection.activeGameObject.tag == "TableViewItem"){
				go.transform.SetParent(Selection.activeGameObject.transform.parent, true);
			}
			
			
			go.name = "TableViewItem" + go.transform.parent.childCount;
			
			//Debug.Log (go.transform.parent.childCount);
			
			GameObject lastChild=null;
			
			if(go.transform.parent.childCount>1){
				lastChild = go.transform.parent.GetChild(go.transform.parent.childCount-2).gameObject;
			}		
				
			CanvasHandler canvasHandler = go.GetComponentInParent(typeof(CanvasHandler)) as CanvasHandler;
					
			if(go.transform.parent.childCount==1){
				go.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);		
			} else {
				//Debug.Log(go.transform.parent.GetChild(go.transform.parent.childCount-2).name + " " + go.transform.parent.GetChild(go.transform.parent.childCount-2).GetComponent<RectTransform>().anchoredPosition.y);
				go.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, go.transform.parent.GetChild(go.transform.parent.childCount-2).GetComponent<RectTransform>().anchoredPosition.y + (-43 * canvasHandler.retinaMultipler));
			}
			go.GetComponent<RectTransform>().sizeDelta = new Vector2(go.GetComponent<RectTransform>().rect.width, 43 * canvasHandler.retinaMultipler);
			
			go.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);

			//Select new added object in hierarchy
			Selection.activeGameObject = go;
			
		} else {
			EditorUtility.DisplayDialog("Table View Item cannot be added to current selection!", "You need to select a TableView.", "OK");
		}						
		
	}

	public override void OnInspectorGUI ()
	{
	
		var iOSTableViewItem = target as iOS_TableViewItem;				
		Undo.RecordObject (iOSTableViewItem, "Inspector");
		
		iOSTableViewItem.showHeaderLine = EditorGUILayout.Toggle("Show Header Line", iOSTableViewItem.showHeaderLine);
		EditorGUILayout.Separator();
		
		iOSTableViewItem.showLeftImage = EditorGUILayout.Toggle("Show Left Image", iOSTableViewItem.showLeftImage);
		if(iOSTableViewItem.showLeftImage==true)
		{
			iOSTableViewItem.LeftImageColor = EditorGUILayout.ColorField("Left Image Color", iOSTableViewItem.LeftImageColor);
			iOSTableViewItem.LeftImage = EditorGUILayout.ObjectField("Left Image", iOSTableViewItem.LeftImage, typeof(Sprite),true) as Sprite;
			//	iOSTableViewItem.ConnectedPanel = EditorGUILayout.ObjectField("Connected Panel", iOSTableViewItem.ConnectedPanel, typeof(GameObject),true) as GameObject;
		}
		EditorGUILayout.Separator();
				
		iOSTableViewItem.showTitleText = EditorGUILayout.Toggle("Show Title Text", iOSTableViewItem.showTitleText);
		if(iOSTableViewItem.showTitleText==true)
		{
			iOSTableViewItem.TitleText = EditorGUILayout.TextField("Title Text", iOSTableViewItem.TitleText);
			iOSTableViewItem.TitleTextColor = EditorGUILayout.ColorField("Title Text Color", iOSTableViewItem.TitleTextColor);
		}
		EditorGUILayout.Separator();
				
		iOSTableViewItem.BackgroundColor = EditorGUILayout.ColorField("Background Color", iOSTableViewItem.BackgroundColor);
		//Debug.Log (iOSTableViewItem.BackgroundColor.a);
		//iOSTableViewItem.BackgroundOpacity = iOSTableViewItem.GetComponent<Image>().color.a*255;
		//iOSTableViewItem.BackgroundOpacity = iOSTableViewItem.BackgroundColor.a*255;
		iOSTableViewItem.BackgroundColor.a = iOSTableViewItem.BackgroundOpacity/255;
		iOSTableViewItem.BackgroundOpacity = EditorGUILayout.Slider("Background Opacity", iOSTableViewItem.BackgroundOpacity, 0, 255);
		iOSTableViewItem.ConnectedPanel = EditorGUILayout.ObjectField("Connected Panel", iOSTableViewItem.ConnectedPanel, typeof(GameObject),true) as GameObject;
		EditorGUILayout.Separator();
		
		iOSTableViewItem.showRightText = EditorGUILayout.Toggle("Show Right Text", iOSTableViewItem.showRightText);
		if(iOSTableViewItem.showRightText==true)
		{
			iOSTableViewItem.ConnectedSwitch = EditorGUILayout.ObjectField("Connected Switch", iOSTableViewItem.ConnectedSwitch, typeof(iOS_Switch),true) as iOS_Switch;					
			iOSTableViewItem.RightText = EditorGUILayout.TextField("Right Text", iOSTableViewItem.RightText);
			iOSTableViewItem.RightTextColor = EditorGUILayout.ColorField("Right Text Color", iOSTableViewItem.RightTextColor);
		}
		EditorGUILayout.Separator();

		iOSTableViewItem.showRightImage = EditorGUILayout.Toggle("Show Right Image", iOSTableViewItem.showRightImage);
		if(iOSTableViewItem.showRightImage==true)
		{
			iOSTableViewItem.RightImageColor = EditorGUILayout.ColorField("Right Image Color", iOSTableViewItem.RightImageColor);
			iOSTableViewItem.RightImage = EditorGUILayout.ObjectField("Right Image", iOSTableViewItem.RightImage, typeof(Sprite),true) as Sprite;
		}
		EditorGUILayout.Separator();

		iOSTableViewItem.showFooterLine = EditorGUILayout.Toggle("Show Footer Line", iOSTableViewItem.showFooterLine);
		if(iOSTableViewItem.showFooterLine){
			iOSTableViewItem.FooterLineLength = EditorGUILayout.Toggle("Footer Line 3/4 Length", iOSTableViewItem.FooterLineLength);
		}
		EditorGUILayout.Separator();

		//Add slider for footer line length...
		

		GameObject HeaderLine = iOSTableViewItem.transform.Find("HeaderLine").gameObject;
		HeaderLine.SetActive(iOSTableViewItem.showHeaderLine);

		GameObject LeftImage = iOSTableViewItem.transform.Find("LeftImage").gameObject;
		LeftImage.SetActive(iOSTableViewItem.showLeftImage);
		LeftImage.GetComponent<Image>().color = iOSTableViewItem.LeftImageColor;
		LeftImage.GetComponent<Image>().sprite = iOSTableViewItem.LeftImage;
		
		
		GameObject TitleTextGO = iOSTableViewItem.transform.Find("TitleText").gameObject;
		TitleTextGO.SetActive(iOSTableViewItem.showTitleText);
		Text TitleText = TitleTextGO.GetComponent<Text>();
		TitleText.text = iOSTableViewItem.TitleText;
		TitleText.color = iOSTableViewItem.TitleTextColor;
				
		GameObject RightTextGO = iOSTableViewItem.transform.Find("RightText").gameObject;
		RightTextGO.SetActive(iOSTableViewItem.showRightText);
		Text RightText = RightTextGO.GetComponent<Text>();
		if(iOSTableViewItem.ConnectedSwitch != null){
			if(iOSTableViewItem.ConnectedSwitch.GetComponent<Toggle>().isOn){
				iOSTableViewItem.RightText = iOSTableViewItem.ConnectedSwitch.OnStateText;
			} else {
				iOSTableViewItem.RightText = iOSTableViewItem.ConnectedSwitch.OffStateText;
			}
		}
		RightText.color = iOSTableViewItem.RightTextColor;
		RightText.text = iOSTableViewItem.RightText;

		iOSTableViewItem.GetComponent<Image>().color = new Color(iOSTableViewItem.BackgroundColor.r, iOSTableViewItem.BackgroundColor.g, iOSTableViewItem.BackgroundColor.b, iOSTableViewItem.BackgroundOpacity/255);

		GameObject RightImage = iOSTableViewItem.transform.Find("RightImage").gameObject;
		RightImage.SetActive(iOSTableViewItem.showRightImage);
		RightImage.GetComponent<Image>().color = iOSTableViewItem.RightImageColor;		
		RightImage.GetComponent<Image>().sprite = iOSTableViewItem.RightImage;

		EditorUtility.SetDirty(target);
		
	}
	
}