﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using UnityEngine.UI;

[CustomEditor(typeof(iOS_SegmentedControl))]
public class iOS_SegmentedControlEditor : Editor 
{
	
	string[] segments =  new[] { "2", "3", "4" };
	string[] numSegments =  new[] { "1", "2", "3", "4" };
	
	private float segmentedControlWidth;
	
	[MenuItem("GameObject/UI/iOS GUI/Segmented Control")]
	private static void iOSGUISegmentedControl(){
		//Debug.Log ("Segmented Control menu item!");
		//Check if Canvas exists?
		Canvas canvas = FindObjectOfType(typeof(Canvas)) as Canvas;
		if (canvas){
			//Debug.Log("Canvas object found: " + canvas.name);
			canvas.pixelPerfect = true;
			canvas.GetComponent<CanvasScaler>().uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
			if(canvas.GetComponent<CanvasHandler>() == null){
				canvas.gameObject.AddComponent<CanvasHandler>();
			}
		} else {
			//Debug.Log("No Canvas object could be found");
			EditorApplication.ExecuteMenuItem("GameObject/UI/Canvas");
			canvas = FindObjectOfType(typeof(Canvas)) as Canvas;
			canvas.pixelPerfect = true;
			canvas.GetComponent<CanvasScaler>().uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
			if(canvas.GetComponent<CanvasHandler>() == null){
				canvas.gameObject.AddComponent<CanvasHandler>();
			}
		}	
		
		//Check if Panel exists?
		iOS_UIPanel panel = FindObjectOfType(typeof(iOS_UIPanel)) as iOS_UIPanel;
		if (panel){ //Panel already exists, do nothing...
			//Debug.Log("Panel object found: " + panel.name);
			if(Selection.activeGameObject == null){ //if no object selected, make panel found the selected object
				Selection.activeGameObject = panel.gameObject;
			}
		} else { //Create Panel
			//Debug.Log("No Panel object could be found");
			EditorApplication.ExecuteMenuItem("GameObject/UI/iOS GUI/UIPanel");
			panel = FindObjectOfType(typeof(iOS_UIPanel)) as iOS_UIPanel;
			Selection.activeGameObject = panel.gameObject;
		}
		
		//Check if selected object is a Panel		
		bool bComponentExists=false;
		if(Selection.activeGameObject.tag == "rootUIPanel" || Selection.activeGameObject.tag == "UIPanel" || Selection.activeGameObject.tag == "UITabPanel"){
			//OK its a panel, now check if Segmented Control already on panel		
			foreach (Transform child in Selection.activeGameObject.transform)
			{
				if(child.tag == "SegmentedControl"){
					bComponentExists=true;
				}
			}			
			if(bComponentExists){
				EditorUtility.DisplayDialog("Cannot add Segmented Control!", "A Segmented Control already exists on the selected UIPanel.", "OK");
			}
						
			if(!bComponentExists){
				//OK its a panel, now check if Tab Bar already on panel		
				foreach (Transform child in Selection.activeGameObject.transform)
				{
					if(child.tag == "TabBar"){
						bComponentExists=true;
					}
				}			
				if(bComponentExists){
					EditorUtility.DisplayDialog("Cannot add Segmented Control!", "A Tab Bar exists on the selected UIPanel.\n\nInstead add a Segmented Control to one of the Tab Bar panels.", "OK");
				}
			}
			
			if(!bComponentExists){
				foreach (Transform child in Selection.activeGameObject.transform)
				{
					if(child.tag == "TableView"){
						bComponentExists=true;
					}
				}	
				if(bComponentExists){
					EditorUtility.DisplayDialog("Cannot add Segmented Control!", "A Table View exists on the selected UIPanel.\n\nRemove the Table View and add a Segmented Control, then add a Table View to one of the Segmented Control panels.", "OK");
				}
			}		

			if(!bComponentExists){
				GameObject go;
				go = Instantiate(Resources.Load("Prefabs/Segmented Control")) as GameObject;
				Undo.RegisterCreatedObjectUndo (go, "Create Segmented Control");
				
				if(Selection.activeGameObject != null){
					if(Selection.activeGameObject.tag == "rootUIPanel" || Selection.activeGameObject.tag == "UIPanel" || Selection.activeGameObject.tag == "UITabPanel"){
						go.transform.parent = Selection.activeGameObject.transform;
					} else {
						go.transform.parent = panel.transform;
					}
				} else {
					go.transform.parent = panel.transform;
				}
				
				go.name = "Segmented Control";
				
				//				go.GetComponent<RectTransform>().localPosition = new Vector3(0f, (go.transform.parent.GetComponent<RectTransform>().rect.height/2) * -1, 0f);	
				go.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
				go.GetComponent<RectTransform>().sizeDelta = new Vector2(0f,go.GetComponent<RectTransform>().rect.height);
				go.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);	
				
				//Select new added object in hierarchy
				Selection.activeGameObject = go;
				
//				EditorUtility.DisplayDialog("Information!", "Move the newly added Tab Bar to the top of the hierarchy within the UIPanel.", "OK");
			}
		} else {
			EditorUtility.DisplayDialog("Segmented Control cannot be added to current selection!", "You need to select a UIPanel.", "OK");
		}						
					
	}

	public override void OnInspectorGUI ()
	{
	
		var iOSSegmentedControl = target as iOS_SegmentedControl;
		Undo.RecordObject (iOSSegmentedControl, "Inspector");
		
		//Debug.Log (iOSSegmentedControl.selectedTabIndex);
		
		iOSSegmentedControl.segmentsIndex = EditorGUILayout.Popup("Number of Segments", iOSSegmentedControl.segmentsIndex, segments);
		iOSSegmentedControl.selectedSegmentIndex = EditorGUILayout.Popup("Selected Segment", iOSSegmentedControl.selectedSegmentIndex, numSegments);
		if(iOSSegmentedControl.selectedSegmentIndex > iOSSegmentedControl.segmentsIndex+1){
			iOSSegmentedControl.selectedSegmentIndex = iOSSegmentedControl.segmentsIndex+1;
		}
		iOSSegmentedControl.SegmentsColor = EditorGUILayout.ColorField("Segments Color", iOSSegmentedControl.SegmentsColor);
		iOSSegmentedControl.BackgroundColor = EditorGUILayout.ColorField("Background Color", iOSSegmentedControl.BackgroundColor);
		EditorGUILayout.Separator();
		
		GameObject Segment1GO = iOSSegmentedControl.transform.Find("Segment1").gameObject;
		Segment1GO.SetActive(false);
		
		GameObject Segment2GO = iOSSegmentedControl.transform.Find("Segment2").gameObject;
		Segment2GO.SetActive(false);
		
		GameObject Segment3GO = iOSSegmentedControl.transform.Find("Segment3").gameObject;
		Segment3GO.SetActive(false);
		
		GameObject Segment4GO = iOSSegmentedControl.transform.Find("Segment4").gameObject;
		Segment4GO.SetActive(false);
		
		//Debug.Log (iOSSegmentedControl.transform.GetComponent<RectTransform>().sizeDelta.x); 

		iOSSegmentedControl.transform.Find("Segment1/Background").transform.GetComponent<Image>().color = iOSSegmentedControl.SegmentsColor;
		iOSSegmentedControl.transform.Find("Segment1/Border").transform.GetComponent<Image>().color = iOSSegmentedControl.SegmentsColor;
		iOSSegmentedControl.transform.Find("Segment1/Text").transform.GetComponent<Text>().color = iOSSegmentedControl.SegmentsColor;
		iOSSegmentedControl.transform.Find("Segment2/Background").transform.GetComponent<Image>().color = iOSSegmentedControl.SegmentsColor;
		iOSSegmentedControl.transform.Find("Segment2/Border").transform.GetComponent<Image>().color = iOSSegmentedControl.SegmentsColor;
		iOSSegmentedControl.transform.Find("Segment2/Text").transform.GetComponent<Text>().color = iOSSegmentedControl.SegmentsColor;
		iOSSegmentedControl.transform.Find("Segment3/Background").transform.GetComponent<Image>().color = iOSSegmentedControl.SegmentsColor;
		iOSSegmentedControl.transform.Find("Segment3/Border").transform.GetComponent<Image>().color = iOSSegmentedControl.SegmentsColor;
		iOSSegmentedControl.transform.Find("Segment3/Text").transform.GetComponent<Text>().color = iOSSegmentedControl.SegmentsColor;
		iOSSegmentedControl.transform.Find("Segment4/Background").transform.GetComponent<Image>().color = iOSSegmentedControl.SegmentsColor;
		iOSSegmentedControl.transform.Find("Segment4/Border").transform.GetComponent<Image>().color = iOSSegmentedControl.SegmentsColor;
		iOSSegmentedControl.transform.Find("Segment4/Text").transform.GetComponent<Text>().color = iOSSegmentedControl.SegmentsColor;
		
		iOSSegmentedControl.transform.Find("Background").transform.GetComponent<Image>().color = iOSSegmentedControl.BackgroundColor;
		
		//Debug.Log (segments[iOSSegmentedControl.segmentsIndex]);
		
		// Calculate orientation of device and set SegmentedControl width
		Canvas canvas = FindObjectOfType(typeof(Canvas)) as Canvas;
		CanvasScaler canvasScaler = canvas.GetComponent<CanvasScaler>();
		
		if(canvasScaler.referenceResolution.x < canvasScaler.referenceResolution.y){ 
			segmentedControlWidth = canvasScaler.referenceResolution.x;
		} else { 
			segmentedControlWidth = canvasScaler.referenceResolution.y;
		}
			
			
		//Defines the number of segments in the TabPanel
		if(segments[iOSSegmentedControl.segmentsIndex] == "2"){
			iOSSegmentedControl.segment1Text = EditorGUILayout.TextField("Segment 1 Text", iOSSegmentedControl.segment1Text);
			iOSSegmentedControl.transform.Find("Segment1/Text").transform.GetComponent<Text>().text = iOSSegmentedControl.segment1Text;
			iOSSegmentedControl.segment2Text = EditorGUILayout.TextField("Segment 2 Text", iOSSegmentedControl.segment2Text);
			iOSSegmentedControl.transform.Find("Segment2/Text").transform.GetComponent<Text>().text = iOSSegmentedControl.segment2Text;
			
			Segment1GO.SetActive(true);
			Segment2GO.SetActive(true);
			Segment3GO.SetActive(false);
			Segment4GO.SetActive(false);
			
			//Calculate and set segment size
			Segment1GO.GetComponent<RectTransform>().sizeDelta = new Vector2((segmentedControlWidth-16)/2, Segment1GO.GetComponent<RectTransform>().sizeDelta.y);
			iOSSegmentedControl.transform.Find("Segment1/Background").transform.GetComponent<RectTransform>().sizeDelta = Segment1GO.GetComponent<RectTransform>().sizeDelta;
			iOSSegmentedControl.transform.Find("Segment1/Border").transform.GetComponent<RectTransform>().sizeDelta = Segment1GO.GetComponent<RectTransform>().sizeDelta;
			iOSSegmentedControl.transform.Find("Segment1/Text").transform.GetComponent<RectTransform>().sizeDelta = Segment1GO.GetComponent<RectTransform>().sizeDelta;
			
			Segment2GO.GetComponent<RectTransform>().sizeDelta = new Vector2((segmentedControlWidth-16)/2, Segment1GO.GetComponent<RectTransform>().sizeDelta.y);
			iOSSegmentedControl.transform.Find("Segment2/Background").transform.GetComponent<RectTransform>().sizeDelta = Segment2GO.GetComponent<RectTransform>().sizeDelta;
			iOSSegmentedControl.transform.Find("Segment2/Border").transform.GetComponent<RectTransform>().sizeDelta = Segment2GO.GetComponent<RectTransform>().sizeDelta;
			iOSSegmentedControl.transform.Find("Segment2/Text").transform.GetComponent<RectTransform>().sizeDelta = Segment2GO.GetComponent<RectTransform>().sizeDelta;
			
			//Position segments	
			Segment1GO.GetComponent<RectTransform>().anchoredPosition = new Vector2((Segment1GO.GetComponent<RectTransform>().sizeDelta.x/2) * -1, -1);
			Segment2GO.GetComponent<RectTransform>().anchoredPosition = new Vector2(Segment2GO.GetComponent<RectTransform>().sizeDelta.x/2, -1);
			
			//Position in center
			iOSSegmentedControl.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
			
			//Change last segment border and background
			iOSSegmentedControl.transform.Find("Segment2/Background").transform.GetComponent<Image>().sprite = iOSSegmentedControl.rightBackground;
			iOSSegmentedControl.transform.Find("Segment2/Border").transform.GetComponent<Image>().sprite = iOSSegmentedControl.rightBorder;
						
		}
		if(segments[iOSSegmentedControl.segmentsIndex] == "3"){
			iOSSegmentedControl.segment1Text = EditorGUILayout.TextField("Segment 1 Text", iOSSegmentedControl.segment1Text);
			iOSSegmentedControl.transform.Find("Segment1/Text").transform.GetComponent<Text>().text = iOSSegmentedControl.segment1Text;
			iOSSegmentedControl.segment2Text = EditorGUILayout.TextField("Segment 2 Text", iOSSegmentedControl.segment2Text);
			iOSSegmentedControl.transform.Find("Segment2/Text").transform.GetComponent<Text>().text = iOSSegmentedControl.segment2Text;
			iOSSegmentedControl.segment3Text = EditorGUILayout.TextField("Segment 3 Text", iOSSegmentedControl.segment3Text);
			iOSSegmentedControl.transform.Find("Segment3/Text").transform.GetComponent<Text>().text = iOSSegmentedControl.segment3Text;
			
			Segment1GO.SetActive(true);
			Segment2GO.SetActive(true);
			Segment3GO.SetActive(true);
			Segment4GO.SetActive(false);

			//Calculate and set segment size
			Segment1GO.GetComponent<RectTransform>().sizeDelta = new Vector2((segmentedControlWidth-16)/3, Segment1GO.GetComponent<RectTransform>().sizeDelta.y);
			iOSSegmentedControl.transform.Find("Segment1/Background").transform.GetComponent<RectTransform>().sizeDelta = Segment1GO.GetComponent<RectTransform>().sizeDelta;
			iOSSegmentedControl.transform.Find("Segment1/Border").transform.GetComponent<RectTransform>().sizeDelta = Segment1GO.GetComponent<RectTransform>().sizeDelta;
			iOSSegmentedControl.transform.Find("Segment1/Text").transform.GetComponent<RectTransform>().sizeDelta = Segment1GO.GetComponent<RectTransform>().sizeDelta;
			
			Segment2GO.GetComponent<RectTransform>().sizeDelta = new Vector2((segmentedControlWidth-16)/3, Segment2GO.GetComponent<RectTransform>().sizeDelta.y);
			iOSSegmentedControl.transform.Find("Segment2/Background").transform.GetComponent<RectTransform>().sizeDelta = Segment2GO.GetComponent<RectTransform>().sizeDelta;
			iOSSegmentedControl.transform.Find("Segment2/Border").transform.GetComponent<RectTransform>().sizeDelta = Segment2GO.GetComponent<RectTransform>().sizeDelta;
			iOSSegmentedControl.transform.Find("Segment2/Text").transform.GetComponent<RectTransform>().sizeDelta = Segment2GO.GetComponent<RectTransform>().sizeDelta;
				
			Segment3GO.GetComponent<RectTransform>().sizeDelta = new Vector2((segmentedControlWidth-16)/3, Segment3GO.GetComponent<RectTransform>().sizeDelta.y);
			iOSSegmentedControl.transform.Find("Segment3/Background").transform.GetComponent<RectTransform>().sizeDelta = Segment3GO.GetComponent<RectTransform>().sizeDelta;
			iOSSegmentedControl.transform.Find("Segment3/Border").transform.GetComponent<RectTransform>().sizeDelta = Segment3GO.GetComponent<RectTransform>().sizeDelta;
			iOSSegmentedControl.transform.Find("Segment3/Text").transform.GetComponent<RectTransform>().sizeDelta = Segment3GO.GetComponent<RectTransform>().sizeDelta;

			//Position segments
			Segment1GO.GetComponent<RectTransform>().anchoredPosition = new Vector2((Segment1GO.GetComponent<RectTransform>().sizeDelta.x) * -1, -1);
			Segment2GO.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -1);
			Segment3GO.GetComponent<RectTransform>().anchoredPosition = new Vector2(Segment3GO.GetComponent<RectTransform>().sizeDelta.x, -1);
			
			//Position in center
			iOSSegmentedControl.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
			
			//Change segment borders and backgrounds
			iOSSegmentedControl.transform.Find("Segment2/Background").transform.GetComponent<Image>().sprite = iOSSegmentedControl.middleBackground;
			iOSSegmentedControl.transform.Find("Segment2/Border").transform.GetComponent<Image>().sprite = iOSSegmentedControl.middleBorder;
			iOSSegmentedControl.transform.Find("Segment3/Background").transform.GetComponent<Image>().sprite = iOSSegmentedControl.rightBackground;
			iOSSegmentedControl.transform.Find("Segment3/Border").transform.GetComponent<Image>().sprite = iOSSegmentedControl.rightBorder;			
			
		}
		if(segments[iOSSegmentedControl.segmentsIndex] == "4"){
			iOSSegmentedControl.segment1Text = EditorGUILayout.TextField("Segment 1 Text", iOSSegmentedControl.segment1Text);
			iOSSegmentedControl.transform.Find("Segment1/Text").transform.GetComponent<Text>().text = iOSSegmentedControl.segment1Text;
			iOSSegmentedControl.segment2Text = EditorGUILayout.TextField("Segment 2 Text", iOSSegmentedControl.segment2Text);
			iOSSegmentedControl.transform.Find("Segment2/Text").transform.GetComponent<Text>().text = iOSSegmentedControl.segment2Text;
			iOSSegmentedControl.segment3Text = EditorGUILayout.TextField("Segment 3 Text", iOSSegmentedControl.segment3Text);
			iOSSegmentedControl.transform.Find("Segment3/Text").transform.GetComponent<Text>().text = iOSSegmentedControl.segment3Text;
			iOSSegmentedControl.segment4Text = EditorGUILayout.TextField("Segment 4 Text", iOSSegmentedControl.segment4Text);
			iOSSegmentedControl.transform.Find("Segment4/Text").transform.GetComponent<Text>().text = iOSSegmentedControl.segment4Text;
			
			Segment1GO.SetActive(true);
			Segment2GO.SetActive(true);
			Segment3GO.SetActive(true);
			Segment4GO.SetActive(true);

			//Calculate and set segment size
			Segment1GO.GetComponent<RectTransform>().sizeDelta = new Vector2((segmentedControlWidth-16)/4, Segment1GO.GetComponent<RectTransform>().sizeDelta.y);
			iOSSegmentedControl.transform.Find("Segment1/Background").transform.GetComponent<RectTransform>().sizeDelta = Segment1GO.GetComponent<RectTransform>().sizeDelta;
			iOSSegmentedControl.transform.Find("Segment1/Border").transform.GetComponent<RectTransform>().sizeDelta = Segment1GO.GetComponent<RectTransform>().sizeDelta;
			iOSSegmentedControl.transform.Find("Segment1/Text").transform.GetComponent<RectTransform>().sizeDelta = Segment1GO.GetComponent<RectTransform>().sizeDelta;
			
			Segment2GO.GetComponent<RectTransform>().sizeDelta = new Vector2((segmentedControlWidth-16)/4, Segment2GO.GetComponent<RectTransform>().sizeDelta.y);
			iOSSegmentedControl.transform.Find("Segment2/Background").transform.GetComponent<RectTransform>().sizeDelta = Segment2GO.GetComponent<RectTransform>().sizeDelta;
			iOSSegmentedControl.transform.Find("Segment2/Border").transform.GetComponent<RectTransform>().sizeDelta = Segment2GO.GetComponent<RectTransform>().sizeDelta;
			iOSSegmentedControl.transform.Find("Segment2/Text").transform.GetComponent<RectTransform>().sizeDelta = Segment2GO.GetComponent<RectTransform>().sizeDelta;
			
			Segment3GO.GetComponent<RectTransform>().sizeDelta = new Vector2((segmentedControlWidth-16)/4, Segment3GO.GetComponent<RectTransform>().sizeDelta.y);
			iOSSegmentedControl.transform.Find("Segment3/Background").transform.GetComponent<RectTransform>().sizeDelta = Segment3GO.GetComponent<RectTransform>().sizeDelta;
			iOSSegmentedControl.transform.Find("Segment3/Border").transform.GetComponent<RectTransform>().sizeDelta = Segment3GO.GetComponent<RectTransform>().sizeDelta;
			iOSSegmentedControl.transform.Find("Segment3/Text").transform.GetComponent<RectTransform>().sizeDelta = Segment3GO.GetComponent<RectTransform>().sizeDelta;
			
			Segment4GO.GetComponent<RectTransform>().sizeDelta = new Vector2((segmentedControlWidth-16)/4, Segment4GO.GetComponent<RectTransform>().sizeDelta.y);
			iOSSegmentedControl.transform.Find("Segment4/Background").transform.GetComponent<RectTransform>().sizeDelta = Segment4GO.GetComponent<RectTransform>().sizeDelta;
			iOSSegmentedControl.transform.Find("Segment4/Border").transform.GetComponent<RectTransform>().sizeDelta = Segment4GO.GetComponent<RectTransform>().sizeDelta;
			iOSSegmentedControl.transform.Find("Segment4/Text").transform.GetComponent<RectTransform>().sizeDelta = Segment4GO.GetComponent<RectTransform>().sizeDelta;

			//Position segments	
			Segment1GO.GetComponent<RectTransform>().anchoredPosition = new Vector2((Segment1GO.GetComponent<RectTransform>().sizeDelta.x*1.5f) * -1, -1);
			Segment2GO.GetComponent<RectTransform>().anchoredPosition = new Vector2((Segment2GO.GetComponent<RectTransform>().sizeDelta.x/2) * -1, -1);
			Segment3GO.GetComponent<RectTransform>().anchoredPosition = new Vector2(Segment3GO.GetComponent<RectTransform>().sizeDelta.x/2, -1);
			Segment4GO.GetComponent<RectTransform>().anchoredPosition = new Vector2(Segment4GO.GetComponent<RectTransform>().sizeDelta.x*1.5f, -1);
			
	
			//Position in center
			iOSSegmentedControl.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
			
			//Change segment borders and backgrounds
			iOSSegmentedControl.transform.Find("Segment2/Background").transform.GetComponent<Image>().sprite = iOSSegmentedControl.middleBackground;
			iOSSegmentedControl.transform.Find("Segment2/Border").transform.GetComponent<Image>().sprite = iOSSegmentedControl.middleBorder;
			iOSSegmentedControl.transform.Find("Segment3/Background").transform.GetComponent<Image>().sprite = iOSSegmentedControl.middleBackground;
			iOSSegmentedControl.transform.Find("Segment3/Border").transform.GetComponent<Image>().sprite = iOSSegmentedControl.middleBorder;
			iOSSegmentedControl.transform.Find("Segment4/Background").transform.GetComponent<Image>().sprite = iOSSegmentedControl.rightBackground;
			iOSSegmentedControl.transform.Find("Segment4/Border").transform.GetComponent<Image>().sprite = iOSSegmentedControl.rightBorder;
			
		}

		EditorUtility.SetDirty(target);
		
	}
	
	
}