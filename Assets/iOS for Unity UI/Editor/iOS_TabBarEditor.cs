﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using UnityEngine.UI;

[CustomEditor(typeof(iOS_TabBar))]
public class iOS_TabBarEditor : Editor 
{
	
	string[] tabs =  new[] { "1", "2", "3", "4", "5" };
	
	[MenuItem("GameObject/UI/iOS GUI/Tab Bar")]
	private static void iOSGUITabBar(){
		//Debug.Log ("Tab Bar menu item!");
		//Check if Canvas exists?
		Canvas canvas = FindObjectOfType(typeof(Canvas)) as Canvas;
		if (canvas){ //Canvas already exists, do nothing...
			//Debug.Log("Canvas object found: " + canvas.name);
			canvas.pixelPerfect = true;
			canvas.GetComponent<CanvasScaler>().uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
			if(canvas.GetComponent<CanvasHandler>() == null){
				canvas.gameObject.AddComponent<CanvasHandler>();
			}
		} else { //Create Canvas
			//Debug.Log("No Canvas object could be found");
			EditorApplication.ExecuteMenuItem("GameObject/UI/Canvas");
			canvas = FindObjectOfType(typeof(Canvas)) as Canvas;
			canvas.pixelPerfect = true;
			canvas.GetComponent<CanvasScaler>().uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
			if(canvas.GetComponent<CanvasHandler>() == null){
				canvas.gameObject.AddComponent<CanvasHandler>();
			}
			
		}	
		
		//Check if Panel exists?
		iOS_UIPanel panel = FindObjectOfType(typeof(iOS_UIPanel)) as iOS_UIPanel;
		if (panel){ //Panel already exists, do nothing...
			//Debug.Log("Panel object found: " + panel.name);
			if(Selection.activeGameObject == null){ //if no object selected, make panel found the selected object
				Selection.activeGameObject = panel.gameObject;
			}
		} else { //Create Panel
			//Debug.Log("No Panel object could be found");
			EditorApplication.ExecuteMenuItem("GameObject/UI/iOS GUI/UIPanel");
			panel = FindObjectOfType(typeof(iOS_UIPanel)) as iOS_UIPanel;
			Selection.activeGameObject = panel.gameObject;
		}
		
		//Check if selected object is a Panel		
		bool bComponentExists=false;
		if(Selection.activeGameObject.tag == "rootUIPanel" || Selection.activeGameObject.tag == "UIPanel"){
			//OK its a panel, now check if Tab Bar already on panel		
			foreach (Transform child in Selection.activeGameObject.transform)
			{
				if(child.tag == "TabBar"){
					bComponentExists=true;
				}
			}			
			if(bComponentExists){
				EditorUtility.DisplayDialog("Cannot add Tab Bar!", "A Tab Bar already exists on the selected UIPanel.", "OK");
				//Debug.Log ("<color=red>WARNING:</color> Cannot add Tab Bar, a Tab Bar already exists on the selected UIPanel.");
			}
			
			if(!bComponentExists){
				foreach (Transform child in Selection.activeGameObject.transform)
				{
					if(child.tag == "NavigationBar"){
						bComponentExists=true;
					}
				}	
				if(bComponentExists){
					EditorUtility.DisplayDialog("Cannot add Tab Bar!", "A Navigation Bar exists on the selected UIPanel.\n\nRemove the Navigation Bar and add a Tab Bar, then re-add a Navigation Bar to one of the Tab Bar panels.", "OK");
					//Debug.Log ("<color=red>WARNING:</color> Cannot add Tab Bar, a Navigation Bar exists on the selected UIPanel. Remove the Navigation Bar and add a Tab Bar, then add a Navigation Bar to one of the Tab Bar panels.");
				}
			}		
			
			if(!bComponentExists){
				foreach (Transform child in Selection.activeGameObject.transform)
				{
					if(child.tag == "ToolBar"){
						bComponentExists=true;
					}
				}	
				if(bComponentExists){
					EditorUtility.DisplayDialog("Cannot add Tab Bar!", "A Tool Bar exists on the selected UIPanel.\n\nRemove the Tool Bar and add a Tab Bar, then re-add a Tool Bar to one of the Tab Bar panels.", "OK");
					//Debug.Log ("<color=red>WARNING:</color> Cannot add Tab Bar, a Navigation Bar exists on the selected UIPanel. Remove the Navigation Bar and add a Tab Bar, then add a Navigation Bar to one of the Tab Bar panels.");
				}
			}	
			
			if(!bComponentExists){
				foreach (Transform child in Selection.activeGameObject.transform)
				{
					if(child.tag == "TableView"){
						bComponentExists=true;
					}
				}	
				if(bComponentExists){
					EditorUtility.DisplayDialog("Cannot add Tab Bar!", "A Table View exists on the selected UIPanel.\n\nRemove the Table View and add a Tab Bar, then re-add a Table View to one of the Tab Bar panels.", "OK");
				}
			}		
			
			if(!bComponentExists){
				foreach (Transform child in Selection.activeGameObject.transform)
				{
					if(child.tag == "SegmentedControl"){
						bComponentExists=true;
					}
				}	
				if(bComponentExists){
					EditorUtility.DisplayDialog("Cannot add Tab Bar!", "A Segmented Control exists on the selected UIPanel.\n\nRemove the Segmented Control and add a Tab Bar, then re-add a Segmented Control to one of the Tab Bar panels.", "OK");
				}
			}		
			
			if(!bComponentExists){
				GameObject go;
				go = Instantiate(Resources.Load("Prefabs/Tab Bar")) as GameObject;
				Undo.RegisterCreatedObjectUndo (go, "Create Tab Bar");
				
				if(Selection.activeGameObject != null){
					if(Selection.activeGameObject.tag == "rootUIPanel" || Selection.activeGameObject.tag == "UIPanel"){
						go.transform.parent = Selection.activeGameObject.transform;
					} else {
						go.transform.parent = panel.transform;
					}
				} else {
					go.transform.parent = panel.transform;
				}
				
				go.name = "Tab Bar";
				
				go.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
				go.GetComponent<RectTransform>().sizeDelta = new Vector2(0f,go.GetComponent<RectTransform>().sizeDelta.y);
				go.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);	
				
				// Make sure Popup Message is first object on UIPanel for Z ordering reasons
				//go.transform.SetAsFirstSibling();
				
				//Select new added object in hierarchy
				Selection.activeGameObject = go;
				
			}
		} else {
			if(Selection.activeGameObject.tag == "UITabPanel"){
				EditorUtility.DisplayDialog("Cannot add Tab Bar to a UITabPanel!", "You need to select a UIPanel.", "OK");
				//Debug.Log ("<color=red>WARNING:</color> Cannot add Tab Bar to a UITabPanel, you need to select a UIPanel.");
			} else {
				EditorUtility.DisplayDialog("Tab Bar cannot be added to current selection!", "You need to select a UIPanel.", "OK");
				//Debug.Log ("<color=red>WARNING:</color> Tab Bar cannot be added to current selection, you need to select a UIPanel.");
			}
		}						
		
	}
	
	public override void OnInspectorGUI ()
	{
		
		var iOSTabBar = target as iOS_TabBar;
		Undo.RecordObject (iOSTabBar, "Inspector");
		
		//Debug.Log (iOSTabBar.selectedTabIndex);
		
		iOSTabBar.tabsIndex = EditorGUILayout.Popup("Number of Tab icons", iOSTabBar.tabsIndex, tabs);
		iOSTabBar.selectedTabIndex = EditorGUILayout.Popup("Selected Tab", iOSTabBar.selectedTabIndex, tabs);
		if(iOSTabBar.selectedTabIndex > iOSTabBar.tabsIndex){
			iOSTabBar.selectedTabIndex = iOSTabBar.tabsIndex;
		}
		iOSTabBar.selectedTabColor = EditorGUILayout.ColorField("Selected Tab Color", iOSTabBar.selectedTabColor);
		iOSTabBar.deselectedTabColor = EditorGUILayout.ColorField("Deselected Tab Color", iOSTabBar.deselectedTabColor);
		EditorGUILayout.Separator();
		
		GameObject TabIcon1GO = iOSTabBar.transform.Find("Icon1").gameObject;
		TabIcon1GO.SetActive(false);
		
		GameObject TabIcon2GO = iOSTabBar.transform.Find("Icon2").gameObject;
		TabIcon2GO.SetActive(false);
		
		GameObject TabIcon3GO = iOSTabBar.transform.Find("Icon3").gameObject;
		TabIcon3GO.SetActive(false);
		
		GameObject TabIcon4GO = iOSTabBar.transform.Find("Icon4").gameObject;
		TabIcon4GO.SetActive(false);
		
		GameObject TabIcon5GO = iOSTabBar.transform.Find("Icon5").gameObject;
		TabIcon5GO.SetActive(false);
		
		GameObject TabPanel1GO = iOSTabBar.transform.Find("TabPanel1").gameObject;
		TabPanel1GO.SetActive(false);
		
		GameObject TabPanel2GO = iOSTabBar.transform.Find("TabPanel2").gameObject;
		TabPanel2GO.SetActive(false);
		
		GameObject TabPanel3GO = iOSTabBar.transform.Find("TabPanel3").gameObject;
		TabPanel3GO.SetActive(false);
		
		GameObject TabPanel4GO = iOSTabBar.transform.Find("TabPanel4").gameObject;
		TabPanel4GO.SetActive(false);
		
		GameObject TabPanel5GO = iOSTabBar.transform.Find("TabPanel5").gameObject;
		TabPanel5GO.SetActive(false);
		
		//Debug.Log (tabs[iOSTabBar.tabsIndex]);
		
		//Defines the number of tabs in the TabPanel
		if(tabs[iOSTabBar.tabsIndex] == "1"){
			TabIcon1GO.SetActive(true);
			TabIcon2GO.SetActive(false);
			TabIcon3GO.SetActive(false);
			TabIcon4GO.SetActive(false);
			TabIcon5GO.SetActive(false);			
			
			TabIcon1GO.GetComponent<RectTransform>().localPosition = new Vector3(0, TabIcon1GO.GetComponent<RectTransform>().localPosition.y, TabIcon1GO.GetComponent<RectTransform>().localPosition.z);	
			
			iOSTabBar.icon1Text = EditorGUILayout.TextField("Icon1 Text", iOSTabBar.icon1Text);
			iOSTabBar.icon1SelectedImage = (Sprite) EditorGUILayout.ObjectField("Icon1 Selected Image", iOSTabBar.icon1SelectedImage, typeof(Sprite), true);			
			iOSTabBar.icon1DeselectedImage = (Sprite) EditorGUILayout.ObjectField("Icon1 Deselected Image", iOSTabBar.icon1DeselectedImage, typeof(Sprite), true);			
			
			TabIcon1GO.GetComponentInChildren<Text>().text = iOSTabBar.icon1Text;
			TabIcon1GO.GetComponentInChildren<Text>().color = iOSTabBar.deselectedTabColor;			
			TabIcon1GO.GetComponent<Image>().color = iOSTabBar.deselectedTabColor;
			TabIcon1GO.GetComponent<Image>().sprite = iOSTabBar.icon1DeselectedImage;
		}
		
		if(tabs[iOSTabBar.tabsIndex] == "2"){
			TabIcon1GO.SetActive(true);
			TabIcon2GO.SetActive(true);
			TabIcon3GO.SetActive(false);
			TabIcon4GO.SetActive(false);
			TabIcon5GO.SetActive(false);
			
			TabIcon1GO.GetComponent<RectTransform>().localPosition = new Vector3(-160, TabIcon1GO.GetComponent<RectTransform>().localPosition.y, TabIcon1GO.GetComponent<RectTransform>().localPosition.z);	
			TabIcon2GO.GetComponent<RectTransform>().localPosition = new Vector3(160, TabIcon2GO.GetComponent<RectTransform>().localPosition.y, TabIcon2GO.GetComponent<RectTransform>().localPosition.z);		
			
			iOSTabBar.icon1Text = EditorGUILayout.TextField("Icon1 Text", iOSTabBar.icon1Text);
			iOSTabBar.icon1SelectedImage = (Sprite) EditorGUILayout.ObjectField("Icon1 Selected Image", iOSTabBar.icon1SelectedImage, typeof(Sprite), true);			
			iOSTabBar.icon1DeselectedImage = (Sprite) EditorGUILayout.ObjectField("Icon1 Deselected Image", iOSTabBar.icon1DeselectedImage, typeof(Sprite), true);			
			EditorGUILayout.Separator();
			iOSTabBar.icon2Text = EditorGUILayout.TextField("Icon2 Text", iOSTabBar.icon2Text);
			iOSTabBar.icon2SelectedImage = (Sprite) EditorGUILayout.ObjectField("Icon2 Selected Image", iOSTabBar.icon2SelectedImage, typeof(Sprite), true);			
			iOSTabBar.icon2DeselectedImage = (Sprite) EditorGUILayout.ObjectField("Icon2 Deselected Image", iOSTabBar.icon2DeselectedImage, typeof(Sprite), true);			
			
			TabIcon1GO.GetComponentInChildren<Text>().text = iOSTabBar.icon1Text;
			TabIcon1GO.GetComponentInChildren<Text>().color = iOSTabBar.deselectedTabColor;			
			TabIcon1GO.GetComponent<Image>().color = iOSTabBar.deselectedTabColor;
			TabIcon1GO.GetComponent<Image>().sprite = iOSTabBar.icon1DeselectedImage;
			TabIcon2GO.GetComponentInChildren<Text>().text = iOSTabBar.icon2Text;
			TabIcon2GO.GetComponentInChildren<Text>().color = iOSTabBar.deselectedTabColor;			
			TabIcon2GO.GetComponent<Image>().color = iOSTabBar.deselectedTabColor;
			TabIcon2GO.GetComponent<Image>().sprite = iOSTabBar.icon2DeselectedImage;
		}
		if(tabs[iOSTabBar.tabsIndex] == "3"){
			TabIcon1GO.SetActive(true);
			TabIcon2GO.SetActive(true);
			TabIcon3GO.SetActive(true);
			TabIcon4GO.SetActive(false);
			TabIcon5GO.SetActive(false);			
			
			TabIcon1GO.GetComponent<RectTransform>().localPosition = new Vector3(-213, TabIcon1GO.GetComponent<RectTransform>().localPosition.y, TabIcon1GO.GetComponent<RectTransform>().localPosition.z);	
			TabIcon2GO.GetComponent<RectTransform>().localPosition = new Vector3(0, TabIcon2GO.GetComponent<RectTransform>().localPosition.y, TabIcon2GO.GetComponent<RectTransform>().localPosition.z);		
			TabIcon3GO.GetComponent<RectTransform>().localPosition = new Vector3(213, TabIcon3GO.GetComponent<RectTransform>().localPosition.y, TabIcon3GO.GetComponent<RectTransform>().localPosition.z);		
			
			iOSTabBar.icon1Text = EditorGUILayout.TextField("Icon1 Text", iOSTabBar.icon1Text);
			iOSTabBar.icon1SelectedImage = (Sprite) EditorGUILayout.ObjectField("Icon1 Selected Image", iOSTabBar.icon1SelectedImage, typeof(Sprite), true);			
			iOSTabBar.icon1DeselectedImage = (Sprite) EditorGUILayout.ObjectField("Icon1 Deselected Image", iOSTabBar.icon1DeselectedImage, typeof(Sprite), true);			
			EditorGUILayout.Separator();
			iOSTabBar.icon2Text = EditorGUILayout.TextField("Icon2 Text", iOSTabBar.icon2Text);
			iOSTabBar.icon2SelectedImage = (Sprite) EditorGUILayout.ObjectField("Icon2 Selected Image", iOSTabBar.icon2SelectedImage, typeof(Sprite), true);			
			iOSTabBar.icon2DeselectedImage = (Sprite) EditorGUILayout.ObjectField("Icon2 Deselected Image", iOSTabBar.icon2DeselectedImage, typeof(Sprite), true);			
			EditorGUILayout.Separator();
			iOSTabBar.icon3Text = EditorGUILayout.TextField("Icon3 Text", iOSTabBar.icon3Text);
			iOSTabBar.icon3SelectedImage = (Sprite) EditorGUILayout.ObjectField("Icon3 Selected Image", iOSTabBar.icon3SelectedImage, typeof(Sprite), true);			
			iOSTabBar.icon3DeselectedImage = (Sprite) EditorGUILayout.ObjectField("Icon3 Deselected Image", iOSTabBar.icon3DeselectedImage, typeof(Sprite), true);			
			
			TabIcon1GO.GetComponentInChildren<Text>().text = iOSTabBar.icon1Text;
			TabIcon1GO.GetComponentInChildren<Text>().color = iOSTabBar.deselectedTabColor;			
			TabIcon1GO.GetComponent<Image>().color = iOSTabBar.deselectedTabColor;
			TabIcon1GO.GetComponent<Image>().sprite = iOSTabBar.icon1DeselectedImage;
			TabIcon2GO.GetComponentInChildren<Text>().text = iOSTabBar.icon2Text;
			TabIcon2GO.GetComponentInChildren<Text>().color = iOSTabBar.deselectedTabColor;			
			TabIcon2GO.GetComponent<Image>().color = iOSTabBar.deselectedTabColor;
			TabIcon2GO.GetComponent<Image>().sprite = iOSTabBar.icon2DeselectedImage;
			TabIcon3GO.GetComponentInChildren<Text>().text = iOSTabBar.icon3Text;
			TabIcon3GO.GetComponentInChildren<Text>().color = iOSTabBar.deselectedTabColor;			
			TabIcon3GO.GetComponent<Image>().color = iOSTabBar.deselectedTabColor;
			TabIcon3GO.GetComponent<Image>().sprite = iOSTabBar.icon3DeselectedImage;
		}
		if(tabs[iOSTabBar.tabsIndex] == "4"){
			TabIcon1GO.SetActive(true);
			TabIcon2GO.SetActive(true);
			TabIcon3GO.SetActive(true);
			TabIcon4GO.SetActive(true);
			TabIcon5GO.SetActive(false);			
			
			TabIcon1GO.GetComponent<RectTransform>().localPosition = new Vector3(-240, TabIcon1GO.GetComponent<RectTransform>().localPosition.y, TabIcon1GO.GetComponent<RectTransform>().localPosition.z);	
			TabIcon2GO.GetComponent<RectTransform>().localPosition = new Vector3(-80, TabIcon2GO.GetComponent<RectTransform>().localPosition.y, TabIcon2GO.GetComponent<RectTransform>().localPosition.z);		
			TabIcon3GO.GetComponent<RectTransform>().localPosition = new Vector3(80, TabIcon3GO.GetComponent<RectTransform>().localPosition.y, TabIcon3GO.GetComponent<RectTransform>().localPosition.z);		
			TabIcon4GO.GetComponent<RectTransform>().localPosition = new Vector3(240, TabIcon4GO.GetComponent<RectTransform>().localPosition.y, TabIcon4GO.GetComponent<RectTransform>().localPosition.z);		
			
			iOSTabBar.icon1Text = EditorGUILayout.TextField("Icon1 Text", iOSTabBar.icon1Text);
			iOSTabBar.icon1SelectedImage = (Sprite) EditorGUILayout.ObjectField("Icon1 Selected Image", iOSTabBar.icon1SelectedImage, typeof(Sprite), true);			
			iOSTabBar.icon1DeselectedImage = (Sprite) EditorGUILayout.ObjectField("Icon1 Deselected Image", iOSTabBar.icon1DeselectedImage, typeof(Sprite), true);			
			EditorGUILayout.Separator();
			iOSTabBar.icon2Text = EditorGUILayout.TextField("Icon2 Text", iOSTabBar.icon2Text);
			iOSTabBar.icon2SelectedImage = (Sprite) EditorGUILayout.ObjectField("Icon2 Selected Image", iOSTabBar.icon2SelectedImage, typeof(Sprite), true);			
			iOSTabBar.icon2DeselectedImage = (Sprite) EditorGUILayout.ObjectField("Icon2 Deselected Image", iOSTabBar.icon2DeselectedImage, typeof(Sprite), true);			
			EditorGUILayout.Separator();
			iOSTabBar.icon3Text = EditorGUILayout.TextField("Icon3 Text", iOSTabBar.icon3Text);
			iOSTabBar.icon3SelectedImage = (Sprite) EditorGUILayout.ObjectField("Icon3 Selected Image", iOSTabBar.icon3SelectedImage, typeof(Sprite), true);			
			iOSTabBar.icon3DeselectedImage = (Sprite) EditorGUILayout.ObjectField("Icon3 Deselected Image", iOSTabBar.icon3DeselectedImage, typeof(Sprite), true);			
			EditorGUILayout.Separator();
			iOSTabBar.icon4Text = EditorGUILayout.TextField("Icon4 Text", iOSTabBar.icon4Text);
			iOSTabBar.icon4SelectedImage = (Sprite) EditorGUILayout.ObjectField("Icon4 Selected Image", iOSTabBar.icon4SelectedImage, typeof(Sprite), true);			
			iOSTabBar.icon4DeselectedImage = (Sprite) EditorGUILayout.ObjectField("Icon4 Deselected Image", iOSTabBar.icon4DeselectedImage, typeof(Sprite), true);			
			
			TabIcon1GO.GetComponentInChildren<Text>().text = iOSTabBar.icon1Text;
			TabIcon1GO.GetComponentInChildren<Text>().color = iOSTabBar.deselectedTabColor;			
			TabIcon1GO.GetComponent<Image>().color = iOSTabBar.deselectedTabColor;
			TabIcon1GO.GetComponent<Image>().sprite = iOSTabBar.icon1DeselectedImage;
			TabIcon2GO.GetComponentInChildren<Text>().text = iOSTabBar.icon2Text;
			TabIcon2GO.GetComponentInChildren<Text>().color = iOSTabBar.deselectedTabColor;			
			TabIcon2GO.GetComponent<Image>().color = iOSTabBar.deselectedTabColor;
			TabIcon2GO.GetComponent<Image>().sprite = iOSTabBar.icon2DeselectedImage;
			TabIcon3GO.GetComponentInChildren<Text>().text = iOSTabBar.icon3Text;
			TabIcon3GO.GetComponentInChildren<Text>().color = iOSTabBar.deselectedTabColor;			
			TabIcon3GO.GetComponent<Image>().color = iOSTabBar.deselectedTabColor;
			TabIcon3GO.GetComponent<Image>().sprite = iOSTabBar.icon3DeselectedImage;
			TabIcon4GO.GetComponentInChildren<Text>().text = iOSTabBar.icon4Text;
			TabIcon4GO.GetComponentInChildren<Text>().color = iOSTabBar.deselectedTabColor;			
			TabIcon4GO.GetComponent<Image>().color = iOSTabBar.deselectedTabColor;
			TabIcon4GO.GetComponent<Image>().sprite = iOSTabBar.icon4DeselectedImage;
		}
		if(tabs[iOSTabBar.tabsIndex] == "5"){
			TabIcon1GO.SetActive(true);
			TabIcon2GO.SetActive(true);
			TabIcon3GO.SetActive(true);
			TabIcon4GO.SetActive(true);
			TabIcon5GO.SetActive(true);			
			
			TabIcon1GO.GetComponent<RectTransform>().localPosition = new Vector3(-256, TabIcon1GO.GetComponent<RectTransform>().localPosition.y, TabIcon1GO.GetComponent<RectTransform>().localPosition.z);	
			TabIcon2GO.GetComponent<RectTransform>().localPosition = new Vector3(-128, TabIcon2GO.GetComponent<RectTransform>().localPosition.y, TabIcon2GO.GetComponent<RectTransform>().localPosition.z);		
			TabIcon3GO.GetComponent<RectTransform>().localPosition = new Vector3(0, TabIcon3GO.GetComponent<RectTransform>().localPosition.y, TabIcon3GO.GetComponent<RectTransform>().localPosition.z);		
			TabIcon4GO.GetComponent<RectTransform>().localPosition = new Vector3(128, TabIcon4GO.GetComponent<RectTransform>().localPosition.y, TabIcon4GO.GetComponent<RectTransform>().localPosition.z);		
			TabIcon5GO.GetComponent<RectTransform>().localPosition = new Vector3(256, TabIcon5GO.GetComponent<RectTransform>().localPosition.y, TabIcon5GO.GetComponent<RectTransform>().localPosition.z);		
			
			iOSTabBar.icon1Text = EditorGUILayout.TextField("Icon1 Text", iOSTabBar.icon1Text);
			iOSTabBar.icon1SelectedImage = (Sprite) EditorGUILayout.ObjectField("Icon1 Selected Image", iOSTabBar.icon1SelectedImage, typeof(Sprite), true);			
			iOSTabBar.icon1DeselectedImage = (Sprite) EditorGUILayout.ObjectField("Icon1 Deselected Image", iOSTabBar.icon1DeselectedImage, typeof(Sprite), true);			
			EditorGUILayout.Separator();
			iOSTabBar.icon2Text = EditorGUILayout.TextField("Icon2 Text", iOSTabBar.icon2Text);
			iOSTabBar.icon2SelectedImage = (Sprite) EditorGUILayout.ObjectField("Icon2 Selected Image", iOSTabBar.icon2SelectedImage, typeof(Sprite), true);			
			iOSTabBar.icon2DeselectedImage = (Sprite) EditorGUILayout.ObjectField("Icon2 Deselected Image", iOSTabBar.icon2DeselectedImage, typeof(Sprite), true);			
			EditorGUILayout.Separator();
			iOSTabBar.icon3Text = EditorGUILayout.TextField("Icon3 Text", iOSTabBar.icon3Text);
			iOSTabBar.icon3SelectedImage = (Sprite) EditorGUILayout.ObjectField("Icon3 Selected Image", iOSTabBar.icon3SelectedImage, typeof(Sprite), true);			
			iOSTabBar.icon3DeselectedImage = (Sprite) EditorGUILayout.ObjectField("Icon3 Deselected Image", iOSTabBar.icon3DeselectedImage, typeof(Sprite), true);			
			EditorGUILayout.Separator();
			iOSTabBar.icon4Text = EditorGUILayout.TextField("Icon4 Text", iOSTabBar.icon4Text);
			iOSTabBar.icon4SelectedImage = (Sprite) EditorGUILayout.ObjectField("Icon4 Selected Image", iOSTabBar.icon4SelectedImage, typeof(Sprite), true);			
			iOSTabBar.icon4DeselectedImage = (Sprite) EditorGUILayout.ObjectField("Icon4 Deselected Image", iOSTabBar.icon4DeselectedImage, typeof(Sprite), true);			
			EditorGUILayout.Separator();
			iOSTabBar.icon5Text = EditorGUILayout.TextField("Icon5 Text", iOSTabBar.icon5Text);
			iOSTabBar.icon5SelectedImage = (Sprite) EditorGUILayout.ObjectField("Icon5 Selected Image", iOSTabBar.icon5SelectedImage, typeof(Sprite), true);			
			iOSTabBar.icon5DeselectedImage = (Sprite) EditorGUILayout.ObjectField("Icon5 Deselected Image", iOSTabBar.icon5DeselectedImage, typeof(Sprite), true);			
			
			TabIcon1GO.GetComponentInChildren<Text>().text = iOSTabBar.icon1Text;
			TabIcon1GO.GetComponentInChildren<Text>().color = iOSTabBar.deselectedTabColor;			
			TabIcon1GO.GetComponent<Image>().color = iOSTabBar.deselectedTabColor;
			TabIcon1GO.GetComponent<Image>().sprite = iOSTabBar.icon1DeselectedImage;
			TabIcon2GO.GetComponentInChildren<Text>().text = iOSTabBar.icon2Text;
			TabIcon2GO.GetComponentInChildren<Text>().color = iOSTabBar.deselectedTabColor;			
			TabIcon2GO.GetComponent<Image>().color = iOSTabBar.deselectedTabColor;
			TabIcon2GO.GetComponent<Image>().sprite = iOSTabBar.icon2DeselectedImage;
			TabIcon3GO.GetComponentInChildren<Text>().text = iOSTabBar.icon3Text;
			TabIcon3GO.GetComponentInChildren<Text>().color = iOSTabBar.deselectedTabColor;			
			TabIcon3GO.GetComponent<Image>().color = iOSTabBar.deselectedTabColor;
			TabIcon3GO.GetComponent<Image>().sprite = iOSTabBar.icon3DeselectedImage;
			TabIcon4GO.GetComponentInChildren<Text>().text = iOSTabBar.icon4Text;
			TabIcon4GO.GetComponentInChildren<Text>().color = iOSTabBar.deselectedTabColor;			
			TabIcon4GO.GetComponent<Image>().color = iOSTabBar.deselectedTabColor;
			TabIcon4GO.GetComponent<Image>().sprite = iOSTabBar.icon4DeselectedImage;
			TabIcon5GO.GetComponentInChildren<Text>().text = iOSTabBar.icon5Text;
			TabIcon5GO.GetComponentInChildren<Text>().color = iOSTabBar.deselectedTabColor;			
			TabIcon5GO.GetComponent<Image>().color = iOSTabBar.deselectedTabColor;
			TabIcon5GO.GetComponent<Image>().sprite = iOSTabBar.icon5DeselectedImage;
		}
		
		
		EditorUtility.SetDirty(target);
		
	}
	
	
}