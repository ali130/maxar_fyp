﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using UnityEngine.UI;

[CustomEditor(typeof(iOS_UIPanel))]
public class iOS_UIPanelEditor : Editor {
	
	[MenuItem("GameObject/UI/iOS GUI/UIPanel")]
	private static void iOSGUIPanel(){
		//Debug.Log ("UIPanel menu item!");
		Canvas canvas = FindObjectOfType(typeof(Canvas)) as Canvas;
		if (canvas){
			//Debug.Log("Canvas object found: " + canvas.name);
			canvas.pixelPerfect = true;
			canvas.GetComponent<CanvasScaler>().uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
			if(canvas.GetComponent<CanvasHandler>() == null){
				canvas.gameObject.AddComponent<CanvasHandler>();
			}
		} else {
			//Debug.Log("No Canvas object could be found");
			EditorApplication.ExecuteMenuItem("GameObject/UI/Canvas");
			canvas = FindObjectOfType(typeof(Canvas)) as Canvas;
			canvas.pixelPerfect = true;
			canvas.GetComponent<CanvasScaler>().uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
			if(canvas.GetComponent<CanvasHandler>() == null){
				canvas.gameObject.AddComponent<CanvasHandler>();
			}
		}	
		
		GameObject go, rp;
		go = Instantiate(Resources.Load("Prefabs/UIPanel")) as GameObject;
		Undo.RegisterCreatedObjectUndo (go, "Create UIPanel");
		
		go.transform.SetParent(canvas.transform);
		rp = GameObject.Find("Canvas/rootUIPanel");
		if(rp){
			go.name = "UIPanel";
			go.tag = "UIPanel";
			
			go.GetComponent<RectTransform>().localPosition = new Vector3((go.transform.parent.childCount-2f) * 2560f, -2560f, 0f);
			
		} else {
			go.name = "rootUIPanel";
			go.tag = "rootUIPanel";
			
			go.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);

		}
		
		go.GetComponent<RectTransform>().sizeDelta = new Vector2(0, 0);
		go.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);	
		
		//Select new added object in hierarchy
		Selection.activeGameObject = go;
		
	}	
	
	public override void OnInspectorGUI ()
	{
		
		var iOSUIPanel = target as iOS_UIPanel;
		Undo.RecordObject (iOSUIPanel, "Inspector");
		
		if(iOSUIPanel.tag == "rootUIPanel" || iOSUIPanel.tag == "UIPanel"){
			PlayerSettings.statusBarHidden = EditorGUILayout.Toggle("Status Bar Hidden", PlayerSettings.statusBarHidden);
			//iOSUIPanel.StatusBarHidden = PlayerSettings.statusBarHidden;
			EditorGUILayout.Separator();
		}
		
		iOSUIPanel.PanelColor = EditorGUILayout.ColorField("UIPanel Color", iOSUIPanel.PanelColor);
		iOSUIPanel.GetComponent<Image>().color = iOSUIPanel.PanelColor;
		
		EditorUtility.SetDirty(target);
		
	}
}
