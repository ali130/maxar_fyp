﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using UnityEngine.UI;

[CustomEditor(typeof(iOS_Image))]
public class uGUI_IOSInterface_ImageEditor : Editor 
{
	
//	string[] tabs =  new[] { "1", "2", "3", "4", "5" };
	
	[MenuItem("GameObject/UI/iOS GUI/Image")]
	private static void iOSGUIImage(){
		//Debug.Log ("Image menu item!");
		
		//Check if selected object can have a button added?		
		if(Selection.activeGameObject != null){
			if(Selection.activeGameObject.tag == "rootUIPanel" || Selection.activeGameObject.tag == "UIPanel" || Selection.activeGameObject.tag == "UITabPanel" || Selection.activeGameObject.tag == "TableViewContainer" || Selection.activeGameObject.tag == "TableViewItem"){
				GameObject go;
				go = Instantiate(Resources.Load("Prefabs/Image")) as GameObject;
				Undo.RegisterCreatedObjectUndo (go, "Create Image");
				
				go.transform.SetParent(Selection.activeGameObject.transform, true);			
				
				go.name = "Image";
				
				if(Selection.activeGameObject.tag == "rootUIPanel" || Selection.activeGameObject.tag == "UIPanel" || Selection.activeGameObject.tag == "UITabPanel"){
					go.GetComponent<RectTransform>().localPosition = new Vector3(0f, 0f, 0f);				
				} else {
					if(Selection.activeGameObject.tag == "TableViewContainer"){
						go.GetComponent<RectTransform>().localPosition = new Vector3(0f, (go.transform.parent.GetComponent<RectTransform>().rect.height/2) - (go.GetComponent<RectTransform>().sizeDelta.y/2), 0f);
					} else {
						go.GetComponent<RectTransform>().localPosition = new Vector3(0f, (go.transform.parent.GetComponent<RectTransform>().rect.height/2) * -1, 0f);
					}	
				}
				go.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);
				
				//Select new added object in hierarchy
				Selection.activeGameObject = go;
				
			} else {
				EditorUtility.DisplayDialog("A Image cannot be added to current selection!", "Please select a UIPanel in the Hierarchy panel.", "OK");
			}						
		} else {
			EditorUtility.DisplayDialog("A Image cannot be added to current selection!", "Please select a UIPanel in the Hierarchy panel.", "OK");
		}						
		
	}

	public override void OnInspectorGUI ()
	{
	
		var iOSImage = target as iOS_Image;
		Undo.RecordObject (iOSImage, "Inspector");
		
		
		iOSImage.image = EditorGUILayout.ObjectField("Image", iOSImage.image, typeof(Sprite),true) as Sprite;
		iOSImage.actualImageSize = EditorGUILayout.Toggle("Use Actual Image Size", iOSImage.actualImageSize);
		
		if(iOSImage.actualImageSize==false){
			iOSImage.width = EditorGUILayout.IntField("Set Image Width", iOSImage.width);			
			iOSImage.height = EditorGUILayout.IntField("Set Image Height", iOSImage.height);
		}
			
						
									
												
		if(iOSImage.image!=null){													
			iOSImage.GetComponent<Image>().sprite = iOSImage.image;
			if(iOSImage.actualImageSize==true){
				iOSImage.GetComponent<RectTransform>().sizeDelta = new Vector2(iOSImage.GetComponent<Image>().sprite.rect.width, iOSImage.GetComponent<Image>().sprite.rect.height);
			} else {
				iOSImage.GetComponent<RectTransform>().sizeDelta = new Vector2(iOSImage.width, iOSImage.height);
			}
		}

		EditorUtility.SetDirty(target);
		
	}
	
}