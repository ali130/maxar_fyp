﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

[CustomEditor(typeof(iOS_PopupMessage))]
public class iOS_PopupMessageEditor : Editor 
{
	
	string[] buttons =  new[] { "1", "2", "3", "4", "5", "6", "7", "8" };
	private float ScreenWidth;
	
	[MenuItem("GameObject/UI/iOS GUI/Popup Message")]
	private static void iOSGUIPopupMessage(){
		//Debug.Log ("Popup Message menu item!");
		
		//Check if Canvas exists?
		Canvas canvas = FindObjectOfType(typeof(Canvas)) as Canvas;
		if (canvas){ //Canvas already exists, do nothing...
			//Debug.Log("Canvas object found: " + canvas.name);
		} else { //Create Canvas
			//Debug.Log("No Canvas object could be found");
			EditorApplication.ExecuteMenuItem("GameObject/UI/Canvas");
			canvas = FindObjectOfType(typeof(Canvas)) as Canvas;
			canvas.pixelPerfect = true;
		}	
		/*
		//Check if Panel exists?
		uGUI_IOSInterface_UIPanel panel = FindObjectOfType(typeof(uGUI_IOSInterface_UIPanel)) as uGUI_IOSInterface_UIPanel;
		if (panel){ //Panel already exists, do nothing...
			//Debug.Log("Panel object found: " + panel.name);
			if(Selection.activeGameObject == null){ //if no object selected, make panel found the selected object
				Selection.activeGameObject = panel.gameObject;
			}
		} else { //Create Panel
			//Debug.Log("No Panel object could be found");
			EditorApplication.ExecuteMenuItem("GameObject/UI/iOS GUI/UIPanel");
			panel = FindObjectOfType(typeof(uGUI_IOSInterface_UIPanel)) as uGUI_IOSInterface_UIPanel;
			Selection.activeGameObject = panel.gameObject;
		}
		
		//Check if selected object is a Panel		
		bool bComponentExists=false;
		if(Selection.activeGameObject.tag == "rootUIPanel" || Selection.activeGameObject.tag == "UIPanel" || Selection.activeGameObject.tag == "UITabPanel"){

			GameObject go;
			go = Instantiate(Resources.Load("Prefabs/Popup Message")) as GameObject;
			Undo.RegisterCreatedObjectUndo (go, "Create Popup Message");
			
			if(Selection.activeGameObject != null){
				if(Selection.activeGameObject.tag == "rootUIPanel" || Selection.activeGameObject.tag == "UIPanel" || Selection.activeGameObject.tag == "UITabPanel"){
					go.transform.parent = Selection.activeGameObject.transform;
				} else {
					go.transform.parent = panel.transform;
				}
			} else {
				go.transform.parent = panel.transform;
			}
			
			go.name = "Popup Message";
			
			go.GetComponent<RectTransform>().localPosition = new Vector3(0f, 0f, 0f);	
			go.GetComponent<RectTransform>().sizeDelta = new Vector2(0f,0f);
			go.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);
			
			go.transform.SetAsLastSibling();
			
			Selection.activeGameObject = go;		
			
		} else {
			EditorUtility.DisplayDialog("Popup Message cannot be added to current selection!", "You need to select a UIPanel.", "OK");
		}						
		*/
	}

	public override void OnInspectorGUI ()
	{
	
		var iOSPopupMessage = target as iOS_PopupMessage;
		Undo.RecordObject (iOSPopupMessage, "Inspector");
		
		iOSPopupMessage.buttonsIndex = EditorGUILayout.Popup("Number of Buttons", iOSPopupMessage.buttonsIndex, buttons);
		EditorGUILayout.Separator();
		
		iOSPopupMessage.titleText = EditorGUILayout.TextField("Title Text", iOSPopupMessage.titleText);
		iOSPopupMessage.titleColor = EditorGUILayout.ColorField("Title Color", iOSPopupMessage.titleColor);			
		EditorGUILayout.Separator();

		iOSPopupMessage.transform.Find("Window/Buttons/Button1/Label").gameObject.SetActive(true);
		iOSPopupMessage.transform.Find("Window/Buttons/Button2/Label").gameObject.SetActive(true);
		iOSPopupMessage.transform.Find("Window/Buttons/Button3/Label").gameObject.SetActive(true);
		iOSPopupMessage.transform.Find("Window/Buttons/Button4/Label").gameObject.SetActive(true);
		iOSPopupMessage.transform.Find("Window/Buttons/Button5/Label").gameObject.SetActive(true);
		iOSPopupMessage.transform.Find("Window/Buttons/Button6/Label").gameObject.SetActive(true);
		iOSPopupMessage.transform.Find("Window/Buttons/Button7/Label").gameObject.SetActive(true);
		iOSPopupMessage.transform.Find("Window/Buttons/Button8/Label").gameObject.SetActive(true);
		iOSPopupMessage.transform.Find("Window/Buttons/Button1/FooterLine").gameObject.SetActive(true);
		iOSPopupMessage.transform.Find("Window/Buttons/Button2/FooterLine").gameObject.SetActive(true);
		iOSPopupMessage.transform.Find("Window/Buttons/Button3/FooterLine").gameObject.SetActive(true);
		iOSPopupMessage.transform.Find("Window/Buttons/Button4/FooterLine").gameObject.SetActive(true);
		iOSPopupMessage.transform.Find("Window/Buttons/Button5/FooterLine").gameObject.SetActive(true);
		iOSPopupMessage.transform.Find("Window/Buttons/Button6/FooterLine").gameObject.SetActive(true);
		iOSPopupMessage.transform.Find("Window/Buttons/Button7/FooterLine").gameObject.SetActive(true);
		iOSPopupMessage.transform.Find("Window/Buttons/Button8/FooterLine").gameObject.SetActive(true);
								
		if(iOSPopupMessage.buttonsIndex == 0){
			iOSPopupMessage.button1Text = EditorGUILayout.TextField("Button1 Text", iOSPopupMessage.button1Text);
			iOSPopupMessage.button1Color = EditorGUILayout.ColorField("Button1 Color", iOSPopupMessage.button1Color);			
			iOSPopupMessage.transform.Find("Window/Buttons/Button1/FooterLine").gameObject.SetActive(false);
			EditorGUILayout.LabelField("Button1 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSPopupMessage.button1GO = EditorGUILayout.ObjectField(iOSPopupMessage.button1GO, typeof(GameObject)) as GameObject;					
			iOSPopupMessage.button1FunctionName = EditorGUILayout.TextField(iOSPopupMessage.button1FunctionName);
			EditorGUILayout.EndHorizontal();
			iOSPopupMessage.transform.Find("Window/Buttons/Button2/FooterLine").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button3/FooterLine").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button4/FooterLine").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button5/FooterLine").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button6/FooterLine").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button7/FooterLine").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button8/FooterLine").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button2/Label").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button3/Label").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button4/Label").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button5/Label").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button6/Label").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button7/Label").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button8/Label").gameObject.SetActive(false);
		}
		if(iOSPopupMessage.buttonsIndex == 1){
			iOSPopupMessage.button1Text = EditorGUILayout.TextField("Button1 Text", iOSPopupMessage.button1Text);
			iOSPopupMessage.button1Color = EditorGUILayout.ColorField("Button1 Color", iOSPopupMessage.button1Color);			
			iOSPopupMessage.transform.Find("Window/Buttons/Button1/FooterLine").gameObject.SetActive(true);
			EditorGUILayout.LabelField("Button1 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSPopupMessage.button1GO = EditorGUILayout.ObjectField(iOSPopupMessage.button1GO, typeof(GameObject)) as GameObject;					
			iOSPopupMessage.button1FunctionName = EditorGUILayout.TextField(iOSPopupMessage.button1FunctionName);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Separator();
			iOSPopupMessage.button2Text = EditorGUILayout.TextField("Button2 Text", iOSPopupMessage.button2Text);
			iOSPopupMessage.button2Color = EditorGUILayout.ColorField("Button2 Color", iOSPopupMessage.button2Color);			
			iOSPopupMessage.transform.Find("Window/Buttons/Button2/FooterLine").gameObject.SetActive(false);
			EditorGUILayout.LabelField("Button2 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSPopupMessage.button2GO = EditorGUILayout.ObjectField(iOSPopupMessage.button2GO, typeof(GameObject)) as GameObject;					
			iOSPopupMessage.button2FunctionName = EditorGUILayout.TextField(iOSPopupMessage.button2FunctionName);
			EditorGUILayout.EndHorizontal();
			iOSPopupMessage.transform.Find("Window/Buttons/Button3/FooterLine").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button4/FooterLine").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button5/FooterLine").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button6/FooterLine").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button7/FooterLine").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button8/FooterLine").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button3/Label").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button4/Label").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button5/Label").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button6/Label").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button7/Label").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button8/Label").gameObject.SetActive(false);
		}
		if(iOSPopupMessage.buttonsIndex == 2){
			iOSPopupMessage.button1Text = EditorGUILayout.TextField("Button1 Text", iOSPopupMessage.button1Text);
			iOSPopupMessage.button1Color = EditorGUILayout.ColorField("Button1 Color", iOSPopupMessage.button1Color);			
			iOSPopupMessage.transform.Find("Window/Buttons/Button1/FooterLine").gameObject.SetActive(true);
			EditorGUILayout.LabelField("Button1 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSPopupMessage.button1GO = EditorGUILayout.ObjectField(iOSPopupMessage.button1GO, typeof(GameObject)) as GameObject;					
			iOSPopupMessage.button1FunctionName = EditorGUILayout.TextField(iOSPopupMessage.button1FunctionName);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Separator();
			iOSPopupMessage.button2Text = EditorGUILayout.TextField("Button2 Text", iOSPopupMessage.button2Text);
			iOSPopupMessage.button2Color = EditorGUILayout.ColorField("Button2 Color", iOSPopupMessage.button2Color);			
			iOSPopupMessage.transform.Find("Window/Buttons/Button2/FooterLine").gameObject.SetActive(true);
			EditorGUILayout.LabelField("Button2 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSPopupMessage.button2GO = EditorGUILayout.ObjectField(iOSPopupMessage.button2GO, typeof(GameObject)) as GameObject;					
			iOSPopupMessage.button2FunctionName = EditorGUILayout.TextField(iOSPopupMessage.button2FunctionName);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Separator();
			iOSPopupMessage.button3Text = EditorGUILayout.TextField("Button3 Text", iOSPopupMessage.button3Text);
			iOSPopupMessage.button3Color = EditorGUILayout.ColorField("Button3 Color", iOSPopupMessage.button3Color);			
			iOSPopupMessage.transform.Find("Window/Buttons/Button3/FooterLine").gameObject.SetActive(false);
			EditorGUILayout.LabelField("Button3 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSPopupMessage.button3GO = EditorGUILayout.ObjectField(iOSPopupMessage.button3GO, typeof(GameObject)) as GameObject;					
			iOSPopupMessage.button3FunctionName = EditorGUILayout.TextField(iOSPopupMessage.button3FunctionName);
			EditorGUILayout.EndHorizontal();
			iOSPopupMessage.transform.Find("Window/Buttons/Button4/FooterLine").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button5/FooterLine").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button6/FooterLine").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button7/FooterLine").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button8/FooterLine").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button4/Label").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button5/Label").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button6/Label").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button7/Label").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button8/Label").gameObject.SetActive(false);
		}
		if(iOSPopupMessage.buttonsIndex == 3){
			iOSPopupMessage.button1Text = EditorGUILayout.TextField("Button1 Text", iOSPopupMessage.button1Text);
			iOSPopupMessage.button1Color = EditorGUILayout.ColorField("Button1 Color", iOSPopupMessage.button1Color);			
			iOSPopupMessage.transform.Find("Window/Buttons/Button1/FooterLine").gameObject.SetActive(true);
			EditorGUILayout.LabelField("Button1 OnClick()");
			EditorGUILayout.BeginHorizontal();
				iOSPopupMessage.button1GO = EditorGUILayout.ObjectField(iOSPopupMessage.button1GO, typeof(GameObject)) as GameObject;					
				iOSPopupMessage.button1FunctionName = EditorGUILayout.TextField(iOSPopupMessage.button1FunctionName);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Separator();
			iOSPopupMessage.button2Text = EditorGUILayout.TextField("Button2 Text", iOSPopupMessage.button2Text);
			iOSPopupMessage.button2Color = EditorGUILayout.ColorField("Button2 Color", iOSPopupMessage.button2Color);			
			iOSPopupMessage.transform.Find("Window/Buttons/Button2/FooterLine").gameObject.SetActive(true);
			EditorGUILayout.LabelField("Button2 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSPopupMessage.button2GO = EditorGUILayout.ObjectField(iOSPopupMessage.button2GO, typeof(GameObject)) as GameObject;					
			iOSPopupMessage.button2FunctionName = EditorGUILayout.TextField(iOSPopupMessage.button2FunctionName);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Separator();
			iOSPopupMessage.button3Text = EditorGUILayout.TextField("Button3 Text", iOSPopupMessage.button3Text);
			iOSPopupMessage.button3Color = EditorGUILayout.ColorField("Button3 Color", iOSPopupMessage.button3Color);			
			iOSPopupMessage.transform.Find("Window/Buttons/Button3/FooterLine").gameObject.SetActive(true);
			EditorGUILayout.LabelField("Button3 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSPopupMessage.button3GO = EditorGUILayout.ObjectField(iOSPopupMessage.button3GO, typeof(GameObject)) as GameObject;					
			iOSPopupMessage.button3FunctionName = EditorGUILayout.TextField(iOSPopupMessage.button3FunctionName);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Separator();
			iOSPopupMessage.button4Text = EditorGUILayout.TextField("Button4 Text", iOSPopupMessage.button4Text);
			iOSPopupMessage.button4Color = EditorGUILayout.ColorField("Button4 Color", iOSPopupMessage.button4Color);			
			iOSPopupMessage.transform.Find("Window/Buttons/Button4/FooterLine").gameObject.SetActive(false);
			EditorGUILayout.LabelField("Button4 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSPopupMessage.button4GO = EditorGUILayout.ObjectField(iOSPopupMessage.button4GO, typeof(GameObject)) as GameObject;					
			iOSPopupMessage.button4FunctionName = EditorGUILayout.TextField(iOSPopupMessage.button4FunctionName);
			EditorGUILayout.EndHorizontal();
			iOSPopupMessage.transform.Find("Window/Buttons/Button5/FooterLine").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button6/FooterLine").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button7/FooterLine").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button8/FooterLine").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button5/Label").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button6/Label").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button7/Label").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button8/Label").gameObject.SetActive(false);			
		}
		if(iOSPopupMessage.buttonsIndex == 4){
			iOSPopupMessage.button1Text = EditorGUILayout.TextField("Button1 Text", iOSPopupMessage.button1Text);
			iOSPopupMessage.button1Color = EditorGUILayout.ColorField("Button1 Color", iOSPopupMessage.button1Color);			
			iOSPopupMessage.transform.Find("Window/Buttons/Button1/FooterLine").gameObject.SetActive(true);
			EditorGUILayout.LabelField("Button1 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSPopupMessage.button1GO = EditorGUILayout.ObjectField(iOSPopupMessage.button1GO, typeof(GameObject)) as GameObject;					
			iOSPopupMessage.button1FunctionName = EditorGUILayout.TextField(iOSPopupMessage.button1FunctionName);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Separator();
			iOSPopupMessage.button2Text = EditorGUILayout.TextField("Button2 Text", iOSPopupMessage.button2Text);
			iOSPopupMessage.button2Color = EditorGUILayout.ColorField("Button2 Color", iOSPopupMessage.button2Color);			
			iOSPopupMessage.transform.Find("Window/Buttons/Button2/FooterLine").gameObject.SetActive(true);
			EditorGUILayout.LabelField("Button2 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSPopupMessage.button2GO = EditorGUILayout.ObjectField(iOSPopupMessage.button2GO, typeof(GameObject)) as GameObject;					
			iOSPopupMessage.button2FunctionName = EditorGUILayout.TextField(iOSPopupMessage.button2FunctionName);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Separator();
			iOSPopupMessage.button3Text = EditorGUILayout.TextField("Button3 Text", iOSPopupMessage.button3Text);
			iOSPopupMessage.button3Color = EditorGUILayout.ColorField("Button3 Color", iOSPopupMessage.button3Color);			
			iOSPopupMessage.transform.Find("Window/Buttons/Button3/FooterLine").gameObject.SetActive(true);
			EditorGUILayout.LabelField("Button3 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSPopupMessage.button3GO = EditorGUILayout.ObjectField(iOSPopupMessage.button3GO, typeof(GameObject)) as GameObject;					
			iOSPopupMessage.button3FunctionName = EditorGUILayout.TextField(iOSPopupMessage.button3FunctionName);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Separator();
			iOSPopupMessage.button4Text = EditorGUILayout.TextField("Button4 Text", iOSPopupMessage.button4Text);
			iOSPopupMessage.button4Color = EditorGUILayout.ColorField("Button4 Color", iOSPopupMessage.button4Color);			
			iOSPopupMessage.transform.Find("Window/Buttons/Button4/FooterLine").gameObject.SetActive(true);
			EditorGUILayout.LabelField("Button4 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSPopupMessage.button4GO = EditorGUILayout.ObjectField(iOSPopupMessage.button4GO, typeof(GameObject)) as GameObject;					
			iOSPopupMessage.button4FunctionName = EditorGUILayout.TextField(iOSPopupMessage.button4FunctionName);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Separator();
			iOSPopupMessage.button5Text = EditorGUILayout.TextField("Button5 Text", iOSPopupMessage.button5Text);
			iOSPopupMessage.button5Color = EditorGUILayout.ColorField("Button5 Color", iOSPopupMessage.button5Color);			
			iOSPopupMessage.transform.Find("Window/Buttons/Button5/FooterLine").gameObject.SetActive(false);
			EditorGUILayout.LabelField("Button5 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSPopupMessage.button5GO = EditorGUILayout.ObjectField(iOSPopupMessage.button5GO, typeof(GameObject)) as GameObject;					
			iOSPopupMessage.button5FunctionName = EditorGUILayout.TextField(iOSPopupMessage.button5FunctionName);
			EditorGUILayout.EndHorizontal();
			iOSPopupMessage.transform.Find("Window/Buttons/Button6/FooterLine").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button7/FooterLine").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button8/FooterLine").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button6/Label").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button7/Label").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button8/Label").gameObject.SetActive(false);
		}
		if(iOSPopupMessage.buttonsIndex == 5){
			iOSPopupMessage.button1Text = EditorGUILayout.TextField("Button1 Text", iOSPopupMessage.button1Text);
			iOSPopupMessage.button1Color = EditorGUILayout.ColorField("Button1 Color", iOSPopupMessage.button1Color);			
			iOSPopupMessage.transform.Find("Window/Buttons/Button1/FooterLine").gameObject.SetActive(true);
			EditorGUILayout.LabelField("Button1 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSPopupMessage.button1GO = EditorGUILayout.ObjectField(iOSPopupMessage.button1GO, typeof(GameObject)) as GameObject;					
			iOSPopupMessage.button1FunctionName = EditorGUILayout.TextField(iOSPopupMessage.button1FunctionName);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Separator();
			iOSPopupMessage.button2Text = EditorGUILayout.TextField("Button2 Text", iOSPopupMessage.button2Text);
			iOSPopupMessage.button2Color = EditorGUILayout.ColorField("Button2 Color", iOSPopupMessage.button2Color);			
			iOSPopupMessage.transform.Find("Window/Buttons/Button2/FooterLine").gameObject.SetActive(true);
			EditorGUILayout.LabelField("Button2 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSPopupMessage.button2GO = EditorGUILayout.ObjectField(iOSPopupMessage.button2GO, typeof(GameObject)) as GameObject;					
			iOSPopupMessage.button2FunctionName = EditorGUILayout.TextField(iOSPopupMessage.button2FunctionName);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Separator();
			iOSPopupMessage.button3Text = EditorGUILayout.TextField("Button3 Text", iOSPopupMessage.button3Text);
			iOSPopupMessage.button3Color = EditorGUILayout.ColorField("Button3 Color", iOSPopupMessage.button3Color);			
			iOSPopupMessage.transform.Find("Window/Buttons/Button3/FooterLine").gameObject.SetActive(true);
			EditorGUILayout.LabelField("Button3 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSPopupMessage.button3GO = EditorGUILayout.ObjectField(iOSPopupMessage.button3GO, typeof(GameObject)) as GameObject;					
			iOSPopupMessage.button3FunctionName = EditorGUILayout.TextField(iOSPopupMessage.button3FunctionName);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Separator();
			iOSPopupMessage.button4Text = EditorGUILayout.TextField("Button4 Text", iOSPopupMessage.button4Text);
			iOSPopupMessage.button4Color = EditorGUILayout.ColorField("Button4 Color", iOSPopupMessage.button4Color);			
			iOSPopupMessage.transform.Find("Window/Buttons/Button4/FooterLine").gameObject.SetActive(true);
			EditorGUILayout.LabelField("Button4 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSPopupMessage.button4GO = EditorGUILayout.ObjectField(iOSPopupMessage.button4GO, typeof(GameObject)) as GameObject;					
			iOSPopupMessage.button4FunctionName = EditorGUILayout.TextField(iOSPopupMessage.button4FunctionName);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Separator();
			iOSPopupMessage.button5Text = EditorGUILayout.TextField("Button5 Text", iOSPopupMessage.button5Text);
			iOSPopupMessage.button5Color = EditorGUILayout.ColorField("Button5 Color", iOSPopupMessage.button5Color);			
			iOSPopupMessage.transform.Find("Window/Buttons/Button5/FooterLine").gameObject.SetActive(true);
			EditorGUILayout.LabelField("Button5 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSPopupMessage.button5GO = EditorGUILayout.ObjectField(iOSPopupMessage.button5GO, typeof(GameObject)) as GameObject;					
			iOSPopupMessage.button5FunctionName = EditorGUILayout.TextField(iOSPopupMessage.button5FunctionName);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Separator();
			iOSPopupMessage.button6Text = EditorGUILayout.TextField("Button6 Text", iOSPopupMessage.button6Text);
			iOSPopupMessage.button6Color = EditorGUILayout.ColorField("Button6 Color", iOSPopupMessage.button6Color);			
			iOSPopupMessage.transform.Find("Window/Buttons/Button6/FooterLine").gameObject.SetActive(false);
			EditorGUILayout.LabelField("Button6 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSPopupMessage.button6GO = EditorGUILayout.ObjectField(iOSPopupMessage.button6GO, typeof(GameObject)) as GameObject;					
			iOSPopupMessage.button6FunctionName = EditorGUILayout.TextField(iOSPopupMessage.button6FunctionName);
			EditorGUILayout.EndHorizontal();
			iOSPopupMessage.transform.Find("Window/Buttons/Button7/FooterLine").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button8/FooterLine").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button7/Label").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button8/Label").gameObject.SetActive(false);
		}
		if(iOSPopupMessage.buttonsIndex == 6){
			iOSPopupMessage.button1Text = EditorGUILayout.TextField("Button1 Text", iOSPopupMessage.button1Text);
			iOSPopupMessage.button1Color = EditorGUILayout.ColorField("Button1 Color", iOSPopupMessage.button1Color);			
			iOSPopupMessage.transform.Find("Window/Buttons/Button1/FooterLine").gameObject.SetActive(true);
			EditorGUILayout.LabelField("Button1 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSPopupMessage.button1GO = EditorGUILayout.ObjectField(iOSPopupMessage.button1GO, typeof(GameObject)) as GameObject;					
			iOSPopupMessage.button1FunctionName = EditorGUILayout.TextField(iOSPopupMessage.button1FunctionName);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Separator();
			iOSPopupMessage.button2Text = EditorGUILayout.TextField("Button2 Text", iOSPopupMessage.button2Text);
			iOSPopupMessage.button2Color = EditorGUILayout.ColorField("Button2 Color", iOSPopupMessage.button2Color);			
			iOSPopupMessage.transform.Find("Window/Buttons/Button2/FooterLine").gameObject.SetActive(true);
			EditorGUILayout.LabelField("Button2 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSPopupMessage.button2GO = EditorGUILayout.ObjectField(iOSPopupMessage.button2GO, typeof(GameObject)) as GameObject;					
			iOSPopupMessage.button2FunctionName = EditorGUILayout.TextField(iOSPopupMessage.button2FunctionName);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Separator();
			iOSPopupMessage.button3Text = EditorGUILayout.TextField("Button3 Text", iOSPopupMessage.button3Text);
			iOSPopupMessage.button3Color = EditorGUILayout.ColorField("Button3 Color", iOSPopupMessage.button3Color);			
			iOSPopupMessage.transform.Find("Window/Buttons/Button3/FooterLine").gameObject.SetActive(true);
			EditorGUILayout.LabelField("Button3 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSPopupMessage.button3GO = EditorGUILayout.ObjectField(iOSPopupMessage.button3GO, typeof(GameObject)) as GameObject;					
			iOSPopupMessage.button3FunctionName = EditorGUILayout.TextField(iOSPopupMessage.button3FunctionName);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Separator();
			iOSPopupMessage.button4Text = EditorGUILayout.TextField("Button4 Text", iOSPopupMessage.button4Text);
			iOSPopupMessage.button4Color = EditorGUILayout.ColorField("Button4 Color", iOSPopupMessage.button4Color);			
			iOSPopupMessage.transform.Find("Window/Buttons/Button4/FooterLine").gameObject.SetActive(true);
			EditorGUILayout.LabelField("Button4 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSPopupMessage.button4GO = EditorGUILayout.ObjectField(iOSPopupMessage.button4GO, typeof(GameObject)) as GameObject;					
			iOSPopupMessage.button4FunctionName = EditorGUILayout.TextField(iOSPopupMessage.button4FunctionName);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Separator();
			iOSPopupMessage.button5Text = EditorGUILayout.TextField("Button5 Text", iOSPopupMessage.button5Text);
			iOSPopupMessage.button5Color = EditorGUILayout.ColorField("Button5 Color", iOSPopupMessage.button5Color);			
			iOSPopupMessage.transform.Find("Window/Buttons/Button5/FooterLine").gameObject.SetActive(true);
			EditorGUILayout.LabelField("Button5 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSPopupMessage.button5GO = EditorGUILayout.ObjectField(iOSPopupMessage.button5GO, typeof(GameObject)) as GameObject;					
			iOSPopupMessage.button5FunctionName = EditorGUILayout.TextField(iOSPopupMessage.button5FunctionName);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Separator();
			iOSPopupMessage.button6Text = EditorGUILayout.TextField("Button6 Text", iOSPopupMessage.button6Text);
			iOSPopupMessage.button6Color = EditorGUILayout.ColorField("Button6 Color", iOSPopupMessage.button6Color);			
			iOSPopupMessage.transform.Find("Window/Buttons/Button6/FooterLine").gameObject.SetActive(true);
			EditorGUILayout.LabelField("Button6 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSPopupMessage.button6GO = EditorGUILayout.ObjectField(iOSPopupMessage.button6GO, typeof(GameObject)) as GameObject;					
			iOSPopupMessage.button6FunctionName = EditorGUILayout.TextField(iOSPopupMessage.button6FunctionName);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Separator();
			iOSPopupMessage.button7Text = EditorGUILayout.TextField("Button7 Text", iOSPopupMessage.button7Text);
			iOSPopupMessage.button7Color = EditorGUILayout.ColorField("Button7 Color", iOSPopupMessage.button7Color);			
			iOSPopupMessage.transform.Find("Window/Buttons/Button7/FooterLine").gameObject.SetActive(false);
			EditorGUILayout.LabelField("Button7 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSPopupMessage.button7GO = EditorGUILayout.ObjectField(iOSPopupMessage.button7GO, typeof(GameObject)) as GameObject;					
			iOSPopupMessage.button7FunctionName = EditorGUILayout.TextField(iOSPopupMessage.button7FunctionName);
			EditorGUILayout.EndHorizontal();
			iOSPopupMessage.transform.Find("Window/Buttons/Button8/FooterLine").gameObject.SetActive(false);
			iOSPopupMessage.transform.Find("Window/Buttons/Button8/Label").gameObject.SetActive(false);
		}
		if(iOSPopupMessage.buttonsIndex == 7){
			iOSPopupMessage.button1Text = EditorGUILayout.TextField("Button1 Text", iOSPopupMessage.button1Text);
			iOSPopupMessage.button1Color = EditorGUILayout.ColorField("Button1 Color", iOSPopupMessage.button1Color);			
			iOSPopupMessage.transform.Find("Window/Buttons/Button1/FooterLine").gameObject.SetActive(true);
			EditorGUILayout.LabelField("Button1 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSPopupMessage.button1GO = EditorGUILayout.ObjectField(iOSPopupMessage.button1GO, typeof(GameObject)) as GameObject;					
			iOSPopupMessage.button1FunctionName = EditorGUILayout.TextField(iOSPopupMessage.button1FunctionName);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Separator();
			iOSPopupMessage.button2Text = EditorGUILayout.TextField("Button2 Text", iOSPopupMessage.button2Text);
			iOSPopupMessage.button2Color = EditorGUILayout.ColorField("Button2 Color", iOSPopupMessage.button2Color);			
			iOSPopupMessage.transform.Find("Window/Buttons/Button2/FooterLine").gameObject.SetActive(true);
			EditorGUILayout.LabelField("Button2 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSPopupMessage.button2GO = EditorGUILayout.ObjectField(iOSPopupMessage.button2GO, typeof(GameObject)) as GameObject;					
			iOSPopupMessage.button2FunctionName = EditorGUILayout.TextField(iOSPopupMessage.button2FunctionName);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Separator();
			iOSPopupMessage.button3Text = EditorGUILayout.TextField("Button3 Text", iOSPopupMessage.button3Text);
			iOSPopupMessage.button3Color = EditorGUILayout.ColorField("Button3 Color", iOSPopupMessage.button3Color);			
			iOSPopupMessage.transform.Find("Window/Buttons/Button3/FooterLine").gameObject.SetActive(true);
			EditorGUILayout.LabelField("Button3 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSPopupMessage.button3GO = EditorGUILayout.ObjectField(iOSPopupMessage.button3GO, typeof(GameObject)) as GameObject;					
			iOSPopupMessage.button3FunctionName = EditorGUILayout.TextField(iOSPopupMessage.button3FunctionName);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Separator();
			iOSPopupMessage.button4Text = EditorGUILayout.TextField("Button4 Text", iOSPopupMessage.button4Text);
			iOSPopupMessage.button4Color = EditorGUILayout.ColorField("Button4 Color", iOSPopupMessage.button4Color);			
			iOSPopupMessage.transform.Find("Window/Buttons/Button4/FooterLine").gameObject.SetActive(true);
			EditorGUILayout.LabelField("Button4 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSPopupMessage.button4GO = EditorGUILayout.ObjectField(iOSPopupMessage.button4GO, typeof(GameObject)) as GameObject;					
			iOSPopupMessage.button4FunctionName = EditorGUILayout.TextField(iOSPopupMessage.button4FunctionName);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Separator();
			iOSPopupMessage.button5Text = EditorGUILayout.TextField("Button5 Text", iOSPopupMessage.button5Text);
			iOSPopupMessage.button5Color = EditorGUILayout.ColorField("Button5 Color", iOSPopupMessage.button5Color);			
			iOSPopupMessage.transform.Find("Window/Buttons/Button5/FooterLine").gameObject.SetActive(true);
			EditorGUILayout.LabelField("Button5 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSPopupMessage.button5GO = EditorGUILayout.ObjectField(iOSPopupMessage.button5GO, typeof(GameObject)) as GameObject;					
			iOSPopupMessage.button5FunctionName = EditorGUILayout.TextField(iOSPopupMessage.button5FunctionName);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Separator();
			iOSPopupMessage.button6Text = EditorGUILayout.TextField("Button6 Text", iOSPopupMessage.button6Text);
			iOSPopupMessage.button6Color = EditorGUILayout.ColorField("Button6 Color", iOSPopupMessage.button6Color);			
			iOSPopupMessage.transform.Find("Window/Buttons/Button6/FooterLine").gameObject.SetActive(true);
			EditorGUILayout.LabelField("Button6 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSPopupMessage.button6GO = EditorGUILayout.ObjectField(iOSPopupMessage.button6GO, typeof(GameObject)) as GameObject;					
			iOSPopupMessage.button6FunctionName = EditorGUILayout.TextField(iOSPopupMessage.button6FunctionName);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Separator();
			iOSPopupMessage.button7Text = EditorGUILayout.TextField("Button7 Text", iOSPopupMessage.button7Text);
			iOSPopupMessage.button7Color = EditorGUILayout.ColorField("Button7 Color", iOSPopupMessage.button7Color);			
			iOSPopupMessage.transform.Find("Window/Buttons/Button7/FooterLine").gameObject.SetActive(true);
			EditorGUILayout.LabelField("Button7 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSPopupMessage.button7GO = EditorGUILayout.ObjectField(iOSPopupMessage.button7GO, typeof(GameObject)) as GameObject;					
			iOSPopupMessage.button7FunctionName = EditorGUILayout.TextField(iOSPopupMessage.button7FunctionName);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Separator();
			iOSPopupMessage.button8Text = EditorGUILayout.TextField("Button8 Text", iOSPopupMessage.button8Text);
			iOSPopupMessage.button8Color = EditorGUILayout.ColorField("Button8 Color", iOSPopupMessage.button8Color);			
			iOSPopupMessage.transform.Find("Window/Buttons/Button8/FooterLine").gameObject.SetActive(false);
			EditorGUILayout.LabelField("Button8 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSPopupMessage.button8GO = EditorGUILayout.ObjectField(iOSPopupMessage.button8GO, typeof(GameObject)) as GameObject;					
			iOSPopupMessage.button8FunctionName = EditorGUILayout.TextField(iOSPopupMessage.button8FunctionName);
			EditorGUILayout.EndHorizontal();
		}

		EditorGUILayout.Separator();
		iOSPopupMessage.buttonCancelText = EditorGUILayout.TextField("Cancel Text", iOSPopupMessage.buttonCancelText);
		iOSPopupMessage.buttonCancelColor = EditorGUILayout.ColorField("Cancel Color", iOSPopupMessage.buttonCancelColor);			
		
		/*iOSPopupMessage.transform.FindChild("Window/Buttons").GetComponent<RectTransform>().sizeDelta = new Vector2(540f, 118f + ((iOSPopupMessage.buttonsIndex+1) * 88f));
		
		// Position popup message at bottom of display, dependent on display height and number of buttons on popup
		ScreenWidth = iOSPopupMessage.transform.parent.GetComponent<RectTransform>().rect.width;
		//Debug.Log (ScreenWidth);
		if(ScreenWidth==640 || ScreenWidth==960 || ScreenWidth==1136 || ScreenWidth==1536 || ScreenWidth==2048 || ScreenWidth==750 || ScreenWidth==1334){//iPhone4, 5, 6 & iPad Retina
			iOSPopupMessage.transform.FindChild("Window").GetComponent<RectTransform>().localPosition = new Vector3(0f, ((iOSPopupMessage.transform.FindChild("Window/Buttons").GetComponent<RectTransform>().rect.height + (iOSPopupMessage.transform.parent.GetComponent<RectTransform>().rect.height/2)) + 136) * -1, 0f);
		}
		if(ScreenWidth==768 || ScreenWidth==1024){//iPad1, 2 & Mini
			iOSPopupMessage.transform.FindChild("Window").GetComponent<RectTransform>().localPosition = new Vector3(0f, (((iOSPopupMessage.transform.FindChild("Window/Buttons").GetComponent<RectTransform>().rect.height/2) + (iOSPopupMessage.transform.parent.GetComponent<RectTransform>().rect.height/2)) + 68f) * -1, 0f);
		}
		if(ScreenWidth==1242 || ScreenWidth==2208){//iPhone6+
			iOSPopupMessage.transform.FindChild("Window").GetComponent<RectTransform>().localPosition = new Vector3(0f, ((((iOSPopupMessage.transform.FindChild("Window/Buttons").GetComponent<RectTransform>().rect.height/2) * 3) + (iOSPopupMessage.transform.parent.GetComponent<RectTransform>().rect.height/2)) + 204f) * -1, 0f);
		}*/
		iOSPopupMessage.transform.Find("Window/Buttons/Title/TitleLabel").GetComponent<Text>().text = iOSPopupMessage.titleText;
		iOSPopupMessage.transform.Find("Window/Buttons/Title/TitleLabel").GetComponent<Text>().color = iOSPopupMessage.titleColor;
		iOSPopupMessage.transform.Find("Window/Buttons/Button1/Label").GetComponent<Text>().text = iOSPopupMessage.button1Text;
		iOSPopupMessage.transform.Find("Window/Buttons/Button1/Label").GetComponent<Text>().color = iOSPopupMessage.button1Color;
		iOSPopupMessage.transform.Find("Window/Buttons/Button2/Label").GetComponent<Text>().text = iOSPopupMessage.button2Text;
		iOSPopupMessage.transform.Find("Window/Buttons/Button2/Label").GetComponent<Text>().color = iOSPopupMessage.button2Color;
		iOSPopupMessage.transform.Find("Window/Buttons/Button3/Label").GetComponent<Text>().text = iOSPopupMessage.button3Text;
		iOSPopupMessage.transform.Find("Window/Buttons/Button3/Label").GetComponent<Text>().color = iOSPopupMessage.button3Color;
		iOSPopupMessage.transform.Find("Window/Buttons/Button4/Label").GetComponent<Text>().text = iOSPopupMessage.button4Text;
		iOSPopupMessage.transform.Find("Window/Buttons/Button4/Label").GetComponent<Text>().color = iOSPopupMessage.button4Color;
		iOSPopupMessage.transform.Find("Window/Buttons/Button5/Label").GetComponent<Text>().text = iOSPopupMessage.button5Text;
		iOSPopupMessage.transform.Find("Window/Buttons/Button5/Label").GetComponent<Text>().color = iOSPopupMessage.button5Color;
		iOSPopupMessage.transform.Find("Window/Buttons/Button6/Label").GetComponent<Text>().text = iOSPopupMessage.button6Text;
		iOSPopupMessage.transform.Find("Window/Buttons/Button6/Label").GetComponent<Text>().color = iOSPopupMessage.button6Color;
		iOSPopupMessage.transform.Find("Window/Buttons/Button7/Label").GetComponent<Text>().text = iOSPopupMessage.button7Text;
		iOSPopupMessage.transform.Find("Window/Buttons/Button7/Label").GetComponent<Text>().color = iOSPopupMessage.button7Color;
		iOSPopupMessage.transform.Find("Window/Buttons/Button8/Label").GetComponent<Text>().text = iOSPopupMessage.button8Text;
		iOSPopupMessage.transform.Find("Window/Buttons/Button8/Label").GetComponent<Text>().color = iOSPopupMessage.button8Color;
		iOSPopupMessage.transform.Find("Window/CancelButton/Label").GetComponent<Text>().text = iOSPopupMessage.buttonCancelText;
		iOSPopupMessage.transform.Find("Window/CancelButton/Label").GetComponent<Text>().color = iOSPopupMessage.buttonCancelColor;
									
		EditorUtility.SetDirty(target);
		
	}
	
}