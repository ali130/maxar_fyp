﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using UnityEngine.UI;

[CustomEditor(typeof(iOS_Button))]
public class iOSButtonEditor : Editor 
{
	
	[MenuItem("GameObject/UI/iOS GUI/Button")]
	private static void iOSGUIButton(){
		//Debug.Log ("Button menu item!");
		
		//Check if selected object can have a button added?		
		if(Selection.activeGameObject != null){
			if(Selection.activeGameObject.tag == "rootUIPanel" || Selection.activeGameObject.tag == "UIPanel" || Selection.activeGameObject.tag == "UITabPanel" || Selection.activeGameObject.tag == "TableViewContainer" || Selection.activeGameObject.tag == "TableViewItem"){
				
				GameObject go;
				go = Instantiate(Resources.Load("Prefabs/Button")) as GameObject;
				Undo.RegisterCreatedObjectUndo (go, "Create Button");
				
				go.transform.SetParent(Selection.activeGameObject.transform, true);			
				
				go.transform.SetAsFirstSibling();
				
				go.name = "Button";
				
				if(Selection.activeGameObject.tag == "rootUIPanel" || Selection.activeGameObject.tag == "UIPanel" || Selection.activeGameObject.tag == "UITabPanel"){
					go.GetComponent<RectTransform>().localPosition = new Vector3(0f, 0f, 0f);				
				} else {
					if(Selection.activeGameObject.tag == "TableViewContainer"){
						go.GetComponent<RectTransform>().localPosition = new Vector3(0f, (go.transform.parent.GetComponent<RectTransform>().rect.height/2) - (go.GetComponent<RectTransform>().sizeDelta.y/2), 0f);
					} else {
						go.GetComponent<RectTransform>().localPosition = new Vector3(0f, (go.transform.parent.GetComponent<RectTransform>().rect.height/2) * -1, 0f);
					}	
				}
				go.GetComponent<RectTransform>().sizeDelta = new Vector2(150f, go.GetComponent<RectTransform>().rect.height);
				go.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);
				
				//Select new added object in hierarchy
				Selection.activeGameObject = go;
				
			} else {
				EditorUtility.DisplayDialog("A Button cannot be added to current selection!", "Please select a UIPanel in the Hierarchy panel.", "OK");
			}						
		} else {
			EditorUtility.DisplayDialog("A Button cannot be added.", "Please select a UIPanel in the Hierarchy panel.", "OK");		
		}
	}

	public override void OnInspectorGUI ()
	{
	
		var iOSButton = target as iOS_Button;
		Undo.RecordObject (iOSButton, "Inspector");
		
		iOSButton.buttonText = EditorGUILayout.TextField("Button Text", iOSButton.buttonText);
		iOSButton.buttonColor = EditorGUILayout.ColorField("Button Color", iOSButton.buttonColor);
		iOSButton.showBorder = EditorGUILayout.Toggle("Show Border", iOSButton.showBorder);
		if(iOSButton.showBorder){
			iOSButton.autoBorderWidth = EditorGUILayout.Toggle("Auto Border Width", iOSButton.autoBorderWidth);
			if(iOSButton.autoBorderWidth==false){
				iOSButton.borderWidth = EditorGUILayout.IntField("Border Width", iOSButton.borderWidth);
			}
		}		

		Text TitleText = iOSButton.GetComponent<Text>();
		TitleText.text = iOSButton.buttonText;
		
		TitleText.color = iOSButton.buttonColor;

		GameObject BorderGO = iOSButton.transform.Find("Border").gameObject;
		BorderGO.SetActive(iOSButton.showBorder);
		BorderGO.GetComponent<Image>().color = iOSButton.buttonColor;
		
		BorderGO.GetComponent<RectTransform>().sizeDelta = new Vector2(iOSButton.borderWidth, BorderGO.GetComponent<RectTransform>().rect.height);
		
				
		EditorUtility.SetDirty(target);
		
	}
	
}