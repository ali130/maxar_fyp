﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using UnityEngine.UI;

[CustomEditor(typeof(iOS_UITabPanel))]
public class iOS_UITabPanelEditor : Editor {
	
	public override void OnInspectorGUI ()
	{
		
		var iOSUITabPanel = target as iOS_UITabPanel;
		Undo.RecordObject (iOSUITabPanel, "Inspector");
		
		iOSUITabPanel.PanelColor = EditorGUILayout.ColorField("UIPanel Color", iOSUITabPanel.PanelColor);
		iOSUITabPanel.GetComponent<Image>().color = iOSUITabPanel.PanelColor;
		
		EditorUtility.SetDirty(target);
		
	}
}
