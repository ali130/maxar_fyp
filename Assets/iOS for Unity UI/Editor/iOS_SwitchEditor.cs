﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using UnityEngine.UI;

[CustomEditor(typeof(iOS_Switch))]
public class iOS_SwitchEditor : Editor 
{
	
//	string[] tabs =  new[] { "1", "2", "3", "4", "5" };
	
	[MenuItem("GameObject/UI/iOS GUI/Switch")]
	private static void iOSGUISwitch(){
		//Debug.Log ("Switch menu item!");
		
		//Check if selected object can have a button added?		
		if(Selection.activeGameObject != null){
			if(Selection.activeGameObject.tag == "rootUIPanel" || Selection.activeGameObject.tag == "UIPanel" || Selection.activeGameObject.tag == "UITabPanel" || Selection.activeGameObject.tag == "TableViewContainer" || Selection.activeGameObject.tag == "TableViewItem"){
				
				GameObject go;
				go = Instantiate(Resources.Load("Prefabs/Switch")) as GameObject;
				Undo.RegisterCreatedObjectUndo (go, "Create Switch");
				
				go.transform.SetParent(Selection.activeGameObject.transform, true);			
				
				go.name = "Switch";
				
				if(Selection.activeGameObject.tag == "rootUIPanel" || Selection.activeGameObject.tag == "UIPanel" || Selection.activeGameObject.tag == "UITabPanel"){
					go.GetComponent<RectTransform>().localPosition = new Vector3(0f, 0f, 0f);				
				} else {
					if(Selection.activeGameObject.tag == "TableViewContainer"){
						go.GetComponent<RectTransform>().localPosition = new Vector3(0f, (go.transform.parent.GetComponent<RectTransform>().rect.height/2) - (go.GetComponent<RectTransform>().sizeDelta.y/2), 0f);
					} else {
						go.GetComponent<RectTransform>().localPosition = new Vector3(0f, (go.transform.parent.GetComponent<RectTransform>().rect.height/2) * -1, 0f);
					}	
				}
				go.GetComponent<RectTransform>().sizeDelta = new Vector2(100f, go.GetComponent<RectTransform>().rect.height);
				go.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);
				
				//Select new added object in hierarchy
				Selection.activeGameObject = go;
				
			} else {
				EditorUtility.DisplayDialog("A Switch cannot be added to current selection!", "Please select a UIPanel in the Hierarchy panel.", "OK");
			}						
		} else {
			EditorUtility.DisplayDialog("A Switch cannot be added to current selection!", "Please select a UIPanel in the Hierarchy panel.", "OK");
		}						
		
	}

	public override void OnInspectorGUI ()
	{
	
		var iOSSwitch = target as iOS_Switch;
		Undo.RecordObject (iOSSwitch, "Inspector");
		
		iOSSwitch.interactable = EditorGUILayout.Toggle("Interactable", iOSSwitch.interactable);
		iOSSwitch.isOn = EditorGUILayout.Toggle("Is On", iOSSwitch.isOn);
		iOSSwitch.OnStateText = EditorGUILayout.TextField("On State Text", iOSSwitch.OnStateText);
		iOSSwitch.OffStateText = EditorGUILayout.TextField("Off State Text", iOSSwitch.OffStateText);
		
				
		iOSSwitch.GetComponent<Toggle>().interactable = iOSSwitch.interactable;
		if (EditorApplication.isPlaying){
			iOSSwitch.isOn = iOSSwitch.GetComponent<Toggle>().isOn;
		} else {
			iOSSwitch.GetComponent<Toggle>().isOn = iOSSwitch.isOn;
		}	
		EditorUtility.SetDirty(target);
		
	}
	
}