﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using UnityEngine.UI;

[CustomEditor(typeof(iOS_ToolBar))]
public class iOS_ToolBarEditor : Editor 
{
	
	string[] icons =  new[] { "1", "2", "3", "4", "5" };
	
	[MenuItem("GameObject/UI/iOS GUI/Tool Bar")]
	private static void iOSGUIToolBar(){
		//Debug.Log ("Tool Bar menu item!");
		//Check if Canvas exists?
		Canvas canvas = FindObjectOfType(typeof(Canvas)) as Canvas;
		if (canvas){
			//Debug.Log("Canvas object found: " + canvas.name);
			canvas.pixelPerfect = true;
			canvas.GetComponent<CanvasScaler>().uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
			if(canvas.GetComponent<CanvasHandler>() == null){
				canvas.gameObject.AddComponent<CanvasHandler>();
			}
		} else {
			//Debug.Log("No Canvas object could be found");
			EditorApplication.ExecuteMenuItem("GameObject/UI/Canvas");
			canvas = FindObjectOfType(typeof(Canvas)) as Canvas;
			canvas.pixelPerfect = true;
			canvas.GetComponent<CanvasScaler>().uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
			if(canvas.GetComponent<CanvasHandler>() == null){
				canvas.gameObject.AddComponent<CanvasHandler>();
			}
		}	
		
		//Check if Panel exists?
		iOS_UIPanel panel = FindObjectOfType(typeof(iOS_UIPanel)) as iOS_UIPanel;
		if (panel){ //Panel already exists, do nothing...
			//Debug.Log("Panel object found: " + panel.name);
			if(Selection.activeGameObject == null){ //if no object selected, make panel found the selected object
				Selection.activeGameObject = panel.gameObject;
			}
		} else { //Create Panel
			//Debug.Log("No Panel object could be found");
			EditorApplication.ExecuteMenuItem("GameObject/UI/iOS GUI/UIPanel");
			panel = FindObjectOfType(typeof(iOS_UIPanel)) as iOS_UIPanel;
			Selection.activeGameObject = panel.gameObject;
		}
		
		//Check if selected object is a Panel		
		bool bComponentExists=false;
		if(Selection.activeGameObject.tag == "rootUIPanel" || Selection.activeGameObject.tag == "UIPanel"){
			//OK its a panel, now check if Tab Bar already on panel		
			foreach (Transform child in Selection.activeGameObject.transform)
			{
				if(child.tag == "ToolBar"){
					bComponentExists=true;
				}
			}			
			if(bComponentExists){
				EditorUtility.DisplayDialog("Cannot add Tool Bar!", "A Tool Bar already exists on the selected UIPanel.", "OK");
			}
						
			if(!bComponentExists){
				foreach (Transform child in Selection.activeGameObject.transform)
				{
					if(child.tag == "TabBar"){
						bComponentExists=true;
					}
				}	
				if(bComponentExists){
					EditorUtility.DisplayDialog("Cannot add Tool Bar!", "A Tab Bar already exists on the selected UIPanel.", "OK");
				}
			}		
			
			if(!bComponentExists){
				GameObject go;
				go = Instantiate(Resources.Load("Prefabs/Tool Bar")) as GameObject;
				Undo.RegisterCreatedObjectUndo (go, "Create Tool Bar");
				
				if(Selection.activeGameObject != null){
					if(Selection.activeGameObject.tag == "rootUIPanel" || Selection.activeGameObject.tag == "UIPanel"){
						go.transform.SetParent(Selection.activeGameObject.transform, false);
					} else {
						go.transform.SetParent(panel.transform, false);
					}
				} else {
					go.transform.SetParent(panel.transform, false);
				}
				
				go.name = "Tool Bar";
				
				go.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
				go.GetComponent<RectTransform>().sizeDelta = new Vector2(0f,go.GetComponent<RectTransform>().sizeDelta.y);
				go.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);	
					
				//Select new added object in hierarchy
				Selection.activeGameObject = go;
				
//				EditorUtility.DisplayDialog("Information!", "Move the newly added Tab Bar to the top of the hierarchy within the UIPanel.", "OK");
			}
		} else {
			if(Selection.activeGameObject.tag == "UITabPanel"){
				EditorUtility.DisplayDialog("Cannot add Tool Bar to a UITabPanel!", "You need to select a UIPanel.", "OK");
			} else {
				EditorUtility.DisplayDialog("Tool Bar cannot be added to current selection!", "You need to select a UIPanel.", "OK");
			}
		}						
					
	}

	public override void OnInspectorGUI ()
	{
	
		var iOSToolbar = target as iOS_ToolBar;
		Undo.RecordObject (iOSToolbar, "Inspector");
		
		//Debug.Log (iOSToolbar.selectedTabIndex);
		
		iOSToolbar.iconsIndex = EditorGUILayout.Popup("Number of Icons", iOSToolbar.iconsIndex, icons);
		iOSToolbar.IconsColor = EditorGUILayout.ColorField("Icons Color", iOSToolbar.IconsColor);
        iOSToolbar.spacing = EditorGUILayout.IntField("Spacing", iOSToolbar.spacing);
        EditorGUILayout.Separator();
		
		GameObject Icon1GO = iOSToolbar.transform.Find("Icon1").gameObject;
		Icon1GO.SetActive(false);
		
		GameObject Icon2GO = iOSToolbar.transform.Find("Icon2").gameObject;
		Icon2GO.SetActive(false);
		
		GameObject Icon3GO = iOSToolbar.transform.Find("Icon3").gameObject;
		Icon3GO.SetActive(false);
		
		GameObject Icon4GO = iOSToolbar.transform.Find("Icon4").gameObject;
		Icon4GO.SetActive(false);
		
		GameObject Icon5GO = iOSToolbar.transform.Find("Icon5").gameObject;
		Icon5GO.SetActive(false);

		Icon1GO.GetComponent<Image>().color = iOSToolbar.IconsColor;
		Icon1GO.GetComponent<Image>().sprite = iOSToolbar.icon1Image;
		Icon2GO.GetComponent<Image>().color = iOSToolbar.IconsColor;
		Icon2GO.GetComponent<Image>().sprite = iOSToolbar.icon2Image;
		Icon3GO.GetComponent<Image>().color = iOSToolbar.IconsColor;
		Icon3GO.GetComponent<Image>().sprite = iOSToolbar.icon3Image;
		Icon4GO.GetComponent<Image>().color = iOSToolbar.IconsColor;
		Icon4GO.GetComponent<Image>().sprite = iOSToolbar.icon4Image;
		Icon5GO.GetComponent<Image>().color = iOSToolbar.IconsColor;
		Icon5GO.GetComponent<Image>().sprite = iOSToolbar.icon5Image;
		
		//Debug.Log (icons[iOSToolbar.iconsIndex]);
		
		//Defines the number of icons in the TabPanel
		if(icons[iOSToolbar.iconsIndex] == "1"){
			Icon1GO.SetActive(true);
			Icon2GO.SetActive(false);
			Icon3GO.SetActive(false);
			Icon4GO.SetActive(false);
			Icon5GO.SetActive(false);			

			Icon1GO.GetComponent<RectTransform>().localPosition = new Vector3(0, Icon1GO.GetComponent<RectTransform>().localPosition.y, Icon1GO.GetComponent<RectTransform>().localPosition.z);	

			iOSToolbar.icon1Image = (Sprite) EditorGUILayout.ObjectField("Icon1 Image", iOSToolbar.icon1Image, typeof(Sprite), true);			
			EditorGUILayout.LabelField("Button1 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSToolbar.button1GO = EditorGUILayout.ObjectField(iOSToolbar.button1GO, typeof(GameObject)) as GameObject;					
			iOSToolbar.button1FunctionName = EditorGUILayout.TextField(iOSToolbar.button1FunctionName);
			EditorGUILayout.EndHorizontal();
			
		}

		if(icons[iOSToolbar.iconsIndex] == "2"){
			Icon1GO.SetActive(true);
			Icon2GO.SetActive(true);
			Icon3GO.SetActive(false);
			Icon4GO.SetActive(false);
			Icon5GO.SetActive(false);
			
			Icon1GO.GetComponent<RectTransform>().localPosition = new Vector3(-160, Icon1GO.GetComponent<RectTransform>().localPosition.y, Icon1GO.GetComponent<RectTransform>().localPosition.z);	
			Icon2GO.GetComponent<RectTransform>().localPosition = new Vector3(160, Icon2GO.GetComponent<RectTransform>().localPosition.y, Icon2GO.GetComponent<RectTransform>().localPosition.z);		

			iOSToolbar.icon1Image = (Sprite) EditorGUILayout.ObjectField("Icon1 Image", iOSToolbar.icon1Image, typeof(Sprite), true);			
			EditorGUILayout.LabelField("Button1 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSToolbar.button1GO = EditorGUILayout.ObjectField(iOSToolbar.button1GO, typeof(GameObject)) as GameObject;					
			iOSToolbar.button1FunctionName = EditorGUILayout.TextField(iOSToolbar.button1FunctionName);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Separator();
			iOSToolbar.icon2Image = (Sprite) EditorGUILayout.ObjectField("Icon2 Image", iOSToolbar.icon2Image, typeof(Sprite), true);			
			EditorGUILayout.LabelField("Button2 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSToolbar.button2GO = EditorGUILayout.ObjectField(iOSToolbar.button2GO, typeof(GameObject)) as GameObject;					
			iOSToolbar.button2FunctionName = EditorGUILayout.TextField(iOSToolbar.button2FunctionName);
			EditorGUILayout.EndHorizontal();
			
		}
		if(icons[iOSToolbar.iconsIndex] == "3"){
			Icon1GO.SetActive(true);
			Icon2GO.SetActive(true);
			Icon3GO.SetActive(true);
			Icon4GO.SetActive(false);
			Icon5GO.SetActive(false);			

			Icon1GO.GetComponent<RectTransform>().localPosition = new Vector3(-213, Icon1GO.GetComponent<RectTransform>().localPosition.y, Icon1GO.GetComponent<RectTransform>().localPosition.z);	
			Icon2GO.GetComponent<RectTransform>().localPosition = new Vector3(0, Icon2GO.GetComponent<RectTransform>().localPosition.y, Icon2GO.GetComponent<RectTransform>().localPosition.z);		
			Icon3GO.GetComponent<RectTransform>().localPosition = new Vector3(213, Icon3GO.GetComponent<RectTransform>().localPosition.y, Icon3GO.GetComponent<RectTransform>().localPosition.z);		

			iOSToolbar.icon1Image = (Sprite) EditorGUILayout.ObjectField("Icon1 Image", iOSToolbar.icon1Image, typeof(Sprite), true);			
			EditorGUILayout.LabelField("Button1 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSToolbar.button1GO = EditorGUILayout.ObjectField(iOSToolbar.button1GO, typeof(GameObject)) as GameObject;					
			iOSToolbar.button1FunctionName = EditorGUILayout.TextField(iOSToolbar.button1FunctionName);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Separator();
			iOSToolbar.icon2Image = (Sprite) EditorGUILayout.ObjectField("Icon2 Image", iOSToolbar.icon2Image, typeof(Sprite), true);			
			EditorGUILayout.LabelField("Button2 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSToolbar.button2GO = EditorGUILayout.ObjectField(iOSToolbar.button2GO, typeof(GameObject)) as GameObject;					
			iOSToolbar.button2FunctionName = EditorGUILayout.TextField(iOSToolbar.button2FunctionName);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Separator();
			iOSToolbar.icon3Image = (Sprite) EditorGUILayout.ObjectField("Icon3 Image", iOSToolbar.icon3Image, typeof(Sprite), true);			
			EditorGUILayout.LabelField("Button3 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSToolbar.button3GO = EditorGUILayout.ObjectField(iOSToolbar.button3GO, typeof(GameObject)) as GameObject;					
			iOSToolbar.button3FunctionName = EditorGUILayout.TextField(iOSToolbar.button3FunctionName);
			EditorGUILayout.EndHorizontal();
			
		}
		if(icons[iOSToolbar.iconsIndex] == "4"){
			Icon1GO.SetActive(true);
			Icon2GO.SetActive(true);
			Icon3GO.SetActive(true);
			Icon4GO.SetActive(true);
			Icon5GO.SetActive(false);			

			Icon1GO.GetComponent<RectTransform>().localPosition = new Vector3(-240, Icon1GO.GetComponent<RectTransform>().localPosition.y, Icon1GO.GetComponent<RectTransform>().localPosition.z);	
			Icon2GO.GetComponent<RectTransform>().localPosition = new Vector3(-80, Icon2GO.GetComponent<RectTransform>().localPosition.y, Icon2GO.GetComponent<RectTransform>().localPosition.z);		
			Icon3GO.GetComponent<RectTransform>().localPosition = new Vector3(80, Icon3GO.GetComponent<RectTransform>().localPosition.y, Icon3GO.GetComponent<RectTransform>().localPosition.z);		
			Icon4GO.GetComponent<RectTransform>().localPosition = new Vector3(240, Icon4GO.GetComponent<RectTransform>().localPosition.y, Icon4GO.GetComponent<RectTransform>().localPosition.z);		

			iOSToolbar.icon1Image = (Sprite) EditorGUILayout.ObjectField("Icon1 Image", iOSToolbar.icon1Image, typeof(Sprite), true);			
			EditorGUILayout.LabelField("Button1 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSToolbar.button1GO = EditorGUILayout.ObjectField(iOSToolbar.button1GO, typeof(GameObject)) as GameObject;					
			iOSToolbar.button1FunctionName = EditorGUILayout.TextField(iOSToolbar.button1FunctionName);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Separator();
			iOSToolbar.icon2Image = (Sprite) EditorGUILayout.ObjectField("Icon2 Image", iOSToolbar.icon2Image, typeof(Sprite), true);			
			EditorGUILayout.LabelField("Button2 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSToolbar.button2GO = EditorGUILayout.ObjectField(iOSToolbar.button2GO, typeof(GameObject)) as GameObject;					
			iOSToolbar.button2FunctionName = EditorGUILayout.TextField(iOSToolbar.button2FunctionName);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Separator();
			iOSToolbar.icon3Image = (Sprite) EditorGUILayout.ObjectField("Icon3 Image", iOSToolbar.icon3Image, typeof(Sprite), true);			
			EditorGUILayout.LabelField("Button3 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSToolbar.button3GO = EditorGUILayout.ObjectField(iOSToolbar.button3GO, typeof(GameObject)) as GameObject;					
			iOSToolbar.button3FunctionName = EditorGUILayout.TextField(iOSToolbar.button3FunctionName);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Separator();
			iOSToolbar.icon4Image = (Sprite) EditorGUILayout.ObjectField("Icon4 Image", iOSToolbar.icon4Image, typeof(Sprite), true);			
			EditorGUILayout.LabelField("Button4 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSToolbar.button4GO = EditorGUILayout.ObjectField(iOSToolbar.button4GO, typeof(GameObject)) as GameObject;					
			iOSToolbar.button4FunctionName = EditorGUILayout.TextField(iOSToolbar.button4FunctionName);
			EditorGUILayout.EndHorizontal();
			
		}
		if(icons[iOSToolbar.iconsIndex] == "5"){
			Icon1GO.SetActive(true);
			Icon2GO.SetActive(true);
			Icon3GO.SetActive(true);
			Icon4GO.SetActive(true);
			Icon5GO.SetActive(true);			

			Icon1GO.GetComponent<RectTransform>().localPosition = new Vector3(-256, Icon1GO.GetComponent<RectTransform>().localPosition.y, Icon1GO.GetComponent<RectTransform>().localPosition.z);	
			Icon2GO.GetComponent<RectTransform>().localPosition = new Vector3(-128, Icon2GO.GetComponent<RectTransform>().localPosition.y, Icon2GO.GetComponent<RectTransform>().localPosition.z);		
			Icon3GO.GetComponent<RectTransform>().localPosition = new Vector3(0, Icon3GO.GetComponent<RectTransform>().localPosition.y, Icon3GO.GetComponent<RectTransform>().localPosition.z);		
			Icon4GO.GetComponent<RectTransform>().localPosition = new Vector3(128, Icon4GO.GetComponent<RectTransform>().localPosition.y, Icon4GO.GetComponent<RectTransform>().localPosition.z);		
			Icon5GO.GetComponent<RectTransform>().localPosition = new Vector3(256, Icon5GO.GetComponent<RectTransform>().localPosition.y, Icon5GO.GetComponent<RectTransform>().localPosition.z);		

			iOSToolbar.icon1Image = (Sprite) EditorGUILayout.ObjectField("Icon1 Image", iOSToolbar.icon1Image, typeof(Sprite), true);			
			EditorGUILayout.LabelField("Button1 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSToolbar.button1GO = EditorGUILayout.ObjectField(iOSToolbar.button1GO, typeof(GameObject)) as GameObject;					
			iOSToolbar.button1FunctionName = EditorGUILayout.TextField(iOSToolbar.button1FunctionName);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Separator();
			iOSToolbar.icon2Image = (Sprite) EditorGUILayout.ObjectField("Icon2 Image", iOSToolbar.icon2Image, typeof(Sprite), true);			
			EditorGUILayout.LabelField("Button2 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSToolbar.button2GO = EditorGUILayout.ObjectField(iOSToolbar.button2GO, typeof(GameObject)) as GameObject;					
			iOSToolbar.button2FunctionName = EditorGUILayout.TextField(iOSToolbar.button2FunctionName);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Separator();
			iOSToolbar.icon3Image = (Sprite) EditorGUILayout.ObjectField("Icon3 Image", iOSToolbar.icon3Image, typeof(Sprite), true);			
			EditorGUILayout.LabelField("Button3 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSToolbar.button3GO = EditorGUILayout.ObjectField(iOSToolbar.button3GO, typeof(GameObject)) as GameObject;					
			iOSToolbar.button3FunctionName = EditorGUILayout.TextField(iOSToolbar.button3FunctionName);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Separator();
			iOSToolbar.icon4Image = (Sprite) EditorGUILayout.ObjectField("Icon4 Image", iOSToolbar.icon4Image, typeof(Sprite), true);			
			EditorGUILayout.LabelField("Button4 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSToolbar.button4GO = EditorGUILayout.ObjectField(iOSToolbar.button4GO, typeof(GameObject)) as GameObject;					
			iOSToolbar.button4FunctionName = EditorGUILayout.TextField(iOSToolbar.button4FunctionName);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Separator();
			iOSToolbar.icon5Image = (Sprite) EditorGUILayout.ObjectField("Icon5 Image", iOSToolbar.icon5Image, typeof(Sprite), true);			
			EditorGUILayout.LabelField("Button5 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSToolbar.button5GO = EditorGUILayout.ObjectField(iOSToolbar.button5GO, typeof(GameObject)) as GameObject;					
			iOSToolbar.button5FunctionName = EditorGUILayout.TextField(iOSToolbar.button5FunctionName);
			EditorGUILayout.EndHorizontal();
			
		}

		EditorUtility.SetDirty(target);
		
	}
	
	
}