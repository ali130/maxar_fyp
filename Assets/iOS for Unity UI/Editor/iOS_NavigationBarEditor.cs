﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using UnityEngine.UI;

[CustomEditor(typeof(iOS_NavigationBar))]
public class iOS_NavigationBarEditor : Editor 
{
	
	[MenuItem("GameObject/UI/iOS GUI/Navigation Bar")]
	private static void iOSGUINavigationBar(){
		//Debug.Log ("Navigation Bar menu item!");
		
		//Check if Canvas exists?
		Canvas canvas = FindObjectOfType(typeof(Canvas)) as Canvas;
		if (canvas){
			//Debug.Log("Canvas object found: " + canvas.name);
			canvas.pixelPerfect = true;
			canvas.GetComponent<CanvasScaler>().uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
			if(canvas.GetComponent<CanvasHandler>() == null){
				canvas.gameObject.AddComponent<CanvasHandler>();
			}
		} else {
			//Debug.Log("No Canvas object could be found");
			EditorApplication.ExecuteMenuItem("GameObject/UI/Canvas");
			canvas = FindObjectOfType(typeof(Canvas)) as Canvas;
			canvas.pixelPerfect = true;
			canvas.GetComponent<CanvasScaler>().uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
			if(canvas.GetComponent<CanvasHandler>() == null){
				canvas.gameObject.AddComponent<CanvasHandler>();
			}
		}	

		//Check if Panel exists?
		iOS_UIPanel panel = FindObjectOfType(typeof(iOS_UIPanel)) as iOS_UIPanel;
		if (panel){ //Panel already exists, do nothing...
			//Debug.Log("Panel object found: " + panel.name);
			if(Selection.activeGameObject == null){ //if no object selected, make panel found the selected object
				Selection.activeGameObject = panel.gameObject;
			}
		} else { //Create Panel
			//Debug.Log("No Panel object could be found");
			EditorApplication.ExecuteMenuItem("GameObject/UI/iOS GUI/UIPanel");
			panel = FindObjectOfType(typeof(iOS_UIPanel)) as iOS_UIPanel;
			Selection.activeGameObject = panel.gameObject;
		}

		//Check if selected object is a Panel		
		bool bComponentExists=false;
		if(Selection.activeGameObject.tag == "rootUIPanel" || Selection.activeGameObject.tag == "UIPanel" || Selection.activeGameObject.tag == "UITabPanel"){
			//OK its a panel, now check if Navigation Bar already on panel		
			foreach (Transform child in Selection.activeGameObject.transform)
			{
				if(child.tag == "NavigationBar"){
					bComponentExists=true;
				}
			}			
			
			if(bComponentExists){
				EditorUtility.DisplayDialog("Cannot add Navigation Bar!", "A Navigation Bar already exists on the selected UIPanel.", "OK");
				//Debug.Log ("<color=red>WARNING:</color> Cannot add Navigation Bar, a Navigation Bar already exists on the selected UIPanel.");
			} 
			
			if(!bComponentExists){
				//OK its a panel, now check if Tab Bar already on panel		
				foreach (Transform child in Selection.activeGameObject.transform)
				{
					if(child.tag == "TabBar"){
						bComponentExists=true;
					}
				}			
				if(bComponentExists){
					EditorUtility.DisplayDialog("Cannot add Navigation Bar!", "A Tab Bar exists on the selected UIPanel.\n\nInstead add a Navigation Bar to one of the Tab Bar panels.", "OK");
				}
			}
				
			if(!bComponentExists){
				GameObject go;
				go = Instantiate(Resources.Load("Prefabs/Navigation Bar")) as GameObject;				
				Undo.RegisterCreatedObjectUndo (go, "Create Navigation Bar");
				
				if(Selection.activeGameObject != null){
					if(Selection.activeGameObject.tag == "rootUIPanel" || Selection.activeGameObject.tag == "UIPanel" || Selection.activeGameObject.tag == "UITabPanel"){
						go.transform.SetParent(Selection.activeGameObject.transform);
					} else {
						go.transform.parent = panel.transform;
					}
				} else {
					go.transform.parent = panel.transform;
				}
				
				go.name = "Navigation Bar";
						
				go.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
				go.GetComponent<RectTransform>().sizeDelta = new Vector2(0f,go.GetComponent<RectTransform>().sizeDelta.y);
				go.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);					
				
				Selection.activeGameObject = go;
			}
		} else {
			EditorUtility.DisplayDialog("Navigation Bar cannot be added to current selection!", "You need to select a UIPanel or UITabPanel.", "OK");
//			Debug.Log ("<color=red>WARNING:</color> Navigation Bar cannot be added to current selection, you need to select a UIPanel.");
		}					
	}
		
	public override void OnInspectorGUI ()
	{
	
		var iOSNavigationBar = target as iOS_NavigationBar;
		Undo.RecordObject (iOSNavigationBar, "Inspector");
		
		iOSNavigationBar.showLeftImage = EditorGUILayout.Toggle("Show Left Image", iOSNavigationBar.showLeftImage);
		if(iOSNavigationBar.showLeftImage==true)
		{
			iOSNavigationBar.LeftImageColor = EditorGUILayout.ColorField("Left Image Color", iOSNavigationBar.LeftImageColor);
			
			iOSNavigationBar.LeftImage = EditorGUILayout.ObjectField("Left Image", iOSNavigationBar.LeftImage, typeof(Sprite),true) as Sprite;
			iOSNavigationBar.ConnectedPanel = EditorGUILayout.ObjectField("Connected Panel", iOSNavigationBar.ConnectedPanel, typeof(GameObject),true) as GameObject;
			EditorGUILayout.LabelField("Left Image & Text OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSNavigationBar.LeftGO = EditorGUILayout.ObjectField(iOSNavigationBar.LeftGO, typeof(GameObject)) as GameObject;					
			iOSNavigationBar.LeftFunctionName = EditorGUILayout.TextField(iOSNavigationBar.LeftFunctionName);
			EditorGUILayout.EndHorizontal();
		}
		EditorGUILayout.Separator();
		
		iOSNavigationBar.showLeftText = EditorGUILayout.Toggle("Show Left Text", iOSNavigationBar.showLeftText);
		if(iOSNavigationBar.showLeftText==true)
		{
			iOSNavigationBar.LeftText = EditorGUILayout.TextField("Left Text", iOSNavigationBar.LeftText);
			iOSNavigationBar.LeftTextColor = EditorGUILayout.ColorField("Left Text Color", iOSNavigationBar.LeftTextColor);
		}
		EditorGUILayout.Separator();
		
		iOSNavigationBar.showTitleText = EditorGUILayout.Toggle("Show Title Text", iOSNavigationBar.showTitleText);
		if(iOSNavigationBar.showTitleText==true)
		{
			iOSNavigationBar.TitleText = EditorGUILayout.TextField("Title Text", iOSNavigationBar.TitleText);
			iOSNavigationBar.TitleTextColor = EditorGUILayout.ColorField("Title Text Color", iOSNavigationBar.TitleTextColor);
		}
		EditorGUILayout.Separator();
		
		iOSNavigationBar.showRightText = EditorGUILayout.Toggle("Show Right Text", iOSNavigationBar.showRightText);
		if(iOSNavigationBar.showRightText==true)
		{
			iOSNavigationBar.RightText = EditorGUILayout.TextField("Right Text", iOSNavigationBar.RightText);
			iOSNavigationBar.RightTextColor = EditorGUILayout.ColorField("Right Text Color", iOSNavigationBar.RightTextColor);
		}
		EditorGUILayout.Separator();
		
		iOSNavigationBar.showRightImage = EditorGUILayout.Toggle("Show Right Image", iOSNavigationBar.showRightImage);
		if(iOSNavigationBar.showRightImage==true)
		{
			iOSNavigationBar.RightImageColor = EditorGUILayout.ColorField("Right Image Color", iOSNavigationBar.RightImageColor);
			iOSNavigationBar.RightImage = EditorGUILayout.ObjectField("Right Image", iOSNavigationBar.RightImage, typeof(Sprite),true) as Sprite;
		}
		EditorGUILayout.Separator();
		
		
		GameObject LeftImage = iOSNavigationBar.transform.Find("LeftImage").gameObject;
		LeftImage.SetActive(iOSNavigationBar.showLeftImage);
		LeftImage.GetComponent<Image>().color = iOSNavigationBar.LeftImageColor;
		LeftImage.GetComponent<Image>().sprite = iOSNavigationBar.LeftImage;
																								
		GameObject LeftTextGO = iOSNavigationBar.transform.Find("LeftText").gameObject;
		LeftTextGO.SetActive(iOSNavigationBar.showLeftText);

		Text LeftText = LeftTextGO.GetComponent<Text>();
		LeftText.text = iOSNavigationBar.LeftText;
		LeftText.color = iOSNavigationBar.LeftTextColor;
		
		GameObject TitleTextGO = iOSNavigationBar.transform.Find("TitleText").gameObject;
		TitleTextGO.SetActive(iOSNavigationBar.showTitleText);
		Text TitleText = TitleTextGO.GetComponent<Text>();
		TitleText.text = iOSNavigationBar.TitleText;
		TitleText.color = iOSNavigationBar.TitleTextColor;
		
		GameObject RightTextGO = iOSNavigationBar.transform.Find("RightText").gameObject;
		RightTextGO.SetActive(iOSNavigationBar.showRightText);
		Text RightText = RightTextGO.GetComponent<Text>();
		RightText.text = iOSNavigationBar.RightText;
		RightText.color = iOSNavigationBar.RightTextColor;
		
		GameObject RightImage = iOSNavigationBar.transform.Find("RightImage").gameObject;
		RightImage.SetActive(iOSNavigationBar.showRightImage);
		RightImage.GetComponent<Image>().color = iOSNavigationBar.RightImageColor;
		RightImage.GetComponent<Image>().sprite = iOSNavigationBar.RightImage;
		
		EditorUtility.SetDirty(target);
		
	}
	
}