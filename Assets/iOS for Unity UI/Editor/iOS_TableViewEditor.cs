﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using UnityEngine.UI;

[CustomEditor(typeof(iOS_TableView))]
public class iOS_TableViewEditor : Editor 
{
		
	[MenuItem("GameObject/UI/iOS GUI/Table View")]
	private static void iOSTableView(){
		//Debug.Log ("Table View menu item!");
		//Check if Canvas exists?
		Canvas canvas = FindObjectOfType(typeof(Canvas)) as Canvas;
		if (canvas){ //Canvas already exists, do nothing...
			//Debug.Log("Canvas object found: " + canvas.name);
		} else { //Create Canvas
			//Debug.Log("No Canvas object could be found");
			EditorApplication.ExecuteMenuItem("GameObject/UI/Canvas");
			canvas = FindObjectOfType(typeof(Canvas)) as Canvas;
			canvas.pixelPerfect = true;
		}	
		
		
		//Check if Panel exists?
		iOS_UIPanel panel = FindObjectOfType(typeof(iOS_UIPanel)) as iOS_UIPanel;
		if (panel){ //Panel already exists, do nothing...
			//Debug.Log("Panel object found: " + panel.name);
			if(Selection.activeGameObject == null){ //if no object selected, make panel found the selected object
				Selection.activeGameObject = panel.gameObject;
			}
		} else { //Create Panel
			//Debug.Log("No Panel object could be found");
			EditorApplication.ExecuteMenuItem("GameObject/UI/iOS GUI/UIPanel");
			panel = FindObjectOfType(typeof(iOS_UIPanel)) as iOS_UIPanel;
			Selection.activeGameObject = panel.gameObject;
		}

		
		//Check if selected object is a Panel		
		bool bComponentExists=false;
		if(Selection.activeGameObject.tag == "rootUIPanel" || Selection.activeGameObject.tag == "UIPanel" || Selection.activeGameObject.tag == "UITabPanel" || Selection.activeGameObject.tag == "UISegmentedPanel"){

			//OK its a panel, now check if Tableview already on panel		
			foreach (Transform child in Selection.activeGameObject.transform)
			{
				if(child.tag == "TableView"){
					bComponentExists=true;
				}
			}			
			if(bComponentExists){
				EditorUtility.DisplayDialog("Cannot add TableView!", "A TableView already exists on the selected UIPanel.", "OK");
				//Debug.Log ("<color=red>WARNING:</color> Cannot add TableView, a TableView already exists on the selected UIPanel.");
			}
			
			if(!bComponentExists){				
				//OK its a panel, now check if Tab Bar already on panel		
				foreach (Transform child in Selection.activeGameObject.transform)
				{
					if(child.tag == "TabBar"){
						bComponentExists=true;
					}
				}			
				if(bComponentExists){
					EditorUtility.DisplayDialog("Cannot add TableView!", "A Tab Bar exists on the selected UIPanel.\n\nInstead add TableView to one of the Tab Bar panels.", "OK");
					//Debug.Log ("<color=red>WARNING:</color> Cannot add Tab Bar, a Tab Bar already exists on the selected UIPanel.");
				}								
			}
			
			if(!bComponentExists){				
				//OK its a panel, now check if Tab Bar already on panel		
				foreach (Transform child in Selection.activeGameObject.transform)
				{
					if(child.tag == "SegmentedControl"){
						bComponentExists=true;
					}
				}			
				if(bComponentExists){
					EditorUtility.DisplayDialog("Cannot add TableView!", "A Segmented Control exists on the selected UIPanel.\n\nInstead add TableView to one of the Segmented Control panels.", "OK");
					//Debug.Log ("<color=red>WARNING:</color> Cannot add Tab Bar, a Tab Bar already exists on the selected UIPanel.");
				}								
			}

			if(!bComponentExists){
				GameObject go;
				go = Instantiate(Resources.Load("Prefabs/TableView")) as GameObject;
				Undo.RegisterCreatedObjectUndo (go, "Create Table View");
				
				if(Selection.activeGameObject != null){
					if(Selection.activeGameObject.tag == "rootUIPanel" || Selection.activeGameObject.tag == "UIPanel" || Selection.activeGameObject.tag == "UITabPanel" || Selection.activeGameObject.tag == "UISegmentedPanel"){
						go.transform.SetParent(Selection.activeGameObject.transform);
					} else {
						go.transform.parent = panel.transform;
					}
				} else {
					go.transform.parent = panel.transform;
				}
				
				go.name = "TableView";
				
				//				go.GetComponent<RectTransform>().localPosition = new Vector3(0f, 0f, 0f);	
				go.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
				go.GetComponent<RectTransform>().sizeDelta = new Vector2(0f, 0f);//go.transform.parent.GetComponent<RectTransform>().rect.height);
				go.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);
				
				//Select new added object in hierarchy
				Selection.activeGameObject = go;
			}
		} else {
			EditorUtility.DisplayDialog("TableView cannot be added to current selection!", "You need to select a UIPanel.", "OK");
//			Debug.Log ("<color=red>WARNING:</color> TableView cannot be added to current selection, you need to select a UIPanel.");
		}						
		
	}
	
	public override void OnInspectorGUI ()
	{

		var iOSTableView = target as iOS_TableView;
		Undo.RecordObject (iOSTableView, "Inspector");
		
		iOSTableView.BackgroundColor = EditorGUILayout.ColorField("Background Color", iOSTableView.BackgroundColor);
		//Debug.Log (TableViewItemObject.BackgroundColor.a);
		//TableViewItemObject.BackgroundOpacity = TableViewItemObject.GetComponent<Image>().color.a*255;
		//TableViewItemObject.BackgroundOpacity = TableViewItemObject.BackgroundColor.a*255;
		iOSTableView.BackgroundColor.a = iOSTableView.BackgroundOpacity/255;
		iOSTableView.BackgroundOpacity = EditorGUILayout.Slider("Background Opacity", iOSTableView.BackgroundOpacity, 0, 255);
		EditorGUILayout.Separator();

		iOSTableView.transform.Find("TableViewContainer").GetComponent<Image>().color = new Color(iOSTableView.BackgroundColor.r, iOSTableView.BackgroundColor.g, iOSTableView.BackgroundColor.b, iOSTableView.BackgroundOpacity/255);
		
								
		EditorUtility.SetDirty(target);
		
	}
	
	
}