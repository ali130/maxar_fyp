﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using UnityEngine.UI;

[CustomEditor(typeof(iOS_TextView))]
public class iOS_TextViewEditor : Editor 
{
//	string[] inputTypes =  new[] { "Integer Number", "Decimal Number", "Alphanumeric", "Name", "Email Address", "Password", "Pin" };
//	string[] keyboardTypes =  new[] { "Default", "Numbers and Punctuation", "URL", "Number Pad", "Phone Pad", "Letters Phone Pad", "Email Address" };
//	string[] validationTypes =  new[] { "Standard", "Autocorrect", "Password" };
	string[] inputTypes =  new[] { "Alphanumeric", "Autocorrect", "Integer Number", "Decimal Number", "Email Address", "URL", "Password", "Pin" };
	string[] placeHolderTypes =  new[] { "Left", "Center", "Right" };
	
	//string[] formatTypes =  new[] { "Black on White No Border", "Black on Grey", "Custom" };
	
	
	[MenuItem("GameObject/UI/iOS GUI/Text View")]
	private static void iOSGUITextView(){
		//Debug.Log ("TextView menu item!");
		
		//Check if selected object can have a button added?		
		if(Selection.activeGameObject != null){
			if(Selection.activeGameObject.tag == "rootUIPanel" || Selection.activeGameObject.tag == "UIPanel" || Selection.activeGameObject.tag == "UITabPanel" || Selection.activeGameObject.tag == "TableViewContainer" || Selection.activeGameObject.tag == "TableViewItem"){

				GameObject go;
				go = Instantiate(Resources.Load("Prefabs/TextView")) as GameObject;
				Undo.RegisterCreatedObjectUndo (go, "Create Text View");
				
				go.transform.SetParent(Selection.activeGameObject.transform, true);			
				
				go.name = "TextView";
				
				if(Selection.activeGameObject.tag == "rootUIPanel" || Selection.activeGameObject.tag == "UIPanel" || Selection.activeGameObject.tag == "UITabPanel"){
					go.GetComponent<RectTransform>().localPosition = new Vector3(0f, 0f, 0f);
				//go.GetComponent<RectTransform>().sizeDelta = new Vector2(-16f,go.GetComponent<RectTransform>().rect.height);
				} else {
					if(Selection.activeGameObject.tag == "TableViewContainer"){
						go.GetComponent<RectTransform>().localPosition = new Vector3(0f, (go.transform.parent.GetComponent<RectTransform>().rect.height/2) - (go.GetComponent<RectTransform>().sizeDelta.y/2), 0f);
					//go.GetComponent<RectTransform>().sizeDelta = new Vector2(-16f,go.GetComponent<RectTransform>().rect.height);
					} else {
						go.GetComponent<RectTransform>().localPosition = new Vector3(0f, (go.transform.parent.GetComponent<RectTransform>().rect.height/2) * -1, 0f);
					//go.GetComponent<RectTransform>().sizeDelta = new Vector2(-16f,go.GetComponent<RectTransform>().rect.height);
					}	
				}
				go.GetComponent<RectTransform>().sizeDelta = new Vector2(200f,go.GetComponent<RectTransform>().rect.height);
				go.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);
				
			//Select new added object in hierarchy
				Selection.activeGameObject = go;
				
			} else {
				EditorUtility.DisplayDialog("A TextView field cannot be added to current selection!", "Please select a UIPanel in the Hierarchy panel.", "OK");
			}						
		} else {
			EditorUtility.DisplayDialog("A TextView field cannot be added to current selection!", "Please select a UIPanel in the Hierarchy panel.", "OK");
		}						
		
	}

	public override void OnInspectorGUI ()
	{
	
		var iOSTextView = target as iOS_TextView;
		Undo.RecordObject (iOSTextView, "Inspector");
		
		//iOSTextView.formatIndex = EditorGUILayout.Popup("Pre-defined Format", iOSTextView.formatIndex, formatTypes);
//		iOSTextView.formatIndex = EditorGUILayout.Popup("Presets", iOSTextView.formatIndex, formatTypes);

		EditorGUILayout.BeginVertical();
		GUILayout.Label("Presets");
		if (GUILayout.Button("Black on White")){
			iOSTextView.TextColor = new Color32(0,0,0,255);
			iOSTextView.BackgroundColor = new Color32(255,255,255,255);
			iOSTextView.PlaceHolderColor = new Color32(200,200,200,255);
			//iOSTextView.ClearButtonColor = new Color32(146,146,146,255);
			iOSTextView.showBorder=false;			
		}
		if (GUILayout.Button("Black on Gray")){
			iOSTextView.TextColor = new Color32(0,0,0,255);
			iOSTextView.BackgroundColor = new Color32(200,200,200,255);
			iOSTextView.PlaceHolderColor = new Color32(255,255,255,255);
			//iOSTextView.ClearButtonColor = new Color32(146,146,146,255);
			iOSTextView.showBorder=false;			
		}		
		if (GUILayout.Button("White on Dark Gray")){
			iOSTextView.TextColor = new Color32(255,255,255,255);
			iOSTextView.BackgroundColor = new Color32(146,146,146,255);
			iOSTextView.PlaceHolderColor = new Color32(200,200,200,255);
			//iOSTextView.ClearButtonColor = new Color32(200,200,200,255);
			iOSTextView.showBorder=false;			
		}		
		EditorGUILayout.EndVertical();		
		EditorGUILayout.Separator();
		
		iOSTextView.showPlaceHolder = EditorGUILayout.Toggle("Show Placeholder Text", iOSTextView.showPlaceHolder);
		if(iOSTextView.showPlaceHolder==true)
		{
			iOSTextView.PlaceHolderText = EditorGUILayout.TextField("Placeholder Text", iOSTextView.PlaceHolderText);
			iOSTextView.PlaceHolderColor = EditorGUILayout.ColorField("Placeholder Color", iOSTextView.PlaceHolderColor);
			iOSTextView.placeHolderIndex = EditorGUILayout.Popup("Placeholder Alignment", iOSTextView.placeHolderIndex, placeHolderTypes);
			//iOSTextView.ClearButtonColor = EditorGUILayout.ColorField("Clear Button Color", iOSTextView.ClearButtonColor);
			//	TableViewItemObject.ConnectedPanel = EditorGUILayout.ObjectField("Connected Panel", TableViewItemObject.ConnectedPanel, typeof(GameObject),true) as GameObject;
		}
		EditorGUILayout.Separator();
				
//		iOSTextView.transform.FindChild("Text").GetComponent<Text>().text = EditorGUILayout.TextField("Text", iOSTextView.transform.FindChild("Text").GetComponent<Text>().text);
//		iOSTextView.GetComponent<InputField>().text = iOSTextView.transform.FindChild("Text").GetComponent<Text>().text;
		iOSTextView.TextColor = EditorGUILayout.ColorField("Text Color", iOSTextView.TextColor);
		EditorGUILayout.Separator();

		iOSTextView.showBorder = EditorGUILayout.Toggle("Show Border", iOSTextView.showBorder);
		if(iOSTextView.showBorder==true)
		{
			iOSTextView.BorderColor = EditorGUILayout.ColorField("Border Color", iOSTextView.BorderColor);
		}
		iOSTextView.showBackground = EditorGUILayout.Toggle("Show Background", iOSTextView.showBackground);
		if(iOSTextView.showBackground==true)
		{
			iOSTextView.BackgroundColor = EditorGUILayout.ColorField("Background Color", iOSTextView.BackgroundColor);
		}
		EditorGUILayout.Separator();
		
		iOSTextView.typesIndex = EditorGUILayout.Popup("Input Type", iOSTextView.typesIndex, inputTypes);
//		iOSTextView.keyboardsIndex = EditorGUILayout.Popup("Keyboard Type", iOSTextView.keyboardsIndex, keyboardTypes);
//		iOSTextView.validationIndex = EditorGUILayout.Popup("Character Validation", iOSTextView.validationIndex, validationTypes);
		iOSTextView.showInputField = EditorGUILayout.Toggle("Show Input Field", iOSTextView.showInputField);

				
				
		GameObject placeHolderGO = iOSTextView.transform.Find("Placeholder").gameObject;
		placeHolderGO.SetActive(iOSTextView.showPlaceHolder);
		Text placeHolderText = placeHolderGO.GetComponent<Text>();
		placeHolderText.text = iOSTextView.PlaceHolderText;
		placeHolderText.color = iOSTextView.PlaceHolderColor;
		if(iOSTextView.placeHolderIndex==0){
			placeHolderText.alignment = TextAnchor.UpperLeft;
		}
		if(iOSTextView.placeHolderIndex==1){
			placeHolderText.alignment = TextAnchor.UpperCenter;
		}
		if(iOSTextView.placeHolderIndex==2){
			placeHolderText.alignment = TextAnchor.UpperRight;
		}
		//GameObject clearButtonGO = iOSTextView.transform.FindChild("ClearButton").gameObject;
		//Image clearButtonImage = clearButtonGO.GetComponent<Image>();
		//clearButtonImage.color = iOSTextView.ClearButtonColor;
		
		GameObject textGO = iOSTextView.transform.Find("Text").gameObject;
		Text textText = textGO.GetComponent<Text>();
		textText.color = iOSTextView.TextColor;

		GameObject showBorderGO = iOSTextView.transform.Find("Border").gameObject;
		showBorderGO.SetActive(iOSTextView.showBorder);
		Image borderImage = showBorderGO.GetComponent<Image>();
		borderImage.color = iOSTextView.BorderColor;
		showBorderGO.GetComponent<RectTransform>().sizeDelta = new Vector2(0f, iOSTextView.GetComponent<RectTransform>().rect.height);		
		
		GameObject showBackgroundGO = iOSTextView.transform.Find("Background").gameObject;
		showBackgroundGO.SetActive(iOSTextView.showBackground);
		Image backgroundImage = showBackgroundGO.GetComponent<Image>();
		backgroundImage.color = iOSTextView.BackgroundColor;
		showBackgroundGO.GetComponent<RectTransform>().sizeDelta = new Vector2(0f, iOSTextView.GetComponent<RectTransform>().rect.height);		
		
		
		if(iOSTextView.typesIndex == 0){ //Alphanumeric
			iOSTextView.transform.GetComponent<InputField>().inputType = InputField.InputType.Standard;
			iOSTextView.transform.GetComponent<InputField>().keyboardType = TouchScreenKeyboardType.ASCIICapable;
			iOSTextView.transform.GetComponent<InputField>().characterValidation = InputField.CharacterValidation.Alphanumeric;
		}
		if(iOSTextView.typesIndex == 1){ //Autocorrect
			iOSTextView.transform.GetComponent<InputField>().inputType = InputField.InputType.AutoCorrect;
			iOSTextView.transform.GetComponent<InputField>().keyboardType = TouchScreenKeyboardType.Default;
			iOSTextView.transform.GetComponent<InputField>().characterValidation = InputField.CharacterValidation.None;
		}
		if(iOSTextView.typesIndex == 2){ //Integer Number
			iOSTextView.transform.GetComponent<InputField>().inputType = InputField.InputType.Standard;
			iOSTextView.transform.GetComponent<InputField>().keyboardType = TouchScreenKeyboardType.NumberPad;
			iOSTextView.transform.GetComponent<InputField>().characterValidation = InputField.CharacterValidation.Integer;
		}
		if(iOSTextView.typesIndex == 3){ //Decimal Number
			iOSTextView.transform.GetComponent<InputField>().inputType = InputField.InputType.Standard;
			iOSTextView.transform.GetComponent<InputField>().keyboardType = TouchScreenKeyboardType.NumbersAndPunctuation;
			iOSTextView.transform.GetComponent<InputField>().characterValidation = InputField.CharacterValidation.Decimal;
		}
		if(iOSTextView.typesIndex == 4){ //Email Address
			iOSTextView.transform.GetComponent<InputField>().inputType = InputField.InputType.Standard;
			iOSTextView.transform.GetComponent<InputField>().keyboardType = TouchScreenKeyboardType.EmailAddress;
			iOSTextView.transform.GetComponent<InputField>().characterValidation = InputField.CharacterValidation.EmailAddress;
		}
		if(iOSTextView.typesIndex == 5){ //URL
			iOSTextView.transform.GetComponent<InputField>().inputType = InputField.InputType.Standard;
			iOSTextView.transform.GetComponent<InputField>().keyboardType = TouchScreenKeyboardType.URL;
			iOSTextView.transform.GetComponent<InputField>().characterValidation = InputField.CharacterValidation.None;
		}
		if(iOSTextView.typesIndex == 6){ //Password
			iOSTextView.transform.GetComponent<InputField>().inputType = InputField.InputType.Password;
			iOSTextView.transform.GetComponent<InputField>().keyboardType = TouchScreenKeyboardType.Default;
			iOSTextView.transform.GetComponent<InputField>().characterValidation = InputField.CharacterValidation.None;
		}
		if(iOSTextView.typesIndex == 7){ //Pin
			iOSTextView.transform.GetComponent<InputField>().inputType = InputField.InputType.Password;
			iOSTextView.transform.GetComponent<InputField>().keyboardType = TouchScreenKeyboardType.NumberPad;
			iOSTextView.transform.GetComponent<InputField>().characterValidation = InputField.CharacterValidation.Integer;
		}
		
		iOSTextView.transform.GetComponent<InputField>().shouldHideMobileInput = !iOSTextView.showInputField;
		

		EditorUtility.SetDirty(target);
		
	}
	
}