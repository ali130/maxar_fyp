﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using UnityEngine.UI;

[CustomEditor(typeof(iOS_Text))]
public class iOS_TextEditor : Editor 
{
//	string[] inputTypes =  new[] { "Integer Number", "Decimal Number", "Alphanumeric", "Name", "Email Address", "Password", "Pin" };
//	string[] keyboardTypes =  new[] { "Default", "Numbers and Punctuation", "URL", "Number Pad", "Phone Pad", "Letters Phone Pad", "Email Address" };
//	string[] validationTypes =  new[] { "Standard", "Autocorrect", "Password" };
	string[] inputTypes =  new[] { "Alphanumeric", "Autocorrect", "Integer Number", "Decimal Number", "Email Address", "URL", "Password", "Pin" };
	string[] placeHolderTypes =  new[] { "Left", "Center", "Right" };
	
	//string[] formatTypes =  new[] { "Black on White No Border", "Black on Grey", "Custom" };
	
	
	[MenuItem("GameObject/UI/iOS GUI/Text")]
	private static void iOSGUIText(){
		//Debug.Log ("Text menu item!");
		
		//Check if selected object can have a button added?		
		if(Selection.activeGameObject != null){
			if(Selection.activeGameObject.tag == "rootUIPanel" || Selection.activeGameObject.tag == "UIPanel" || Selection.activeGameObject.tag == "UITabPanel" || Selection.activeGameObject.tag == "TableViewContainer" || Selection.activeGameObject.tag == "TableViewItem"){

				GameObject go;
				go = Instantiate(Resources.Load("Prefabs/Text")) as GameObject;
				Undo.RegisterCreatedObjectUndo (go, "Create Text");
				
				go.transform.SetParent(Selection.activeGameObject.transform, true);			
				
				go.name = "Text";
				
				if(Selection.activeGameObject.tag == "rootUIPanel" || Selection.activeGameObject.tag == "UIPanel" || Selection.activeGameObject.tag == "UITabPanel"){
					go.GetComponent<RectTransform>().localPosition = new Vector3(0f, 0f, 0f);
				//go.GetComponent<RectTransform>().sizeDelta = new Vector2(-16f,go.GetComponent<RectTransform>().rect.height);
				} else {
					if(Selection.activeGameObject.tag == "TableViewContainer"){
						go.GetComponent<RectTransform>().localPosition = new Vector3(0f, (go.transform.parent.GetComponent<RectTransform>().rect.height/2) - (go.GetComponent<RectTransform>().sizeDelta.y/2), 0f);
					//go.GetComponent<RectTransform>().sizeDelta = new Vector2(-16f,go.GetComponent<RectTransform>().rect.height);
					} else {
						go.GetComponent<RectTransform>().localPosition = new Vector3(0f, (go.transform.parent.GetComponent<RectTransform>().rect.height/2) * -1, 0f);
					//go.GetComponent<RectTransform>().sizeDelta = new Vector2(-16f,go.GetComponent<RectTransform>().rect.height);
					}	
				}
				go.GetComponent<RectTransform>().sizeDelta = new Vector2(200f,29f);
				go.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);
				
			//Select new added object in hierarchy
				Selection.activeGameObject = go;
				
			} else {
				EditorUtility.DisplayDialog("A Text field cannot be added to current selection!", "Please select a UIPanel in the Hierarchy panel.", "OK");
			}						
		} else {
			EditorUtility.DisplayDialog("A Text field cannot be added to current selection!", "Please select a UIPanel in the Hierarchy panel.", "OK");
		}						
		
	}

	public override void OnInspectorGUI ()
	{
	
		var iOSText = target as iOS_Text;
		Undo.RecordObject (iOSText, "Inspector");
		
		//iOSText.formatIndex = EditorGUILayout.Popup("Pre-defined Format", iOSText.formatIndex, formatTypes);
//		iOSText.formatIndex = EditorGUILayout.Popup("Presets", iOSText.formatIndex, formatTypes);

		EditorGUILayout.BeginVertical();
		GUILayout.Label("Presets");
		if (GUILayout.Button("Black on White")){
			iOSText.TextColor = new Color32(0,0,0,255);
			iOSText.BackgroundColor = new Color32(255,255,255,255);
			iOSText.PlaceHolderColor = new Color32(200,200,200,255);
			iOSText.ClearButtonColor = new Color32(146,146,146,255);
			iOSText.showBorder=false;			
		}
		if (GUILayout.Button("Black on Gray")){
			iOSText.TextColor = new Color32(0,0,0,255);
			iOSText.BackgroundColor = new Color32(200,200,200,255);
			iOSText.PlaceHolderColor = new Color32(255,255,255,255);
			iOSText.ClearButtonColor = new Color32(146,146,146,255);
			iOSText.showBorder=false;			
		}		
		if (GUILayout.Button("White on Dark Gray")){
			iOSText.TextColor = new Color32(255,255,255,255);
			iOSText.BackgroundColor = new Color32(146,146,146,255);
			iOSText.PlaceHolderColor = new Color32(200,200,200,255);
			iOSText.ClearButtonColor = new Color32(200,200,200,255);
			iOSText.showBorder=false;			
		}		
		EditorGUILayout.EndVertical();		
		EditorGUILayout.Separator();
		
		iOSText.showPlaceHolder = EditorGUILayout.Toggle("Show Placeholder Text", iOSText.showPlaceHolder);
		if(iOSText.showPlaceHolder==true)
		{
			iOSText.PlaceHolderText = EditorGUILayout.TextField("Placeholder Text", iOSText.PlaceHolderText);
			iOSText.PlaceHolderColor = EditorGUILayout.ColorField("Placeholder Color", iOSText.PlaceHolderColor);
			iOSText.placeHolderIndex = EditorGUILayout.Popup("Placeholder Alignment", iOSText.placeHolderIndex, placeHolderTypes);
			iOSText.ClearButtonColor = EditorGUILayout.ColorField("Clear Button Color", iOSText.ClearButtonColor);
			//	TableViewItemObject.ConnectedPanel = EditorGUILayout.ObjectField("Connected Panel", TableViewItemObject.ConnectedPanel, typeof(GameObject),true) as GameObject;
		}
		EditorGUILayout.Separator();
				
//		iOSText.transform.FindChild("Text").GetComponent<Text>().text = EditorGUILayout.TextField("Text", iOSText.transform.FindChild("Text").GetComponent<Text>().text);
//		iOSText.GetComponent<InputField>().text = iOSText.transform.FindChild("Text").GetComponent<Text>().text;
		iOSText.TextColor = EditorGUILayout.ColorField("Text Color", iOSText.TextColor);
		EditorGUILayout.Separator();

		iOSText.showBorder = EditorGUILayout.Toggle("Show Border", iOSText.showBorder);
		if(iOSText.showBorder==true)
		{
			iOSText.BorderColor = EditorGUILayout.ColorField("Border Color", iOSText.BorderColor);
		}
		iOSText.showBackground = EditorGUILayout.Toggle("Show Background", iOSText.showBackground);
		if(iOSText.showBackground==true)
		{
			iOSText.BackgroundColor = EditorGUILayout.ColorField("Background Color", iOSText.BackgroundColor);
		}
		EditorGUILayout.Separator();
		
		iOSText.typesIndex = EditorGUILayout.Popup("Input Type", iOSText.typesIndex, inputTypes);
//		iOSText.keyboardsIndex = EditorGUILayout.Popup("Keyboard Type", iOSText.keyboardsIndex, keyboardTypes);
//		iOSText.validationIndex = EditorGUILayout.Popup("Character Validation", iOSText.validationIndex, validationTypes);
		iOSText.showInputField = EditorGUILayout.Toggle("Show Input Field", iOSText.showInputField);

				
				
		GameObject placeHolderGO = iOSText.transform.Find("Placeholder").gameObject;
		placeHolderGO.SetActive(iOSText.showPlaceHolder);
		Text placeHolderText = placeHolderGO.GetComponent<Text>();
		placeHolderText.text = iOSText.PlaceHolderText;
		placeHolderText.color = iOSText.PlaceHolderColor;
		if(iOSText.placeHolderIndex==0){
			placeHolderText.alignment = TextAnchor.MiddleLeft;
		}
		if(iOSText.placeHolderIndex==1){
			placeHolderText.alignment = TextAnchor.MiddleCenter;
		}
		if(iOSText.placeHolderIndex==2){
			placeHolderText.alignment = TextAnchor.MiddleRight;
		}
		GameObject clearButtonGO = iOSText.transform.Find("ClearButton").gameObject;
		Image clearButtonImage = clearButtonGO.GetComponent<Image>();
		clearButtonImage.color = iOSText.ClearButtonColor;
		
		GameObject textGO = iOSText.transform.Find("Text").gameObject;
		Text textText = textGO.GetComponent<Text>();
		textText.color = iOSText.TextColor;

		GameObject showBorderGO = iOSText.transform.Find("Border").gameObject;
		showBorderGO.SetActive(iOSText.showBorder);
		Image borderImage = showBorderGO.GetComponent<Image>();
		borderImage.color = iOSText.BorderColor;
		
		GameObject showBackgroundGO = iOSText.transform.Find("Background").gameObject;
		showBackgroundGO.SetActive(iOSText.showBackground);
		Image backgroundImage = showBackgroundGO.GetComponent<Image>();
		backgroundImage.color = iOSText.BackgroundColor;
		
		if(iOSText.typesIndex == 0){ //Alphanumeric
			iOSText.transform.GetComponent<InputField>().inputType = InputField.InputType.Standard;
			iOSText.transform.GetComponent<InputField>().keyboardType = TouchScreenKeyboardType.ASCIICapable;
			iOSText.transform.GetComponent<InputField>().characterValidation = InputField.CharacterValidation.Alphanumeric;
		}
		if(iOSText.typesIndex == 1){ //Autocorrect
			iOSText.transform.GetComponent<InputField>().inputType = InputField.InputType.AutoCorrect;
			iOSText.transform.GetComponent<InputField>().keyboardType = TouchScreenKeyboardType.Default;
			iOSText.transform.GetComponent<InputField>().characterValidation = InputField.CharacterValidation.None;
		}
		if(iOSText.typesIndex == 2){ //Integer Number
			iOSText.transform.GetComponent<InputField>().inputType = InputField.InputType.Standard;
			iOSText.transform.GetComponent<InputField>().keyboardType = TouchScreenKeyboardType.NumberPad;
			iOSText.transform.GetComponent<InputField>().characterValidation = InputField.CharacterValidation.Integer;
		}
		if(iOSText.typesIndex == 3){ //Decimal Number
			iOSText.transform.GetComponent<InputField>().inputType = InputField.InputType.Standard;
			iOSText.transform.GetComponent<InputField>().keyboardType = TouchScreenKeyboardType.NumbersAndPunctuation;
			iOSText.transform.GetComponent<InputField>().characterValidation = InputField.CharacterValidation.Decimal;
		}
		if(iOSText.typesIndex == 4){ //Email Address
			iOSText.transform.GetComponent<InputField>().inputType = InputField.InputType.Standard;
			iOSText.transform.GetComponent<InputField>().keyboardType = TouchScreenKeyboardType.EmailAddress;
			iOSText.transform.GetComponent<InputField>().characterValidation = InputField.CharacterValidation.EmailAddress;
		}
		if(iOSText.typesIndex == 5){ //URL
			iOSText.transform.GetComponent<InputField>().inputType = InputField.InputType.Standard;
			iOSText.transform.GetComponent<InputField>().keyboardType = TouchScreenKeyboardType.URL;
			iOSText.transform.GetComponent<InputField>().characterValidation = InputField.CharacterValidation.None;
		}
		if(iOSText.typesIndex == 6){ //Password
			iOSText.transform.GetComponent<InputField>().inputType = InputField.InputType.Password;
			iOSText.transform.GetComponent<InputField>().keyboardType = TouchScreenKeyboardType.Default;
			iOSText.transform.GetComponent<InputField>().characterValidation = InputField.CharacterValidation.None;
		}
		if(iOSText.typesIndex == 7){ //Pin
			iOSText.transform.GetComponent<InputField>().inputType = InputField.InputType.Password;
			iOSText.transform.GetComponent<InputField>().keyboardType = TouchScreenKeyboardType.NumberPad;
			iOSText.transform.GetComponent<InputField>().characterValidation = InputField.CharacterValidation.Integer;
		}
		
		iOSText.transform.GetComponent<InputField>().shouldHideMobileInput = !iOSText.showInputField;
		
		
		EditorUtility.SetDirty(target);
		
	}
	
}