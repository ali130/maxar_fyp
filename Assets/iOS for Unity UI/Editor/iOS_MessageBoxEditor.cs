﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using UnityEngine.UI;

[CustomEditor(typeof(iOS_MessageBox))]
public class iOS_MessageBoxEditor : Editor 
{
	
	string[] buttons =  new[] { "1", "2" };
	
	[MenuItem("GameObject/UI/iOS GUI/Message Box")]
	private static void iOSGUIMessageBox(){
		//Debug.Log ("Message Box menu item!");
		
		//Check if Canvas exists?
		Canvas canvas = FindObjectOfType(typeof(Canvas)) as Canvas;
		if (canvas){ //Canvas already exists, do nothing...
			//Debug.Log("Canvas object found: " + canvas.name);
		} else { //Create Canvas
			//Debug.Log("No Canvas object could be found");
			EditorApplication.ExecuteMenuItem("GameObject/UI/Canvas");
			canvas = FindObjectOfType(typeof(Canvas)) as Canvas;
			canvas.pixelPerfect = true;
		}	
		
		//Check if Panel exists?
		/*	uGUI_IOSInterface_UIPanel panel = FindObjectOfType(typeof(uGUI_IOSInterface_UIPanel)) as uGUI_IOSInterface_UIPanel;
		if (panel){ //Panel already exists, do nothing...
			//Debug.Log("Panel object found: " + panel.name);
			if(Selection.activeGameObject == null){ //if no object selected, make panel found the selected object
				Selection.activeGameObject = panel.gameObject;
			}
		} else { //Create Panel
			//Debug.Log("No Panel object could be found");
			EditorApplication.ExecuteMenuItem("GameObject/UI/iOS GUI/UIPanel");
			panel = FindObjectOfType(typeof(uGUI_IOSInterface_UIPanel)) as uGUI_IOSInterface_UIPanel;
			Selection.activeGameObject = panel.gameObject;
		}
		
		//Check if selected object is a Panel		
		bool bComponentExists=false;
		if(Selection.activeGameObject.tag == "rootUIPanel" || Selection.activeGameObject.tag == "UIPanel" || Selection.activeGameObject.tag == "UITabPanel"){
			
			GameObject go;
			go = Instantiate(Resources.Load("Prefabs/Message Box")) as GameObject;
			Undo.RegisterCreatedObjectUndo (go, "Create Message Box");
			
			if(Selection.activeGameObject != null){
				if(Selection.activeGameObject.tag == "rootUIPanel" || Selection.activeGameObject.tag == "UIPanel" || Selection.activeGameObject.tag == "UITabPanel"){
					go.transform.parent = Selection.activeGameObject.transform;
				} else {
					go.transform.parent = panel.transform;
				}
			} else {
				go.transform.parent = panel.transform;
			}
			
			go.name = "Message Box";
			
			go.GetComponent<RectTransform>().localPosition = new Vector3(0f, 0f, 0f);	
			go.GetComponent<RectTransform>().sizeDelta = new Vector2(0f,0f);
			go.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);
			
			// Make sure Popup Message is last object on UIPanel for Z ordering reasons
			go.transform.SetAsLastSibling();

			Selection.activeGameObject = go;		
			
		} else {
			EditorUtility.DisplayDialog("Message Box cannot be added to current selection!", "You need to select a UIPanel.", "OK");
		}						
		*/
	}

	public override void OnInspectorGUI ()
	{
	
		var iOSMessageBox = target as iOS_MessageBox;
		Undo.RecordObject (iOSMessageBox, "Inspector");
		
		iOSMessageBox.isShown = EditorGUILayout.Toggle("isShown", iOSMessageBox.isShown);
		iOSMessageBox.titleText = EditorGUILayout.TextField("Title Text", iOSMessageBox.titleText);
		iOSMessageBox.titleColor = EditorGUILayout.ColorField("Title Color", iOSMessageBox.titleColor);			
		EditorGUILayout.Separator();

		iOSMessageBox.buttonsIndex = EditorGUILayout.Popup("Number of Buttons", iOSMessageBox.buttonsIndex, buttons);
		EditorGUILayout.Separator();
		
		
		if(iOSMessageBox.buttonsIndex == 0){
			iOSMessageBox.middleButtonText = EditorGUILayout.TextField("Button Text", iOSMessageBox.middleButtonText);
			iOSMessageBox.middleButtonColor = EditorGUILayout.ColorField("Button Color", iOSMessageBox.middleButtonColor);			
			iOSMessageBox.transform.Find("Window/Buttons/SingleButton").gameObject.SetActive(true);
			iOSMessageBox.transform.Find("Window/Buttons/LeftButton").gameObject.SetActive(false);
			iOSMessageBox.transform.Find("Window/Buttons/RightButton").gameObject.SetActive(false);
			iOSMessageBox.transform.Find("Window/Buttons/MidLine").gameObject.SetActive(false);

			iOSMessageBox.buttonCancel = EditorGUILayout.Toggle("Cancel Button?", iOSMessageBox.buttonCancel);
			if(iOSMessageBox.buttonCancel==false){
				EditorGUILayout.LabelField("Button1 OnClick()");
				EditorGUILayout.BeginHorizontal();
				iOSMessageBox.button1GO = EditorGUILayout.ObjectField(iOSMessageBox.button1GO, typeof(GameObject)) as GameObject;					
				iOSMessageBox.button1FunctionName = EditorGUILayout.TextField(iOSMessageBox.button1FunctionName);
				EditorGUILayout.EndHorizontal();
			}
		}
		if(iOSMessageBox.buttonsIndex == 1){
			iOSMessageBox.transform.Find("Window/Buttons/SingleButton").gameObject.SetActive(false);
			iOSMessageBox.leftButtonText = EditorGUILayout.TextField("Left Button Text", iOSMessageBox.leftButtonText);
			iOSMessageBox.leftButtonColor = EditorGUILayout.ColorField("Left Button Color", iOSMessageBox.leftButtonColor);			
			iOSMessageBox.transform.Find("Window/Buttons/LeftButton").gameObject.SetActive(true);			
			EditorGUILayout.LabelField("Button1 OnClick()");
			EditorGUILayout.BeginHorizontal();
			iOSMessageBox.button1GO = EditorGUILayout.ObjectField(iOSMessageBox.button1GO, typeof(GameObject)) as GameObject;					
			iOSMessageBox.button1FunctionName = EditorGUILayout.TextField(iOSMessageBox.button1FunctionName);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Separator();	
			iOSMessageBox.rightButtonText = EditorGUILayout.TextField("Right Button Text", iOSMessageBox.rightButtonText);
			iOSMessageBox.rightButtonColor = EditorGUILayout.ColorField("Right Button Color", iOSMessageBox.rightButtonColor);			
			iOSMessageBox.transform.Find("Window/Buttons/RightButton").gameObject.SetActive(true);
			iOSMessageBox.transform.Find("Window/Buttons/MidLine").gameObject.SetActive(true);
			iOSMessageBox.buttonCancel = EditorGUILayout.Toggle("Cancel Button?", iOSMessageBox.buttonCancel);
			if(iOSMessageBox.buttonCancel==false){			
				EditorGUILayout.LabelField("Button2 OnClick()");
				EditorGUILayout.BeginHorizontal();
				iOSMessageBox.button2GO = EditorGUILayout.ObjectField(iOSMessageBox.button2GO, typeof(GameObject)) as GameObject;					
				iOSMessageBox.button2FunctionName = EditorGUILayout.TextField(iOSMessageBox.button2FunctionName);
				EditorGUILayout.EndHorizontal();
			}
		}
			
//		iOSMessageBox.transform.FindChild("Interface/Window").GetComponent<RectTransform>().sizeDelta = new Vector2(540f, 118f + ((iOSMessageBox.buttonsIndex+1) * 88f));

		iOSMessageBox.transform.Find("Window/Title/TitleLabel").GetComponent<Text>().text = iOSMessageBox.titleText;
		iOSMessageBox.transform.Find("Window/Title/TitleLabel").GetComponent<Text>().color = iOSMessageBox.titleColor;
		iOSMessageBox.transform.Find("Window/Buttons/SingleButton").GetComponent<Text>().text = iOSMessageBox.middleButtonText;
		iOSMessageBox.transform.Find("Window/Buttons/SingleButton").GetComponent<Text>().color = iOSMessageBox.middleButtonColor;
		iOSMessageBox.transform.Find("Window/Buttons/LeftButton").GetComponent<Text>().text = iOSMessageBox.leftButtonText;
		iOSMessageBox.transform.Find("Window/Buttons/LeftButton").GetComponent<Text>().color = iOSMessageBox.leftButtonColor;
		iOSMessageBox.transform.Find("Window/Buttons/RightButton").GetComponent<Text>().text = iOSMessageBox.rightButtonText;
		iOSMessageBox.transform.Find("Window/Buttons/RightButton").GetComponent<Text>().color = iOSMessageBox.rightButtonColor;
		iOSMessageBox.transform.gameObject.SetActive(iOSMessageBox.isShown);
		
		
		EditorUtility.SetDirty(target);
		
	}
	
}