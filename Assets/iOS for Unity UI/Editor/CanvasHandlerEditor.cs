﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using UnityEngine.UI;

[CustomEditor(typeof(CanvasHandler))]
public class CanvasHandlerEditor : Editor {
	
	string[] options = new string[] {"", "1x", "2x", "3x", "4x"};
	
	
	public override void OnInspectorGUI ()
	{
		
		var iOSCanvasHandler = target as CanvasHandler;
		Undo.RecordObject (iOSCanvasHandler, "Inspector");
		
		iOSCanvasHandler.useDeviceScreenResolution = EditorGUILayout.Toggle("Use Device Screen Resolution", iOSCanvasHandler.useDeviceScreenResolution);
		if(!iOSCanvasHandler.useDeviceScreenResolution){
			iOSCanvasHandler.defaultScreenWidth = EditorGUILayout.IntField("Target Screen Width", iOSCanvasHandler.defaultScreenWidth);
			PlayerSettings.defaultScreenWidth = iOSCanvasHandler.defaultScreenWidth;
			PlayerSettings.defaultWebScreenWidth = PlayerSettings.defaultScreenWidth;
			
			iOSCanvasHandler.defaultScreenHeight = EditorGUILayout.IntField("Target Screen Height", iOSCanvasHandler.defaultScreenHeight);
			PlayerSettings.defaultScreenHeight = iOSCanvasHandler.defaultScreenHeight;
			PlayerSettings.defaultWebScreenHeight = PlayerSettings.defaultScreenHeight;
			
		}
		EditorGUILayout.Separator();
		
		iOSCanvasHandler.retinaMultipler = EditorGUILayout.Popup("Resolution", iOSCanvasHandler.retinaMultipler, options);
		
		EditorUtility.SetDirty(target);
		
	}
}
