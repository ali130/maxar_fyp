﻿using UnityEngine;
using UnityEditor;

public class iOSGUI_Toolbox : EditorWindow {
	
	Texture ActivityIndicator;
	Texture Button;
	Texture DatePicker;
	Texture Image;
	Texture Label;
	Texture NavigationBar;
	Texture PickerView;
	Texture Progress;
	Texture SearchBar;
	Texture SegmentedControl;
	Texture Slider;
	Texture Stepper;
	Texture Switch;
	Texture TabBar;
	Texture TableView;
	Texture TableViewItem;
	Texture Text;
	Texture TextView;
	Texture ToolBar;
	Texture UIPanel;
	Texture MessageBox;
	Texture PopupMessage;
	
	Texture[] buttonTexture;
	string[] sResource;
	string[] sTooltip;
	string[] sMenuItem;
	
	Vector2 scrollViewPosition;
	int i=0;
	
	// Add menu named "My Window" to the Window menu
	[MenuItem ("Window/iOS GUI Toolbox")]
	static void Init () {
		// Get existing open window or if none, make a new one:
		iOSGUI_Toolbox window = (iOSGUI_Toolbox)EditorWindow.GetWindow (typeof (iOSGUI_Toolbox));
	}
		
	void OnGUI () {
		buttonTexture = new Texture[22];
		sResource = new string[22]; 
		sTooltip = new string[22]; 
		sMenuItem = new string[22]; 
		
		i=0;
		sResource[i] = "UIToolbox/UIPanel";
		sTooltip[i] = "UIPanel";
		sMenuItem[i] = "GameObject/UI/iOS GUI/UIPanel";
		i++;
		sResource[i] = "UIToolbox/TabBar";
		sTooltip[i] = "Tab Bar";
		sMenuItem[i] = "GameObject/UI/iOS GUI/Tab Bar";
		i++;
		sResource[i] = "UIToolbox/NavigationBar";
		sTooltip[i] = "Navigation Bar";
		sMenuItem[i] = "GameObject/UI/iOS GUI/Navigation Bar";
		i++;
		sResource[i] = "UIToolbox/Toolbar";
		sTooltip[i] = "Toolbar";
		sMenuItem[i] = "GameObject/UI/iOS GUI/Tool Bar";
		i++;
		sResource[i] = "UIToolbox/SegmentedControl";
		sTooltip[i] = "Segmented Control";
		sMenuItem[i] = "GameObject/UI/iOS GUI/Segmented Control";
		i++;
		sResource[i] = "UIToolbox/Image";
		sTooltip[i] = "Image";
		sMenuItem[i] = "GameObject/UI/iOS GUI/Image";
		i++;
		sResource[i] = "UIToolbox/TableView";
		sTooltip[i] = "Table View";
		sMenuItem[i] = "GameObject/UI/iOS GUI/Table View";
		i++;
		sResource[i] = "UIToolbox/TableViewItem";
		sTooltip[i] = "Table View Item";
		sMenuItem[i] = "GameObject/UI/iOS GUI/Table View Item";
		i++;
		sResource[i] = "UIToolbox/Button";
		sTooltip[i] = "Button";
		sMenuItem[i] = "GameObject/UI/iOS GUI/Button";
		i++;
		sResource[i] = "UIToolbox/Label";
		sTooltip[i] = "Label";
		sMenuItem[i] = "GameObject/UI/iOS GUI/Label";
		i++;
		sResource[i] = "UIToolbox/Switch";
		sTooltip[i] = "Switch";
		sMenuItem[i] = "GameObject/UI/iOS GUI/Switch";
		i++;
		sResource[i] = "UIToolbox/Text";
		sTooltip[i] = "Text";
		sMenuItem[i] = "GameObject/UI/iOS GUI/Text";
		i++;
		sResource[i] = "UIToolbox/TextView";
		sTooltip[i] = "Text View";
		sMenuItem[i] = "GameObject/UI/iOS GUI/Text View";
		i++;
		sResource[i] = "UIToolbox/PopupMessage";
		sTooltip[i] = "Popup Message";
		sMenuItem[i] = "GameObject/UI/iOS GUI/Popup Message";
		i++;
		sResource[i] = "UIToolbox/MessageBox";
		sTooltip[i] = "Message Box";
		sMenuItem[i] = "GameObject/UI/iOS GUI/Message Box";
		i++;
		sResource[i] = "UIToolbox/Slider";
		sTooltip[i] = "Slider";
		sMenuItem[i] = "GameObject/UI/iOS GUI/Slider";
		i++;
		sResource[i] = "UIToolbox/Stepper";
		sTooltip[i] = "Stepper";
		sMenuItem[i] = "GameObject/UI/iOS GUI/Stepper";
		i++;
		sResource[i] = "UIToolbox/Progress";
		sTooltip[i] = "Progress";
		sMenuItem[i] = "GameObject/UI/iOS GUI/Progress";
		i++;
		sResource[i] = "UIToolbox/DatePicker";
		sTooltip[i] = "Date Picker";
		sMenuItem[i] = "GameObject/UI/iOS GUI/Date Picker";
		i++;
		sResource[i] = "UIToolbox/PickerView";
		sTooltip[i] = "PickerView";
		sMenuItem[i] = "GameObject/UI/iOS GUI/Picker View";
		i++;
		sResource[i] = "UIToolbox/SearchBar";
		sTooltip[i] = "Search Bar";
		sMenuItem[i] = "GameObject/UI/iOS GUI/Search Bar";
		i++;
		sResource[i] = "UIToolbox/ActivityIndicator";
		sTooltip[i] = "Activity Indicator";
		sMenuItem[i] = "GameObject/UI/iOS GUI/Activity Indicator";
		
		
		
		scrollViewPosition = EditorGUILayout.BeginScrollView (scrollViewPosition); 
		i=0;
		GUILayout.BeginHorizontal();	
		buttonTexture[i] = Resources.Load(sResource[i], typeof(Texture)) as Texture;
		if(GUILayout.Button(new GUIContent(buttonTexture[i], sTooltip[i]),GUILayout.Width(100), GUILayout.Height(100))){
			EditorApplication.ExecuteMenuItem(sMenuItem[i]);
		}
		i++;
		buttonTexture[i] = Resources.Load(sResource[i], typeof(Texture)) as Texture;
		if(GUILayout.Button(new GUIContent(buttonTexture[i], sTooltip[i]),GUILayout.Width(100), GUILayout.Height(100))){
			EditorApplication.ExecuteMenuItem(sMenuItem[i]);
		}
		GUILayout.EndHorizontal();
		
		i++;
		GUILayout.BeginHorizontal();	
		buttonTexture[i] = Resources.Load(sResource[i], typeof(Texture)) as Texture;
		if(GUILayout.Button(new GUIContent(buttonTexture[i], sTooltip[i]),GUILayout.Width(100), GUILayout.Height(100))){
			EditorApplication.ExecuteMenuItem(sMenuItem[i]);
		}
		i++;
		buttonTexture[i] = Resources.Load(sResource[i], typeof(Texture)) as Texture;
		if(GUILayout.Button(new GUIContent(buttonTexture[i], sTooltip[i]),GUILayout.Width(100), GUILayout.Height(100))){
			EditorApplication.ExecuteMenuItem(sMenuItem[i]);
		}
		GUILayout.EndHorizontal();
		
		i++;
		GUILayout.BeginHorizontal();	
		buttonTexture[i] = Resources.Load(sResource[i], typeof(Texture)) as Texture;
		if(GUILayout.Button(new GUIContent(buttonTexture[i], sTooltip[i]),GUILayout.Width(100), GUILayout.Height(100))){
			EditorApplication.ExecuteMenuItem(sMenuItem[i]);
		}
		i++;
		buttonTexture[i] = Resources.Load(sResource[i], typeof(Texture)) as Texture;
		if(GUILayout.Button(new GUIContent(buttonTexture[i], sTooltip[i]),GUILayout.Width(100), GUILayout.Height(100))){
			EditorApplication.ExecuteMenuItem(sMenuItem[i]);
		}
		GUILayout.EndHorizontal();

		i++;
		GUILayout.BeginHorizontal();	
		buttonTexture[i] = Resources.Load(sResource[i], typeof(Texture)) as Texture;
		if(GUILayout.Button(new GUIContent(buttonTexture[i], sTooltip[i]),GUILayout.Width(100), GUILayout.Height(100))){
			EditorApplication.ExecuteMenuItem(sMenuItem[i]);
		}
		i++;
		buttonTexture[i] = Resources.Load(sResource[i], typeof(Texture)) as Texture;
		if(GUILayout.Button(new GUIContent(buttonTexture[i], sTooltip[i]),GUILayout.Width(100), GUILayout.Height(100))){
			EditorApplication.ExecuteMenuItem(sMenuItem[i]);
		}
		GUILayout.EndHorizontal();
		
		i++;
		GUILayout.BeginHorizontal();	
		buttonTexture[i] = Resources.Load(sResource[i], typeof(Texture)) as Texture;
		if(GUILayout.Button(new GUIContent(buttonTexture[i], sTooltip[i]),GUILayout.Width(100), GUILayout.Height(100))){
			EditorApplication.ExecuteMenuItem(sMenuItem[i]);
		}
		i++;
		buttonTexture[i] = Resources.Load(sResource[i], typeof(Texture)) as Texture;
		if(GUILayout.Button(new GUIContent(buttonTexture[i], sTooltip[i]),GUILayout.Width(100), GUILayout.Height(100))){
			EditorApplication.ExecuteMenuItem(sMenuItem[i]);
		}
		GUILayout.EndHorizontal();

		i++;
		GUILayout.BeginHorizontal();	
		buttonTexture[i] = Resources.Load(sResource[i], typeof(Texture)) as Texture;
		if(GUILayout.Button(new GUIContent(buttonTexture[i], sTooltip[i]),GUILayout.Width(100), GUILayout.Height(100))){
			EditorApplication.ExecuteMenuItem(sMenuItem[i]);
		}
		i++;
		buttonTexture[i] = Resources.Load(sResource[i], typeof(Texture)) as Texture;
		if(GUILayout.Button(new GUIContent(buttonTexture[i], sTooltip[i]),GUILayout.Width(100), GUILayout.Height(100))){
			EditorApplication.ExecuteMenuItem(sMenuItem[i]);
		}
		GUILayout.EndHorizontal();
		
		i++;
		GUILayout.BeginHorizontal();	
		buttonTexture[i] = Resources.Load(sResource[i], typeof(Texture)) as Texture;
		if(GUILayout.Button(new GUIContent(buttonTexture[i], sTooltip[i]),GUILayout.Width(100), GUILayout.Height(100))){
			EditorApplication.ExecuteMenuItem(sMenuItem[i]);
		}
		i++;
		buttonTexture[i] = Resources.Load(sResource[i], typeof(Texture)) as Texture;
		if(GUILayout.Button(new GUIContent(buttonTexture[i], sTooltip[i]),GUILayout.Width(100), GUILayout.Height(100))){
			EditorApplication.ExecuteMenuItem(sMenuItem[i]);
		}
		GUILayout.EndHorizontal();
		
		i++;
		GUILayout.BeginHorizontal();	
		buttonTexture[i] = Resources.Load(sResource[i], typeof(Texture)) as Texture;
		if(GUILayout.Button(new GUIContent(buttonTexture[i], sTooltip[i]),GUILayout.Width(100), GUILayout.Height(100))){
			EditorApplication.ExecuteMenuItem(sMenuItem[i]);
		}
		GUI.enabled = false;
		i++;
		buttonTexture[i] = Resources.Load(sResource[i], typeof(Texture)) as Texture;
		if(GUILayout.Button(new GUIContent(buttonTexture[i], sTooltip[i]),GUILayout.Width(100), GUILayout.Height(100))){
			EditorApplication.ExecuteMenuItem(sMenuItem[i]);
		}
		GUILayout.EndHorizontal();
		
		i++;
		GUILayout.BeginHorizontal();	
		buttonTexture[i] = Resources.Load(sResource[i], typeof(Texture)) as Texture;
		if(GUILayout.Button(new GUIContent(buttonTexture[i], sTooltip[i]),GUILayout.Width(100), GUILayout.Height(100))){
			EditorApplication.ExecuteMenuItem(sMenuItem[i]);
		}
		i++;
		buttonTexture[i] = Resources.Load(sResource[i], typeof(Texture)) as Texture;
		if(GUILayout.Button(new GUIContent(buttonTexture[i], sTooltip[i]),GUILayout.Width(100), GUILayout.Height(100))){
			EditorApplication.ExecuteMenuItem(sMenuItem[i]);
		}
		GUILayout.EndHorizontal();
		
		i++;
		GUILayout.BeginHorizontal();	
		buttonTexture[i] = Resources.Load(sResource[i], typeof(Texture)) as Texture;
		if(GUILayout.Button(new GUIContent(buttonTexture[i], sTooltip[i]),GUILayout.Width(100), GUILayout.Height(100))){
			EditorApplication.ExecuteMenuItem(sMenuItem[i]);
		}
		i++;
		buttonTexture[i] = Resources.Load(sResource[i], typeof(Texture)) as Texture;
		if(GUILayout.Button(new GUIContent(buttonTexture[i], sTooltip[i]),GUILayout.Width(100), GUILayout.Height(100))){
			EditorApplication.ExecuteMenuItem(sMenuItem[i]);
		}
		GUILayout.EndHorizontal();
		
		i++;
		GUILayout.BeginHorizontal();	
		buttonTexture[i] = Resources.Load(sResource[i], typeof(Texture)) as Texture;
		if(GUILayout.Button(new GUIContent(buttonTexture[i], sTooltip[i]),GUILayout.Width(100), GUILayout.Height(100))){
			EditorApplication.ExecuteMenuItem(sMenuItem[i]);
		}
		i++;
		buttonTexture[i] = Resources.Load(sResource[i], typeof(Texture)) as Texture;
		if(GUILayout.Button(new GUIContent(buttonTexture[i], sTooltip[i]),GUILayout.Width(100), GUILayout.Height(100))){
			EditorApplication.ExecuteMenuItem(sMenuItem[i]);
		}
		GUI.enabled = true;
		GUILayout.EndHorizontal();
		EditorGUILayout.EndScrollView();
	}
}
