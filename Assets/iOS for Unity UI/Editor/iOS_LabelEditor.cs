﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using UnityEngine.UI;

[CustomEditor(typeof(iOS_Label))]
public class iOS_LabelEditor : Editor 
{
	
//	string[] tabs =  new[] { "1", "2", "3", "4", "5" };
	
	[MenuItem("GameObject/UI/iOS GUI/Label")]
	private static void iOSGUILabel(){
		//Debug.Log ("Label menu item!");
		
		//Check if selected object can have a button added?		
		if(Selection.activeGameObject.tag == "rootUIPanel" || Selection.activeGameObject.tag == "UIPanel" || Selection.activeGameObject.tag == "UITabPanel" || Selection.activeGameObject.tag == "TableViewContainer" || Selection.activeGameObject.tag == "TableViewItem"){

			GameObject go;
			go = Instantiate(Resources.Load("Prefabs/Label")) as GameObject;
			Undo.RegisterCreatedObjectUndo (go, "Create Label");

			go.transform.SetParent(Selection.activeGameObject.transform, true);			
			
			go.name = "Label";
			
			if(Selection.activeGameObject.tag == "rootUIPanel" || Selection.activeGameObject.tag == "UIPanel" || Selection.activeGameObject.tag == "UITabPanel"){
				go.GetComponent<RectTransform>().localPosition = new Vector3(0f, 0f, 0f);				
			} else {
				if(Selection.activeGameObject.tag == "TableViewContainer"){
					go.GetComponent<RectTransform>().localPosition = new Vector3(0f, (go.transform.parent.GetComponent<RectTransform>().rect.height/2) - (go.GetComponent<RectTransform>().sizeDelta.y/2), 0f);
				} else {
					go.GetComponent<RectTransform>().localPosition = new Vector3(0f, (go.transform.parent.GetComponent<RectTransform>().rect.height/2) * -1, 0f);
				}	
			}
			go.GetComponent<RectTransform>().sizeDelta = new Vector2(150f, go.GetComponent<RectTransform>().rect.height);
			go.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);

			//Select new added object in hierarchy
			Selection.activeGameObject = go;
			
		} else {
			EditorUtility.DisplayDialog("A Label cannot be added to current selection!", "Refer to the support documentation for more information.", "OK");
		}						
		
	}

	public override void OnInspectorGUI ()
	{
	
		var iOSLabel = target as iOS_Label;
		Undo.RecordObject (iOSLabel, "Inspector");
		
		iOSLabel.labelText = EditorGUILayout.TextField("Label Text", iOSLabel.labelText);

        iOSLabel.size = EditorGUILayout.IntField("Font Size", iOSLabel.size);

        iOSLabel.labelColor = EditorGUILayout.ColorField("Label Color", iOSLabel.labelColor);

		iOSLabel.ConnectedSwitch = EditorGUILayout.ObjectField("Connected Switch", iOSLabel.ConnectedSwitch, typeof(iOS_Switch),true) as iOS_Switch;					

		if(iOSLabel.ConnectedSwitch != null){
			if(iOSLabel.ConnectedSwitch.GetComponent<Toggle>().isOn==true){
				iOSLabel.labelText = iOSLabel.ConnectedSwitch.OnStateText;
			} else {
				iOSLabel.labelText = iOSLabel.ConnectedSwitch.OffStateText;
			}
		}
		
		Text TitleText = iOSLabel.GetComponent<Text>();
		TitleText.text = iOSLabel.labelText;
		TitleText.color = iOSLabel.labelColor;

		iOSLabel.GetComponent<RectTransform>().sizeDelta = new Vector2(iOSLabel.labelText.Length * 20, iOSLabel.GetComponent<RectTransform>().rect.height);
		
		EditorUtility.SetDirty(target);
		
	}
		
	
}