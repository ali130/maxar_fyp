﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Maxar;

namespace Maxar
{
    public class PresentationInspector : MonoBehaviour
    {
        public PresentationSaver loader;
        public DynamicList PresentationList;
        public bool initialized;

        // Use this for initialization
        private void Start() {
            initialized = false;
        }

        // Update is called once per frame
        private void Update() {
            if (!initialized) {
                loader = ScriptableObject.CreateInstance<PresentationSaver>();
                if (PresentationList.container.transform.childCount > 0) {
                    PresentationList.container.transform.parent.GetComponent<ScrollRect>().normalizedPosition = new Vector2(0, 1);
                    while (PresentationList.container.transform.childCount > 0) {
                        PresentationList.Delete(PresentationList.container.transform.childCount - 1);
                    }
                }
                loader.LoadCache();


                foreach (PresentationCacheClass cache in loader.CacheList) {
                    PresentationListItem presentationItem = PresentationList.Append("Prefabs/ListItem/PresentationListItem").GetComponent<PresentationListItem>();
                    presentationItem.id = cache.ID;
                    //PresentationListItem.slide = Presentation.slides[pageItem.id];
                    presentationItem.ChangeName(cache.filename);
                    presentationItem.ChangeLeftName(cache.type.ToString());
                }

                PresentationList.container.transform.parent.GetComponent<ScrollRect>().normalizedPosition = new Vector2(0, 1);
                initialized = true;
            }
        }
    }
}
