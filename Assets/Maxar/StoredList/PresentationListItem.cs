﻿using System.IO;
using UnityEngine;
using UnityEngine.UI;
using Maxar;
using UnityEngine.SceneManagement;

public class PresentationListItem : MonoBehaviour {
    public string id;
    private PresentationSaver loader;
    private PresentationInspector presentationInspector;

    public void Pressed() {
        Presentation.Load(id);
        switch (Presentation.type) {
            case PresentationType.GroundPlane:
                SceneManager.LoadScene("EditorGroundPlane");
                break;
            case PresentationType.ObjectAnchor:
                SceneManager.LoadScene("EditorObject");
                break;
        }
    }

    public void Delete() {
        File.Delete(Path.Combine(Application.persistentDataPath, id + ".json"));
        loader.DeleteCache(id);
        Debug.Log("Delete presentation with ID: " + id);
        transform.parent.parent.GetComponent<DynamicList>().Delete(transform.GetSiblingIndex());
    }

    public void ChangeName(string newName) {
        GetComponent<iOS_TableViewItem>().TitleText = newName;
        transform.GetChild(2).gameObject.GetComponent<Text>().text = newName;
    }
    public void ChangeLeftName(string name) {
        GetComponent<iOS_TableViewItem>().RightText = name;
        transform.GetChild(3).gameObject.GetComponent<Text>().text = name;
    }

    // Use this for initialization
    private void Start() {
        //id = transform.GetSiblingIndex();
        loader = FindObjectOfType<PresentationSaver>();
        presentationInspector = transform.parent.parent.parent.GetComponent<PresentationInspector>();
    }

    // Update is called once per frame
    private void Update() {
        //id = transform.GetSiblingIndex();
    }
}
