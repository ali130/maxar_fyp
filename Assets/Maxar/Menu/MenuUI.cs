﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Maxar;

public class MenuUI : MonoBehaviour {

    public void LoadButtonPressed() {
        SceneManager.LoadScene("StoredList");
    }
    public void NewGroundButtonPressed() {
        Presentation.Create(PresentationType.GroundPlane);
        SceneManager.LoadScene("EditorGroundPlane");
    }

    public void NewObjectButtonPressed() {
        Presentation.Create(PresentationType.ObjectAnchor);
        SceneManager.LoadScene("EditorObject");
    }

    // Use this for initialization
    private void Awake() {
        Application.targetFrameRate = 60;
    }
}
