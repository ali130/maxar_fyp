using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Maxar;
using Newtonsoft.Json;

namespace Maxar {
    [Serializable]
    public class PresentationSaver : ScriptableObject {
        [SerializeField]
        public List<PresentationCacheClass> CacheList;

        //public string dataPath;

        private void OnEnable() {
            hideFlags = HideFlags.HideInHierarchy;
            //if (LoadedList == null)
                //LoadedList = new List<PresentationCacheClass>();
            //FindDataPath();
        }

        public string dataPath {
            get { return Path.Combine(Application.persistentDataPath, "PresentationData.json"); }
        }

        public void SaveCache(PresentationObject obj)
        {
            LoadCache();
            PresentationCacheClass presentationCache = new PresentationCacheClass(obj.ID, obj.filename, obj.slides.Count, obj.type);
            if (!CheckDupCache(presentationCache))
            {
                CacheList.Add(presentationCache);
            }
            JsonSerializer serializer = new JsonSerializer
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                TypeNameHandling = TypeNameHandling.Auto,
                Formatting = Formatting.Indented
            };

            using (StreamWriter sw = new StreamWriter(dataPath, false))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                serializer.Serialize(writer, this);
                Debug.Log("Cache Saved");
            }
        }

        public void DeleteCache(string id)
        {
            PresentationCacheClass cache = CacheList.Find(obj => obj.ID == id);
            if (cache != null)
            {
                CacheList.Remove(cache);
                JsonSerializer serializer = new JsonSerializer
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                    TypeNameHandling = TypeNameHandling.Auto,
                    Formatting = Formatting.Indented
                };

                using (StreamWriter sw = new StreamWriter(dataPath, false))
                using (JsonWriter writer = new JsonTextWriter(sw))
                {
                    serializer.Serialize(writer, this);
                    Debug.Log("Cache Saved");
                }
            }
        }

        public bool CheckDupCache(PresentationCacheClass cache)
        {
            foreach (PresentationCacheClass elem in CacheList)
            {
                if (elem.ID == cache.ID)
                {
                    elem.filename = cache.filename;
                    elem.numOfSlides = cache.numOfSlides;
                    return true;
                }
            }
            return false;
        }

        public void LoadCache(){
            //FindDataPath();
            CacheList = new List<PresentationCacheClass>();
            try
            {
                using (StreamReader streamReader = File.OpenText(dataPath))
                {
                    string jsonString = "";
                    try
                    {
                        jsonString = streamReader.ReadToEnd();
                    }
                    catch (Exception e)
                    {
                        Debug.Log(e.Message);
                    }

                    Debug.Log(jsonString);

                    if (jsonString != "")
                    {
                        PresentationSaver tempList = JsonConvert.DeserializeObject<PresentationSaver>(jsonString, new JsonSerializerSettings
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                            TypeNameHandling = TypeNameHandling.Auto,
                            Formatting = Formatting.Indented
                        });
                        Debug.Log("tempList.LoadedList"+tempList.CacheList);
                        if (tempList != null)
                        {
                            foreach (PresentationCacheClass LoadedData in tempList.CacheList)
                            {
                                Debug.Log("Loaded One Data");
                                CacheList.Add(LoadedData);
                            }
                        }
                    }
                }
            } catch (Exception e)
            {
                Debug.Log(e.Message);

            }
        }
    }
}