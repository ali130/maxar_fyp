﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEditor;

namespace Maxar {
    public enum PresentationType { GroundPlane, ObjectAnchor }

    public static class Presentation {
        public static PresentationType type;
        public static string filename;
        public static List<Slide> slides;
        public static int stageWidth, stageDepth;

        public static PresentationObject presentationObject;
        
        // Create a new presentation
        public static void Create(PresentationType presentationType) {
            Clear();

            type = presentationType;
            filename = "Untitled";
            slides = new List<Slide> {
                ScriptableObject.CreateInstance<Slide>()
            };
            stageWidth = 1;
            stageDepth = 1;

            presentationObject = ScriptableObject.CreateInstance<PresentationObject>();
        }

        // Save existing presentation
        public static void Save() {
            presentationObject.Save();
        }

        //Load Specific Presentation
        public static void Load(string id) {
            Clear();

            presentationObject = ScriptableObject.CreateInstance<PresentationObject>();
            presentationObject.Load(id);
            type = presentationObject.type;
            filename = presentationObject.filename;
            stageDepth = presentationObject.stageDepth;
            stageWidth = presentationObject.stageWidth;
            slides = presentationObject.slides;
        }

        // Clear all attributes of the presentation
        public static void Clear() {
            filename = "";
            stageWidth = 0;
            stageDepth = 0;
            if (slides != null) {
                foreach (Slide slide in slides) {
                    slide.elementList.Clear();
                    slide.animationList.Clear();
                }
                slides.Clear();
            }
        }
    }
}