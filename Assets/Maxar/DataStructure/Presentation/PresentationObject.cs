using System;
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Newtonsoft.Json;

namespace Maxar
{
    [Serializable]
    public class PresentationObject : ScriptableObject {
        public string filename;

        [SerializeField]
        public List<Slide> slides;
        public int stageDepth, stageWidth;
        public string ID;
        private string dataPath;
        public PresentationType type;

        private void OnEnable()
        {

            hideFlags = HideFlags.HideInHierarchy;
            filename = Presentation.filename;
            slides = Presentation.slides;
            stageDepth = Presentation.stageDepth;
            stageWidth = Presentation.stageWidth;
            type = Presentation.type;
            InitID();
            FindDataPath(ID);
        }

        private void InitID()
        {
            System.Random random = new System.Random();
            int num = random.Next(100000, 999999);
            string str = num.ToString();
            while (IDisDup(str))
            {
                num = random.Next(100000, 999999);
                str = num.ToString();
            }
            ID = str;
        }

        private bool IDisDup(string str)
        {
            PresentationSaver saver = ScriptableObject.CreateInstance<PresentationSaver>();
            saver.LoadCache();
            foreach (PresentationCacheClass ppt in saver.CacheList)
            {
                if (ppt.ID == str) return true;
            }
            return false;
        }

        private void FindDataPath(string id)
        {
            dataPath = Path.Combine(Application.persistentDataPath, id + ".json");

        }

        public void Save() {
            // Update presentation attributes
            filename = Presentation.filename;
            stageDepth = Presentation.stageDepth;
            stageWidth = Presentation.stageWidth;

            //Save Presentation in Cache List
            PresentationSaver saver = ScriptableObject.CreateInstance<PresentationSaver>();
            saver.SaveCache(this);

            //Serialize Presentation Object
            JsonSerializer serializer = new JsonSerializer
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                TypeNameHandling = TypeNameHandling.Auto,
                Formatting = Formatting.Indented
            };

            using (StreamWriter sw = new StreamWriter(dataPath, false))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                serializer.Serialize(writer, this);
                Debug.Log("Object Saved");
            }

        }

        public void Load(string id){
            FindDataPath(id);
            try
            {
                using (StreamReader streamReader = File.OpenText(dataPath))
                {
                    string jsonString = "";
                    try
                    {
                        jsonString = streamReader.ReadToEnd();
                    }
                    catch (Exception e)
                    {
                        Debug.Log(e.Message);
                    }

                    if (jsonString != "")
                    {
                        PresentationObject obj = JsonConvert.DeserializeObject<PresentationObject>(jsonString, new JsonSerializerSettings
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                            TypeNameHandling = TypeNameHandling.Auto,
                            Formatting = Formatting.Indented
                        });
                        filename = obj.filename;
                        slides = obj.slides;
                        stageDepth = obj.stageDepth;
                        stageWidth = obj.stageWidth;
                        ID = obj.ID;
                        type = obj.type;
                    }
                }
            }
            catch (Exception e)
            {
                Debug.Log(e.Message);

            }
        }
    }
}