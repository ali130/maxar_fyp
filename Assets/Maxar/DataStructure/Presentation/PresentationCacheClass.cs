﻿namespace Maxar
{
    public class PresentationCacheClass
    {
        public string ID;
        public string filename;
        public int numOfSlides;
        public PresentationType type;


        public PresentationCacheClass(string ID, string filename, int numOfSlides, PresentationType type) {
            this.ID = ID;
            this.filename = filename;
            this.numOfSlides = numOfSlides;
            this.type = type;

        }
    }
}
