﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

namespace Maxar {
    [Serializable]
    public class Slide : ScriptableObject {
        public List<Element> elementList;
        public List<ElementAnimation> animationList;
        public string objectTargetPath;
        public string objectTargetName;

        public Slide() {
            elementList = new List<Element>();
            animationList = new List<ElementAnimation>();
            objectTargetPath = "";
            objectTargetName = "";
        }

        public Slide(List<Element> elementList, List<ElementAnimation> animationList, string objectTargetPath) {
            this.elementList = elementList;
            this.animationList = animationList;
            this.objectTargetPath = objectTargetPath;
        }

        public void GetFinalState() {
            foreach (Element elem in elementList) {
                elem.obj.transform.localPosition = elem.objPosition;
                elem.obj.transform.localRotation = elem.objRotation;
                elem.obj.transform.localScale = new Vector3(elem.objScale * elem.scaleFactor, elem.objScale * elem.scaleFactor, elem.objScale * elem.scaleFactor);
                elem.DisplayOpacity(elem.objOpacity);
            }

            foreach (ElementAnimation anim in animationList) {
                anim.elem.obj.transform.localPosition = anim.endState.position;
                anim.elem.obj.transform.localRotation = anim.endState.rotation;
                anim.elem.obj.transform.localScale = new Vector3(anim.endState.scale * anim.elem.scaleFactor, anim.endState.scale * anim.elem.scaleFactor, anim.endState.scale * anim.elem.scaleFactor);
                anim.elem.DisplayOpacity(anim.endState.opacity);
            }
        }
    }
}