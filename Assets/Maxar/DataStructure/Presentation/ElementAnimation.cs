﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

namespace Maxar {
    [Serializable]
    public class ElementAnimation : ScriptableObject {
        public int pageId, elemId;
        public ElementState startState;
        public ElementState endState;
        public float duration;

        public class ElementState {
            public Vector3 position;
            public Quaternion rotation;
            public float scale;
            public float opacity;
        }

        [JsonIgnore]
        public Animation anim;
        private AnimationClip clip;
        private AnimationCurve posX, posY, posZ, rotX, rotY, rotZ, sca, opa;
        [JsonIgnore]
        public Element elem;

        public ElementAnimation() {
            pageId = 0;
            elemId = 0;
            startState = new ElementState();
            endState = new ElementState();
            duration = 0.0f;
        }

        public void Update() {
            elem = Presentation.slides[pageId].elementList[elemId];
            anim = elem.obj.GetComponent<Animation>();
            anim.wrapMode = WrapMode.Once;
            if (anim.GetClip("clip") != null) {
                anim.RemoveClip("clip");
            }

            // create a new AnimationClip
            clip = new AnimationClip
            {
                legacy = true
            };

            // create curves and assign to the clip
            posX = AnimationCurve.Linear(0.0f, startState.position.x, duration, endState.position.x);
            posY = AnimationCurve.Linear(0.0f, startState.position.y, duration, endState.position.y);
            posZ = AnimationCurve.Linear(0.0f, startState.position.z, duration, endState.position.z);
            rotX = AnimationCurve.Linear(0.0f, startState.rotation.eulerAngles.x, duration, endState.rotation.eulerAngles.x);
            rotY = AnimationCurve.Linear(0.0f, startState.rotation.eulerAngles.y, duration, endState.rotation.eulerAngles.y);
            rotZ = AnimationCurve.Linear(0.0f, startState.rotation.eulerAngles.z, duration, endState.rotation.eulerAngles.z);
            sca = AnimationCurve.Linear(0.0f, startState.scale * elem.scaleFactor, duration, endState.scale * elem.scaleFactor);
            opa = AnimationCurve.Linear(0.0f, 1.0f - startState.opacity, duration, 1.0f - endState.opacity);
            clip.SetCurve("", typeof(Transform), "localPosition.x", posX);
            clip.SetCurve("", typeof(Transform), "localPosition.y", posY);
            clip.SetCurve("", typeof(Transform), "localPosition.z", posZ);
            clip.SetCurve("", typeof(Transform), "localEulerAngles.x", rotX);
            clip.SetCurve("", typeof(Transform), "localEulerAngles.y", rotY);
            clip.SetCurve("", typeof(Transform), "localEulerAngles.z", rotZ);
            clip.SetCurve("", typeof(Transform), "localScale.x", sca);
            clip.SetCurve("", typeof(Transform), "localScale.y", sca);
            clip.SetCurve("", typeof(Transform), "localScale.z", sca);
            foreach (KeyValuePair<string, GameObject> child in GetChildrenPaths(elem.obj)) {
                if (child.Value.GetComponent<MeshRenderer>() != null) {
                    for (int i = 0; i < child.Value.GetComponent<MeshRenderer>().materials.Length; ++i) {
                        clip.SetCurve(child.Key, typeof(Material), "[" + i.ToString() + "]._Transparency", opa);
                    }
                }
                if (child.Value.GetComponent<SkinnedMeshRenderer>() != null) {
                    for (int i = 0; i < child.Value.GetComponent<SkinnedMeshRenderer>().materials.Length; ++i) {
                        clip.SetCurve(child.Key, typeof(Material), "[" + i.ToString() + "]._Transparency", opa);
                    }
                }
            }

            anim.AddClip(clip, "clip");
        }

        public void Play() {
            // Animate the GameObject
            anim.Play("clip");
        }

        private Dictionary<string, GameObject> GetChildrenPaths(GameObject root, bool showRoot = false) {
            Dictionary<string, GameObject> results = new Dictionary<string, GameObject>();

            if (root.transform.childCount > 0) {
                if (showRoot) {
                    results.Add(root.name, root);
                }
                foreach (Transform child in root.transform) {
                    foreach (KeyValuePair<string, GameObject> entry in GetChildrenPaths(child.gameObject, true)) {
                        if (showRoot) {
                            results.Add(root.name + "/" + entry.Key, entry.Value);
                        } else {
                            results.Add(entry.Key, entry.Value);
                        }
                    }
                }
            } else {
                if (showRoot) {
                    results.Add(root.name, root);
                } else {
                    results.Add("", root);
                }
            }

            return results;
        }
    }
}
