﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Maxar {
    [Serializable]
    public class Stage : Element {
        public int width, depth;  // Aspect ratio

        // Initialize object
        public override void Create(GameObject parent = null) {
            obj = GameObject.CreatePrimitive(PrimitiveType.Plane);
            obj.GetComponent<MeshRenderer>().material = Resources.Load<Material>("Materials/Stage/GroundPlaneIndicator") as Material;
            objScale = 1.0f;
            scaleFactor = 1.0f;
            Destroy(obj.GetComponent<MeshCollider>());
            obj.AddComponent<BoxCollider>();
            obj.name = objName;
            Refresh();
        }
        
        // Update object
        public override void Refresh(GameObject parent = null) {
            obj.transform.localScale = new Vector3(width * 0.1f, 1, depth * 0.1f);

            objPosition = obj.transform.position;
            objRotation = obj.transform.rotation;
        }

        public override void ApplyMeshRenderer() {

        }

        public override void DisplayOpacity(float opacity) {

        }
    }
}
