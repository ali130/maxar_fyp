﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Maxar {
    public class Model3D : Element {
        public string path;
        private bool meshRendererApplied;

        // Initialize object
        public override void Create(GameObject parent = null) {
        

            GameObject newModel = Instantiate(Resources.Load(path)) as GameObject;

            // Add parent if needed
            if (newModel.GetComponent<MeshRenderer>() != null) {
                obj = new GameObject(objName);
                newModel.transform.parent = obj.transform;
                newModel.transform.position = new Vector3(0, 0, 0);
            } else {
                obj = newModel;
                obj.name = objName;
            }

            
            //scaleFactor = 1.0f;

            // Set model
            obj.AddComponent<MeshRenderer>();

            foreach (Transform child in obj.transform) {
                if (child.gameObject.GetComponent<MeshRenderer>() != null || child.gameObject.GetComponent<SkinnedMeshRenderer>() != null) {
                    child.gameObject.AddComponent<BoxCollider>();
                }
            }

            obj.AddComponent<Animation>().playAutomatically = false;

            meshRendererApplied = false;
            Refresh(parent);
        }

        // Update object
        public override void Refresh(GameObject parent = null) {
            Vector3 parentScale = new Vector3(1.0f, 1.0f, 1.0f);
            if (parent != null) {
                parentScale = parent.transform.localScale;
            }

            obj.transform.localScale = new Vector3(objScale * scaleFactor * parentScale.x, objScale * scaleFactor * parentScale.y, objScale * scaleFactor * parentScale.z);
            ApplyMeshRenderer();
            DisplayOpacity(objOpacity);

            if (parent != null) {
                obj.transform.parent = parent.transform;
            }
            obj.transform.localPosition = objPosition;
            obj.transform.localRotation = objRotation;
        }

        public override void ApplyMeshRenderer() {
            List<Material> materialList = new List<Material>();
            foreach (MeshRenderer childRenderer in obj.GetComponentsInChildren<MeshRenderer>()) {
                if (childRenderer.gameObject.GetInstanceID() != obj.GetInstanceID()) {
                    Material[] childMaterials = meshRendererApplied ? childRenderer.sharedMaterials : childRenderer.materials;
                    for (int i = 0; i < childMaterials.Length; ++i) {
                        childMaterials[i].shader = Resources.Load<Shader>("Shaders/Double Sided Transparent Diffuse Bump") as Shader;
                        materialList.Add(childMaterials[i]);
                        childRenderer.sharedMaterials[i] = materialList[materialList.Count - 1];
                    }
                }
            }
            foreach (SkinnedMeshRenderer childRenderer in obj.GetComponentsInChildren<SkinnedMeshRenderer>()) {
                if (childRenderer.gameObject.GetInstanceID() != obj.GetInstanceID()) {
                    Material[] childMaterials = meshRendererApplied ? childRenderer.sharedMaterials : childRenderer.materials;
                    for (int i = 0; i < childMaterials.Length; ++i) {
                        childMaterials[i].shader = Resources.Load<Shader>("Shaders/Double Sided Transparent Diffuse Bump") as Shader;
                        materialList.Add(childMaterials[i]);
                        childRenderer.sharedMaterials[i] = materialList[materialList.Count - 1];
                    }
                }
            }
            if (!meshRendererApplied) {
                obj.GetComponent<MeshRenderer>().materials = materialList.ToArray();
                meshRendererApplied = true;
            }
        }

        public override void DisplayOpacity(float opacity) {
            foreach (Material material in obj.GetComponent<MeshRenderer>().sharedMaterials) {
                material.SetFloat("_Transparency", 1.0f - opacity);
            }
        }
    }
}