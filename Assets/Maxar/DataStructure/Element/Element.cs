﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using Newtonsoft.Json;

namespace Maxar {
    [Serializable]
    public abstract class Element : ScriptableObject {
        public string objName;
        public Vector3 objPosition;
        public Quaternion objRotation;
        public float objScale;
        public float scaleFactor;
        public float objOpacity;

        [JsonIgnore]
        public GameObject obj { get; set; }

        public abstract void Create(GameObject parent = null);
        public abstract void Refresh(GameObject parent = null);
        public abstract void ApplyMeshRenderer();
        public abstract void DisplayOpacity(float opacity);
    }
}