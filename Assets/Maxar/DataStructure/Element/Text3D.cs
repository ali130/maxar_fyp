﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

namespace Maxar {
    [Serializable]
    public class Text3D : Element {    
        public string text;

        // Initialize object
        public override void Create(GameObject parent = null) {
            obj = Instantiate(Resources.Load("Prefabs/Text3D/text")) as GameObject;
            obj.name = objName;
            scaleFactor = 0.02f;

            // Set text
            Refresh(parent);
            obj.GetComponent<SimpleHelvetica>().AddBoxCollider();
        }

        // Update object
        public override void Refresh(GameObject parent = null) {
            Vector3 parentScale = new Vector3(1.0f, 1.0f, 1.0f);
            if (parent != null) {
                parentScale = parent.transform.localScale;
            }

            obj.GetComponent<SimpleHelvetica>().Text = text;
            obj.transform.localScale = new Vector3(objScale * scaleFactor * parentScale.x, objScale * scaleFactor * parentScale.y, objScale * scaleFactor * parentScale.z);
            DisplayOpacity(objOpacity);
            ApplyMeshRenderer();
            obj.GetComponent<SimpleHelvetica>().GenerateText();

            if (parent != null) {
                obj.transform.parent = parent.transform;
            }
            obj.transform.localPosition = objPosition;
            obj.transform.localRotation = objRotation;
        }

        public override void ApplyMeshRenderer() {
            obj.GetComponent<SimpleHelvetica>().ApplyMeshRenderer();
        }

        // Override to cater for Simple Helvetica
        public override void DisplayOpacity(float opacity) {
            obj.GetComponent<MeshRenderer>().material.SetFloat("_Transparency", 1.0f - opacity);
            ApplyMeshRenderer();
        }
    }
}
