﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DynamicList : MonoBehaviour {
    public GameObject container;
    public int bottom;  // Margin at the bottom
    public int itemHeight;

    private void FixBorders() {
        for (int i = 0; i < container.transform.childCount; ++i) {
            iOS_TableViewItem item = container.transform.GetChild(i).GetComponent<iOS_TableViewItem>();

            // Normal configuaration
            item.showHeaderLine = false;
            item.showFooterLine = true;
            item.FooterLineLength = true;

            // Table top and bottom
            if (i == 0) {
                item.showHeaderLine = true;
            }
            if (i == container.transform.childCount - 1) {
                item.FooterLineLength = false;
            }

            GameObject HeaderLine = item.transform.GetChild(0).gameObject;
            HeaderLine.SetActive(item.showHeaderLine);
        }
    }

    public GameObject Append(string prefabPath) {
        GameObject obj;
        obj = Instantiate(Resources.Load(prefabPath)) as GameObject;
        obj.transform.SetParent(container.transform, true);
        obj.name = "TableViewItem" + obj.transform.parent.childCount;

        GameObject lastChild = null;

        if (obj.transform.parent.childCount > 1) {
            lastChild = obj.transform.parent.GetChild(obj.transform.parent.childCount - 2).gameObject;
        }

        CanvasHandler canvasHandler = obj.GetComponentInParent(typeof(CanvasHandler)) as CanvasHandler;

        if (obj.transform.parent.childCount == 1) {
            obj.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
        } else {
            obj.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, lastChild.GetComponent<RectTransform>().anchoredPosition.y + (-itemHeight * canvasHandler.retinaMultipler));
        }
        obj.GetComponent<RectTransform>().sizeDelta = new Vector2(obj.GetComponent<RectTransform>().rect.width, itemHeight * canvasHandler.retinaMultipler);

        obj.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);

        float anchor = obj.GetComponent<RectTransform>().localPosition.y + container.GetComponent<RectTransform>().rect.height / 2 - (itemHeight + bottom) * canvasHandler.retinaMultipler;
        if (anchor < 0) {
            container.GetComponent<RectTransform>().offsetMin += new Vector2(0, anchor);
            container.transform.parent.GetComponent<ScrollRect>().normalizedPosition = new Vector2(0, 0);
        }

        FixBorders();

        return obj;
    }

    public void Delete(int index) {
        GameObject obj = container.transform.GetChild(index).gameObject;
        CanvasHandler canvasHandler = obj.GetComponentInParent(typeof(CanvasHandler)) as CanvasHandler;

        for (int i = index + 1; i < container.transform.childCount; ++i) {
            GameObject child = container.transform.GetChild(i).gameObject;
            child.GetComponent<RectTransform>().anchoredPosition += new Vector2(0, itemHeight * canvasHandler.retinaMultipler);
        }

        DestroyImmediate(obj);

        if (container.transform.childCount >= 1) {
            GameObject lastChild = container.transform.GetChild(container.transform.childCount - 1).gameObject;
            float excess = container.GetComponent<RectTransform>().rect.height - (Screen.height - bottom * canvasHandler.retinaMultipler);
            if (excess > 0.0f) {
                container.GetComponent<RectTransform>().offsetMin += new Vector2(0, Mathf.Min(excess, itemHeight * canvasHandler.retinaMultipler));
            }
        }

        FixBorders();
    }
}
