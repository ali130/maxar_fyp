﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToolBarButton : MonoBehaviour {
    public bool active;
    private Image image;

    public void Enable() {
        active = true;
        image.raycastTarget = true;
        //image.color = new Color(0.0f, 122.0f, 255.0f, 255.0f);
    }

    public void Disable() {
        active = false;
        image.raycastTarget = false;
        //image.color = new Color(200.0f, 200.0f, 200.0f, 255.0f);
    }

    // Use this for initialization
    private void Start() {
        image = GetComponent<Image>();
        if (active) {
            Enable();
        } else {
            Disable();
        }
    }
}
