﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardInput : ScriptableObject {
    public string inputName, inputText;
    public TouchScreenKeyboard keyboard;

    public void Open(string name, string text, TouchScreenKeyboardType type, bool autocorrection) {
        inputName = name;
        inputText = text;
        keyboard = TouchScreenKeyboard.Open(inputText, type, autocorrection);
    }
}
