﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Maxar;

public class PageListItem : MonoBehaviour {
    public int id;
    private EditorSlideLoader loader;
    private PageInspector pageInspector;
    public Slide slide;

    public void Pressed() {
        EditorUI.page = id;
        loader.Load();
    }

    public void ChangeName(string newName) {
        GetComponent<iOS_TableViewItem>().TitleText = newName;
        transform.GetChild(2).gameObject.GetComponent<Text>().text = newName;
    }

    // Use this for initialization
    void Start() {
        id = transform.GetSiblingIndex();
        loader = FindObjectOfType<EditorSlideLoader>();
        pageInspector = transform.parent.parent.parent.GetComponent<PageInspector>();
	}
	
	// Update is called once per frame
	void Update() {
        id = transform.GetSiblingIndex();
    }
}
