﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ARObjectCopier : MonoBehaviour {

	// Use this for initialization
	private void Start() {
        List<string> streamingFiles = new List<string>(Directory.GetFiles(Application.streamingAssetsPath, "*.arobject"));
        List<string> persistentFiles = new List<string>(Directory.GetFiles(Application.persistentDataPath, "*.arobject"));
        if (streamingFiles.Count > 0) {
            foreach (string file in streamingFiles) {
                string relativeSourcePath = file.Substring(Application.streamingAssetsPath.Length + 1);
                string targetPath = Path.Combine(Application.persistentDataPath, relativeSourcePath);
                if (!persistentFiles.Contains(targetPath)) {
                    File.Copy(file, targetPath);
                }
            }
        }
        SceneManager.LoadScene("Menu");
    }
}
