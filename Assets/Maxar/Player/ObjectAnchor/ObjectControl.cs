﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Maxar;

public class ObjectControl : MonoBehaviour {
    private EventSystem eventSystem;
    public Slide slide;
    public List<Element> elementList;
    public ElementAnimation elementAnimation;
    public int animSeq;
    public bool first, last;

    public void Play() {
        elementAnimation.Update();
        Debug.Log("Playing anmimation with ID: " + EditorUI.animSeq.ToString());
        elementAnimation.Play();
    }

    public void NextFrame() {
        if (animSeq < slide.animationList.Count - 1) {
            animSeq += 1;
            elementAnimation = slide.animationList[animSeq];
            Play();
        } else {
            last = true;
        }
    }

    public void PrevFrame() {
        if (animSeq >= 0) {
            elementAnimation.elem.obj.transform.localPosition = elementAnimation.startState.position;
            elementAnimation.elem.obj.transform.localRotation = elementAnimation.startState.rotation;
            elementAnimation.elem.obj.transform.localScale = new Vector3(elementAnimation.startState.scale * elementAnimation.elem.scaleFactor, elementAnimation.startState.scale * elementAnimation.elem.scaleFactor, elementAnimation.startState.scale * elementAnimation.elem.scaleFactor);
            elementAnimation.elem.DisplayOpacity(elementAnimation.startState.opacity);
            animSeq -= 1;
            if (animSeq >= 0) {
                elementAnimation = slide.animationList[animSeq];
            }
        } else {
            first = true;
        }
    }

    // Use this for initialization
    private void Start() {
        eventSystem = FindObjectOfType<EventSystem>();
        animSeq = -1;
	}

    // Update is called once per frame
    private void Update() {
        if (elementAnimation != null) {
            if (elementAnimation.anim != null) {
                if (elementAnimation.anim.isPlaying) {
                    eventSystem.gameObject.SetActive(false);
                } else {
                    eventSystem.gameObject.SetActive(true);
                }
            }
        }
    }
}
