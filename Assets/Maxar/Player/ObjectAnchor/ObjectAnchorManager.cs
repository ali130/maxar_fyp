﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.iOS;

public class ObjectAnchorManager : MonoBehaviour {
    public Camera cam;
    public PlayerObjectUI playerUI;
    public PlayerObjectLoader loader;
    public Dictionary<string, GameObject> anchorMap;
    public List<ARReferenceObject> objectList;
    public bool selected;
    public string selectedId;
    private bool sessionStarted;

    static UnityARSessionNativeInterface session {
        get { return UnityARSessionNativeInterface.GetARSessionNativeInterface(); }
    }

    // Return clicked object's name
    private string ObjectClicked(Touch touch) {
        RaycastHit hitInfo = new RaycastHit();
        bool hit = Physics.Raycast(cam.ScreenPointToRay(touch.position), out hitInfo);
        GameObject obj;

        if (hit) {
            obj = hitInfo.transform.gameObject;
            selected = true;
            selectedId = obj.GetComponent<ObjectControl>().slide.objectTargetName;
        } else {
            selected = false;
            selectedId = "";
        }

        return selectedId;
    }

    public void StartDetection() {
        //create a set out of the ARReferenceObjects
        IntPtr ptrReferenceObjectsSet = session.CreateNativeReferenceObjectsSet(objectList);

        //restart session without resetting tracking 
        ARKitWorldTrackingSessionConfiguration config = new ARKitWorldTrackingSessionConfiguration {
            planeDetection = UnityARPlaneDetection.Horizontal,
            alignment = UnityARAlignment.UnityARAlignmentGravity,
            getPointCloudData = true,
            enableLightEstimation = true,
            enableAutoFocus = true,

            //use object set from above to detect objects
            dynamicReferenceObjectsPtr = ptrReferenceObjectsSet
        };

        session.RunWithConfigAndOptions(config, UnityARSessionRunOption.ARSessionRunOptionRemoveExistingAnchors | UnityARSessionRunOption.ARSessionRunOptionResetTracking);
        UnityARSessionNativeInterface.ARFrameUpdatedEvent += FirstFrameUpdate;
    }

    private void FirstFrameUpdate(UnityARCamera ARCamera) {
        sessionStarted = true;
        UnityARSessionNativeInterface.ARFrameUpdatedEvent -= FirstFrameUpdate;
    }

    private void AddObjectAnchor(ARObjectAnchor arObjectAnchor) {
        Debug.Log("object anchor added");
        if (anchorMap.ContainsKey(arObjectAnchor.referenceObjectName)) {
            Vector3 position = UnityARMatrixOps.GetPosition(arObjectAnchor.transform);
            Quaternion rotation = UnityARMatrixOps.GetRotation(arObjectAnchor.transform);

            GameObject obj = anchorMap[arObjectAnchor.referenceObjectName];
            obj.transform.position = position;
            obj.transform.rotation = rotation;
            loader.Show(obj);
        }
    }

    private void UpdateObjectAnchor(ARObjectAnchor arObjectAnchor) {
        //Debug.Log("object anchor updated");
        if (anchorMap.ContainsKey(arObjectAnchor.referenceObjectName)) {
            Vector3 position = UnityARMatrixOps.GetPosition(arObjectAnchor.transform);
            Quaternion rotation = UnityARMatrixOps.GetRotation(arObjectAnchor.transform);

            GameObject obj = anchorMap[arObjectAnchor.referenceObjectName];
            obj.transform.position = position;
            obj.transform.rotation = rotation;
        }
    }

    private void RemoveObjectAnchor(ARObjectAnchor arObjectAnchor) {
        Debug.Log("object anchor removed");
        if (anchorMap.ContainsKey(arObjectAnchor.referenceObjectName)) {
            GameObject obj = anchorMap[arObjectAnchor.referenceObjectName];
            loader.Hide(obj);
        }
    }

    // Use this for initialization
    private void Start() {
        selectedId = "";
        UnityARSessionNativeInterface.ARObjectAnchorAddedEvent += AddObjectAnchor;
        UnityARSessionNativeInterface.ARObjectAnchorUpdatedEvent += UpdateObjectAnchor;
        UnityARSessionNativeInterface.ARObjectAnchorRemovedEvent += RemoveObjectAnchor;
        sessionStarted = false;
    }

    // Update is called once per frame
    private void Update() {
        if (cam != null && sessionStarted) {
            // Update camera pose
            Matrix4x4 matrix = session.GetCameraPose();
            cam.transform.localPosition = UnityARMatrixOps.GetPosition(matrix);
            cam.transform.localRotation = UnityARMatrixOps.GetRotation(matrix);

            cam.projectionMatrix = session.GetCameraProjection();

            if (Input.touchCount == 1) {
                Touch touch = Input.GetTouch(0);
                if ((touch.phase == TouchPhase.Ended) && !playerUI.IsTouchOnUI(touch)) {
                    ObjectClicked(touch);
                }
            }
        }
    }

    private void OnDestroy() {
        anchorMap.Clear();
        objectList.Clear();
        selected = false;
        selectedId = "";
        UnityARSessionNativeInterface.ARObjectAnchorAddedEvent -= AddObjectAnchor;
        UnityARSessionNativeInterface.ARObjectAnchorUpdatedEvent -= UpdateObjectAnchor;
        UnityARSessionNativeInterface.ARObjectAnchorRemovedEvent -= RemoveObjectAnchor;
    }
}
