﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.XR.iOS;
using Maxar;

public class PlayerObjectLoader : MonoBehaviour {
    public ObjectAnchorManager objectAnchorManager;

    public void Load() {
        if (objectAnchorManager.anchorMap == null) {
            objectAnchorManager.anchorMap = new Dictionary<string, GameObject>();
        } else {
            foreach (GameObject obj in objectAnchorManager.anchorMap.Values) {
                Destroy(obj);
            }
            objectAnchorManager.anchorMap.Clear();
        }
        if (objectAnchorManager.objectList == null) {
            objectAnchorManager.objectList = new List<ARReferenceObject>();
        } else {
            objectAnchorManager.objectList.Clear();
        }

        foreach (Slide slide in Presentation.slides) {
            if (!objectAnchorManager.anchorMap.ContainsKey(slide.objectTargetName)) {
                GameObject obj = new GameObject();
                ARReferenceObject refObj = ARReferenceObject.Load(Path.Combine(Application.persistentDataPath, slide.objectTargetPath));

                // Add ObjectControl to anchor
                ObjectControl control = obj.AddComponent<ObjectControl>();
                control.slide = slide;
                control.elementList = slide.elementList;
                foreach (Element elem in control.elementList) {
                    elem.Create(obj);
                    Debug.Log("Added " + elem.objName + " to " + slide.objectTargetName);
                }
                Debug.Log(control.elementList.Count.ToString() + " object(s) loaded");
                control.first = true;
                control.last = slide.animationList.Count == 0;

                // Add BoxCollider to anchor
                BoxCollider boxCollider = obj.AddComponent<BoxCollider>();
                boxCollider.center = refObj.center;
                boxCollider.size = refObj.extent;

                // Register to ObjectAnchorManager
                objectAnchorManager.anchorMap.Add(slide.objectTargetName, obj);
                objectAnchorManager.objectList.Add(refObj);
            }
        }

        if (objectAnchorManager.objectList == null) {
            objectAnchorManager.objectList = new List<ARReferenceObject>();
            foreach (GameObject obj in objectAnchorManager.anchorMap.Values) {
                ObjectControl control = obj.GetComponent<ObjectControl>();
                objectAnchorManager.objectList.Add(ARReferenceObject.Load(Path.Combine(Application.persistentDataPath, control.slide.objectTargetPath)));
            }
        }
    }

    public void Show(GameObject obj) {
        if (objectAnchorManager.anchorMap.ContainsValue(obj)) {
            foreach (Element elem in obj.GetComponent<ObjectControl>().elementList) {
                elem.obj.SetActive(true);
            }
        }
    }

    public void Hide(GameObject obj) {
        if (objectAnchorManager.anchorMap.ContainsValue(obj)) {
            foreach (Element elem in obj.GetComponent<ObjectControl>().elementList) {
                elem.obj.SetActive(false);
            }
        }
    }

    public void HideAll() {
        foreach (GameObject obj in objectAnchorManager.anchorMap.Values) {
            foreach (Element elem in obj.GetComponent<ObjectControl>().elementList) {
                elem.obj.SetActive(false);
            }
        }
    }
}
