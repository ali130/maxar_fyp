﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerObjectUI : PlayerUI {
    public ObjectAnchorManager objManager;
    public PlayerObjectLoader loader;
    public static int page = 0;
    public static bool started = false;
    private ObjectControl objControl;
    private string selectedId;

    public override void BackButtonPressed() {
        page = 0;
        started = false;
        SceneManager.LoadScene("EditorObject");
    }

    public override void StartButtonPressed() {

    }

    public override void NextButtonPressed() {
        if (objManager.selected) {
            objControl.NextFrame();
        }
    }

    public override void PrevButtonPressed() {
        if (objManager.selected) {
            objControl.PrevFrame();
        }
    }

    // Use this for initialization
    private void Awake() {
        Application.targetFrameRate = 60;
        loader.Load();
        loader.HideAll();
        selectedId = "";

        // Start object detection
        objManager.StartDetection();
    }
	
	// Update is called once per frame
	private void Update() {
		if (objManager.selected) {
            if (objManager.selectedId != selectedId) {
                selectedId = objManager.selectedId;
                objControl = objManager.anchorMap[objManager.selectedId].GetComponent<ObjectControl>();
                toolBar.gameObject.SetActive(true);
            }
            navBar.TitleText = selectedId + " " + (objControl.animSeq + 2).ToString() + " of " + (objControl.slide.animationList.Count + 1).ToString();
            navBar.transform.GetChild(2).GetComponent<Text>().text = navBar.TitleText;
        } else {
            selectedId = "";
            navBar.TitleText = "Find Object(s)";
            navBar.transform.GetChild(2).GetComponent<Text>().text = navBar.TitleText;
            toolBar.gameObject.SetActive(false);
        }
    }
}
