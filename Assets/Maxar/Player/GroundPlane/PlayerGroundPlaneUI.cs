﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Maxar;

public class PlayerGroundPlaneUI : PlayerUI {
    public GroundPlaneManager planeManager;
    public PlayerSlideLoader loader;
    public SlidesControl slidesControl;
    public static int page = 0;
    public static bool started = false;

    public override void BackButtonPressed() {
        page = 0;
        started = false;
        SceneManager.LoadScene("EditorGroundPlane");
    }

    public override void StartButtonPressed() {
        navBar.transform.GetChild(3).gameObject.SetActive(false);
        planeManager.planeIndicator.obj.SetActive(false);
        toolBar.gameObject.SetActive(true);
        started = true;
    }

    public override void NextButtonPressed() {
        slidesControl.NextFrame();
    }

    public override void PrevButtonPressed() {
        slidesControl.PrevFrame();
    }

    // Use this for initialization
    private void Awake() {
        Application.targetFrameRate = 60;
        loader.Load();
        loader.HideAll();

        // Create the plane indicator
        planeManager.planeIndicator = ScriptableObject.CreateInstance<Stage>();
        planeManager.planeIndicator.width = Presentation.stageWidth;
        planeManager.planeIndicator.depth = Presentation.stageDepth;
        planeManager.planeIndicator.Create();
        planeManager.planeIndicator.obj.transform.parent = planeManager.gameObject.transform;
        planeManager.planeIndicator.obj.SetActive(false);
    }

    // Update is called once per frame
    private void Update() {
        if (started) {
            navBar.TitleText = "Page " + (page + 1).ToString() + " of " + Presentation.slides.Count;
            navBar.transform.GetChild(2).GetComponent<Text>().text = navBar.TitleText;
        } else if (planeManager.anchorSet) {
            navBar.showRightText = true;
            navBar.transform.GetChild(3).gameObject.SetActive(true);
        }
    }
}