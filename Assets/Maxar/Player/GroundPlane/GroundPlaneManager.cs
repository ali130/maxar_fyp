﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.iOS;
using Maxar;

public class GroundPlaneManager : MonoBehaviour {
    public PlayerGroundPlaneUI playerUI;
    public PlayerSlideLoader loader;
    public Stage planeIndicator;
    public bool anchorSet;
    public float scale;
    private float speed;
    public GameObject planePrefab;
    private static UnityARAnchorManager unityARAnchorManager;
    public ARPlaneAnchorGameObject selectedPlane;
    public float maxRayDistance = 30.0f;
    public LayerMask collisionLayer = 1 << 10;  // ARKit Plane layer
    private int maxTouchCount;

    private bool HitTest(ARPoint point, ARHitTestResultType resultTypes, out ARHitTestResult hit) {
        List<ARHitTestResult> hitResults = UnityARSessionNativeInterface.GetARSessionNativeInterface().HitTest(point, resultTypes);
        if (hitResults.Count > 0) {
            foreach (var hitResult in hitResults) {
                hit = hitResult;
                return true;
            }
        }
        hit = new ARHitTestResult();
        return false;
    }

    private void ChangeScale(Touch touch1, Touch touch2) {
        // Find the position in the previous frame of each touch.
        Vector2 touch1Prev = touch1.position - touch1.deltaPosition;
        Vector2 touch2Prev = touch2.position - touch2.deltaPosition;

        // Find the magnitude of the vector (the distance) between the touches in each frame.
        float distPrev = (touch1Prev - touch2Prev).magnitude;
        float dist = (touch1.position - touch2.position).magnitude;

        // Find the difference in the distances between each frame.
        float distDiff = dist - distPrev;

        // Change the scale based on the change in distance between the touches.
        scale += distDiff * speed;
        if (scale < 0.1f) {
            scale = 0.1f;
        }
    }

    private void DrawIndicator() {
        Vector3 screenPosition = Camera.main.ScreenToViewportPoint(new Vector2(Screen.width / 2, Screen.height / 2));
        ARHitTestResult hit;
        ARPoint point = new ARPoint {
            x = screenPosition.x,
            y = screenPosition.y
        };

        // prioritize reults types
        ARHitTestResultType[] resultTypes = {
                        //ARHitTestResultType.ARHitTestResultTypeExistingPlaneUsingGeometry,
                        ARHitTestResultType.ARHitTestResultTypeExistingPlaneUsingExtent, 
                        // if you want to use infinite planes use this:
                        //ARHitTestResultType.ARHitTestResultTypeExistingPlane,
                        //ARHitTestResultType.ARHitTestResultTypeEstimatedHorizontalPlane, 
                        //ARHitTestResultType.ARHitTestResultTypeEstimatedVerticalPlane, 
                        //ARHitTestResultType.ARHitTestResultTypeFeaturePoint
                    };

        foreach (ARHitTestResultType resultType in resultTypes) {
            if (HitTest(point, resultType, out hit)) {
                Vector3 cameraForward = Camera.main.transform.forward;
                Vector3 cameraBearing = new Vector3(cameraForward.x, 0, cameraForward.z).normalized;
                planeIndicator.obj.transform.position = UnityARMatrixOps.GetPosition(hit.worldTransform);
                planeIndicator.obj.transform.rotation = Quaternion.LookRotation(cameraBearing);
                planeIndicator.obj.SetActive(true);
                return;
            }
        }
        planeIndicator.obj.SetActive(false);
    }

    public void SetAnchor() {
        Vector3 screenPosition = Camera.main.ScreenToViewportPoint(new Vector2(Screen.width / 2, Screen.height / 2));
        ARHitTestResult hit;
        ARPoint point = new ARPoint {
            x = screenPosition.x,
            y = screenPosition.y
        };

        // prioritize reults types
        ARHitTestResultType[] resultTypes = {
                        //ARHitTestResultType.ARHitTestResultTypeExistingPlaneUsingGeometry,
                        ARHitTestResultType.ARHitTestResultTypeExistingPlaneUsingExtent, 
                        // if you want to use infinite planes use this:
                        //ARHitTestResultType.ARHitTestResultTypeExistingPlane,
                        //ARHitTestResultType.ARHitTestResultTypeEstimatedHorizontalPlane, 
                        //ARHitTestResultType.ARHitTestResultTypeEstimatedVerticalPlane, 
                        //ARHitTestResultType.ARHitTestResultTypeFeaturePoint
                    };

        foreach (ARHitTestResultType resultType in resultTypes) {
            if (HitTest(point, resultType, out hit)) {
                Vector3 cameraForward = Camera.main.transform.forward;
                Vector3 cameraBearing = new Vector3(cameraForward.x, 0, cameraForward.z).normalized;
                transform.position = UnityARMatrixOps.GetPosition(hit.worldTransform);
                transform.rotation = Quaternion.LookRotation(cameraBearing);
                loader.ShowAll();
                anchorSet = true;
                return;
            }
        }
    }

    public void UnsetAnchor() {
        anchorSet = false;
        loader.HideAll();
    }

    // Use this for initialization
    private void Start() {
        anchorSet = false;
        scale = 1.0f;
        speed = 0.01f;
        if (unityARAnchorManager == null) {
            unityARAnchorManager = new UnityARAnchorManager();
        }
        UnityARUtility.InitializePlanePrefab(planePrefab);
        maxTouchCount = 0;
    }

    // Update is called once per frame
    private void Update() {
        if (!PlayerGroundPlaneUI.started) {
            switch (Input.touchCount) {
                case 0:
                    maxTouchCount = 0;
                    DrawIndicator();
                    break;
                case 1:
                    if (maxTouchCount < 1) {
                        maxTouchCount = 1;
                    }
                    Touch touch = Input.GetTouch(0);
                    if ((touch.phase == TouchPhase.Ended) && !playerUI.IsTouchOnUI(touch) && maxTouchCount <= 1) {
                        SetAnchor();
                    }
                    break;
                case 2:
                    if (maxTouchCount < 2) {
                        maxTouchCount = 2;
                    }
                    Touch touch1 = Input.GetTouch(0), touch2 = Input.GetTouch(1);
                    if (((touch1.phase == TouchPhase.Moved) && !playerUI.IsTouchOnUI(touch1))
                     || ((touch2.phase == TouchPhase.Moved) && !playerUI.IsTouchOnUI(touch2))) {
                        if (anchorSet) {
                            planeIndicator.obj.transform.position = transform.position;
                            planeIndicator.obj.transform.rotation = transform.rotation;
                        }
                        ChangeScale(touch1, touch2);
                        transform.localScale = new Vector3(scale, scale, scale);
                        DrawIndicator();
                    }
                    break;
            }
        }
    }

    private void OnDestroy() {
        UnsetAnchor();
    }
}

