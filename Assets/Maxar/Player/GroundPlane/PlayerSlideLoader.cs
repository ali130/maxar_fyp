﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Maxar;

public class PlayerSlideLoader : MonoBehaviour {
    public SlidesControl slidesControl;
    public GameObject stage;

    public void Load() {
        if (slidesControl.elementList != null) {
            foreach (Element elem in slidesControl.elementList) {
                Destroy(elem.obj);
            }
        }

        // Load visual elements
        slidesControl.elementList = Presentation.slides[PlayerGroundPlaneUI.page].elementList;
        foreach (Element elem in slidesControl.elementList) {
            elem.Create(stage);
            Debug.Log("Added " + elem.objName + " to stage");
        }

        Debug.Log(slidesControl.elementList.Count.ToString() + " object(s) loaded");
    }

    public void ShowAll() {
        foreach (Element elem in slidesControl.elementList) {
            elem.obj.SetActive(true);
        }
    }

    public void HideAll() {
        foreach (Element elem in slidesControl.elementList) {
            elem.obj.SetActive(false);
        }
    }
}
