﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlayerUI : MonoBehaviour {
    public CanvasHandler canvas;
    public iOS_NavigationBar navBar;
    public iOS_ToolBar toolBar;

    public bool IsTouchOnUI(Touch touch) {
        float top = 0.0f, bottom = 0.0f;
        int multiplier = canvas.retinaMultipler;
        if (navBar != null) {
            if (navBar.gameObject.activeSelf) {
                top = 44.0f * multiplier;
            }
        }
        if (toolBar != null) {
            if (toolBar.gameObject.activeSelf) {
                bottom = 44.0f * multiplier;
            }
        }
        return !((touch.position.y > bottom) && (touch.position.y < Screen.height - top));
    }

    public abstract void BackButtonPressed();
    public abstract void StartButtonPressed();
    public abstract void NextButtonPressed();
    public abstract void PrevButtonPressed();
}
