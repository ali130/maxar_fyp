﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BluetoothControl : MonoBehaviour {
    public PlayerUI playerUI;

    private void HandleExternalInput(KeyCode receivedKeyCode) {
        switch (receivedKeyCode) {
            case KeyCode.S:
                playerUI.PrevButtonPressed();
                break;
            case KeyCode.W:
                playerUI.NextButtonPressed();
                break;
        }
    }

    // Use this for initialization
    private void Start() {
        ExternalInputController.instance.OnExternalInputDidReceiveKey += HandleExternalInput;
    }	
}
