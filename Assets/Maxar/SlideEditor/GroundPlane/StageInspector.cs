﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Maxar;

public class StageInspector : MonoBehaviour {
    public ElementManager elemManager;
    public ElementControl elemControl;
    private KeyboardInput keyboard;
    private Stage stage;
    public iOS_TableViewItem widthField, depthField;
    public bool initialized;

    public void ChangeWidth() {
        keyboard = ScriptableObject.CreateInstance<KeyboardInput>();
        keyboard.Open("width", stage.width.ToString(), TouchScreenKeyboardType.NumberPad, false);
    }

    public void ChangeDepth() {
        keyboard = ScriptableObject.CreateInstance<KeyboardInput>();
        keyboard.Open("depth", stage.depth.ToString(), TouchScreenKeyboardType.NumberPad, false);
    }

    // Update is called once per frame
    private void Update() {
        if (elemControl.selected) {
            if ((!initialized) && (elemControl.SelectedType() == typeof(Stage))) {
                stage = elemManager.stage;
                widthField.RightText = Presentation.stageWidth.ToString();
                depthField.RightText = Presentation.stageDepth.ToString();
                initialized = true;
            }
        }
    }

    private void OnDisable() {
        stage = null;
        widthField.RightText = "";
        depthField.RightText = "";
        initialized = false;
        if (keyboard != null) {
            keyboard.keyboard.active = false;
        }
    }

    // Update text while user is typing
    private void OnGUI() {
        if (keyboard != null) {
            if (!TouchScreenKeyboard.visible) {
                switch (keyboard.inputName) {
                    case "width":
                        int newWidth;
                        if (int.TryParse(keyboard.keyboard.text, out newWidth)) {
                            if (newWidth > 0) {
                                Presentation.stageWidth = newWidth;
                            }
                        }
                        break;
                    case "depth":
                        int newDepth;
                        if (int.TryParse(keyboard.keyboard.text, out newDepth)) {
                            if (newDepth > 0) {
                                Presentation.stageDepth = newDepth;
                            }
                        }
                        break;
                }
                elemManager.ModifyElement(stage);
                widthField.RightText = Presentation.stageWidth.ToString();
                depthField.RightText = Presentation.stageDepth.ToString();
                Destroy(keyboard);
            } else {
                switch (keyboard.inputName) {
                    case "width":
                        widthField.RightText = keyboard.keyboard.text;
                        break;
                    case "depth":
                        depthField.RightText = keyboard.keyboard.text;
                        break;
                }
            }
        }
    }
}
