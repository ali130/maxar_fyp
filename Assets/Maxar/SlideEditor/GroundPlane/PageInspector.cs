﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Maxar;

public class PageInspector : MonoBehaviour {
    public EditorSlideLoader loader;
    public DynamicList pageList;
    public bool initialized;

    public void Create() {
        Presentation.slides.Add(ScriptableObject.CreateInstance<Slide>());
        PageListItem pageItem = pageList.Append("Prefabs/ListItem/PageListItem").GetComponent<PageListItem>();
        pageItem.slide = Presentation.slides[Presentation.slides.Count - 1];
        pageItem.ChangeName("Slide " + (Presentation.slides.Count).ToString());
        Debug.Log("Page created");

        EditorUI.page = Presentation.slides.Count - 1;
        loader.Load();
    }

    public void Delete() {
        if (Presentation.slides.Count > 1) {
            Presentation.slides.RemoveAt(EditorUI.page);
            pageList.Delete(EditorUI.page);

            for (int i = 0; i < pageList.container.transform.childCount; ++i) {
                PageListItem pageItem = pageList.container.transform.GetChild(i).GetComponent<PageListItem>();
                pageItem.slide = Presentation.slides[i];
                pageItem.ChangeName("Slide " + (i + 1).ToString());
            }

            if (EditorUI.page >= Presentation.slides.Count) {
                EditorUI.page = Presentation.slides.Count - 1;
            }
            loader.Load();
        }
    }

    // Update is called once per frame
    private void Update() {
        if (!initialized) {
            if (pageList.container.transform.childCount > 0) {
                pageList.container.transform.parent.GetComponent<ScrollRect>().normalizedPosition = new Vector2(0, 1);
                while (pageList.container.transform.childCount > 0) {
                    pageList.Delete(pageList.container.transform.childCount - 1);
                }
            }

            foreach (Slide slide in Presentation.slides) {
                PageListItem pageItem = pageList.Append("Prefabs/ListItem/PageListItem").GetComponent<PageListItem>();
                pageItem.id = pageItem.gameObject.transform.GetSiblingIndex();
                pageItem.slide = Presentation.slides[pageItem.id];
                pageItem.ChangeName("Slide " + (pageItem.id + 1).ToString());
            }

            pageList.container.transform.parent.GetComponent<ScrollRect>().normalizedPosition = new Vector2(0, 1);
            initialized = true;
        }
    }
}
