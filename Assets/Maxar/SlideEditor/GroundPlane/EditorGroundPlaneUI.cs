﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Maxar;

public class EditorGroundPlaneUI : EditorUI {
    public PageInspector pageInspector;

    public override void PlayerButtonPressed() {
        PlayerGroundPlaneUI.page = 0;
        SceneManager.LoadScene("PlayerGroundPlane");
        reload = true;
    }

    // Use this for initialization
    private void Awake() {
        Application.targetFrameRate = 60;
        cam = Camera.main;
        selectedId = 0;

        // Update panel height
        RectTransform leftRectTransform = transform.GetChild(0).GetChild(4).gameObject.GetComponent<RectTransform>();
        leftRectTransform.offsetMax = new Vector2(leftRectTransform.offsetMin.x, -navBar.GetComponent<RectTransform>().rect.height);
        RectTransform rightRectTransform = transform.GetChild(0).GetChild(5).gameObject.GetComponent<RectTransform>();
        rightRectTransform.offsetMax = new Vector2(rightRectTransform.offsetMin.x, -navBar.GetComponent<RectTransform>().rect.height);

        // Initialize camera position
        if (!reload) {
            cam.transform.position = new Vector3(
                                                 0,
                                                 cam.transform.position.y,
                                                 cam.transform.position.z - Presentation.stageDepth
                                                );
            CameraControl.position = cam.transform.position;
            CameraControl.rotation = new Quaternion();
            CameraControl.shiftVal = 0.35f;
        } else {
            // Move camera into previous position
            cam.transform.position = CameraControl.position;
            cam.transform.rotation = CameraControl.rotation;
            cam.fieldOfView = CameraControl.fieldOfView;
            CameraControl.shiftVal = 0.35f;
            reload = false;
        }

        loader.Load();
        if (elemManager.stage == null) {
            elemManager.CreateStage(Presentation.stageWidth, Presentation.stageDepth);
        }
        pageInspector.initialized = false;
    }

    // Update is called once per frame
    private void Update() {
        // Update panel height
        RectTransform leftRectTransform = transform.GetChild(0).GetChild(4).gameObject.GetComponent<RectTransform>();
        leftRectTransform.offsetMax = new Vector2(leftRectTransform.offsetMin.x, -navBar.GetComponent<RectTransform>().rect.height);
        RectTransform rightRectTransform = transform.GetChild(0).GetChild(5).gameObject.GetComponent<RectTransform>();
        rightRectTransform.offsetMax = new Vector2(rightRectTransform.offsetMin.x, -navBar.GetComponent<RectTransform>().rect.height);

        if (animationEditor) {
            pageInspector.gameObject.SetActive(false);
            stageInspector.gameObject.SetActive(false);
            textInspector.gameObject.SetActive(false);
            modelChooser.gameObject.SetActive(false);
            modelInspector.gameObject.SetActive(false);
            rightToolBar.gameObject.SetActive(false);
            if (!animationInspector.gameObject.activeSelf)
            {
                // Deselect any object
                if (elemControl.selected)
                {
                    elemControl.ChangeLayer(elemManager.elementMap[selectedId].obj, "Default");
                    selectedId = 0;
                    elemControl.selectedId = 0;
                    elemControl.selected = false;
                }

                animationInspector.gameObject.SetActive(true);
                animationInspector.initializedAnim = false;
                animationInspector.initializedKeyframe = false;
            }
        } else if (modelSelection) {
            pageInspector.gameObject.SetActive(false);
            textInspector.gameObject.SetActive(false);
            modelInspector.gameObject.SetActive(false);
            animationInspector.gameObject.SetActive(false);
            rightToolBar.gameObject.SetActive(false);
            stageInspector.gameObject.SetActive(false);
            if (!modelChooser.gameObject.activeSelf) {
                // Deselect any object
                if (elemControl.selected) {
                    elemControl.ChangeLayer(elemManager.elementMap[selectedId].obj, "Default");
                    selectedId = 0;
                    elemControl.selectedId = 0;
                    elemControl.selected = false;
                }

                modelChooser.gameObject.SetActive(true);
                modelChooser.initialized = false;
            }
        } else {
            if (elemControl.selected) {
                if (elemControl.SelectedType() == typeof(Stage)) {
                    pageInspector.gameObject.SetActive(false);
                    textInspector.gameObject.SetActive(false);
                    modelChooser.gameObject.SetActive(false);
                    modelInspector.gameObject.SetActive(false);
                    animationInspector.gameObject.SetActive(false);
                    rightToolBar.gameObject.SetActive(false);
                    stageInspector.gameObject.SetActive(true);
                    if (selectedId != elemControl.selectedId)
                    {
                        stageInspector.initialized = false;
                        selectedId = elemControl.selectedId;
                    }
                } else if (elemControl.SelectedType() == typeof(Text3D)) {
                    pageInspector.gameObject.SetActive(false);
                    modelChooser.gameObject.SetActive(false);
                    modelInspector.gameObject.SetActive(false);
                    stageInspector.gameObject.SetActive(false);
                    animationInspector.gameObject.SetActive(false);
                    rightToolBar.gameObject.SetActive(false);
                    textInspector.gameObject.SetActive(true);
                    if (selectedId != elemControl.selectedId)
                    {
                        textInspector.initialized = false;
                        selectedId = elemControl.selectedId;
                    }
                } else if (elemControl.SelectedType() == typeof(Model3D)) {
                    pageInspector.gameObject.SetActive(false);
                    modelChooser.gameObject.SetActive(false);
                    textInspector.gameObject.SetActive(false);
                    stageInspector.gameObject.SetActive(false);
                    animationInspector.gameObject.SetActive(false);
                    rightToolBar.gameObject.SetActive(false);
                    modelInspector.gameObject.SetActive(true);
                    if (selectedId != elemControl.selectedId) {
                        modelInspector.initialized = false;
                        selectedId = elemControl.selectedId;
                    }
                }
            } else {
                textInspector.gameObject.SetActive(false);
                modelChooser.gameObject.SetActive(false);
                modelInspector.gameObject.SetActive(false);
                animationInspector.gameObject.SetActive(false);
                stageInspector.gameObject.SetActive(false);
                rightToolBar.gameObject.SetActive(true);
                pageInspector.gameObject.SetActive(true);
            }
        }
    }
}
