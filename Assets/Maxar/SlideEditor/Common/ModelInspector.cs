﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Maxar;

public class ModelInspector : MonoBehaviour {
    public ElementManager elemManager;
    public ElementControl elemControl;
    private KeyboardInput keyboard;
    private Model3D model;
    public iOS_TableViewItem scaleField, opacityField;
    public iOS_TableViewItem deleteButton;
    public bool initialized;

    public void ChangeScale() {
        keyboard = ScriptableObject.CreateInstance<KeyboardInput>();
        keyboard.Open("scale", model.objScale.ToString(), TouchScreenKeyboardType.NumbersAndPunctuation, false);
    }

    public void ChangeOpacity() {
        keyboard = ScriptableObject.CreateInstance<KeyboardInput>();
        keyboard.Open("opacity", model.objOpacity.ToString(), TouchScreenKeyboardType.NumbersAndPunctuation, false);
    }

    public void Delete() {
        elemManager.DeleteElement(model);
        if (keyboard != null) {
            keyboard.keyboard.active = false;
        }
    }
	
	// Update is called once per frame
	private void Update() {
        if (elemControl.selected){
            if ((!initialized) && (elemControl.SelectedType() == typeof(Model3D))) {
                model = (Model3D)elemManager.elementMap[elemControl.selectedId];
                scaleField.RightText = model.objScale.ToString();
                opacityField.RightText = model.objOpacity.ToString();
                initialized = true;
            }
        }
    }

    private void OnDisable() {
        model = null;
        scaleField.RightText = "";
        opacityField.RightText = "";
        initialized = false;
        if (keyboard != null) {
            keyboard.keyboard.active = false;
        }
    }

    // Update text while user is typing
    private void OnGUI() {
        if (keyboard != null) {
            if (!TouchScreenKeyboard.visible) {
                switch (keyboard.inputName) {
                    case "scale":
                        float newScale;
                        if (float.TryParse(keyboard.keyboard.text, out newScale)) {
                            if (newScale > 0.0f) {
                                model.obj.transform.localScale = new Vector3(newScale * model.scaleFactor, newScale * model.scaleFactor, newScale * model.scaleFactor);
                            }
                        }
                        break;
                    case "opacity":
                        float newOpacity;
                        if (float.TryParse(keyboard.keyboard.text, out newOpacity)) {
                            if ((newOpacity >= 0.0f) && (newOpacity <= 1.0f)) {
                                model.DisplayOpacity(newOpacity);
                            }
                        }
                        break;
                }
                elemManager.ModifyElement(model);
                scaleField.RightText = model.objScale.ToString();
                opacityField.RightText = model.objOpacity.ToString();
                Destroy(keyboard);
            } else {
                switch (keyboard.inputName) {
                    case "scale":
                        scaleField.RightText = keyboard.keyboard.text;
                        break;
                    case "opacity":
                        opacityField.RightText = keyboard.keyboard.text;
                        break;
                }
            }
        }
    }
}
