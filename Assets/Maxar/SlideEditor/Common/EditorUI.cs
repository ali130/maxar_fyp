﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Maxar;

public abstract class EditorUI : MonoBehaviour {
    public CanvasHandler canvas;
    public GameObject leftArea;
    public StageInspector stageInspector;
    public TextInspector textInspector;
    public ModelChooser modelChooser;
    public ModelInspector modelInspector;
    public AnimationInspector animationInspector;
    public iOS_NavigationBar navBar;
    public iOS_ToolBar rightToolBar;

    public Camera cam;
    public CameraControl camControl;
    public ElementControl elemControl;
    public ElementManager elemManager;
    public AnimationManager animManager;
    public EditorSlideLoader loader;
    public int selectedId;
    private KeyboardInput keyboard;
    public Text titleText;

    public static bool modelSelection = false;
    public static bool animationEditor = false;
    public static int page = 0;
    public static int animSeq = 0;  // Animation sequence number
    public static bool reload = false;

    public bool IsTouchOnUI(Touch touch) {
        int multiplier = canvas.retinaMultipler;
        return !((touch.position.x > Screen.width * 0.25) && (touch.position.y > 44.0f * multiplier) && (touch.position.y < Screen.height - 44.0f * multiplier));
    }

    public void BackButtonPressed() {
        modelSelection = false;
        animationEditor = false;
        page = 0;
        animSeq = 0;  // Animation sequence number
        reload = false;
        SceneManager.LoadScene("Menu");
    }

    public void SaveButtonPressed() {
        Presentation.Save();
    }

    public void TextButtonPressed() {
        Text3D newText = ScriptableObject.CreateInstance<Text3D>();
        newText.text = "Text";
        newText.objScale = 1.0f;
        newText.objOpacity = 1.0f;
        elemManager.CreateElement(newText, "text");
        if (elemControl.selected) {
            if (elemControl.SelectedType() == typeof(Stage)) {
                elemControl.ChangeLayer(elemManager.stage.obj, "Default");
            } else {
                elemControl.ChangeLayer(elemManager.elementMap[elemControl.selectedId].obj, "Default");
            }
        }
        selectedId = newText.obj.GetInstanceID();
        elemControl.selectedId = selectedId;
        elemControl.ChangeLayer(newText.obj, "Contour");
        elemControl.selected = true;
        textInspector.initialized = false;
    }

    public void ModelButtonPressed() {
        modelSelection = true;
    }

    public void AnimationButtonPressed() {
        animationEditor = true;
    }

    public void ChangeFilename() {
        keyboard = ScriptableObject.CreateInstance<KeyboardInput>();
        keyboard.Open("filename", Presentation.filename, TouchScreenKeyboardType.Default, false);
    }

    public abstract void PlayerButtonPressed();

    private void Start() {
        titleText.text = Presentation.filename;
    }

    private void OnGUI() {
        if (keyboard != null) {
            if (!TouchScreenKeyboard.visible) {
                switch (keyboard.inputName) {
                    case "filename":
                        Presentation.filename = keyboard.keyboard.text;
                        break;
                }
                titleText.text = Presentation.filename;
                Destroy(keyboard);
            } else {
                titleText.text = keyboard.keyboard.text;
            }
        }
    }
}
