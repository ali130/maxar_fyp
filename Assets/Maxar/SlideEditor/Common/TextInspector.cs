﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Maxar;

public class TextInspector : MonoBehaviour {
    public ElementManager elemManager;
    public ElementControl elemControl;
    private KeyboardInput keyboard;
    private Text3D text;
    public iOS_TableViewItem textField, scaleField, opacityField;
    public iOS_TableViewItem deleteButton;
    public bool initialized;

    public void ChangeText() {
        keyboard = ScriptableObject.CreateInstance<KeyboardInput>();
        keyboard.Open("text", text.text, TouchScreenKeyboardType.Default, true);
    }

    public void ChangeScale() {
        keyboard = ScriptableObject.CreateInstance<KeyboardInput>();
        keyboard.Open("scale", text.objScale.ToString(), TouchScreenKeyboardType.NumbersAndPunctuation, false);
    }

    public void ChangeOpacity() {
        keyboard = ScriptableObject.CreateInstance<KeyboardInput>();
        keyboard.Open("opacity", text.objOpacity.ToString(), TouchScreenKeyboardType.NumbersAndPunctuation, false);
    }

    public void Delete() {
        elemManager.DeleteElement(text);
        if (keyboard != null) {
            keyboard.keyboard.active = false;
        }
    }

    // Update is called once per frame
    private void Update() {
        if (elemControl.selected) {
            if ((!initialized) && (elemControl.SelectedType() == typeof(Text3D))) {
                text = (Text3D)elemManager.elementMap[elemControl.selectedId];
                textField.RightText = text.text;
                scaleField.RightText = text.objScale.ToString();
                opacityField.RightText = text.objOpacity.ToString();
                initialized = true;
            }
        }
	}

    private void OnDisable() {
        text = null;
        textField.RightText = "";
        scaleField.RightText = "";
        opacityField.RightText = "";
        initialized = false;
        if (keyboard != null) {
            keyboard.keyboard.active = false;
        }
    }

    // Update text while user is typing
    private void OnGUI() {
        if (keyboard != null) {
            if (!TouchScreenKeyboard.visible) {
                switch (keyboard.inputName) {
                    case "text":
                        if (keyboard.keyboard.text != "") {
                            text.text = keyboard.keyboard.text;
                        }
                        break;
                    case "scale":
                        float newScale;
                        if (float.TryParse(keyboard.keyboard.text, out newScale)) {
                            if (newScale > 0.0f) {
                                text.obj.transform.localScale = new Vector3(newScale * text.scaleFactor, newScale * text.scaleFactor, newScale * text.scaleFactor);
                            }
                        }
                        break;
                    case "opacity":
                        float newOpacity;
                        if (float.TryParse(keyboard.keyboard.text, out newOpacity)) {
                            if ((newOpacity >= 0.0f) && (newOpacity <= 1.0f)) {
                                text.DisplayOpacity(newOpacity);
                            }
                        }
                        break;
                }
                elemManager.ModifyElement(text);
                textField.RightText = text.text;
                scaleField.RightText = text.objScale.ToString();
                opacityField.RightText = text.objOpacity.ToString();
                Destroy(keyboard);
            } else {
                switch (keyboard.inputName) {
                    case "text":
                        textField.RightText = keyboard.keyboard.text;
                        break;
                    case "scale":
                        scaleField.RightText = keyboard.keyboard.text;
                        break;
                    case "opacity":
                        opacityField.RightText = keyboard.keyboard.text;
                        break;
                }
            }
        }
    }
}
