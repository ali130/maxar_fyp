﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class ModelChooser : MonoBehaviour {
    public EditorUI editorUI;
    public ElementManager elemManager;
    public ElementControl elemControl;
    public DynamicList modelList;
    public bool initialized;

    public void BackButtonPressed() {
        EditorUI.modelSelection = false;
    }

    private void ClearModelList() {
        modelList.container.transform.parent.GetComponent<ScrollRect>().normalizedPosition = new Vector2(0, 1);
        while (modelList.container.transform.childCount > 0) {
            modelList.Delete(modelList.container.transform.childCount - 1);
        }
    }

    private void PopulateModelList() {
        if (modelList.container.transform.childCount > 0) {
            ClearModelList();
        }

        TextAsset textAsset = (TextAsset)Resources.Load("Model3D", typeof(TextAsset));
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.Load(new StringReader(textAsset.text));

        XmlNodeList nodeList = xmlDoc.SelectNodes("/ModelList/Model");
        foreach (XmlNode node in nodeList) {
            ModelListItem modelItem = modelList.Append("Prefabs/ListItem/ModelListItem").GetComponent<ModelListItem>();
            modelItem.modelName = node["Name"].InnerText;
            modelItem.modelPath = node["Path"].InnerText;
            float.TryParse(node["Scale"].InnerText, out modelItem.scaleFactor);
            modelItem.ChangeName(modelItem.modelName);
        }
    }
	
	// Update is called once per frame
	private void Update() {
        if (EditorUI.modelSelection) {
            elemControl.disableMove = true;
            elemControl.disableSelection = true;
            if (!initialized) {
                PopulateModelList();
                initialized = true;
            }
        }
    }

    private void OnDisable() {
        elemControl.disableMove = false;
        elemControl.disableSelection = false;
        ClearModelList();
        initialized = false;
    }
}
