﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Maxar;

public class ElementManager : MonoBehaviour {
    public AnimationManager animManager;
    public ElementControl elemControl;
    private bool moving;
    public Stage stage;  // Stage representation
    public Dictionary<int, Element> elementMap;  // All elements in a slide with their instance IDs as the keys

    // Load all objects in a slide
    public void Refresh() {
        moving = false;

        if (elementMap == null) {
            elementMap = new Dictionary<int, Element>();
        } else {
            foreach (Element elem in elementMap.Values) {
                Destroy(elem.obj);
            }
            elementMap.Clear();
        }

        foreach (Element elem in Presentation.slides[EditorUI.page].elementList) {
            elem.Create();
            elementMap.Add(elem.obj.GetInstanceID(), elem);
            Debug.Log("Loaded object with ID: " + elem.obj.GetInstanceID().ToString());
        }
    }

    public void Clear() {
        moving = false;
        Destroy(stage);
        foreach (Element elem in elementMap.Values) {
            Destroy(elem.obj);
        }
        elementMap.Clear();
    }

    public void CreateStage(int width, int depth) {
        // Initialize stage
        stage = ScriptableObject.CreateInstance<Stage>();
        stage.objName = "stage";
        stage.width = width;
        stage.depth = depth;
        stage.objOpacity = 1.0f;
        stage.Create();

        Debug.Log("Created stage with ID: " + stage.obj.GetInstanceID().ToString());
    }

    public void CreateElement(Element elem, string name) {
        if (elem.GetType() != typeof(Stage)) {
            elem.objName = name;
            elem.objPosition = Vector3.zero;
            elem.objRotation = new Quaternion();
            elem.objScale = 1.0f;
            elem.Create();

            elementMap.Add(elem.obj.GetInstanceID(), elem);
            Presentation.slides[EditorUI.page].elementList.Add(elem);
            Debug.Log("Created object with ID: " + elem.obj.GetInstanceID().ToString());
        } else {
            Debug.Log("Stage cannot be created!");
        }
    }

    public void ModifyElement(Element elem) {
        if (elem.GetType() == typeof(Stage)) {
            if ((Presentation.stageWidth > 0) && (Presentation.stageDepth > 0)) {
                ((Stage)elem).width = Presentation.stageWidth;
                ((Stage)elem).depth = Presentation.stageDepth;
            } else {
                Presentation.stageWidth = ((Stage)elem).width;
                Presentation.stageDepth = ((Stage)elem).depth;
            }
            elem.Refresh();
            Debug.Log("Modified stage");
        } else {
            elem.objPosition = elem.obj.transform.position;
            elem.objRotation = elem.obj.transform.rotation;

            float newScale = elem.obj.transform.localScale.x / elem.scaleFactor;
            if (newScale > 0.0f) {
                elem.objScale = newScale;
            }

            float newOpacity = 1.0f - elem.obj.GetComponent<MeshRenderer>().sharedMaterial.GetFloat("_Transparency");
            if ((newOpacity >= 0.0f) && (newOpacity <= 1.0f)) {
                elem.objOpacity = newOpacity;
            }

            animManager.MatchAnimationStates();
            elem.Refresh();
            Debug.Log("Modified object with ID: " + elem.obj.GetInstanceID().ToString());
        }
    }

    public void DeleteElement(Element elem) {
        if (elem.GetType() != typeof(Stage)) {
            int id = elem.obj.GetInstanceID();

            // Deselect object if necessary
            if (elemControl.selected && (elemControl.selectedId == id))
            {
                elemControl.selected = false;
                elemControl.selectedId = 0;
            }

            // Destroy object
            Destroy(elem.obj);

            // Remove related animations
            animManager.RemoveRelatedAnimations(Presentation.slides[EditorUI.page].elementList.IndexOf(elem));

            // Register on Element Manager
            elementMap.Remove(id);
            Presentation.slides[EditorUI.page].elementList.Remove(elem);
            Debug.Log("Deleted object with ID: " + id.ToString());
        } else {
            Debug.Log("Stage cannot be deleted!");
        }
    }
	
	// Update is called once per frame
	private void Update() {
        if (!EditorUI.animationEditor) {
            // Change of state
            if (elemControl.moving != moving) {
                // Update state
                moving = elemControl.moving;
                if (!moving && elemControl.selected) {
                    if (elemControl.SelectedType() != typeof(Stage)) {
                        ModifyElement(elementMap[elemControl.selectedId]);
                    }
                }
            }
        }
    }
}
