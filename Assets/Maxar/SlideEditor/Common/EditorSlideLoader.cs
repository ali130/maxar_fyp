﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Maxar;

public class EditorSlideLoader : MonoBehaviour {
    public ElementManager elemManager;
    public AnimationManager animManager;
    public PointCloudVisualizer pointCloud;

    public void Load() {
        if (elemManager != null) {
            if (EditorUI.page >= 0) {
                elemManager.Refresh();
            } else {
                elemManager.Clear();
            }
        }
        if (animManager != null) {
            if (EditorUI.page >= 0) {
                animManager.Refresh();
            } else {
                animManager.Clear();
            }
        }
        if (pointCloud != null) {
            if (EditorUI.page >= 0) {
                pointCloud.Load(Path.Combine(Application.persistentDataPath, Presentation.slides[EditorUI.page].objectTargetPath));
            } else {
                pointCloud.Clear();
            }
        }
    }
}
