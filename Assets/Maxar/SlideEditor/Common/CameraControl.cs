﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraControl : MonoBehaviour {
    // Camera movement mode
    public bool freeze;
    public bool UIInteraction;

	// Speed of movement
	private float speed;

	// Rotation angles
	private float rotX;
	private float rotY;

    // Attached camera
    private Camera cam;

    // Editor UI scripts
    public EditorUI editorUI;

    // Camera transformation
    public static Vector3 position;
    public static Quaternion rotation;
    public static float fieldOfView;
    public static float shiftVal;

    public Vector3 GetLookVal() {
        return (new Vector3(0, 0 + shiftVal, 0));
    }

    // Orbit camera around origin
    private void Orbit(Touch touch) {
        rotX = -touch.deltaPosition.y * speed;
        float angle = Mathf.Abs(Vector3.Angle(transform.forward, Vector3.down));
        if (rotX > 0) {
            rotX = Mathf.Min(rotX, angle - 0.1f);
        } else if (rotX < 0) {
            rotX = Mathf.Max(rotX, angle - 179.9f);
        }
        rotY = touch.deltaPosition.x * speed;
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, 0);
        transform.RotateAround(Vector3.zero, Vector3.up, rotY);
        transform.RotateAround(Vector3.zero, transform.right, rotX);
        transform.LookAt(GetLookVal());
	}

    // Camera zoom
    private void Zoom(Touch touch1, Touch touch2) {
        // Find the position in the previous frame of each touch.
        Vector2 touch1Prev = touch1.position - touch1.deltaPosition;
        Vector2 touch2Prev = touch2.position - touch2.deltaPosition;

        // Find the magnitude of the vector (the distance) between the touches in each frame.
        float distPrev = (touch1Prev - touch2Prev).magnitude;
        float dist = (touch1.position - touch2.position).magnitude;

        // Find the difference in the distances between each frame.
        float distDiff = distPrev - dist;

        // Change the field of view based on the change in distance between the touches.
        cam.fieldOfView += distDiff * speed;

        // Clamp the field of view to make sure it's between 0 and 180.
        cam.fieldOfView = Mathf.Clamp(cam.fieldOfView, 0.1f, 179.9f);
    }

    // Use this for initialization
    private void Start() {
        speed = 0.1f;
        cam = GetComponent<Camera>();
        freeze = false;
        UIInteraction = false;
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, 0);

        transform.LookAt(GetLookVal());
    }

    // Update is called once per frame
    private void LateUpdate() {
        switch (Input.touchCount) {
            case 0:
                UIInteraction = false;
                break;
            case 1:
                if (!freeze && !UIInteraction) {
                    Touch touch = Input.GetTouch(0);
                    // Do not check click position when interacting with UI
                    if ((touch.phase == TouchPhase.Began) && editorUI.IsTouchOnUI(touch)) {
                        UIInteraction = true;
                        return;
                    }
                    Orbit(touch);
                }
                break;
            case 2:
                if (!freeze && !UIInteraction) {
                    Touch touch1 = Input.GetTouch(0), touch2 = Input.GetTouch(1);
                    // Do not check click position when interacting with UI
                    if ((touch1.phase == TouchPhase.Began) && editorUI.IsTouchOnUI(touch1)
                       && (touch2.phase == TouchPhase.Began) && editorUI.IsTouchOnUI(touch2)) {
                        UIInteraction = true;
                        return;
                    }
                    Zoom(touch1, touch2);
                }
                break;
        }

        position = transform.position;
        rotation = transform.rotation;
        fieldOfView = cam.fieldOfView;
    }
}
