﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Maxar;

public class ModelListItem : MonoBehaviour {
    public string modelName;
    public string modelPath;
    public float scaleFactor;
    private ModelChooser modelChooser;

    public void Pressed() {
        Model3D newModel = ScriptableObject.CreateInstance<Model3D>();
        newModel.path = modelPath;
        newModel.objScale = 1.0f;
        newModel.objOpacity = 1.0f;
        newModel.scaleFactor = scaleFactor;
        modelChooser.elemManager.CreateElement(newModel, "model3d");
        if (modelChooser.elemControl.selected) {
            if (modelChooser.elemControl.SelectedType() == typeof(Stage)) {
                modelChooser.elemControl.ChangeLayer(modelChooser.elemManager.stage.obj, "Default");
            } else {
                modelChooser.elemControl.ChangeLayer(modelChooser.elemManager.elementMap[modelChooser.elemControl.selectedId].obj, "Default");
            }
        }
        modelChooser.editorUI.selectedId = newModel.obj.GetInstanceID();
        modelChooser.elemControl.selectedId = modelChooser.editorUI.selectedId;
        modelChooser.elemControl.ChangeLayer(newModel.obj, "Contour");
        modelChooser.elemControl.selected = true;
        modelChooser.editorUI.modelInspector.initialized = false;
        EditorUI.modelSelection = false;
    }

    public void ChangeName(string newName) {
        GetComponent<iOS_TableViewItem>().TitleText = newName;
        transform.GetChild(2).gameObject.GetComponent<Text>().text = newName;
    }

    // Use this for initialization
    private void Start() {
        modelChooser = FindObjectOfType<ModelChooser>();
	}
}
