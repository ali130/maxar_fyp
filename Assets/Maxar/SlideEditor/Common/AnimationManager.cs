﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Maxar;

public class AnimationManager : MonoBehaviour {
    public ElementManager elemManager;
    public ElementControl elemControl;
    private bool moving;
    public List<ElementAnimation> animationList;  // List of animations

    // Load all objects in a slide
    public void Refresh() {
        EditorUI.animSeq = 0;
        moving = false;
        animationList = Presentation.slides[EditorUI.page].animationList;
        foreach (ElementAnimation anim in animationList) {
            anim.Update();
        }
    }

    public void Clear() {
        moving = false;
        animationList.Clear();
    }

    // Match start states to the previous end states
    public void MatchAnimationStates() {
        ElementAnimation anim, last;
        Element elem;

        // Maintain a list of last known state indices
        int[] lastKnown = new int[elemManager.elementMap.Count];
        for (int i = 0; i < lastKnown.Length; ++i) {
            lastKnown[i] = -1;
        }

        if (animationList.Count > 0) {
            for (int i = 0; i < animationList.Count; ++i) {
                anim = animationList[i];
                if (lastKnown[anim.elemId] == -1) {  // No last known state
                    elem = Presentation.slides[anim.pageId].elementList[anim.elemId];
                    anim.startState.position = elem.objPosition;
                    anim.startState.rotation = elem.objRotation;
                    anim.startState.scale = elem.objScale;
                    anim.startState.opacity = elem.objOpacity;
                } else {  // Last known state exist
                    last = animationList[lastKnown[anim.elemId]];
                    anim.startState.position = last.endState.position;
                    anim.startState.rotation = last.endState.rotation;
                    anim.startState.scale = last.endState.scale;
                    anim.startState.opacity = last.endState.opacity;
                }
                lastKnown[anim.elemId] = i;
            }
        }
    }

    public void RemoveRelatedAnimations(int elemId) {
        for (int i = 0; i < animationList.Count;) {
            if (animationList[i].elemId == elemId) {
                animationList.RemoveAt(i);
            } else {
                ++i;
            }
        }
        for (int i = 0; i < animationList.Count; ++i) {
            if (animationList[i].elemId >= elemId) {
                animationList[i].elemId -= 1;
            }
        }
    }

    public void CreateAnimation(ElementAnimation anim, Element elem) {
        if (elem.GetType() != typeof(Stage)) {
            anim.Update();
            animationList.Add(anim);
            Debug.Log("Created animation with ID: " + EditorUI.animSeq.ToString());
        }

    }

    public void ModifyAnimation(ElementAnimation anim, Element elem) {
        if (anim.elem.obj.GetInstanceID() == animationList[EditorUI.animSeq].elem.obj.GetInstanceID()) {
            anim.endState.position = elem.obj.transform.localPosition;
            anim.endState.rotation = elem.obj.transform.localRotation;

            float newScale = elem.obj.transform.localScale.x / elem.scaleFactor;
            if (newScale > 0.0f) {
                anim.endState.scale = newScale;
            }

            float newOpacity = 1.0f - elem.obj.GetComponent<MeshRenderer>().sharedMaterial.GetFloat("_Transparency");
            if ((newOpacity >= 0.0f) && (newOpacity <= 1.0f)) {
                anim.endState.opacity = newOpacity;
            }

            MatchAnimationStates();
            anim.Update();
            Debug.Log("Modified animation with ID: " + EditorUI.animSeq.ToString());
        }
    }

    public void DeleteAnimation(ElementAnimation anim) {
        animationList.Remove(anim);
        MatchAnimationStates();
    }
    	
	// Update is called once per frame
	private void Update() {
        if (EditorUI.animationEditor) {
            // Change of state
            if (elemControl.moving != moving) {
                // Update state
                moving = elemControl.moving;
                if (!moving && elemControl.selected && EditorUI.animSeq < animationList.Count) {
                    if (elemControl.selectedId == animationList[EditorUI.animSeq].elem.obj.GetInstanceID()) {
                        ModifyAnimation(animationList[EditorUI.animSeq], elemManager.elementMap[elemControl.selectedId]);
                    }
                }
            }
        }
	}
}
