﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Maxar;

public class ElementControl : MonoBehaviour {
    public Camera cam;  // Reference to Main Camera
    public ElementManager elemManager;
    public EditorUI editorUI;
    private float stationaryTimer;

    // Object selection
    public bool selected;  // Whether an object is selected
    public int selectedId;  // Instance ID of the selected object
    public bool disableMove;  // Disable movement only
    public bool disableSelection;  // Disable selection only
    public bool disableAll;  // Disable all controls (selection, movement)

    // Object moving
    private Plane horizontal, vertical;  // Plane for movement of objects
    private Vector3 moveOrigin;  // Original posititon before moving in each update
    private float rotateOrigin;  // Original angle before rotation in each update
    public bool moving;  // Whether an object is moving
    public bool hMove, vMove;  // Moving direction

    // Transformation mode
    public bool localTransform;  // Whether to use local coordinates instead

    // Change layer of a GameObject together with its children
    public void ChangeLayer(GameObject obj, string layer) {
        // Change layer
        int newLayer = LayerMask.NameToLayer(layer);
        obj.layer = newLayer;
        foreach (Transform child in obj.GetComponentsInChildren<Transform>(true)) {
            child.gameObject.layer = newLayer;
        }
    }

    // Return clicked object's instance ID
    private int ObjectClicked(Touch touch) {
        RaycastHit hitInfo = new RaycastHit();
        bool hit = Physics.Raycast(cam.ScreenPointToRay(touch.position), out hitInfo);
        GameObject obj;

        // Hit
        if (hit) {
            // Deselect previous object
            if (selected) {
                // Retrieve reference to previous object
                obj = selectedId == elemManager.stage.obj.GetInstanceID() ? elemManager.stage.obj : elemManager.elementMap[selectedId].obj;

                // Change layer to "Default"
                ChangeLayer(obj, "Default");
            }

            // Retrieve reference to object
            obj = hitInfo.transform.parent == null ? hitInfo.transform.gameObject : hitInfo.transform.parent.gameObject;

            // Change layer to "Contour"
            ChangeLayer(obj, "Contour");

            Debug.Log("Selected object with ID: " + obj.GetInstanceID().ToString());
            selected = true;
            return obj.GetInstanceID();
        }

        // Miss
        if (selected) {
            // Retrieve reference to object
            obj = selectedId == elemManager.stage.obj.GetInstanceID() ? elemManager.stage.obj : elemManager.elementMap[selectedId].obj;

            // Change layer to "Default"
            ChangeLayer(obj, "Default");

            Debug.Log("Deselected");
        }
        selected = false;
        return 0;
    }

    // Return the type of selected object
    public Type SelectedType() {
        return selectedId == elemManager.stage.obj.GetInstanceID() ? typeof(Stage) : elemManager.elementMap[selectedId].GetType();
    }

    // Initialize horizontal move
    private void HMoveInit(Touch touch) {
        RaycastHit hitInfo = new RaycastHit();
        bool hit = Physics.Raycast(cam.ScreenPointToRay(touch.position), out hitInfo);

        // Hit
        if (hit) {
            // Retrieve reference to object
            GameObject obj;
            obj = hitInfo.transform.parent == null ? hitInfo.transform.gameObject : hitInfo.transform.parent.gameObject;

            // Selected object is touched
            if ((obj.GetInstanceID() == selectedId) && (obj.GetInstanceID() != elemManager.stage.obj.GetInstanceID())) {
                moveOrigin = hitInfo.point;
                horizontal = new Plane(Vector3.up, moveOrigin);
                vertical = new Plane(cam.transform.forward, moveOrigin);
                cam.GetComponent<CameraControl>().freeze = true;
                //moving = true;
                hMove = true;
                vMove = false;
            }
        }
    }

    // Move selected object horizontally
    private void HMove(Touch touch) {
        Vector3 newPos;
        float distance;
        Ray ray = cam.ScreenPointToRay(touch.position);

        // Move object horizontally
        if (horizontal.Raycast(ray, out distance)) {
            Element elem = elemManager.elementMap[selectedId];
            newPos = ray.GetPoint(distance);
            if (localTransform) {
                elem.obj.transform.position += newPos - moveOrigin;
            } else {
                elem.obj.transform.localPosition += newPos - moveOrigin;
            }
            moveOrigin = newPos;
            vertical = new Plane(cam.transform.forward, moveOrigin);
        }
    }

    private void VMoveInit(Touch touch) {
        RaycastHit hitInfo = new RaycastHit();
        bool hit;

        // Hit
        hit = Physics.Raycast(cam.ScreenPointToRay(touch.position), out hitInfo);
        if (hit) {
            // Retrieve reference to object
            GameObject obj;
            obj = hitInfo.transform.parent == null ? hitInfo.transform.gameObject : hitInfo.transform.parent.gameObject;

            // Selected object is touched
            if ((obj.GetInstanceID() == selectedId) && (obj.GetInstanceID() != elemManager.stage.obj.GetInstanceID())) {
                moveOrigin = hitInfo.point;
                horizontal = new Plane(Vector3.up, moveOrigin);
                vertical = new Plane(cam.transform.forward, moveOrigin);
                cam.GetComponent<CameraControl>().freeze = true;
                //moving = true;
                vMove = true;
                hMove = false;
            }
        }
    }

    private void VMove(Touch touch) {
        Vector3 newPos;
        float distance;
        Ray ray = cam.ScreenPointToRay(touch.position);

        // Move object vertically
        if (vertical.Raycast(ray, out distance)) {
            Element elem = elemManager.elementMap[selectedId];
            newPos = ray.GetPoint(distance);
            if (localTransform) {
                elem.obj.transform.position += newPos - moveOrigin;
            } else {
                elem.obj.transform.localPosition += newPos - moveOrigin;
            }
            moveOrigin = newPos;
            horizontal = new Plane(Vector3.up, moveOrigin);
        }
    }

    private float GetAngle(Touch touch1, Touch touch2) {
        float dx = touch1.position.x - touch2.position.x;
        float dy = touch1.position.y - touch2.position.y;
        return Mathf.Atan2(dy, dx);
    }

    private void RotateInit(Touch touch1, Touch touch2) {
        RaycastHit hitInfo = new RaycastHit();
        bool hit;
        GameObject obj;

        // Check if touch1 is on the selected object
        hit = Physics.Raycast(cam.ScreenPointToRay(touch1.position), out hitInfo);
        if (hit) {
            obj = hitInfo.transform.parent == null ? hitInfo.transform.gameObject : hitInfo.transform.parent.gameObject;
            // Selected object is touched
            if ((obj.GetInstanceID() == selectedId) && (obj.GetInstanceID() != elemManager.stage.obj.GetInstanceID())) {
                rotateOrigin = GetAngle(touch1, touch2);
                cam.GetComponent<CameraControl>().freeze = true;
                moving = true;
                return;
            }
        }

        // Check touch2
        hit = Physics.Raycast(cam.ScreenPointToRay(touch2.position), out hitInfo);
        if (hit) {
            obj = hitInfo.transform.parent == null ? hitInfo.transform.gameObject : hitInfo.transform.parent.gameObject;
            // Selected object is touched
            if ((obj.GetInstanceID() == selectedId) && (obj.GetInstanceID() != elemManager.stage.obj.GetInstanceID())) {
                rotateOrigin = GetAngle(touch1, touch2);
                cam.GetComponent<CameraControl>().freeze = true;
                moving = true;
                return;
            }
        }
    }

    private void Rotate(Touch touch1, Touch touch2) {
        // Find required angles
        float newAngle = GetAngle(touch1, touch2);
        float angleDiff = rotateOrigin - newAngle;
        float smallestDiff = angleDiff - (Mathf.PI * 2) * Mathf.Round(angleDiff / (Mathf.PI * 2));

        // Rotate element
        Element elem = elemManager.elementMap[selectedId];
        elem.obj.transform.Rotate(Vector3.up, smallestDiff * Mathf.Rad2Deg);
        rotateOrigin = newAngle;
    }

    // Use this for initialization
    private void Start() {
        selected = false;
        localTransform = false;
        selectedId = 0;
        disableMove = false;
        disableSelection = false;
        disableAll = false;
        moving = false;
        hMove = false;
        vMove = false;
        stationaryTimer = 0.0f;
    }
	
	// Update is called once per frame
	private void LateUpdate() {
        switch (Input.touchCount) {
            case 0:  // Reset if no touch is registered
                disableAll = false;
                cam.GetComponent<CameraControl>().freeze = false;
                moving = false;
                hMove = false;
                vMove = false;
                stationaryTimer = 0.0f;
                break;
            case 1:  // Moving or selection
                Touch touch = Input.GetTouch(0);

                // Do not check click position when interacting with UI
                if ((touch.phase == TouchPhase.Began) && editorUI.IsTouchOnUI(touch)) {
                    disableAll = true;
                }

                if (!disableAll) {
                    switch (touch.phase) {
                        case TouchPhase.Began:
                            if (!disableMove && selected) {
                                HMoveInit(touch);
                            }
                            stationaryTimer = 0.0f;
                            break;
                        case TouchPhase.Moved:
                            if (!disableMove && selected && hMove) {
                                HMove(touch);
                            } else if (!disableMove && selected && vMove) {
                                VMove(touch);
                            }
                            if (!vMove && !moving) {
                                if (touch.deltaPosition == Vector2.zero) {
                                    stationaryTimer += touch.deltaTime;
                                    if (stationaryTimer >= 1.0f) {
                                        VMoveInit(touch);
                                        stationaryTimer = 0.0f;
                                    }
                                } else {
                                    moving = true;
                                    stationaryTimer = 0.0f;
                                }
                            }
                            break;
                        case TouchPhase.Stationary:
                            if (!vMove && !moving) {
                                stationaryTimer += touch.deltaTime;
                                if (stationaryTimer >= 1.0f) {
                                    VMoveInit(touch);
                                    stationaryTimer = 0.0f;
                                }
                            }
                            break;
                        case TouchPhase.Ended:
                            if (touch.tapCount == 1) {
                                if (!disableSelection) {
                                    selectedId = ObjectClicked(touch);
                                }
                                disableAll = false;
                                cam.GetComponent<CameraControl>().freeze = false;
                                moving = false;
                                hMove = false;
                                vMove = false;
                                stationaryTimer = 0.0f;
                            }
                            break;
                    }
                }
                break;
            case 2:  // Rotation
                Touch touch1 = Input.GetTouch(0), touch2 = Input.GetTouch(1);

                // Do not check click position when interacting with UI
                if ((touch1.phase == TouchPhase.Began) && editorUI.IsTouchOnUI(touch1)
                   && (touch2.phase == TouchPhase.Began) && editorUI.IsTouchOnUI(touch2)) {
                    disableAll = true;
                }

                if (!disableAll && !disableMove) {
                    if (!moving) {
                        RotateInit(touch1, touch2);
                        if (moving) {
                            Rotate(touch1, touch2);
                        }
                    } else {
                        Rotate(touch1, touch2);
                    }
                }
                stationaryTimer = 0.0f;
                break;
        }
    }
}
