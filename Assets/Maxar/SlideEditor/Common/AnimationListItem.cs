﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Maxar;

public class AnimationListItem : MonoBehaviour {
    public int id;
    private AnimationInspector animInspector;
    public ElementAnimation anim;

    public void Pressed() {
        animInspector.elementAnimation = anim;
        EditorUI.animSeq = id;
        Debug.Log("Selected animation with ID: " + id.ToString());
    }

    public void Delete() {
        animInspector.Delete(id);
        Debug.Log("Delete animation with ID: " + id.ToString());
        transform.parent.parent.GetComponent<DynamicList>().Delete(id);
    }

    public void ChangeName(string newName) {
        GetComponent<iOS_TableViewItem>().TitleText = newName;
        transform.GetChild(2).gameObject.GetComponent<Text>().text = newName;
    }

    // Use this for initialization
    private void Start() {
		id = transform.GetSiblingIndex();
        animInspector = transform.parent.parent.parent.parent.GetComponent<AnimationInspector>();
    }
	
	// Update is called once per frame
	private void Update() {
        id = transform.GetSiblingIndex();
    }
}
