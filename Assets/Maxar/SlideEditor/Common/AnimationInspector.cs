﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Maxar;

public class AnimationInspector : MonoBehaviour {
    public AnimationManager animManager;
    public ElementManager elemManager;
    public ElementControl elemControl;
    public iOS_UIPanel listPanel;
    public iOS_UIPanel keyframePanel;
    public DynamicList animList;
    public EventSystem eventSystem;
    private bool playAll;

    // Keyframe management
    public ElementAnimation elementAnimation;
    private KeyboardInput keyboard;
    public iOS_TableViewItem scaleField, opacityField, durationField;
    public ToolBarButton backButtonAnim, backButtonKeyframe, addButton, playButton;
    public bool initializedAnim, initializedKeyframe;

    public void BackButtonAnimPressed() {
        EditorUI.animationEditor = false;
    }

    public void BackButtonKeyframePressed() {
        // Revert all elements to the final state
        Presentation.slides[EditorUI.page].GetFinalState();

        elementAnimation = null;
        elemControl.disableMove = false;
        elemControl.disableSelection = false;
        scaleField.RightText = "";
        opacityField.RightText = "";
        durationField.RightText = "";
        keyframePanel.gameObject.SetActive(false);
        initializedKeyframe = false;
    }

    public void ChangeScale() {
        keyboard = ScriptableObject.CreateInstance<KeyboardInput>();
        keyboard.Open("scale", elementAnimation.endState.scale.ToString(), TouchScreenKeyboardType.NumbersAndPunctuation, false);
    }

    public void ChangeOpacity() {
        keyboard = ScriptableObject.CreateInstance<KeyboardInput>();
        keyboard.Open("opacity", elementAnimation.endState.opacity.ToString(), TouchScreenKeyboardType.NumbersAndPunctuation, false);
    }

    public void ChangeDuration() {
        keyboard = ScriptableObject.CreateInstance<KeyboardInput>();
        keyboard.Open("duration", elementAnimation.duration.ToString(), TouchScreenKeyboardType.NumbersAndPunctuation, false);
    }

    private void ClearAnimList() {
        animList.container.transform.parent.GetComponent<ScrollRect>().normalizedPosition = new Vector2(0, 1);
        while (animList.container.transform.childCount > 0) {
            animList.Delete(animList.container.transform.childCount - 1);
        }
    }

    private void PopulateAnimList() {
        if (animList.container.transform.childCount > 0) {
            ClearAnimList();
        }

        foreach (ElementAnimation anim in animManager.animationList) {
            AnimationListItem animItem = animList.Append("Prefabs/ListItem/AnimationListItem").GetComponent<AnimationListItem>();
            animItem.anim = animManager.animationList[animItem.id];
            animItem.ChangeName(animItem.anim.elemId.ToString() + ": Move");
        }

        animList.container.transform.parent.GetComponent<ScrollRect>().normalizedPosition = new Vector2(0, 1);
    }

    public void Play() {
        elementAnimation.Update();
        Debug.Log("Playing anmimation with ID: " + EditorUI.animSeq.ToString());
        elementAnimation.Play();
    }

    public void PlayAll() {
        if (animManager.animationList.Count > 0) {
            // Revert all elements to its original state
            foreach (Element elem in elemManager.elementMap.Values) {
                elem.obj.transform.position = elem.objPosition;
                elem.obj.transform.rotation = elem.objRotation;
                elem.obj.transform.localScale = new Vector3(elem.objScale * elem.scaleFactor, elem.objScale * elem.scaleFactor, elem.objScale * elem.scaleFactor);
                elem.DisplayOpacity(elem.objOpacity);
            }

            EditorUI.animSeq = 0;
            elementAnimation = animManager.animationList[0];
            Play();
            playAll = true;
        }
    }

    public void Create() {
        if (elemControl.selected) {
            if (elemControl.SelectedType() != typeof(Stage)) {
                ElementAnimation newAnimation = ScriptableObject.CreateInstance<ElementAnimation>();
                Element elem = elemManager.elementMap[elemControl.selectedId];
                newAnimation.pageId = EditorUI.page;
                newAnimation.elemId = Presentation.slides[EditorUI.page].elementList.IndexOf(elem);
                newAnimation.duration = 1.0f;

                // Default states
                newAnimation.startState.position = elem.obj.transform.position;
                newAnimation.startState.rotation = elem.obj.transform.rotation;
                newAnimation.startState.scale = elem.objScale;
                newAnimation.startState.opacity = elem.objOpacity;
                newAnimation.endState.position = newAnimation.startState.position;
                newAnimation.endState.rotation = newAnimation.startState.rotation;
                newAnimation.endState.scale = newAnimation.startState.scale;
                newAnimation.endState.opacity = newAnimation.startState.opacity;

                if (animManager.animationList.Count > 0) {
                    // Set startState to the previous endState of the object
                    for (int i = 0; i < animManager.animationList.Count; ++i) {
                        if (animManager.animationList[i].elem.obj.GetInstanceID() == elem.obj.GetInstanceID()) {
                            newAnimation.startState.position = animManager.animationList[i].endState.position;
                            newAnimation.startState.rotation = animManager.animationList[i].endState.rotation;
                            newAnimation.startState.scale = animManager.animationList[i].endState.scale;
                            newAnimation.startState.opacity = animManager.animationList[i].endState.opacity;
                        }
                    }
                    newAnimation.endState.position = newAnimation.startState.position;
                    newAnimation.endState.rotation = newAnimation.startState.rotation;
                    newAnimation.endState.scale = newAnimation.startState.scale;
                    newAnimation.endState.opacity = newAnimation.startState.opacity;
                }

                animManager.CreateAnimation(newAnimation, elem);
                EditorUI.animSeq = animManager.animationList.Count - 1;
                elementAnimation = animManager.animationList[EditorUI.animSeq];
                elementAnimation.Update();

                AnimationListItem animItem = animList.Append("Prefabs/ListItem/AnimationListItem").GetComponent<AnimationListItem>();
                animItem.anim = elementAnimation;
                animItem.ChangeName(elementAnimation.elemId.ToString() + ": Move");
                initializedKeyframe = false;
            }
        }
    }

    public void Delete(int id) {
        animManager.DeleteAnimation(animManager.animationList[id]);
        elementAnimation = null;
        scaleField.RightText = "";
        opacityField.RightText = "";
        durationField.RightText = "";

        Presentation.slides[EditorUI.page].GetFinalState();
    }
	
	// Update is called once per frame
	private void Update() {
        if (EditorUI.animationEditor) {
            elemControl.disableMove = true;
            if (!initializedAnim) {
                Presentation.slides[EditorUI.page].GetFinalState();
                PopulateAnimList();
                EditorUI.animSeq = 0;
                initializedAnim = true;
                initializedKeyframe = false;
                playAll = false;
            }

            // Trap all action if playAll is set
            if (playAll) {
                if (elemControl.selected) {
                    elemControl.ChangeLayer(elemManager.elementMap[elemControl.selectedId].obj, "Default");
                }

                eventSystem.gameObject.SetActive(false);
                if (EditorUI.animSeq < animManager.animationList.Count) {
                    if (!elementAnimation.anim.isPlaying) {
                        EditorUI.animSeq += 1;
                        if (EditorUI.animSeq < animManager.animationList.Count) {
                            elementAnimation = animManager.animationList[EditorUI.animSeq];
                            Play();
                        }
                    }
                } else {
                    if (elemControl.selected) {
                        elemControl.ChangeLayer(elemManager.elementMap[elemControl.selectedId].obj, "Contour");
                    }

                    EditorUI.animSeq = 0;
                    elementAnimation = null;
                    eventSystem.gameObject.SetActive(true);
                    playAll = false;
                }
                return;
            }

            if (elementAnimation != null) {
                if (!initializedKeyframe) {
                    // Initialize variables and object
                    scaleField.RightText = elementAnimation.endState.scale.ToString();
                    opacityField.RightText = elementAnimation.endState.opacity.ToString();
                    durationField.RightText = elementAnimation.duration.ToString() + " s";
                    elementAnimation.elem.obj.transform.position = elementAnimation.endState.position;
                    elementAnimation.elem.obj.transform.rotation = elementAnimation.endState.rotation;
                    elementAnimation.elem.obj.transform.localScale = new Vector3(elementAnimation.endState.scale * elementAnimation.elem.scaleFactor, elementAnimation.endState.scale * elementAnimation.elem.scaleFactor, elementAnimation.endState.scale * elementAnimation.elem.scaleFactor);
                    elementAnimation.elem.DisplayOpacity(elementAnimation.endState.opacity);

                    // Deselect any object
                    if (elemControl.selected && (elemControl.selectedId != elementAnimation.elem.obj.GetInstanceID())) {
                        elemControl.ChangeLayer(elemManager.elementMap[elemControl.selectedId].obj, "Default");
                    }

                    // Select object
                    elemControl.selectedId = elementAnimation.elem.obj.GetInstanceID();
                    elemControl.selected = true;
                    elemControl.ChangeLayer(elementAnimation.elem.obj, "Contour");

                    // Set interaction parameters
                    listPanel.gameObject.SetActive(false);
                    keyframePanel.gameObject.SetActive(true);
                    elemControl.disableMove = false;
                    elemControl.disableSelection = true;
                    initializedKeyframe = true;
                }
                if (elementAnimation.anim.isPlaying) {
                    eventSystem.gameObject.SetActive(false);
                    elemControl.disableMove = true;
                    if (elemControl.selected) {
                        elemControl.ChangeLayer(elemManager.elementMap[elemControl.selectedId].obj, "Default");
                    }
                } else {
                    eventSystem.gameObject.SetActive(true);
                    elemControl.disableMove = false;
                    if (elemControl.selected) {
                        elemControl.ChangeLayer(elemManager.elementMap[elemControl.selectedId].obj, "Contour");
                    }
                }
            } else {
                scaleField.RightText = "";
                opacityField.RightText = "";
                durationField.RightText = "";
                elemControl.disableSelection = false;
                keyframePanel.gameObject.SetActive(false);
                listPanel.gameObject.SetActive(true);
            }

            if (elemControl.selected) {
                addButton.Enable();
                if (elementAnimation == null) {
                    elemControl.disableMove = true;
                } else {
                    elemControl.disableMove = elemControl.selectedId != elementAnimation.elem.obj.GetInstanceID();
                }
            } else {
                elemControl.disableMove = false;
                addButton.Disable();
            }
        }
	}

    private void OnDisable() {
        // Deselect any object
        if (elemControl.selected) {
            elemControl.ChangeLayer(elemManager.elementMap[elemControl.selectedId].obj, "Default");
            elemControl.selectedId = 0;
            elemControl.selected = false;
        }

        // Revert all elements to its original state
        foreach (Element elem in elemManager.elementMap.Values) {
            elem.obj.transform.position = elem.objPosition;
            elem.obj.transform.rotation = elem.objRotation;
            elem.obj.transform.localScale = new Vector3(elem.objScale * elem.scaleFactor, elem.objScale * elem.scaleFactor, elem.objScale * elem.scaleFactor);
            elem.DisplayOpacity(elem.objOpacity);
        }

        elementAnimation = null;
        elemControl.disableMove = false;
        elemControl.disableSelection = false;
        scaleField.RightText = "";
        opacityField.RightText = "";
        durationField.RightText = "";
        keyframePanel.gameObject.SetActive(false);
        ClearAnimList();
        initializedAnim = false;
        initializedKeyframe = false;
        playAll = false;
    }

    // Update text while user is typing
    private void OnGUI() {
        if (keyboard != null) {
            if (!TouchScreenKeyboard.visible) {
                switch (keyboard.inputName) {
                    case "scale":
                        float newScale;
                        if (float.TryParse(keyboard.keyboard.text, out newScale)) {
                            if (newScale > 0.0f) {
                                elementAnimation.elem.obj.transform.localScale = new Vector3(newScale * elementAnimation.elem.scaleFactor, newScale * elementAnimation.elem.scaleFactor, newScale * elementAnimation.elem.scaleFactor);
                            }
                        }
                        break;
                    case "opacity":
                        float newOpacity;
                        if (float.TryParse(keyboard.keyboard.text, out newOpacity)) {
                            if ((newOpacity >= 0.0f) && (newOpacity <= 1.0f)) {
                                elementAnimation.elem.DisplayOpacity(newOpacity);
                            }
                        }
                        break;
                    case "duration":
                        float newDuration;
                        if (float.TryParse(keyboard.keyboard.text, out newDuration)) {
                            if (newDuration > 0.0f) {
                                elementAnimation.duration = newDuration;
                            }
                        }
                        break;
                }
                animManager.ModifyAnimation(elementAnimation, elementAnimation.elem);
                scaleField.RightText = elementAnimation.endState.scale.ToString();
                opacityField.RightText = elementAnimation.endState.opacity.ToString();
                durationField.RightText = elementAnimation.duration.ToString() + " s";
                Destroy(keyboard);
            } else {
                switch (keyboard.inputName) {
                    case "scale":
                        scaleField.RightText = keyboard.keyboard.text;
                        break;
                    case "opacity":
                        opacityField.RightText = keyboard.keyboard.text;
                        break;
                    case "duration":
                        durationField.RightText = keyboard.keyboard.text;
                        break;
                }
            }
        }
    }
}
