﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawContour : MonoBehaviour {
    private Camera cam, temp;
    public Shader contour, white;
    private Material mat;

    private void OnRenderImage(RenderTexture source, RenderTexture destination) {
        // Set up the temporary camera
        temp.CopyFrom(cam);
        temp.rect = new Rect(0, 0, 1, 1);
        temp.clearFlags = CameraClearFlags.Color;
        temp.backgroundColor = Color.black;

        // Cull any layer which is not the contour
        temp.cullingMask = 1 << LayerMask.NameToLayer("Contour");

        // Temporary RenderTexture
        RenderTexture texture = new RenderTexture(source.width, source.height, 0, RenderTextureFormat.R8);
        texture.Create();

        // Set camera's target texture
        temp.targetTexture = texture;

        // Render all objects with the custom shader
        temp.RenderWithShader(white, "");

        // Copy the temporary RenderTexture to the final image
        Graphics.Blit(source, destination);
        Graphics.Blit(texture, destination, mat);

        // Release the temporary RenderTexture
        texture.Release();
    }

    // Use this for initialization
    private void Start() {
        cam = GetComponent<Camera>();
        mat = new Material(contour);

        // Create temporary camera for rendering contours
        if (temp == null) {
            temp = new GameObject("Temporary Camera").AddComponent<Camera>();
            temp.enabled = false;
        }
    }
}
