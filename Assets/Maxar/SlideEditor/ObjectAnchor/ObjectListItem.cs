﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using Maxar;

public class ObjectListItem : MonoBehaviour {
    public string objPath;
    public string objName;
    private EditorSlideLoader loader;

    public void Pressed() {
        Slide slide = Presentation.slides[EditorUI.page];
        slide.objectTargetPath = objPath;
        slide.objectTargetName = objName;

        loader.Load();
        EditorObjectUI.objectSelection = false;
        Debug.Log("Object anchor created");
    }

    public void ChangeName(string newName) {
        GetComponent<iOS_TableViewItem>().TitleText = newName;
        transform.GetChild(2).gameObject.GetComponent<Text>().text = newName;
    }

    // Use this for initialization
    private void Start() {
        loader = FindObjectOfType<EditorSlideLoader>();
    }
}
