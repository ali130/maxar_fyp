﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.XR.iOS;

public class PointCloudVisualizer : MonoBehaviour {
    public CameraControl cam;
    public ARReferenceObject obj;

    private Mesh CreateMesh(Vector3[] points) {
        Mesh mesh = new Mesh();
        int[] indices = new int[points.Length];
        for (int i = 0; i < points.Length; ++i) {
            indices[i] = i;
        }
        mesh.vertices = points;
        mesh.SetIndices(indices, MeshTopology.Points, 0);
        return mesh;
    }

    public void Load(string path) {
        obj = null;

        if (File.Exists(path)) {
            if (Path.GetExtension(path) == ".arobject") {
                obj = ARReferenceObject.Load(path);
            }
        }

        if (obj == null) {
            Clear();
        } else {
            GetComponent<MeshFilter>().mesh = CreateMesh(obj.pointCloud.Points);
            CameraControl.shiftVal = obj.center.y;
            cam.gameObject.transform.LookAt(cam.GetLookVal());
        }
    }

    public void Clear() {
        Destroy(GetComponent<MeshFilter>().mesh);
    }
}
