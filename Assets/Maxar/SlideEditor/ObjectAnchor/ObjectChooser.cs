﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.iOS;
using Maxar;

public class ObjectChooser : MonoBehaviour {
    public EditorObjectUI editorUI;
    public ObjectInspector objectInspector;
    public ElementControl elemControl;
    public PointCloudVisualizer pointCloud;
    public DynamicList objectList;
    public bool initialized;
    public HashSet<string> chosenObjectSet;

    public void ClearObjectList() {
        objectList.container.transform.parent.GetComponent<ScrollRect>().normalizedPosition = new Vector2(0, 1);
        while (objectList.container.transform.childCount > 0) {
            objectList.Delete(objectList.container.transform.childCount - 1);
        }
    }

    public void GetSetOfChosenObjects() {
        chosenObjectSet.Clear();

        foreach (Slide slide in Presentation.slides) {
            chosenObjectSet.Add(slide.objectTargetName);
        }
    }

    private void PopulateObjectList() {
        if (objectList.container.transform.childCount > 0) {
            ClearObjectList();
        }

        GetSetOfChosenObjects();

        string[] files = Directory.GetFiles(Application.persistentDataPath, "*.arobject");
        if (files.Length > 0) {
            foreach (string file in files) {
                string objPath = file.Substring(Application.persistentDataPath.Length + 1);
                string objName = Path.GetFileNameWithoutExtension(objPath);
                if (!chosenObjectSet.Contains(objName)) {
                    ObjectListItem objectItem = objectList.Append("Prefabs/ListItem/ObjectListItem").GetComponent<ObjectListItem>();
                    objectItem.objPath = objPath;
                    objectItem.objName = objName;
                    objectItem.ChangeName(objectItem.objName);
                }
            }
        }
    }

    // Use this for initialization
    private void Start() {
        chosenObjectSet = new HashSet<string>();
    }

    // Update is called once per frame
    private void Update() {
		if (EditorObjectUI.objectSelection) {
            elemControl.disableMove = true;
            elemControl.disableSelection = true;
            if (!initialized) {
                PopulateObjectList();
                initialized = true;
            }
        }
    }

    private void OnDisable() {
        elemControl.disableMove = false;
        elemControl.disableSelection = false;
        initialized = false;
        objectInspector.initialized = false;
        if (objectList.GetComponentInParent<CanvasHandler>() != null) {
            ClearObjectList();
        }
    }
}
