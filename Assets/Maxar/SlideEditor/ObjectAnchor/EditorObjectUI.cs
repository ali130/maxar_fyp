﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using Maxar;

public class EditorObjectUI : EditorUI {
    public ObjectInspector objectInspector;
    public ObjectChooser objectChooser;
    public PointCloudVisualizer pointCloud;
    public static bool objectSelection;

    public override void PlayerButtonPressed() {
        PlayerObjectUI.page = 0;
        SceneManager.LoadScene("PlayerObject");
        reload = true;
    }

    // Use this for initialization
    private void Awake() {
        Application.targetFrameRate = 60;
        cam = Camera.main;
        selectedId = 0;

        // Update panel height
        RectTransform leftRectTransform = transform.GetChild(0).GetChild(4).gameObject.GetComponent<RectTransform>();
        leftRectTransform.offsetMax = new Vector2(leftRectTransform.offsetMin.x, -navBar.GetComponent<RectTransform>().rect.height);
        RectTransform rightRectTransform = transform.GetChild(0).GetChild(5).gameObject.GetComponent<RectTransform>();
        rightRectTransform.offsetMax = new Vector2(rightRectTransform.offsetMin.x, -navBar.GetComponent<RectTransform>().rect.height);

        // Initialize camera position
        if (!reload) {
            cam.transform.position = new Vector3(
                                                 0,
                                                 cam.transform.position.y,
                                                 cam.transform.position.z - Presentation.stageDepth
                                                );
            CameraControl.position = cam.transform.position;
            CameraControl.rotation = new Quaternion();

        } else {
            // Move camera into previous position
            cam.transform.position = CameraControl.position;
            cam.transform.rotation = CameraControl.rotation;
            cam.fieldOfView = CameraControl.fieldOfView;
            reload = false;
        }

        loader.Load();
        if (Presentation.slides[page].objectTargetPath == "" || Presentation.slides[page].objectTargetName == "") {
            // Choose target from a list
            objectSelection = true;
        } else {
            objectSelection = false;
        }
        elemManager.stage = ScriptableObject.CreateInstance<Stage>();  // Dummy object
        elemManager.stage.obj = new GameObject("stage");
        objectInspector.initialized = false;
    }
	
	// Update is called once per frame
    private void Update() {
        // Update panel height
        RectTransform leftRectTransform = transform.GetChild(0).GetChild(4).gameObject.GetComponent<RectTransform>();
        leftRectTransform.offsetMax = new Vector2(leftRectTransform.offsetMin.x, -navBar.GetComponent<RectTransform>().rect.height);
        RectTransform rightRectTransform = transform.GetChild(0).GetChild(5).gameObject.GetComponent<RectTransform>();
        rightRectTransform.offsetMax = new Vector2(rightRectTransform.offsetMin.x, -navBar.GetComponent<RectTransform>().rect.height);

        if (objectSelection) {
            objectInspector.gameObject.SetActive(false);
            stageInspector.gameObject.SetActive(false);
            textInspector.gameObject.SetActive(false);
            modelChooser.gameObject.SetActive(false);
            modelInspector.gameObject.SetActive(false);
            animationInspector.gameObject.SetActive(false);
            rightToolBar.gameObject.SetActive(false);
            if (!objectChooser.gameObject.activeSelf) {
                // Deselect any object
                if (elemControl.selected) {
                    elemControl.ChangeLayer(elemManager.elementMap[selectedId].obj, "Default");
                    selectedId = 0;
                    elemControl.selectedId = 0;
                    elemControl.selected = false;
                }

                objectChooser.gameObject.SetActive(true);
                objectChooser.initialized = false;
            }
        } else if (animationEditor) {
            objectChooser.gameObject.SetActive(false);
            objectInspector.gameObject.SetActive(false);
            stageInspector.gameObject.SetActive(false);
            textInspector.gameObject.SetActive(false);
            modelChooser.gameObject.SetActive(false);
            modelInspector.gameObject.SetActive(false);
            rightToolBar.gameObject.SetActive(false);
            if (!animationInspector.gameObject.activeSelf) {
                // Deselect any object
                if (elemControl.selected) {
                    elemControl.ChangeLayer(elemManager.elementMap[selectedId].obj, "Default");
                    selectedId = 0;
                    elemControl.selectedId = 0;
                    elemControl.selected = false;
                }

                animationInspector.gameObject.SetActive(true);
                animationInspector.initializedAnim = false;
                animationInspector.initializedKeyframe = false;
            }
        } else if (modelSelection) {
            objectChooser.gameObject.SetActive(false);
            objectInspector.gameObject.SetActive(false);
            textInspector.gameObject.SetActive(false);
            modelInspector.gameObject.SetActive(false);
            animationInspector.gameObject.SetActive(false);
            rightToolBar.gameObject.SetActive(false);
            stageInspector.gameObject.SetActive(false);
            if (!modelChooser.gameObject.activeSelf) {
                // Deselect any object
                if (elemControl.selected) {
                    elemControl.ChangeLayer(elemManager.elementMap[selectedId].obj, "Default");
                    selectedId = 0;
                    elemControl.selectedId = 0;
                    elemControl.selected = false;
                }

                modelChooser.gameObject.SetActive(true);
                modelChooser.initialized = false;
            }
        } else {
            if (elemControl.selected) {
                if (elemControl.SelectedType() == typeof(Stage)) {
                    objectChooser.gameObject.SetActive(false);
                    objectInspector.gameObject.SetActive(false);
                    textInspector.gameObject.SetActive(false);
                    modelChooser.gameObject.SetActive(false);
                    modelInspector.gameObject.SetActive(false);
                    animationInspector.gameObject.SetActive(false);
                    rightToolBar.gameObject.SetActive(false);
                    stageInspector.gameObject.SetActive(true);
                    if (selectedId != elemControl.selectedId) {
                        stageInspector.initialized = false;
                        selectedId = elemControl.selectedId;
                    }
                } else if (elemControl.SelectedType() == typeof(Text3D)) {
                    objectChooser.gameObject.SetActive(false);
                    objectInspector.gameObject.SetActive(false);
                    modelChooser.gameObject.SetActive(false);
                    modelInspector.gameObject.SetActive(false);
                    stageInspector.gameObject.SetActive(false);
                    animationInspector.gameObject.SetActive(false);
                    rightToolBar.gameObject.SetActive(false);
                    textInspector.gameObject.SetActive(true);
                    if (selectedId != elemControl.selectedId) {
                        textInspector.initialized = false;
                        selectedId = elemControl.selectedId;
                    }
                } else if (elemControl.SelectedType() == typeof(Model3D)) {
                    objectChooser.gameObject.SetActive(false);
                    objectInspector.gameObject.SetActive(false);
                    modelChooser.gameObject.SetActive(false);
                    textInspector.gameObject.SetActive(false);
                    stageInspector.gameObject.SetActive(false);
                    animationInspector.gameObject.SetActive(false);
                    rightToolBar.gameObject.SetActive(false);
                    modelInspector.gameObject.SetActive(true);
                    if (selectedId != elemControl.selectedId) {
                        modelInspector.initialized = false;
                        selectedId = elemControl.selectedId;
                    }
                }
            } else {
                objectChooser.gameObject.SetActive(false);
                textInspector.gameObject.SetActive(false);
                modelChooser.gameObject.SetActive(false);
                modelInspector.gameObject.SetActive(false);
                animationInspector.gameObject.SetActive(false);
                stageInspector.gameObject.SetActive(false);
                rightToolBar.gameObject.SetActive(true);
                objectInspector.gameObject.SetActive(true);
            }
        }
    }
}
