﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using Maxar;

public class ObjectInspector : MonoBehaviour {
    public EditorSlideLoader loader;
    public ObjectChooser objectChooser;
    public PointCloudVisualizer pointCloud;
    public DynamicList objectList;
    public ToolBarButton addButton;
    public bool initialized;

    public void Create() {
        string[] files = Directory.GetFiles(Application.persistentDataPath, "*.arobject");
        if (files.Length > 0) {
            objectChooser.GetSetOfChosenObjects();
            foreach (string file in files) {
                Debug.Log(file);
                string objName = Path.GetFileNameWithoutExtension(file);
                if (!objectChooser.chosenObjectSet.Contains(objName)) {
                    Presentation.slides.Add(ScriptableObject.CreateInstance<Slide>());
                    EditorUI.page = Presentation.slides.Count - 1;
                    loader.Load();
                    pointCloud.Clear();
                    EditorObjectUI.objectSelection = true;
                    Debug.Log(file);
                    break;
                }
            }
        }
    }

    public void Delete() {
        if (Presentation.slides.Count > 1) {
            Presentation.slides.RemoveAt(EditorUI.page);
            objectList.Delete(EditorUI.page);

            for (int i = 0; i < objectList.container.transform.childCount; ++i) {
                PageListItem pageItem = objectList.container.transform.GetChild(i).GetComponent<PageListItem>();
                pageItem.slide = Presentation.slides[i];
                pageItem.ChangeName(pageItem.slide.objectTargetName);
            }

            if (EditorUI.page >= Presentation.slides.Count) {
                EditorUI.page = Presentation.slides.Count - 1;
            }
            loader.Load();
        } else {
            Presentation.slides[EditorUI.page].objectTargetName = "";
            Presentation.slides[EditorUI.page].objectTargetPath = "";
            loader.Load();
            EditorObjectUI.objectSelection = true;
        }
    }

    // Update is called once per frame
    private void Update() {
        if (!initialized) {
            if (objectList.container.transform.childCount > 0) {
                objectList.container.transform.parent.GetComponent<ScrollRect>().normalizedPosition = new Vector2(0, 1);
                while (objectList.container.transform.childCount > 0) {
                    objectList.Delete(objectList.container.transform.childCount - 1);
                }
            }

            foreach (Slide slide in Presentation.slides) {
                PageListItem pageItem = objectList.Append("Prefabs/ListItem/PageListItem").GetComponent<PageListItem>();
                pageItem.id = pageItem.gameObject.transform.GetSiblingIndex();
                pageItem.slide = Presentation.slides[pageItem.id];
                pageItem.ChangeName(pageItem.slide.objectTargetName);
            }

            objectList.container.transform.parent.GetComponent<ScrollRect>().normalizedPosition = new Vector2(0, 1);
            initialized = true;
        }
    }
}
